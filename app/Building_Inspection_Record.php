<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Building_Inspection_Record extends Model {
    //
    protected $table = 'building_inspection_record';

    /**
     * The "booting" method of the model.
     * Only search the result when its status is 1.
     * @return void
     */
    protected static function boot() {
        parent::boot();
        static::addGlobalScope('status', function(Builder $builder) {
            $builder->where('status', '=', 1);
        });
    }


    // Database: 1-many connection, 1 item only belongs to 1 area
    public function item() {
        return $this->belongsTo('App\Building_Inspection_Item','bii_id');
    }

    /**
       * Save inspection records by case
       * @param array $inspectionResult
       * @return boolean
    */
    public function UpdateInspectionsByCase($result) {
        return self::insert($result);
    }
}
