<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
//use App\Services\Mgr\Cases\CaseService;
use App\Services\Mgr\Reservations\ReservationsService;

class RoutineCase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'RoutineCase';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create routine case automatically';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        //
        // $result = (new CaseService())->dailyCheckRoutineCase();
        $result = (new ReservationsService())->dailyCheckRoutine();
        print_r($result);
        return $result;
    }
}
