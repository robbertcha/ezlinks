<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    //
    public $table = "unit";

    public function tenant() {
        return $this->hasMany('App\Tenant_Detail');
    }

    public function user() {
        return $this->hasMany('App\User');
    }

    public function someFunction(){
             return Unit::$table;
    }

    
    public function get_all_units($building_id) {
        return $this->select("id", "unit_title", "unit_status")
                        ->where('status', 1)
                        ->where('building_id', '=', $building_id)
                        ->get()->toArray();
    }

    public static function getDetailByID($id) {
        return self::select('id','unit_title', 'unit_status', 'created_at')
                    ->where('id', '=', $id)
                    ->where('status', '=', 1)
                    ->orderBy('created_at', 'asc')
                    ->get()->first();
    }

    public static function getDetailByTitle($title) {
        return self::select('id','unit_title', 'unit_status', 'created_at')
                    ->where('unit_title', '=', $title)
                    ->where('status', '=', 1)
                    ->orderBy('created_at', 'asc')
                    ->get()->first();
    }
}
