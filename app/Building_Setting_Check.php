<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;


class Building_Setting_Check extends Model {
    //
    public $table = "building_setting_check";
    protected $fillable = ['slug','title', 'description', 'url', 'done'];


    public static function getSettingCheckByBid($bid) {
        return self::select("building_setting_check.id","building_setting_check.title","building_setting_check.slug","building_setting_check.description","building_setting_check.url","building_setting.setting_table",'building_setting.setting_field')
                    ->leftJoin('building_setting', 'building_setting.slug', '=', 'building_setting_check.slug')
                    ->where('building_setting_check.building_id', '=', $bid)
                    ->where('building_setting_check.status', '=', 1)
                    ->where('building_setting_check.done', '=', 0)
                    ->orderBy('building_setting.id', 'asc')
                    ->get()->ToArray();
    }

    public static function getSettingCountByBid($bid) {
        return self::where('building_id', '=', $bid)
                        ->where('status', '=', 1)
                        ->get()->count();
         
    }

    

    public static function unfinalizeSetting($bid, $slug) {
        
        $setting = self::where('status', '=', 1)
                        -> where('building_id', '=', $bid)
                        -> where('slug', '=', $slug)
                        -> where('done', '=', 1);
        if ($setting -> exists()) {
            $setting = $setting -> update(['done' => '0']);
            return TRUE;
        } else {
            return;
        }
    }

    public static function skipAllSettings($bid) {
        return self::where('building_id', $bid)
                    ->update(['done' => 1]);
            
    }

    public static function copySettingCheck($bid, $data) {
        $slug = new building_setting_check;
        $slug = $slug::getBuildingSlug($bid);

        foreach ($data as $key => $value) {
            $setting_check = new building_setting_check;

            if(!in_array($value['slug'], $slug, false)){
                $setting_check['building_id'] = $bid;
                $setting_check['slug'] = $value['slug'];
                $setting_check['title'] = $value['title'];
                $setting_check['description'] = $value['description'];
                $setting_check['url'] = $value['url'];
                $setting_check['done'] = 0;
                $setting_check['status'] = 1;
                $setting_check->save();
            }
            
        }
        return $data;
    }

    public static function slugCheck($bid, $slug) {
        $building_setting = self::where('building_id', '=', $bid)
                                ->where('slug', '=', $slug)
                                ->where('status', '=', 1)
                                ->where('done', '=', 0);
        $done_setting = self::where('building_id', '=', $bid)
                                ->where('slug', '=', $slug)
                                ->where('status', '=', 1)
                                ->where('done', '=', 1);
        if ($building_setting -> exists()) {
            $building_setting = $building_setting -> first() -> update(['done' => '1']);
            return TRUE;
        } elseif($done_setting -> exists()) {
            return TRUE;
        } else {
            return FALSE;
        }  
    }

    public static function getBuildingSlug($bid){
        return  self::select('slug')
                        ->where('building_id', '=', $bid)
                        ->where("status","=","1")
                        ->get()->pluck('slug')->toArray();
    }

    
}
