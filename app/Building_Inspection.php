<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Building_Inspection extends Model
{
    //
    protected $table = 'building_inspection';

    public static function get_building_inspections_by_building($building_id) {
        return self::select('building_inspection.id','building_inspection.title','building_inspection.created_at', 
                        DB::raw("(
                            SELECT ifnull(
                                GROUP_CONCAT( 
                                    CONCAT(bii.id,'-',bii.title,'-', bii.frequency)
                                SEPARATOR '|'),
                            '') as items
                            from building_inspection_area as bia
                            left join building_inspection_item as bii on bii.bi_area_id = bia.id and bii.status = 1
                            where bia.status = 1
                            and bii.status = 1
                            and bia.bi_id = building_inspection.id
                        ) as items") 
                    )
                    ->where('building_inspection.status', '=', 1)
                    ->where('building_inspection.building_id', '=', $building_id)
                    ->when(isset($_REQUEST['input_search']), function ($query) {
                        $query->where('building_inspection.title', 'like', '%' . $_REQUEST['input_search'] . '%');
                    })
                    ->get()->ToArray();
    }

    public static function get_building_inspections_by_date($building_id, $date_from, $date_to) {
        return self::select('building_inspection.id','building_inspection.title','building_inspection.created_at',
                        DB::raw("(
                            SELECT ifnull(
                                GROUP_CONCAT( 
                                    CONCAT(bii.id,'-',bii.title,'-', bii.frequency)
                                SEPARATOR '|'),
                            '') as items
                            from building_inspection_area as bia
                            left join building_inspection_item as bii on bii.bi_area_id = bia.id and bii.status = 1
                            where bia.status = 1
                            and bii.status = 1
                            and bia.bi_id = building_inspection.id
                        ) as items") 
                    )
                    ->where('building_inspection.status', '=', 1)
                    ->where(function($query) use ($date_from, $date_to) {
                        $query->whereBetween('created_at',[$date_from, $date_to]);
                    })
                    ->where('building_inspection.building_id', '=', $building_id)
                    ->when(isset($_REQUEST['input_search']), function ($query) {
                        $query->where('building_inspection.title', 'like', '%' . $_REQUEST['input_search'] . '%');
                    })
                    ->get()->ToArray();
    }
}
