<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Unit;
use App\Building_Inspection_Area;
use App\Building_Amenity;
use App\Building_Inspection_Item;
use App\Building_Asset;

class Building_Log extends Model
{
    //
    protected $table = 'building_log';

      public function generate_reservation_log($record_id, $user, $building_id, $action, $typeId, $location, $bi_id, $amenityId, $car_plate, $progress, $starttime, $endtime, $recurrenceRule) {
            $type = "Task";
            $bl = new Building_Log;
            
            if ($typeId == 1 || $typeId == 2) {
                  $recurrence = null;
                  if (strpos(strtolower($recurrenceRule), 'daily') == true) {
                        $recurrence = 'daily';
                  } else if (strpos(strtolower($recurrenceRule), 'weekly') == true) {
                        $recurrence = 'weekly';
                  } else if (strpos(strtolower($recurrenceRule), 'monthly') == true) {
                        $recurrence = 'monthly';
                  } else if (strpos(strtolower($recurrenceRule), 'yearly') == true) {
                        $recurrence = 'yearly';
                  } 
            }

            if ($typeId == 1) {
                  $type = "Task";
                  $bl->object_id = null;
                  $bl->object = null;

                  if ($location != null || $location != '') {
                        $unit = new Unit;
                        $location = $unit->getDetailByTitle($location);
                        
                        if ($location != null || $location != '') {
                              $location = " at ".$location->unit_title;
                        } else {
                              $location = " at ".$location;
                        }
                  }

                  $msg = $user." ". $action . " a ".$recurrence." ".$type." - ".$location." at ".$starttime;
            } else if ($typeId == 2) {
                  $type = "Inspection";

                  if ($bi_id > 0) {
                        $bia = new Building_Inspection_Area;
                        $inspection_area = $bia->get_inspection_area($bi_id);
                  }

                  $bl->object_id = $bi_id;
                  $bl->object = 'building_inspection_area';

                  $msg = $user." ". $action . " a ".$recurrence." ".$inspection_area->area_name." ".$type." Booking at ".$starttime;
            } else if ($typeId == 3) {
                  $type = "Amenity";

                  if ($amenityId > 0) {
                        $ba = new Building_Amenity;
                        $amenity = $ba->getAmenityDetailsByAID($amenityId);
                  }

                  $bl->object_id = $amenityId;
                  $bl->object = 'building_amenity';

                  $msg = $user." ". $action . " a ".$type." Booking - ".$amenity->name." at ".$starttime;
            } else if ($typeId == 4) {
                  $type = "Carpark";
                  $bl->object_id = null;
                  $bl->object = null;

                  $msg = $user." ". $action . " a ".$type." Booking - ".$car_plate." at ".$starttime;
            }

            $bl->building_id = $building_id;
            $bl->record_id = $record_id;
            $bl->type = "Reservation";
            $bl->action = $type;
            $bl->msg = $msg;
            $bl->progress = null;
            $bl->status = 1;
            $bl->save();

            if ($typeId == 2) {
                  $bii = new Building_Inspection_Item;
                  $bii_arr = $bii->getDetailsByAreaID($bi_id);

                  for ($i = 0; $i < count($bii_arr); $i++) {
                        $bl_items = new Building_Log;

                        $bl_items->building_id = $building_id;
                        $bl_items->record_id = $record_id;
                        $bl_items->object_id = $bii_arr[$i]['asset_id'];
                        $bl_items->object = 'building_assets';
                        $bl_items->type = "Reservation";
                        $bl_items->action = $type;
                        $bl_items->msg = $msg;
                        $bl_items->progress = null;
                        $bl_items->status = 1;
                        $bl_items->save();
                  }
            }
      }

      public function generate_bii_log($record_id, $user, $building_id, $bii_id, $result, $hid) {
            date_default_timezone_set('Australia/Brisbane');
            $asset = [];
            $type = "Inspection";
            $bl = new Building_Log;
            $bii = new Building_Inspection_Item;
            $ba = new Building_Asset;

            $bii_record = $bii->getDetailsByID($bii_id);
            $a = $result == 0 ? 'FALSE' : 'TRUE';

            if ($bii_record->asset_id > 0) {
                  $asset = $ba->getDetailByID($bii_record->asset_id);

                  $msg = $user." inspected ".$asset->name." at ". date('m/d/Y h:i:s a', time()).", [Hid: ".$hid.", Result: ".$a."]";
                 
                  $bl->building_id = $building_id;
                  $bl->record_id = $record_id;
                  $bl->type = $type;
                  $bl->object = 'building_assets';
                  $bl->object_id = $bii_record->asset_id;
                  $bl->action = null;
                  $bl->msg = $msg;
                  $bl->progress = null;
                  $bl->status = 1;
                  $bl->save();

                  return $bl;
            }            
      }

      public function generate_del_log($record_id, $user, $building_id, $type) {
            $record = self::select('id','object','object_id')
                        ->where('record_id', '=', $record_id)
                        ->get()->first();

            $bl = new Building_Log;
            $bl->building_id = $building_id;
            $bl->record_id = $record_id;
            $bl->type = $type;
            $bl->object_id = $record->object_id;
            $bl->object = $record->object;
            $bl->action = 'Delete';
            $bl->msg = $user.' deletes a '.$type.' [Record ID: '.$record_id.']';
            $bl->progress = null;
            $bl->status = 1;
            $bl->save();
      }

      public function generate_case_log($record_id, $user, $building_id, $action, $object, $object_id, $progress) {
            $bl = new Building_Log;
            $bl->building_id = $building_id;
            $bl->record_id = $record_id;
            $bl->type = "Case";
            $bl->object_id = $object_id;
            $bl->object = $object;
            $bl->action = $action;
            $bl->msg = $user.' '.$action.' a case [Case ID: '.$record_id.', Progress: '.$progress.']';
            $bl->progress = $progress;
            $bl->status = 1;
            $bl->save();
      }
}
