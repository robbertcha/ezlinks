<?php

namespace App;

// use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use NotificationChannels\WebPush\HasPushSubscriptions; //import the trait
use Auth;

class Building_Admin extends Authenticatable
{
    use Notifiable;
    use HasPushSubscriptions; // add the trait to your class
    // The authentication guard for admin
    protected $guard = 'building_admin';
    public $table = "building_admin";
    protected $fillable = [
        'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function building() {
        return $this->belongsTo('App\Building');
    }

    public function getBuildingInfoAttribute(){
        return $this::with('building')->find(Auth::user()->id); 
    }

    public static function getBuildingAdminByBID($building_id){
        return self::select("id","name", "email")
                    ->where('building_id', '=', $building_id)
                    ->get()->toArray();
    }
}
