<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TenantVerification extends Model
{
    //
    public $table = "tenant_verifications";
    protected $guarded = [];

    public function tenant()
    {
        return $this->belongsTo('App\Tenant', 'tenant_id');
    }
}
