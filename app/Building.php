<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Building extends Model {
    //
    protected $table = 'building';

    public function __construct() {
        date_default_timezone_set('Australia/Brisbane');
    }

    public function hasAdmin(){
        return $this->hasOne('Building_Admin', 'building_id', 'id');
    }

    // Database: 1-many connection, 1 area can has many inspection items
    public function assets() {
        return $this->hasMany('App\Building_Asset','building_id', 'id');
    }

    /**
     * The "booting" method of the model.
     * Only search the result when its status is 1.
     * @return void
     */
    protected static function boot() {
        parent::boot();
        static::addGlobalScope('status', function(Builder $builder) {
            $builder->where('status', '=', 1);
        });
    }

    /**
     * Get inspection items by id
     * @param int $id building inspection area id
     * @return array inspection items
     */
    public static function getAssetsByBuildingID($id) {
        return self::find($id)->assets->toArray();
    }

    /**
       * Get building infor by building id
       * @param int $bid
       * @return object
    */
    public static function getBuildingInfoById($bid) {
        return self::select('name','code','logo','address','postcode','surburb','state','website','phone','fax')
                    ->where('id', '=', $bid)
                    ->where('status', '=', 1)
                    ->get()->first();
    }

    public static function getLogoByBuildingId($bid) {
        return self::select('logo')
                    ->where('id', '=', $bid)
                    ->where('status', '=', 1)
                    ->get()->ToArray();
    }

    public static function getPhoneByBuildingId($bid) {
        return self::select('phone')
                    ->where('id', '=', $bid)
                    ->where('status', '=', 1)
                    ->get()->ToArray();
    }

}
