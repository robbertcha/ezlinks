<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ParcelMail extends Mailable
{
    use Queueable, SerializesModels;
    public $action;
    public $email_title;
    public $recipient_lists;
    public $content_arr;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($action, $email_title, $recipient, $content_arr)
    {
        //
        $this->action = $action;
        $this->recipient = $recipient;
        $this->email_title = $email_title;
        $this->content_arr = $content_arr;
        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->action == 'new_parcel') {
            $domain = 'http://www.ezlinks.local/tenants/parcel';
            return $this->view('emails.parcelEmail')->from('robbertcha@gmail.com', 'admin')->with(['domain'=>$domain])->subject($this->email_title);        }
        // return $this->view('emails.caseContractorEmail')->from($this->email, $this->name)->subject($this->email_title)->with([ 'email_title' => $this->email_title, 'email_content'=>$this->email_content ]);
    }
}
