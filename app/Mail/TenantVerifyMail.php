<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TenantVerifyMail extends Mailable
{
    use Queueable, SerializesModels;
    public $tenant;
    public $unit_title;
    public $verification_token;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($tenant, $unit_title, $verification_token)
    {
        //
        $this->tenant = $tenant;
        $this->unit_title = $unit_title;
        $this->verification_token = $verification_token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.tenantVerification')->from('admin@ezlinks.com.au', 'EzLinks Team');
    }
}
