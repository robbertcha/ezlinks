<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CaseMail extends Mailable
{
    use Queueable, SerializesModels;
    public $action;
    public $email_title;
    public $recipient;
    public $content;
    public $content_arr;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($action, $email_title, $recipient, $content, $content_arr)
    {
        //
        $this->action = $action;
        $this->recipient = $recipient;
        $this->email_title = $email_title;
        $this->content = $content;
        $this->content_arr = $content_arr;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->action == 'create_case') {
            return $this->view('emails.caseContractorEmail')
                        ->from( $this->content_arr['sender_email'], $this->content_arr['sender_name'])
                        ->subject($this->email_title)
                        ->with([ 'email_title' => $this->email_title]);;
        }
        
    }
}
