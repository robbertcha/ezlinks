<?php

namespace App\Mail;

use Config;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotificationMail extends Mailable
{
    use Queueable, SerializesModels;
    public $action;
    public $email_title;
    public $recipient_lists;
    public $content_arr;
    public $content;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($action, $email_title, $recipient,$announcement_content, $content_arr) {
        //
        $this->action = $action;
        $this->recipient = $recipient;
        $this->email_title = $email_title;
        $this->content_arr = $content_arr;
        $this->content = $announcement_content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        if ($this->action == 'create_announcement') {
            $url = Config::get('app.url');
            $domain = $url.'/tenants/announcement';

            return $this->view('emails.announcementNotifications')
                        ->from($this->content_arr['email'], $this->content_arr['name'])
                        ->with(['domain'=>$domain,'content'=>$this->content])
                        ->subject($this->email_title)
                        ->attachData($this->file);
        }
    }
        // $this->withSwiftMessage(function ($message)  {
        //     // $message->object_arr = $this->object_arr ;
        //     $domain = 'http://www.ezlinks.local/tenants/announcement';
        // });
        

    // public function generate_url() {

    //     $domain = 'http://www.ezlinks.local/tenants/announcement';

    //     return $domain;

    // }
}
