<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Building_Amenity extends Model
{
    //
    public $table = "building_amenity";
    public function getAmenityDetailsByAID($aid) {
        return $this->select("name","booking_before","allocation_limit","hours_limit","rules")
                    ->where('id', '=', $aid)
                    ->where('status', '=', 1)
                    ->get()->first();
    }


    public function getAllAmenitiesByBID($building_id){
        return $this->select('name', 'id','booking_before','allocation_limit','hours_limit','rules','start_time', 'end_time', 'name as text', 'id as value','color')
                    ->where('status', '=', 1)
                    ->where('building_id', '=', $building_id)
                    ->groupBy('id')
                    ->orderBy('created_at', 'desc')
                    ->get()->toArray();
    }
}
