<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Building_FAQ extends Model
{
    //
    public $table = "building_faq";

    public static function getFAQListsBySlug($slug) {
        return self::select("id","title","description","icon","slug")
                    ->where('slug', '=', $slug)
                    ->where('status', '=', 1)
                    ->get()->ToArray();
    }

}
