<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Building_Inspection_Item extends Model
{
    //
    protected $table = 'building_inspection_item';
    protected $fillable = ['id','bi_area_id', 'asset_id', 'title', 'status'];

    /**
     * The "booting" method of the model.
     * Only search the result when its status is 1.
     * @return void
     */
    protected static function boot() {
        parent::boot();
        static::addGlobalScope('status', function(Builder $builder) {
            $builder->where('status', '=', 1);
        });
    }

    // Database: 1-many connection, 1 item only belongs to 1 area
    public function area() {
        return $this->belongsTo('App\Building_Inspection_Area','bi_area_id');
    }

    // Database: 1-many connection, 1 item can has many records
    public function records() {
        return $this->hasMany('App\Building_Inspection_Record','bii_id', 'id');
    }

    /**
     * Remove items by inspection area id
     * @param int $bi_id inspection area id
     */
    public function removeItemsByAreaID($bi_id) {
        return self::where('bi_area_id', '=', $bi_id)->update(['status' => 0 ]);
    }

    /**
     * Create or update inspection item
     * @param request inspection item
     * @return id
     */
    public function saveItem($request) {
        $request['id'] = 158;
        $request['title'] = 123;
        return self::updateOrCreate(['id'=> $request['id']], $request);
    }
    
    /**
     * Get Inspections and result by area ID & case ID
     * @param int $area_id
     * @param int $case_id
     * @return array
     */
    public static function getDetailsByAreaID($bi_id, $case_id) {
        return  DB::table('building_inspection_item as bii')
                    ->select('bii.id', 'bii.title', 'bir.result', 'bir.msg')
                    ->leftJoin('building_inspection_record as bir', function($join) use ($case_id)
                    {
                        $join->on('bii.id', '=', 'bir.bii_id')
                            // ->where('bir.case_id', '=', $case_id)
                            ->where('bir.status', '=', 1);
                    })
                    ->where('bii.bi_area_id', '=', $bi_id)
                    ->where('bii.status', '=', 1)
                    ->orderBy('bii.created_at', 'desc')
                    ->get()->ToArray();
    }


    public static function get_building_inspection_area($bi_id) {
        return self::select('id', 'bi_id', 'area_name', 'status')
                    ->where('$bi_id', '=', $bi_id)
                    -> orderBy('created_at', 'desc')
                    -> get()-> ToArray();
    }

    public static function getDetailsByID($bii_id) {
        return self::select('id', 'bi_area_id', 'asset_id', 'title', 'status')
                    ->where('id', '=', $bii_id)
                    -> orderBy('created_at', 'desc')
                    -> get()->first();
    }
}
