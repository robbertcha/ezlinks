<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Building_Template extends Model{
    //
    protected $table = 'building_template';
    protected $fillable = ['tag', 'title', 'content', 'status', 'building_id', 'id'];

    public static function get_template_by_tag($building_id) {
        return self::select('id', 'tag', 'title', 'content')
                    -> where('status', '=', 1)
                    -> where('building_id', '=', $building_id)
                    -> orderBy('created_at', 'desc')
                    -> get()-> ToArray();
    }

    public static function get_tag($building_id) {
        return self::select('tag')
                    -> where('status', '=', 1)
                    -> where('building_id', '=', $building_id)
                    -> orderBy('created_at', 'desc')
                    -> get()-> ToArray();
    }

    /**
       * Get template list by building id
       * @param int $building id
       * @return array
    */
    public function get_template_list($building_id) {
        $list = Building_Template::select('id','title', 'content as original_content' )
                            ->where('status', '=', 1)
                            ->where('building_id', '=', $building_id)
                            ->get()->toArray();
        
        foreach ($list as $key => $val) {
            $list[$key]['content'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1',html_entity_decode($val['original_content']));
            $list[$key]['substr_content'] = substr(strip_tags($list[$key]['content']), 0, 120);
        }
        return $list;
    }
    
}
