<?php

namespace App;

use App\Model\Mgr\Cases\BuildingCase;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

use DateTime;
use DateInterval;
use DatePeriod;
use DateTimeZone;
use \Carbon\Carbon;


class Amenity_Reservation extends Model
{
    //
    public $table = "amenity_reservation";
    protected $fillable = ['id','building_id', 'amenity_id', 'tenant_id', 'case_id', 'blg_admin_id','contractor_id','bi_id','type','status','booking_time_from','booking_time_to','next_date','booking_recurring','progress','parent_id','time_id','location','car_plate','booking_exception','booking_end','details','progress','comment','next_date','status'];

    /**
     * The "booting" method of the model.
     * Only search the result when its status is 1.
     * @return void
     */
    protected static function boot() {
        parent::boot();

        static::addGlobalScope('status', function(Builder $builder) {
            $builder->where('status', '=', 1);
        });
    }

    public function __construct()
    {
        date_default_timezone_set('Australia/Brisbane');
    }

    /**
     * Get reservation array where next date is before yesterday, only type = job.
     * @return array  
     */
    public function getRoutines() {
        $todayDate = date("Y-m-d", time());
        return $this::where('booking_recurring', '!=', null)
                    ->where('type', '=', 1)
                    ->where('status', '=', 1)
                    ->where('parent_id', '=', null)
                    ->where(function ($query) use ($todayDate) {
                        $query->where('next_date', '<=', $todayDate)
                                ->orWhere('next_date', '=', null);
                    })
                    ->get()->ToArray();
    }

    /**
     * Bind one to one relationship with cases.
     */
    public function case() {
        return $this->hasOne('App\Model\Mgr\Cases\BuildingCase', 'id', 'case_id');
    }

    /**
     * Set booking end time by id
     * @param int $id amenity reservation id
     * @param datetime $time current time
     */
    public function setBookingEnd($id, $time) {
        return self::where('id', '=', $id)->update(['booking_end' => $time ]);
    }

    /**
     * Create or update a amenity reservation record
     * @param request array of amenity reservation
     * @return not known
     */
    public function saveReservation($request) {
        return self::updateOrCreate(['id'=>$request['id']], $request);
    }

    /**
     * Count next date by recurring
     * @param array $bi amenity reservation record
     * @return array
     */
    public static function calculate_next_date($bi) {
        if (strpos(strtoupper($bi['booking_recurring']), 'WEEKLY') !== false) {
            $total_distance = floor((time() - strtotime($bi['booking_time_from']))/(86400*7));
                
            $last_start_date =  strtotime("+".$total_distance." weeks", strtotime($bi['booking_time_from']));
            $last_to_date =  strtotime("+".$total_distance." weeks", strtotime($bi['booking_time_to']));
            $b['next_booking_time_from'] =  date("Y-m-d H:i:s", strtotime("+1 week", $last_start_date));
            $b['next_booking_time_to'] =  date("Y-m-d H:i:s", strtotime("+1 week", $last_to_date));
            $b['next_date'] = $b['next_booking_time_from'];
            return $b;
        } else if (strpos(strtoupper($bi['booking_recurring']), 'DAILY') !== false) {
            $total_distance = floor((time() - strtotime($bi['booking_time_from']))/(86400));
                
            $last_start_date =  strtotime("+".$total_distance." days", strtotime($bi['booking_time_from']));
            $last_to_date =  strtotime("+".$total_distance." days", strtotime($bi['booking_time_to']));
            $b['next_booking_time_from'] =  date("Y-m-d H:i:s", strtotime("+1 day", $last_start_date));
            $b['next_booking_time_to'] =  date("Y-m-d H:i:s", strtotime("+1 day", $last_to_date));
            $b['next_date'] = $b['next_booking_time_from'];
            return $b;
        } else if (strpos(strtoupper($bi['booking_recurring']), 'YEARLY') !== false) {
            $total_distance = floor((time() - strtotime($bi['booking_time_from']))/(86400*365));
            
            $last_start_date =  strtotime("+".$total_distance." years", strtotime($bi['booking_time_from']));
            $last_to_date =  strtotime("+".$total_distance." years", strtotime($bi['booking_time_to']));
            $b['next_booking_time_from'] =  date("Y-m-d H:i:s", strtotime("+1 year", $last_start_date));
            $b['next_booking_time_to'] =  date("Y-m-d H:i:s", strtotime("+1 year", $last_to_date));
            $b['next_date'] = $b['next_booking_time_from'];
            return $b;
        } else if (strpos(strtoupper($bi['booking_recurring']), 'MONTHLY') !== false) {
            $start    = (new DateTime($bi['booking_time_from']))->modify('first day of this month');
            $end      = (new DateTime(date('Y-m-d')))->modify('first day of next month');
            $interval = DateInterval::createFromDateString('1 month');
            $mon_period = new DatePeriod($start, $interval, $end);

            $count = 0;
            foreach ($mon_period as $dt) {
                $count++;
            }
   
            $current_start_mon = date("Y-m-d H:i:s", strtotime("+".($count-1)." month", strtotime($bi['booking_time_from'])));
            $today_time = strtotime(date("Y-m-d H:i:s"));
            $expire_time = strtotime($current_start_mon);
            if ($expire_time < $today_time) { 
                $b['next_booking_time_from'] = date("Y-m-d H:i:s", strtotime("+".$count." month", strtotime($bi['booking_time_from'])));
                $b['next_booking_time_to'] = date("Y-m-d H:i:s", strtotime("+".$count." month", strtotime($bi['booking_time_to'])));
                $b['next_date'] = $b['next_booking_time_from'];
                return $b;
            } else {
                $b['next_booking_time_from'] = date("Y-m-d H:i:s", strtotime("+".($count-1)." month", strtotime($bi['booking_time_from'])));
                $b['next_booking_time_to'] = date("Y-m-d H:i:s", strtotime("+".($count-1)." month", strtotime($bi['booking_time_to'])));
                $b['next_date'] = $b['next_booking_time_from'];
                return $b;
            }

            return  null;
        } else {
            return  null;
        }
    }

    /**
     * Get single record by building id and inspection area id
     * @param int $bia_id
     * @param int $building_id
     * @return object amenity_reservation record
     */
    public function getSchedulerByInspectionID($bia_id, $building_id) {
        return self::where('bi_id', $bia_id)->where('building_id', $building_id)->first();
    }

    /**
     * Get jobs
     * @param int $building_id
     * @param int $page current page number
     * @param string $type - u: upcoming, o: on-going, h: history
     * @param string $search_term - search term
     * @return array
     */
    public function getJobsByBuildingID($building_id, $page, $type = 'u', $search_term) {
        return DB::table('amenity_reservation as ar')
                    ->select("ar.id", "ar.tenant_id", "ar.bi_id as bi_id", "ar.parent_id", "ar.type as typeId", "ar.booking_time_from as start", "ar.booking_time_to as end",    "ar.booking_recurring", "ar.booking_exception as recurrenceException", "ar.car_plate", "ar.progress", "ar.amenity_id", "ar.details", "ar.comment", "ar.booking_end", "ar.case_id", "ar.updated_at", "building_inspection_area.area_name", "cases.case_type",
                            DB::raw("(
                                SELECT ifnull(
                                    GROUP_CONCAT( 
                                        bii.title
                                    SEPARATOR ' -> '),
                                '') as items
                                from building_inspection_item as bii
                                where bii.bi_area_id = building_inspection_area.id 
                                and building_inspection_area.status = 1
                                and bii.status = 1
                            ) as bi_items"),  
                            DB::raw("(CASE 
                                WHEN  par.booking_recurring = '' THEN 'NONE'
                                WHEN  par.booking_recurring = null THEN 'NONE'
                                WHEN  par.booking_recurring LIKE '%DAILY%' THEN 'DAILY'
                                WHEN  par.booking_recurring LIKE '%WEEKLY%' THEN 'WEEKLY'
                                WHEN  par.booking_recurring LIKE '%MONTHLY%' THEN 'MONTHLY'
                                WHEN  par.booking_recurring LIKE '%YEARLY%' THEN 'YEARLY'
                            END) AS frequency"),
                            DB::raw("(CASE 
                                WHEN  cases.progress = '1' THEN 'New'
                                WHEN  cases.progress = '2'THEN 'In Progress'
                                WHEN  cases.progress = '3' THEN 'Completed'
                                WHEN  cases.progress = '4' THEN 'Closed'
                                WHEN  cases.progress = '5' THEN 'Awaiting Contractor'
                                WHEN  cases.progress = '6' THEN 'Awaiting Approval'
                                WHEN  cases.progress = '7' THEN 'Commitee Approval'
                                WHEN  cases.progress = '8' THEN 'Admin Approval'
                                WHEN  cases.progress = '9' THEN 'Building Manager Discretion'
                            END) AS progress")
                        )
                        ->leftJoin('amenity_reservation as par','par.id','=','ar.parent_id')
                        ->leftJoin('building_inspection_area','building_inspection_area.id','=','par.bi_id')
                        ->leftJoin('cases','cases.id','=','ar.case_id')
                        ->where('ar.parent_id', '>', 0)
                        ->where('ar.status', '=', 1)
                        ->where('ar.building_id', '=', $building_id)
                        ->when($type == 'u', function ($query) {
                            $query->where('ar.booking_time_from', '>',  Carbon::now());
                        })
                        ->when($type == 'o', function ($query) {
                            $query->where('ar.booking_time_from', '<',  Carbon::now());
                            $query->where('cases.progress', '!=', 3);
                            $query->where('cases.progress', '!=', 4);
                        })
                        ->when($type == 'h', function ($query) {
                            $query->where('cases.progress', '=', 3);
                            $query->orWhere('cases.progress', '=', 4);
                        })
                        ->where('ar.type', 1)
                        ->when($search_term != null && $search_term != '' && $search_term != 'null', function ($query) use ($search_term) {
                            $query->where('ar.details', 'like', '%' . $search_term . '%');
                        })
                        ->orderBy('ar.id', 'desc')
                        ->paginate(12, ['*'], 'page', $page);
    }

    /**
     * Create sub event & case when type is job
     * 
     * @param $request event object
     * @param $user user detail
     * @param $starttime event start time
     * @param $endtime event end time
     * @param $event_id event id
     * @return no
     */
    public static function createSubEvent($request, $user, $starttime, $endtime, $event_id) {
        $case = [];
        $bc = new BuildingCase();
        $case = $bc->createCaseFromSchedule($request, $user);

        $sub_ar = new Amenity_Reservation;
        $sub_ar = self::initialEvent($request, $user, $starttime, $endtime);
        $sub_ar->parent_id = $event_id;
        $sub_ar->case_id = isset($case['id']) ? $case['id'] : null;
        $sub_ar->booking_recurring = null;
        $sub_ar->next_date = null;

        // time id format : 2021_01_01_14_00_00
        $time_id = $sub_ar->booking_time_from;
        $time_id = str_replace(" ","_", $time_id);
        $time_id = str_replace(":","_", $time_id);
        $time_id = str_replace("-","_", $time_id);
        $sub_ar->time_id = $time_id;
        $sub_ar->save();
    }

    /**
     * @param $request form data
     * @param $user user data
     * @param $starttime time
     * @param $endtime time
     */
    public static function initialEvent($request, $user, $starttime, $endtime) {
        if ($request['id'] > 0) {
            // Update
            $ar = Amenity_Reservation::find($request['id']);
        } else {
            // Create
            $ar = new Amenity_Reservation;
        }

        $ar->building_id = $user['building_id'];
        $ar->case_id =  $request['case_id'];
        $ar->parent_id =  $request['case_id'] > 0 ? $request['id'] : null;
        $ar->amenity_id = isset($request['amenityId']) ? $request['amenityId'] : $request['amenity_id'];
        $ar->tenant_id =  isset($request['newTenant']) ? $request['newTenant'] : $request['tenant_id'];
        $ar->blg_admin_id =  $user['id'];
        // $ar->bii_id = request('bii_id');
        $ar->location = null;
        $ar->task_type = null;
        $ar->car_plate = $request['car_plate'];
        $ar->booking_recurring = $request['booking_recurring'];

        $ar->booking_time_from = $starttime;
        $ar->booking_time_to = $endtime;
        $ar->details = $request['details'];
        $ar->progress = $request['progress'];
        $ar->comment = $request['comment'];
        $ar->next_date = isset($request['next_date']) ? $request['next_date'] : null;
        $ar->type = isset($request['typeId']) ? $request['typeId'] : $request['type'];
        $ar->contractor_id = isset($request['contractor_list']) ? $request['contractor_list'] : $request['contractor_id'];
        $ar->bi_id =  isset($request['bi_list']) ? $request['bi_list'] : $request['bi_id'];
        $ar->status = 1;

        return $ar;
    }

    /**
     * Get job type array 
     * @return job type array
     */
    public function getSchedulerType() {
        return array(
            array("value" => '1', "text" => "Job", "color" => ''),
            array("value" => '3', "text" => "Reservation", "color" => ''),
            array("value" => '4', "text" => "Parking", "color" => '')
        );
    }

    /**
     * Get default event status 
     * @return array
     */
    public function getEventStatusList() {
        return array(
            array("value" => '0', "text" => "Pending"),
            array("value" => '1', "text" => "Approve"),
            array("value" => '2', "text" => "Reject"),
        );
    }

    /**
     * Get list of reservation
     * 
     * For event type = Job
     * Note: Main reservation event -> parent_id = null && case_id = null && time_id = null
     * Note: Hidden reservation event used to connect to case table -> parent_id > 0 && case_id > 0 && time_id != null
     * Note: Reservation event created on case -> parent_id = null && case_id > 0 && time_id = null
     * 
     * @param date $first_day       First day of the filter
     * @param date $last_day        Last day of the filter
     * @param string $type          Job Type
     * @param int $building_id      building id
     * @param string $search_txt    search keyword
     * @param int $page             page number
     * @param string $order         asc or desc
     * @param int $page_qty         page number
     * @param boolean $hideClosedBookings
     * @return array
     */
    public function get_reservation_list($first_day = null, $last_day = null, $type, $building_id, $search_txt = '', $page = 1, $order = 'asc', $page_qty = 200, $event_id = null, $hideClosedBookings) {
        $ba_id = null;
        if ($first_day == 'null' || $first_day == null || $first_day == '' || $last_day == 'null' ||  $last_day == null || $last_day == '') {
            $first_day = '2010-01-01';
            $last_day = '2030-12-31';
        } elseif ($first_day == $last_day) {
           $your_date = strtotime("1 day", strtotime($first_day));
           $last_day = date("Y-m-d", $your_date);
        }

        if ($type == null || $type == '') {
            $type_arr = array(1,3,4);
        } else if ($type == 'a') {
            $type_arr = array(3, 4);
        } else if (strpos($type,'ba-') !== false) {
            $type_arr = array(3);
            $ba_id = str_replace("ba-","", $type);
        } else {
            $type_arr[] = $type;
        }

        $list = DB::table('amenity_reservation as ar')
                    ->select(
                        "ar.id", "ar.tenant_id", "ar.bi_id as bi_id", "ar.parent_id", "ar.type as typeId", "ar.booking_time_from as start", "ar.booking_time_to as end", "ar.booking_recurring", "ar.booking_exception as recurrenceException", "ar.car_plate", "ar.progress", "ar.amenity_id", "ar.details", "ar.comment", "ar.booking_end", "a.name as amenity_title", "ar.case_id", "ar.updated_at", 
                        DB::raw(
                            "(CASE 
                                WHEN (ar.booking_recurring IS NOT NULL AND ar.booking_recurring != '') AND (ar.booking_end IS NOT NULL AND ar.booking_end != '') THEN CONCAT(ar.booking_recurring, ';UNTIL=',ar.booking_end)
                                WHEN (ar.booking_recurring IS NULL OR ar.booking_recurring = '') AND (ar.booking_end IS NOT NULL AND ar.booking_end != '') THEN  CONCAT('UNTIL=',ar.booking_end)
                                WHEN (ar.booking_recurring IS NOT NULL AND ar.booking_recurring != '') AND (ar.booking_end IS NULL OR ar.booking_end = '') THEN ar.booking_recurring
                                WHEN (ar.booking_recurring IS NULL AND ar.booking_recurring = '') AND (ar.booking_end IS NULL AND ar.booking_end = '') THEN NULL
                            END) AS recurrenceRule 
                            "
                        ),
                        DB::raw(
                            "(CASE 
                                WHEN ar.progress = 0 THEN 'Pending'
                                WHEN ar.progress = 1 THEN 'Approved'
                                WHEN ar.progress = 2 THEN 'Rejected'
                            END) AS progress_title 
                            "
                        ),
                        DB::raw(
                            "(CASE 
                                WHEN ar.type = 1 THEN ar.comment 
                                WHEN ar.type = 3 THEN a.name 
                                WHEN ar.type = 4 THEN ar.car_plate
                            END) AS event_object 
                            "
                        ),
                        DB::raw(
                            "(CASE 
                                WHEN ar.type = 1 THEN c.comp_name 
                                WHEN ar.type = 3 OR ar.type = 4 THEN CONCAT(td.first_name,' ', td.last_name, ' [', u.unit_title, '] ') 
                            END) AS event_details
                            "
                        ), 
                        DB::raw("(SELECT GROUP_CONCAT(DISTINCT CONCAT(ar2.case_id,',', ar2.time_id, ',', cases.case_type, ',', ar2.id, ',', ar2.booking_time_from, ',', cases.progress) SEPARATOR '|') as inspection_history 
                                    from amenity_reservation as ar2 
                                    left join cases on cases.id = ar2.case_id
                                    where ar2.status = 1 
                                    and ar2.parent_id = ar.id
                                ) as inspection_history")
                    )
                    ->join('tenant_detail as td',function($join){
                        $join->on('td.user_id','=','ar.tenant_id');
                    }, null,null,'left')
                    ->join('building_amenity as a',function($join){
                        $join->on('a.id','=','ar.amenity_id');
                    }, null,null,'left')
                    ->join('unit as u',function($join){
                        $join->on('u.id','=','td.unit_id');
                    }, null,null,'left')                    
                    ->join('contractor as c',function($join){
                        $join->on('c.id','=','ar.contractor_id');
                    }, null,null,'left')
                    ->where('ar.status', '=', 1)
                    ->where('ar.building_id', '=', $building_id)
                    ->where('ar.time_id', '=', null)
                    ->where(function ($q) use ($first_day, $last_day)  {
                        $q->whereBetween('ar.booking_time_from', [$first_day, $last_day])
                            ->orWhere(function ($query)  {
                                $query->OrWhere('ar.booking_recurring', '=', 'FREQ=DAILY');
                                $query->OrWhere('ar.booking_recurring', '=', 'FREQ=WEEKLY');
                                $query->OrWhere('ar.booking_recurring', '=', 'FREQ=MONTHLY');
                                $query->OrWhere('ar.booking_recurring', '=', 'FREQ=YEARLY');
                            });
                    })
                    ->where(function ($q) use ($first_day, $last_day)  {
                          $q->where('ar.booking_end', '=', null);
                          $q->OrWhere('ar.booking_end', '>', $last_day);
                          $q->orWhereBetween('ar.booking_time_from', [$first_day, $last_day]);
                    })
                    ->whereIn('ar.type', $type_arr)
                    ->when($ba_id != null, function ($q) use ($ba_id)  {
                        $q->where('a.id', '=', $ba_id);
                    })
                    ->when($event_id != null, function ($q) use ($event_id)  {
                        $q->where('ar.id', '=', $event_id);
                    })
                    ->when($hideClosedBookings == true, function ($q)  {
                        $q->where('ar.booking_time_from', '>', Carbon::now());
                    })
                    ->when($search_txt != 'null' && $search_txt != null && $search_txt != '', function ($query) use ($search_txt)  {
                        $query->where(function ($q) use ($search_txt) {
                            $q->where('c.comp_name', 'like', '%' . $search_txt . '%');
                            $q->OrWhere('td.first_name', 'like', '%' . $search_txt . '%');
                            $q->OrWhere('td.last_name', 'like', '%' . $search_txt . '%');
                        });
                    })
                    ->orderBy('ar.id', $order)
                    ->paginate($page_qty, ['*'], 'page', $page);

        $this->updateBookingExceptionTimeZone($list);
        return $list;
    }

    /**
       * Get all reservation events by case id
       * @param int $case_id
       * @return array
    */
    public function getEventsByCaseID($case_id) {
        return self::select("amenity_reservation.id as id", "tenant_id", "details", "c.id as cid", "c.comp_name", "booking_time_from", "booking_time_to", "amenity_id as amenityId", "comment", "progress as status")
                        ->join('contractor as c',function($join){
                            $join->on('c.id','=','contractor_id');
                        }, null,null,'left')
                        ->where('amenity_reservation.status', '=', 1)
                        ->where('amenity_reservation.case_id', '=', $case_id)
                        ->get()->toArray();     
    }

    /**
     * For Temp use
     * Convert recurrenceException into UTC timezone
     * @param array $list
     * @return array 
     */
    public function updateBookingExceptionTimeZone($list) {
        for($i = 0 ; $i < count($list); $i++) {
            // recurrenceException
            if (!empty($list[$i]->recurrenceException)) {
                $new_date = new DateTime($list[$i]->recurrenceException);
                $new_date->setTimezone(new DateTimeZone('UTC'));;
                $list[$i]->recurrenceException = $new_date->format(DATE_ISO8601);
            }
        }
    }

    /**
     * Save event
     * @param array $request Event posted by building manager
     * @param object $user Current user
     * @param DateTime $starttime Event start time format: Y-m-d H:i:s
     * @param DateTime $endtime Event end time format: Y-m-d H:i:s
     * @return event ID
     */
    public static function saveEvent($request, $user, $starttime, $endtime) {
        if (isset($request['amenityId']) == false) {
            $request['amenityId'] = null;
        }

        if (isset($request['newTenant']) == false) {
            $request['newTenant'] = null;
        }

        if (isset($request['location']) == false) {
            $request['location'] = null;
        }

        if (isset($request['tasktype']) == false) {
            $request['tasktype'] = null;
        }

        if (isset($request['car_plate']) == false) {
            $request['car_plate'] = null;
        }

        if (isset($request['details']) == false) {
            $request['details'] = null;
        }

        if (isset($request['details']) == false) {
            $request['details'] = null;
        }

        if (isset($request['comment']) == false) {
            $request['comment'] = null;
        }

        if (isset($request['contractor_list']) == false) {
            $request['contractor_list'] = null;
        }
        
        $ar = self::initialEvent($request, $user, $starttime, $endtime);
        $ar->next_date = null;
        if (empty($request['id']) && $ar->parent_id > 0) {
            $ar->next_date = $starttime;
        }
        $ar->save();

        // create sub event & case 
        if ($request['case_id'] == null) {
            if (((isset($request['typeId']) && $request['typeId'] == 1) || (isset($request['type']) && $request['type'] == 1)) ) {
                self::createSubEvent($request, $user, $starttime, $endtime, $ar->id);
            }
        }
        return $ar->id;
    }

    /**
     * Remove event
     * If action = job, then add today as booking_end, if not, set status = 0
     * @param int $id
     * @return
     */
    public function rmEventByID($id, $type, $date, $parentID = null) {
        if ($type == 1) {
            // Record is sub-event if parent id > 0; record is a single event in the case if parent id = null
            if ($parentID > 0) {
                return self::where('id', '=', $id)->update(array('booking_end' => $date));
            } else {
                return self::where('id', '=', $id)->update(array('status' => 0));
            }
        } else {
            return self::where('id', '=', $id)->update(array('status' => 0));
        }
    }

    /**
     * Remove sub-events By Parent ID
     * @param int $id
     * @return
     */
    public function rmEventsByParentID($id) {
        return self::where('parent_id', '=', $id)->update(array('status' => 0));
    }

    /**
     * Get start time & parent id by event id
     * @param int $id
     * @return array id & start_time
     */
    public function  getStartTimeByEventID($id) {
        return self::select('id', 'booking_time_from', 'parent_id', 'case_id')->where('id', '=', $id)->get()->first();
    }

    /**
     * Insert Booking exception time
     * @param int $id
     * @param Datetime $booking_exception_time - format: Y-m-d H:i:s
     * @return array
     */
    public function insertBookingExceptionTime($id, $booking_exception_time) {
        return self::where('id', '=', $id)->update(array('booking_exception' => $booking_exception_time));
    }

    /**
     * Get all sub events && cases after today by parent id, so we get count of total of this array
     * @param int $parent_id - parent id
     * @param datetime $datetime - current date and time, format: Y-m-d H:i:s
     * @param boolean $setCaseFilter - set filter only case exists
     * @return array
     */
    public function getSubEventsByParentID($parentID, $timeFrom = null, $timeTo = null, $setCaseFilter = true) {
        return self::select('id', 'case_id','booking_time_from', 'progress')
                    ->where('parent_id', '=', $parentID)
                    ->when($setCaseFilter == true, function ($query) {
                        $query->where('case_id', '>', 0);
                    })
                    ->when(!empty($timeFrom), function ($query) use ($timeFrom) {
                        $query->where('booking_time_from', '>', $timeFrom);
                    })
                    ->when(!empty($timeTo), function ($query) use ($timeTo) {
                        $query->where('booking_time_to', '<=', $timeTo);
                    })
                    ->where('status', '=', 1)
                    ->get()->toArray();
    }

    /**
     * Remove sub-events by sub-event ids array
     * @param array sub event ids
     * @return
     */
    public function removeSubEventsByIDs($subEventIDs) {
        return self::whereIn('id', $subEventIDs)->update(array('status' => 0));
    }

    








    public static function get_task_by_date($building_id, $date_from, $date_to) {
        return self::select('type', 'location', 'task_type', 'details', 'progress', 'comment', 'created_at')
                    ->where('building_id', '=', $building_id)
                    ->where(function($query) use ($date_from, $date_to) {
                        $query->whereBetween('created_at',[$date_from, $date_to]);
                    })
                    ->where('status', '=', 1)
                    ->orderBy('created_at', 'asc')
                    ->get()->toArray();
    }


    public static function get_bookingexception_by_id ($id) {
        return self::where('status', '=', 1)
                    ->where('id', '=', $id)
                    ->pluck('booking_exception');
    }

    // deprecated
    public static function update_event ($request, $user, $starttime, $endtime) {
        if ($request['typeId'] == 1 || $request['typeId'] == 2) {
            $a = self::update_recurring_time($request['id']);

            if ($a) {
                return self::saveEvent($request, $user, $starttime, $endtime);
            }
        } else {
            return self::where('id', '=', $request['id'])->update(
                array(
                    'blg_admin_id' => $user['id'], 
                    'progress' => $request['progress'] == null || $request['progress'] == '' ? 0 : $request['progress'], 
                    'comment' => $request['comment'], 
                    'booking_time_from' => $starttime, 
                    'booking_time_to' => $endtime, 
                    'details' => $request['details'], 
                    'tenant_id' => $request['newTenant'], 
                    'amenity_id' => $request['amenityId'], 
                    'bi_id' => $request['bi_list'], 
                    // 'bii_id' => request('bii_id'),
                    'location' => $request['location'],
                    'task_type' => $request['tasktype'],
                    'car_plate' => $request['car_plate'],
                    'booking_recurring' => $request['inspection_frequency_txt'],
                    'contractor_id' => $request['contractor_list'], 
                    'type' => $request['typeId'], 
                    'building_id' =>  $user['building_id']
                )
            );
        }
    }


    // deprecated
    public static function update_recurring_time ($id) {
        $ar = self::where('id', '=',$id)->first();
        $time = date("Y-m-d H:i:s", strtotime("-1 minutes", strtotime("now")));
        $br = $ar->booking_recurring == 'none' ? null : $ar->booking_recurring.';';

        return $ar::where('id', '=', $id)->update(['booking_recurring' => $br."UNTIL=" .$time]);
       
    }
}
