<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{
    //
    protected $table = 'building_announcement';
    protected $dates = ['created_at'];


    public static function getList($page = 1, $qty = 20) {
        
        $Announcement = DB::table('building_announcement')
                            ->select('building_announcement.id', 'building_announcement.admin_id', 'users.name',  'building_announcement.content', 'building_announcement.created_at' , 'building_announcement.recipients')
                            ->leftJoin('users','users.id','=','building_announcement.admin_id')
                            ->where('status', '=', 1)
                            ->orderBy('created_at', 'desc')
                            ->paginate($page);

        
        return $Announcement;
    }
}
