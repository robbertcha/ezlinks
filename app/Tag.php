<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model {
    //
    protected $table = "tag";
    protected $fillable = ['type', 'title', 'building_id', 'description', 'status'];

     /**
     * Get tag by type
     * @param var $type
     * @param int $building_id
     * 
     */
    public function getTagByType($type, $building_id) {
        return self::where('status', '=', 1)
                    ->where('type', '=', $type)
                    ->where('building_id', '=', $building_id)->where('status', '=', '1')->get()->toArray();
    }

    /**
     * Get unit tags by building id
     * @param int $building_id
     * @return array
     */
    public function getUnitTags($building_id) {
        return self::where('building_id', '=', $building_id)
                                    ->where('status', '=', '1')
                                    ->where('type', '=', 'unit')
                                    ->get()->keyBy('id');
    }




    public static function get_unit_tag($building_id) {
        $type = 'unit';
        return self::select('id', 'title', 'description')
                    ->where('building_id', '=', $building_id)
                    ->where('type', '=', $type)
                    ->where('status', '=', 1)
                    ->orderBy('created_at', 'desc')
                    ->get()->toArray();
    }

    public static function get_announcement_tag($building_id) {
        $type = 'announcement';
        return self::select('id', 'title', 'description')
                    ->where('building_id', '=', $building_id)
                    ->where('type', '=', $type)
                    ->where('status', '=', 1)
                    ->orderBy('created_at', 'desc')
                    ->get()-> toArray();
    }


}
