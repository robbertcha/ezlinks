<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Building_Case_Comment extends Model
{
    //
    public $table = "case_comment";

    public function get_case_comments($cid) {
        return $this::select("id", "case_id", "author_id", "author", "comment", "created_at")
                        ->where('case_id', '=', $cid)
                        ->where('status', '=', 1)
                        ->get()->toArray();
    }

}
