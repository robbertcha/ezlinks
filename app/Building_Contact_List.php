<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;


class Building_Contact_List extends Model
{

    public $table = "contact_list";

    public static function defaultContactList(){

        Schema::create('contact_list', function($table)
                    {
                        $table->increments('id');
                        $table->integer('building_id')->default(1);
                        $table->string('country');
                        $table->string('location');
                        $table->string('name');
                        $table->string('number');
                        $table->string('email');
                        $table->integer('status')->default(1);
                    });

        DB::table('contact_list')->insert([
            ['name' => 'Police/Fire/Ambulance','number' => '000','status'=>3],
            ['name' => 'ES assistance in floods and storms','number' => '132500','status'=>3],
            ['name' => 'Police attendance except Victoria','number' => '131444','status'=>3]
        ]);
    }


    public static function getContactLists() {
        $building_id=$user['building_id'];
        $contactList = DB::table('contact_list')
            ->select('id','name','number','country','location','email','status')
            ->whereIn('status',[1,3])
            ->where('building_id', $building_id)
            ->get()->toArray();

        return response()->json($contactList);
    }

    public static function storeContact($request){

        $user = Auth::user();
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'number' => 'required',
        ]);

        if(Auth::check()){
            if(empty(request('id'))){
                DB::table('contact_list')->insert([
                    ['name' => request('name'),'number' => strval(request('number')), 'email' => request('email')]
                ]);
            }else{
                DB::table('contact_list')
                    ->where('id',request('id'))
                    ->update(['name'=> request('name'),'number' => request('number'), 'email' => request('email')]);
            }
        };

        return ;
    }

    public static function editContactList($request,$id){

        $user = Auth::user();
        if(Auth::check()){
            $building_id=$user['building_id'];
            $contactList = DB::table('contact_list')
            ->select('id','name','number','country','location','email','status')
                                                ->where('status',1)
                                                ->where('building_id', $building_id)
                                                ->where('id',$id)
                                                ->get()->toArray();
            }
            return response()->json($contactList);

    }

    public static function deleteContact($request,$id){

            $user = Auth::user();
            if(Auth::check()){
                $building_id=$user['building_id'];
                DB::table('contact_list')
                                ->where('id',$id)
                                ->update(['status'=>0]);
                }
            return;
    }










}
