<?php
namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Building_couriers extends Model {

    protected $table = "building_couriers";
    protected $fillable = ['tenant_id', 'admin_id', 'building_id', 'unique_code', 
    'courier_comp', 'picker_name', 'picker_cert_type', 'picker_cert_number', 'barcode', 'pickup_at', 'status', 'parcel_status', 'comment'];

    /**
     * Getting new parcel list by user id.
     */
    public static function get_new_parcel_list($user_id) {
        return self::select('building_couriers.id', 'barcode', 'courier_comp', 'building_couriers.created_at', 'parcel_status', 'building_couriers.comment', 'tenant_detail.first_name', 'building_admin.name') 
                            -> leftJoin('tenant_detail', 'tenant_detail.user_id', '=', 'building_couriers.tenant_id')
                            -> leftJoin('building_admin', 'building_admin.id', '=', 'admin_id')
                            -> where('tenant_id', '=', $user_id)
                            -> where('status', '=', 1)
                            -> where(function($query) {
                                $query->where('parcel_status', '1')
                                ->orWhere('parcel_status', '2');
                            })
                            -> where('picker_name', '=', null)
                            -> orderBy('created_at', 'desc', 'parcel_status', 'desc')
                            -> get() -> ToArray();

    }
    /**
     * Getting parcel history list by user id.
     */
    public static function get_parcel_history_list($user_id) {
        return self::select('building_couriers.id', 'barcode', 'courier_comp', 'picker_name', 'pickup_at', 'building_couriers.created_at', 'comment', 'picker_cert_type', 'unique_code', 'tenant_detail.first_name', 'building_admin.name')
                            -> leftJoin('tenant_detail', 'tenant_detail.user_id', '=', 'building_couriers.tenant_id')
                            -> leftJoin('building_admin', 'building_admin.id', '=', 'admin_id')
                            -> where('tenant_id', '=', $user_id)
                            -> where('status', '=', 1)
                            -> where('parcel_status','=', 3)
                            -> orderBy('building_couriers.updated_at', 'desc')
                            -> get() -> ToArray();
    }

    /**
     * Creating a unique code for each parcel if its code is not existed yet and return it;
     */
    public static function create_unique_code($user_id, $parcel_id) {
        
        $user_id = $user_id;
        
        $unique_code = rand(000001,999999);
        
        $parcel_id = $parcel_id;
        
        $parcel = self::where('status', '=', 1)
                    -> where('tenant_id', '=', $user_id)
                    -> where('id', '=', $parcel_id)
                    -> where('parcel_status', '=', 1);
                    
        if ($parcel -> exists()) {
            
            $parcel = $parcel -> update(['unique_code' => $unique_code, 'parcel_status' => 2]);
            return $parcel;
        } else {
            
            return;
        }

    }

    /**
     * Getting the unique code by user id & parcel id.
     */
    public static function get_code($user_id, $parcel_id) {
        
        return self::select('unique_code')
                    -> where('tenant_id', '=', $user_id)
                    -> where('id', '=', $parcel_id)
                    -> where('parcel_status', '=', 2)
                    -> where('status', '=', 1)
                    -> first();

    }
}
