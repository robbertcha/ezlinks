<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Case_Progress extends Model
{
    //
    protected $table = 'case_progress';

    /**
     * Get logs by case id
     * @param int $case_id
     * @return array
     */
    public function get_log($cid) {
        return Case_Progress::select("id", "message", "created_at")
                                ->where('case_id', '=', $cid)
                                // ->orderBy('created_at', 'desc')          
                                ->get()->toArray();
    }
}
