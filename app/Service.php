<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    //
    public $table = "product";
    public function mgradmin() {
        return $this->belongsTo('App\MgrAdmin');
    }
}
