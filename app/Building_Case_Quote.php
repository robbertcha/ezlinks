<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Building_Case_Quote extends Model
{
    //
    protected $table = 'case_quote';

    public function get_case_quotes($cid) {
        return self::select("case_quote.id", "c.id as cid", "c.comp_name","case_quote.contractor_id" , "case_quote.quote_value", 'case_quote.quote_doc', DB::raw("DATE_FORMAT(case_quote.created_at, '%d %M') as created_at"))
                                    ->join('contractor as c',function($join){
                                        $join->on('c.id','=','case_quote.contractor_id');
                                    }, null,null,'left')
                                    ->where('case_quote.case_id', '=', $cid)
                                    ->where('case_quote.status', '=', 1)
                                    ->get()->toArray();
    }
}
