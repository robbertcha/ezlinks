<?php

namespace App\Http\Controllers\Mgr;

use File;
use App\Service;
use App\MgrAdmin;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class MgrServiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:building_admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('mgr.service_schedule');
    }


    public function test_doc(Request $request) {
        date_default_timezone_set('Australia/Brisbane');
            $fileName = 'test123_'.$request->doc_file->getClientOriginalName();

            $path = public_path().'/uploads/';
            File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);

            $request->doc_file->move(public_path().'/uploads/', $fileName);
    }


    public function service_list()
    {
        //
        $all_products = DB::table('product')
        ->select("product.*", "building_admin.name",
                DB::raw("(SELECT  GROUP_CONCAT(merchant.name SEPARATOR ', ') FROM merchant WHERE merchant.id IN (1,2)) as merchant_name")
        )
        ->leftJoin('building_admin','building_admin.id','=','product.mgr_admin_id')
        ->orderBy('created_at', 'desc')
        ->paginate(1);
        return view('mgr.service_list' , compact('all_products'));
    }

    public function create_service() {
        return view('mgr.create_service');
    }

    public function service_provider_list() {
        $sproviders = DB::table('merchant')
        ->select("merchant.*")
        ->orderBy('created_at', 'desc')
        ->paginate(1);
        return view('mgr.service_provider_list' , compact('sproviders'));

    }

    public function create_service_provider() {
        return view('mgr.new_service_provider' );
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        //
    }
}
