<?php

namespace App\Http\Controllers\Mgr;

use Auth;
use File;
use \Carbon\Carbon;

use App\Setting;
use App\Building;
use App\Building_Admin;
use App\Amenity_Reservation;
use App\Building_Admin_Profile;
use App\Building_Log;
use App\Building_Case;
use App\Building_Stats;
use App\Building_Asset;
use App\Building_Setting_Check;
use App\Building_Asset_Doc;
use App\Building_Inventory;
use App\Building_Inventory_Doc;
use App\Building_Inspection;
use App\Building_Inspection_Record;
use App\Building_Inspection_History;
use App\Building_Inspection_Area;
use App\Building_Inspection_Item;
use App\Building_Category;
use App\Building_Template;

use App\Unit;
use App\User;
use App\Building_Amenity;
use App\Contractor;

use Notification;
use App\Notifications\PushDemo;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Mgr\MgrSettingCheckController;
use App\Http\Controllers\Mgr\MgrReservationController;

// service
use App\Services\Mgr\Reservations\ReservationsService;
use App\Services\Mgr\Inspections\InspectionService;
use App\Services\Mgr\Building\BuildingService;

class MgrSettingController extends Controller
{
    protected $cate = [];

    public function __construct() {
        $this->middleware('auth:building_admin');

        $this->cate[0]['id'] = 1;
        $this->cate[0]['parentId'] = null;
        $this->cate[0]['Name'] = 'Asset';

        // $this->cate[1]['id'] = 2;
        // $this->cate[1]['parentId'] = null;
        // $this->cate[1]['Name'] = 'Announcement';

        // $this->cate[2]['id'] = 3;
        // $this->cate[2]['parentId'] = null;
        // $this->cate[2]['Name'] = 'Case';

        // $this->cate[3]['id'] = 4;
        // $this->cate[3]['parentId'] = null;
        // $this->cate[3]['Name'] = 'Maintainance';

        // $this->cate[4]['id'] = 5;
        // $this->cate[4]['parentId'] = null;
        // $this->cate[4]['Name'] = 'Contractor';

        $this->cate[1]['id'] = 6;
        $this->cate[1]['parentId'] = null;
        $this->cate[1]['Name'] = 'Inventory';

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        return view('mgr.blg_init');
    }

    public function get_category($type) {
        $user = Auth::user();
        if (Auth::check()) {

            $building_id = $user['building_id'];
            $model = new Building_Category();
            $cate_array = [];
            $i = 0;
            
            // get category by type (asset or inventory)
            if ($type == 'asset') {
                $category = $model::get_asset_category($building_id);
            } else if ($type == 'inventory') {
                $category = $model::get_inventory_category($building_id);   
            } else {
                $record['success'] = false;
                return response()->json($record);
            }
            foreach ($category as $key => $val) {
                $cate_array[$i]['id'] = $val['id'];
                $cate_array[$i]['Name'] = $val['name'];
                $cate_array[$i]['Description'] = $val['description'];
                $i++;
            }
            $record['success'] = true;
            $record['cate'] = $cate_array;
           
        }
        return response()->json($record);
    }

    public static function add_cate(Request $request) {
        $user = Auth::user();
        
        $record = [];
        if (Auth::check()) {
            $building_id = $user['building_id'];
            $cate_type = $request->cate_type;
            if ($cate_type == 'asset') {
                $cate_id = 1;
            } elseif ($cate_type == 'inventory') {
                $cate_id = 6;
            } else {
                return;
            }
            
            $model = new Building_Category();
            $new_cate_id = $model::add_category($request, $building_id, $cate_id);
            if ($new_cate_id) {
                $record['success'] = true;
                $record['new_cate_id'] = $new_cate_id;
            } else {
                $record['success'] = false;
            }
            return response()->json($record);
        }
    }

    // remove category
    public static function remove_cate(Request $request) {
        $user = Auth::user();
        $record = [];
        if (Auth::check()) {
            $model = new Building_Category();
            $id = $request->id;
            $remove_cate = $model::remove_category($id);
            $remove_cate ? $record['success'] = true : $record['success'] = false;
            return response()->json($record);
        }
    }

    // update category
    public static function update_cate(Request $request) {
        $user = Auth::user();
        $record = [];
        if (Auth::check()) {
            $model = new Building_Category();
            $id = $request->id;
            $name = $request->name;
            $description = $request->description;
            
            $update_cate = $model::update_category($id, $name, $description);
            $update_cate ? $record['success'] = true : $record['success'] = false;
            return response()->json($record);
        }
    }

    public function insert_webpush_info(Request $request) {
        $user = Auth::user();
        $record = array();
        if (Auth::check()) {
            $record['p256dh'] = $request->subscription['keys']['p256dh'];
            $record['auth'] = $request->subscription['keys']['auth'];
            $record['uniqueid'] = $request->uniqueid;
            $record['endpoint'] = $request->subscription['endpoint'];
            $record['expirationtime'] = $request->subscription['expirationTime'];

            //$user = Auth::user();
            $user->updatePushSubscription( $record['endpoint'], $record['p256dh'], $record['auth']);

            // if ($request->action == 'new') {
            //     $push = new push_subscriptions;
            // } else {
            //     $push = push_subscriptions::where('user_id' , '=', $user->id)->first();
            // }

            // $push->user_id = $user->id;
            // $push->uniqueid = $request->uniqueid;
            // $push->endpoint = $request->subscription['endpoint'];
            // $push->auth = $request->subscription['keys']['auth'];
            // $push->p256dh = $request->subscription['keys']['p256dh'];
            // $push->expiredTime = $request->subscription['expirationTime'];
            // $push->save();

            // if ($push->id > 0) {
            //      $record['success'] = true;
            //     $record['id'] = $push->id;
            // } else {
            //     $record['success'] = false;
            //     $record['msg'] = "We cannot process your request due to some technical problems, please try again later or contact your building manager, thanks.";
            // }
        }

        return response()->json($record);
    }


    
    public function push(){
        $user = Auth::user();
        $record = array();

        if (Auth::check()) {
            Notification::send(Building_Admin::find($user->id),new PushDemo());
        }
    }



    public function comp_info(){
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];

            $building = Building::select('name', 'code','logo','address','postcode','surburb','state','website','phone','fax','description')
                        ->where('status', '=', 1)
                        ->where('id', '=', $building_id)
                        ->first();

            return view('mgr.comp_info', compact('building'));
        } else {

        }
    }

    public function initial_update_building_logo(Request $request) {
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];
            $building = Building::find($building_id);

             // add logo
             if (isset($request->comp_logo) && $request->comp_logo != null && $request->comp_logo != '') {
                date_default_timezone_set('Australia/Brisbane');
                $fileName = time().'_'.$building_id.'_'.$request->comp_logo->getClientOriginalName();
                $path = public_path().'/uploads/building_logos/'.date('Y_m_d');
                File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
                $request->comp_logo->move(public_path().'/uploads/building_logos/'.date('Y_m_d').'/', $fileName);
                $building->logo = '/uploads/building_logos/'.date('Y_m_d').'/'.$fileName;
            }

            $building->save();

            $setting_check = new MgrSettingCheckController;
            $logo_check = $setting_check::checkSettingMethod($building_id, 'setting_logo');

            $record['success'] = true;
            $record['action'] = 'update_building_logo';
            $record['building_id'] = $building_id;
        } else {
            $record['success'] = false;
        }

        return response()->json($record);
    }


    public function initial_update_building(Request $request) {
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];

            $building = Building::find($building_id);
            $building->name = request('name');
            $building->address = request('address');
            $building->postcode = request('postcode');
            $building->surburb = request('surburb');
            $building->state = request('state');
            $building->website = request('website');
            $building->phone = request('phone');
            $building->save();

            $setting_check = new MgrSettingCheckController;
            $phone_check = $setting_check::checkSettingMethod($building_id, 'setting_phone');
            
            $record['success'] = true;
            $record['action'] = 'update_building_info';
            $record['building_id'] = $building_id;
        } else {
            $record['success'] = false;
        }
        return response()->json($record);
    }

    public function update_building(Request $request) {
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];

            $building = Building::find($building_id);
            $building->name = request('txt_buildingname');
            $building->address = request('txt_buildingaddr');
            $building->postcode = request('txt_buildingpostcode');
            $building->surburb = request('txt_buildingsurburb');
            $building->state = request('txt_buildingstate');
            $building->website = request('txt_buildingwebsite');
            $building->phone = request('txt_buildingphone');
            $building->fax = request('txt_buildingfax');
            $building->description = request('building_intro');


            // add logo
            if (isset($request->comp_logo) && $request->comp_logo != null && $request->comp_logo != '') {
                date_default_timezone_set('Australia/Brisbane');
                $fileName = time().'_'.$building_id.'_'.$request->comp_logo->getClientOriginalName();
                $path = public_path().'/uploads/building_logos/'.date('Y_m_d');
                File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
                $request->comp_logo->move(public_path().'/uploads/building_logos/'.date('Y_m_d').'/', $fileName);
                $building->logo = '/uploads/building_logos/'.date('Y_m_d').'/'.$fileName;
            }

            $building->save();

            $setting_check = new MgrSettingCheckController;
            $logo_check = $setting_check::checkSettingMethod($building_id, 'setting_logo');
            $phone_check = $setting_check::checkSettingMethod($building_id, 'setting_phone');
            
            
            $record['success'] = true;
            $record['action'] = 'update_building';
            $record['building_id'] = $building_id;

        } else {

        }
        return redirect('mgr/setting');
    }

    public function asset_list($p=null){
        $user = Auth::user();
        if (Auth::check()) {
            $search = new \stdClass;
            $building_id = $user['building_id'];
 

            if (isset($_REQUEST['input_search']) && $_REQUEST['input_search'] != '' && $_REQUEST['input_search'] != null) {
                $search->input_search = $_REQUEST['input_search'];
            }


            if (isset($_REQUEST['asset_cate']) && $_REQUEST['asset_cate'] != '' && $_REQUEST['asset_cate'] != null) {
                $search->asset_cate = $_REQUEST['asset_cate'];
            }

            $assets = Building_Asset::select('building_assets.id','building_assets.cate_id','building_category.name as cate_name', 'building_assets.name', 'building_assets.value','building_assets.barcode','building_assets.location','building_assets.serial_no','building_assets.make','building_assets.model','building_assets.warranty_title','building_assets.warranty_expiry','building_assets.note')
                            ->leftJoin('building_category','building_category.id','=','building_assets.cate_id')
                            ->where('building_assets.status', '=', 1)
                            ->where('building_assets.building_id', '=', $building_id)
                            ->when(isset($_REQUEST['input_search']), function ($query) {
                                $query->where('building_assets.name', 'like', '%' . $_REQUEST['input_search'] . '%');
                            })
                            ->when(isset($_REQUEST['asset_cate']) && $_REQUEST['asset_cate'] > 0, function ($query) {
                                $query->where('building_assets.cate_id', $_REQUEST['asset_cate']);
                            })
                            ->paginate(12, ['*'], 'page', $p);

            $asset_cate = $this->get_catelist_by_mainID(1);               
            
            if(empty($p)){
                return view('mgr.asset_list', compact('assets','asset_cate','search'));
            }else{
                return response()->json(['assets' => $assets,'asset_cate' => $asset_cate,'search' => $search]);
            }
        } else {

        }
    }

    public function create_asset(){
        $asset = new \stdClass;
        $asset_doc = new \stdClass;
        $asset_cate = $this->get_catelist_by_mainID(1);

        return view('mgr.create_asset', compact('asset','asset_doc', 'asset_cate'));
    }

    public function edit_asset($aid){
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];

            $asset = Building_Asset::select('id','name', 'value','cate_id','barcode','location','serial_no','make','model','warranty_title','warranty_expiry','note','photo')
                            ->where('status', '=', 1)
                            ->where('building_id', '=', $building_id)
                            ->where('id', '=', $aid)
                            ->first();

            $asset_doc = Building_Asset_Doc::select('id','title', 'path','size','type as extension')
                                                ->where('status', '=', 1)
                                                ->where('asset_id', '=', $aid)
                                                ->get()->toArray();

            $asset_cate = $this->get_catelist_by_mainID(1);

            return view('mgr.create_asset', compact('asset','asset_doc', 'asset_cate'));
        } else {

        }
    }

    public function save_asset(Request $request){
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];

            // $building = Building::find($building_id);
            if (request('asset_id') > 0) {
                $asset = Building_Asset::find(request('asset_id'));
            } else {
                $asset = new Building_Asset;
                $asset->status = 1;
            }
            $asset->name = request('txt_assetname');
            $asset->building_id = $building_id;
            $asset->value = request('txt_assetval');
            $asset->cate_id = request('asset_cate');
            $asset->barcode = request('txt_assetbarcode');
            $asset->location = request('txt_assetloc');
            $asset->serial_no = request('txt_assetserial');
            $asset->make = request('txt_assetmake');
            $asset->model = request('txt_assetmodel');
            $asset->warranty_title = request('txt_assetwarranty');
            $asset->warranty_expiry = request('txt_assetwarrantydue');
            $asset->note = request('asset_note');
            $asset->photo = request('asset_original_photo');

            // add logo
            if (isset($request->asset_photo) && $request->asset_photo != null && $request->asset_photo != '') {
                date_default_timezone_set('Australia/Brisbane');
                foreach ($request->asset_photo as $key => $val) {
                    $fileName = time().'_'.$building_id.'_'.$val->getClientOriginalName();
                    $path = public_path().'/uploads/building_assets/'.date('Y_m_d');
                    File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
                    $val->move(public_path().'/uploads/building_assets/'.date('Y_m_d').'/', $fileName);
                    $asset->photo = '/uploads/building_assets/'.date('Y_m_d').'/'.$fileName;
                }
            }
            $asset->save();

            if (request('asset_id') > 0) {
                $asset_id = request('asset_id');
            } else {
                $asset_id = $asset->id;

                // update stats
                $building_stats = new Building_Stats();
                $building_stats->increase('total_asset', 1, $user['building_id']);
            }



            // update current docs
            if (isset($request->asset_doc_id) && $request->asset_doc_id != null && $request->asset_doc_id != '') {
                // $arr = implode(",",$request->asset_doc_id);
                DB::table('building_asset_docs')
                    ->whereNotIn('id', $request->asset_doc_id)
                    ->where('asset_id', $asset_id)
                    ->update(['status' => 0 ]);
          
            }else {
                DB::table('building_asset_docs')
                    ->where('asset_id', $asset_id)
                    ->update(['status' => 0 ]);
            }

            // save new docs
            if (isset($request->asset_docs) && $request->asset_docs != null && $request->asset_docs != '') {
                date_default_timezone_set('Australia/Brisbane');

                foreach ($request->asset_docs as $key => $val) {
                    $asset_docs = new Building_Asset_Doc;
                    $size = $val->getClientSize();
                    $type = $val->getClientOriginalExtension();

                    $fileName = time().'_'.$building_id.'_'.$val->getClientOriginalName();
                    $path = public_path().'/uploads/building_docs/'.date('Y_m_d');
                    File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
                    $val->move(public_path().'/uploads/building_docs/'.date('Y_m_d').'/', $fileName);

                    
                    $asset_docs->asset_id = $asset_id;
                    $asset_docs->title =  $fileName;
                    $asset_docs->path = '/uploads/building_docs/'.date('Y_m_d').'/'.$fileName;
                    $asset_docs->size = $size;
                    $asset_docs->type = $type;
                    $asset_docs->status = 1;
                    $asset_docs->save();
                }
            }

            
        } else {

        }
        return redirect('mgr/assets');
    }

    

    public function del_asset($id) {
        $user = Auth::user();
        $muti_id = explode(',',$id);
        $all_id = array();
        foreach($muti_id as $value){
            $all_id[]=intval($value);
        }
        if (Auth::check()) {
                $building_id = $user['building_id'];

                DB::table('building_assets')
                        ->whereIn('id', $all_id)
                        ->where('building_id', '=', $building_id)
                        ->update(['status' => 0 ]);
                
                // update stats
                $building_stats = new Building_Stats();
                $building_stats->decrease('total_asset', count($all_id), $user['building_id']);

                return redirect('mgr/assets');
            } 
        }


    public function inventory_list($p=null){
        $user = Auth::user();
        if (Auth::check()) {
            $search = new \stdClass;
            $building_id = $user['building_id'];

            if (isset($_REQUEST['input_search']) && $_REQUEST['input_search'] != '' && $_REQUEST['input_search'] != null) {
                $search->input_search = $_REQUEST['input_search'];
            }


            if (isset($_REQUEST['inv_cate']) && $_REQUEST['inv_cate'] != '' && $_REQUEST['inv_cate'] != null) {
                $search->inv_cate = $_REQUEST['inv_cate'];
            }


            $inv = Building_Inventory::select('building_inventory.id','building_inventory.name','building_category.name as cate_name', 'building_inventory.val','building_inventory.cate_id','building_inventory.qty','building_inventory.make','building_inventory.model','building_inventory.note')
                                ->leftJoin('building_category','building_category.id','=','building_inventory.cate_id')
                                ->where('building_inventory.status', '=', 1)
                                ->where('building_inventory.building_id', '=', $building_id)
                                ->when(isset($_REQUEST['input_search']), function ($query) {
                                    $query->where('building_inventory.name', 'like', '%' . $_REQUEST['input_search'] . '%');
                                })
                                ->when(isset($_REQUEST['inv_cate']) && $_REQUEST['inv_cate'] > 0, function ($query) {
                                    $query->where('building_inventory.cate_id', $_REQUEST['inv_cate']);
                                })
                                ->paginate(1, ['*'], 'page', $p);

            $inv_cate = $this->get_catelist_by_mainID(6);

            if(empty($p)){
                return view('mgr.inventory_list', compact('inv','inv_cate','search'));
            }else{
                return response()->json(['inv' => $inv,'inv_cate' => $inv_cate,'search' => $search]);
            }
        } else {

        }
    }

    public function create_inventory(){
        $inv = [];
        $inv_doc = [];
        $inv_cate = $this->get_catelist_by_mainID(6);
        return view('mgr.create_inventory', compact('inv','inv_doc','inv_cate'));
    }

    public function edit_inv($aid){
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];

            $inv = Building_Inventory::select('id','name', 'val','cate_id','qty','make','model','note','photo')
                            ->where('status', '=', 1)
                            ->where('building_id', '=', $building_id)
                            ->where('id', '=', $aid)
                            ->first();

            $inv_doc = Building_Inventory_Doc::select('id','title', 'path','size','type as extension')
                                                ->where('status', '=', 1)
                                                ->where('inv_id', '=', $aid)
                                                ->get()->toArray();
            
            $inv_cate = $this->get_catelist_by_mainID(6);

            return view('mgr.create_inventory', compact('inv','inv_doc','inv_cate'));
        } else {

        }
    }

    public function save_inv(Request $request){
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];

            if (request('inv_id') > 0) {
                $inv = Building_Inventory::find(request('inv_id'));
            } else {
                $inv = new Building_Inventory;
                $inv->status = 1;
            }

            $inv->name = request('txt_invname');
            $inv->building_id = $building_id;
            $inv->val = request('txt_invval');
            $inv->qty = request('txt_invqty');
            $inv->cate_id = request('ivt_cate');
            $inv->make = request('txt_invmake');
            $inv->model = request('txt_invmodel');
            $inv->note = request('txt_invnote');
            $inv->photo = request('inv_original_photo');

            // add logo
            if (isset($request->inv_photo) && $request->inv_photo != null && $request->inv_photo != '') {
                date_default_timezone_set('Australia/Brisbane');
                foreach ($request->inv_photo as $key => $val) {
                    $fileName = time().'_'.$building_id.'_'.$val->getClientOriginalName();
                    $path = public_path().'/uploads/building_invs/'.date('Y_m_d');
                    File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
                    $val->move(public_path().'/uploads/building_invs/'.date('Y_m_d').'/', $fileName);
                    $inv->photo = '/uploads/building_invs/'.date('Y_m_d').'/'.$fileName;
                }
            }
            $inv->save();

            if (request('inv_id') > 0) {
                $inv_id = request('inv_id');
            } else {
                $inv_id = $inv->id;
            }


            // update current docs
            if (isset($request->inv_doc_id) && $request->inv_doc_id != null && $request->inv_doc_id != '') {
                // $arr = implode(",",$request->asset_doc_id);
                DB::table('building_inventory_doc')
                    ->whereNotIn('id', $request->inv_doc_id)
                    ->where('inv_id', $inv_id)
                    ->update(['status' => 0 ]);
            } else {
                DB::table('building_inventory_doc')
                    ->where('inv_id', $inv_id)
                    ->update(['status' => 0 ]);
            }

            // save new docs
            if (isset($request->inv_docs) && $request->inv_docs != null && $request->inv_docs != '') {
                date_default_timezone_set('Australia/Brisbane');

                foreach ($request->inv_docs as $key => $val) {
                    $inv_docs = new Building_Inventory_Doc;
                    $size = $val->getClientSize();
                    $type = $val->getClientOriginalExtension();

                    $fileName = time().'_'.$building_id.'_'.$val->getClientOriginalName();
                    $path = public_path().'/uploads/building_invs_docs/'.date('Y_m_d');
                    File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
                    $val->move(public_path().'/uploads/building_invs_docs/'.date('Y_m_d').'/', $fileName);

                    $inv_docs->inv_id = $inv_id;
                    $inv_docs->title =  $fileName;
                    $inv_docs->path = '/building_docs/'.date('Y_m_d').'/'.$fileName;
                    $inv_docs->size = $size;
                    $inv_docs->type = $type;
                    $inv_docs->status = 1;
                    $inv_docs->save();
                }
            }



        } else {

        }
        return redirect('mgr/inventory');
    }

    public function del_inv($id) {
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];

            DB::table('building_inventory')
                    ->where('id', $id)
                    ->where('building_id', '=', $building_id)
                    ->update(['status' => 0 ]);

            return redirect('mgr/inventory');
        } else {

        }
    }

    // public function category_list(){
    //     $user = Auth::user();
    //     if (Auth::check()) {
    //         $building_id = $user['building_id'];
    //         $bc = Building_Category::select('id','parent_id','cate_id','name','description')
    //                                 ->where('status', '=', 1)
    //                                 ->where('building_id', '=', $building_id)
    //                                 ->orderBy('cate_id', 'ASC')
    //                                 ->get()->toArray();
            

    //         $cate = $this->cate;
            

    //         $i = 2;
    //         foreach ($bc as $key => $val) {
    //             if ($val['parent_id'] == 0) {
    //                 $cate[$i]['id'] = $val['id'];
    //                 $cate[$i]['parentId'] = $val['cate_id'];
    //                 $cate[$i]['Name'] = $val['name'];
    //                 $cate[$i]['Description'] = $val['description'];
    //             } else {
    //                 $cate[$i]['id'] = $val['id'];
    //                 $cate[$i]['parentId'] = $val['parent_id'];
    //                 $cate[$i]['Name'] = $val['name'];
    //                 $cate[$i]['Description'] = $val['description'];
    //             }
    //             $i++;
    //         }

    //         // print_r($cate);
    //     } else {

    //     }

    //     return view('mgr.category_list', compact('cate'));
    // }

    public function get_catelist_by_mainID($id) {
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];
        
            return Building_Category::select('id','parent_id','cate_id','name','description')
                                    ->where('status', '=', 1)
                                    ->where(function($query) use ($id) {
                                        $query->where('parent_id', $id)
                                        ->orWhere('cate_id', $id);
                                    })
                                    ->where('building_id', '=', $building_id)
                                    ->orderBy('cate_id', 'ASC')
                                    ->get()->toArray();
        } else {

        }
    }


    public function save_cate(Request $request){
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];

            $cate = new Building_Category;
            $cate->name = request('cate_name');
            $cate->building_id = $building_id;
            $cate->cate_id = request('cate_id');
            $cate->parent_id = request('parent_id');
            $cate->description = request('cate_description');
            $cate->status = 1;

            $cate->save();
            // $setting_check = new MgrSettingCheckController;
            // $logo_check = $setting_check::checkSettingMethod($building_id, 'category');

            $record['success'] = true;
            $record['id'] = $cate->id;
        } else {

        }
        // return redirect('mgr/category');
        return response()->json($record);
    }


    public function remove_area(Request $request) {
        $user = Auth::user();

        if (Auth::check()) {
            DB::table('building_inspection_area')
                    ->where('id', $request->area_id)
                    ->update(['status' => 0 ]);

            $record['success'] = true;
            $record['id'] = $request->area_id;
        } 
        return response()->json($record);
    }


    public function remove_area_item(Request $request) {
        $user = Auth::user();

        if (Auth::check()) {
            DB::table('building_inspection_item')
                    ->where('id', $request->area_item_id)
                    ->update(['status' => 0 ]);

            $record['success'] = true;
            $record['id'] = $request->area_item_id;
        } 
        return response()->json($record);
    }

    public function create_inspection() {
        $user = Auth::user();
        if (Auth::check()) {
            $bia = [];
            $building_id = $user['building_id'];
            $ba = new Building_Asset;
            $assets = $ba->getListByBuildingID($building_id);

        } else {

        }
        return view('mgr.inspection_details', compact('bia','assets'));
    }

    public function inspection_list($page = 1, $search_term = null){
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];
        } 
        return view('mgr.inspection_list', compact('page','search_term'));
    }


    public function get_inspection_list($page, $search_term){
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];
            $bi = Building_Inspection_Area::select('building_inspection_area.id','building_inspection_area.area_name',
                                            DB::raw("(CASE 
                                                WHEN  building_inspection_area.frequency = '' THEN 'NONE'
                                                WHEN  building_inspection_area.frequency = '0' THEN 'NONE'
                                                WHEN  building_inspection_area.frequency = '1' THEN 'DAILY'
                                                WHEN  building_inspection_area.frequency = '2' THEN 'WEEKLY'
                                                WHEN  building_inspection_area.frequency = '3' THEN 'MONTHLY'
                                                WHEN  building_inspection_area.frequency = '4' THEN 'YEARLY'
                                            END) AS frequency"),
                                            DB::raw("(select GROUP_CONCAT( bii.title SEPARATOR ' -> ') as actions from building_inspection_item as bii where bii.bi_area_id = building_inspection_area.id AND bii.status = 1)  as actions"),
                                        'building_inspection_area.created_at')
                                        ->where('building_inspection_area.status', '=', 1)
                                        ->where('building_inspection_area.building_id', '=', $building_id)
                                        ->when($search_term != null && $search_term != '' && $search_term != 'null', function ($query) use ($search_term) {
                                            $query->where('building_inspection_area.area_name', 'like', '%' . $search_term . '%');
                                        })
                                        ->paginate(12, ['*'], 'page', $page);
        } else {

        }
        return response()->json($bi);
    }

    public function get_asset_inspection_history($asset_id) {
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];

            $h = Building_Inspection_Item::select('bir.id as bir_id','building_inspection_item.id','building_inspection_item.title', 'bia.start_date','bia.time_from','bia.time_to','bir.result','bir.msg', 'bir.created_at', 'bih.inspection_time', DB::raw("(CASE 
                                                        WHEN  bia.frequency = '' THEN 'NONE'
                                                        WHEN  bia.frequency = '0' THEN 'NONE'
                                                        WHEN  bia.frequency = '1' THEN 'DAILY'
                                                        WHEN  bia.frequency = '2' THEN 'WEEKLY'
                                                        WHEN  bia.frequency = '3' THEN 'MONTHLY'
                                                        WHEN  bia.frequency = '4' THEN 'YEARLY'
                                                    END) AS frequency"))
                                            ->leftJoin('building_inspection_area as bia','bia.id','=','building_inspection_item.bi_area_id')
                                            ->leftJoin('building_inspection_record as bir','bir.bii_id','=','building_inspection_item.id')
                                            ->leftJoin('building_inspection_history as bih','bih.bi_id','=','bia.id')
                                            ->where('building_inspection_item.asset_id', '=', $asset_id)
                                            ->where('building_inspection_item.status', '=', 1)
                                            ->where('bia.status', '=', 1)
                                            ->groupBy('bir.id')
                                            ->orderBy('bir.id', 'desc')
                                            ->get()->toArray();;
        } else {

        }
        return response()->json($h);
    }


    public function skip_initial_setting(Request $request) {
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];

            $setting_check = new Building_Setting_Check;
            $result = $setting_check::skipAllSettings($building_id);

            $record['success'] = $result > 0 ? true : false;
        } else {
            $record['success'] = false;
        }
        $record['building_id'] = $building_id;
        return response()->json($record);
    }

    public function create_inspection_items(Request $request) {
        date_default_timezone_set('Australia/Brisbane');
        $user = Auth::user();
        if (Auth::check()) {
            $new_bir = [];
            $building_id = $user['building_id'];
            $inspection_items = $request->inspection_items;
            $ar_id = $request->ar_id;

            $bih = new Building_Inspection_History;
            $bih->ar_id = $ar_id;
            $bih->bi_id = count($inspection_items) > 0 ? $inspection_items[0]['bi_area_id'] : null;
            $bih->building_id = $building_id;
            $bih->admin_id =  $user['id'];
            $bih->inspection_time = strtotime($request->start) . '-' . strtotime($request->end);
            $bih->status = 1;
            $bih->save();

            if ($bih->id > 0) {
                for ($i = 0; $i < count($inspection_items); $i++) {
                    $bir = new Building_Inspection_Record;
                    $bir->bih_id = $bih->id;
                    $bir->bii_id = $inspection_items[$i]['id'];
                    $bir->result = $inspection_items[$i]['result'];
                    $bir->msg = $inspection_items[$i]['msg'];
                    $bir->status = 1;
                    $bir->save();

                    $new_bir[] = $bir->id;

                    // update log
                    $log = new Building_Log;
                    $log_bi_data = $log->generate_bii_log($bir->id, $user['name'], $user['building_id'], $inspection_items[$i]['id'], $inspection_items[$i]['result'], $bih->id);
                }
            }
        } else {

        }

        $record['success'] = true;
        $record['bi_id'] = $request->ar_id;
        $record['new_bir'] = $new_bir;
        return response()->json($record);
    }

    public function save_inspection(Request $request){
        date_default_timezone_set('Australia/Brisbane');
        $user = Auth::user();
        if (Auth::check()) {

            $building_id = $user['building_id'];
            $inspection_id = $request->inspection_id;
            $inspection_area = $request->txt_areaname;
            $area = $request->area;


            if ($inspection_id > 0) {
                $bia = Building_Inspection_area::find($inspection_id);
            } else {
                $bia = new Building_Inspection_area;
            }

            
            if (count($area) > 0) {
                foreach ($area as $k => $v) {
                    $bia->building_id = $building_id;
                    $bia->area_name = $inspection_area;
                    $bia->status = 1;
                    $bia->save();

                    if ($inspection_id == null || $inspection_id == '') {
                        $inspection_id = $bia->id; 
                    }

                    if (isset($v['item']) && count($v['item']) > 0) {
                        foreach ($v['item'] as $item_k => $item_v) {
                            if ($item_v['id'] == null || $item_v['id'] == '') {
                                $bii = new Building_Inspection_Item;
                            } else {
                                $bii = Building_Inspection_Item::find($item_v['id']);
                            }


                            switch ($item_v['frequency']) {
                                case "none":
                                    $f = 0;
                                    break;
                                case "DAILY":
                                    $f = 1;
                                    break;
                                case "WEEKLY":
                                    $f = 2;
                                    break;
                                case "MONTHLY":
                                    $f = 3;
                                    break;
                                case "YEARLY":
                                    $f = 4;
                                    break;
                                default:
                                    $f = 0;
                            }


                            $bii->bi_area_id = $inspection_id;
                            $bii->title = $item_v['title'];
                            $bii->frequency = $f;
                            $bii->start_date = $item_v['startdate'];
                            // $bii->pass = $item_v['pass'];
                            // $bii->comment = $item_v['comment'];
                            $bii->status = 1;
                            $bii->save();






                            if (isset($bia->id) && isset($bii->id)) {
                                $isNew = true;
                                $date_reservation = date('Y-m-d', strtotime($item_v['startdate']));
                                $start_time = date('H:i:s', strtotime($item_v['starttime']));
                                $end_time = date('H:i:s', strtotime($item_v['endtime']));
            
                                $start_time = $date_reservation.' '.$start_time;
                                $end_time = $date_reservation.' '.$end_time;


                                if ($item_v['rid'] > 0) {
                                    $isNew = false;
                                    $ar = Amenity_Reservation::find($item_v['rid']);
                                } else {
                                    $ar = new Amenity_Reservation;
                                }

                                $ar->blg_admin_id = $user['id'];
                                $ar->case_id = 0;
                                $ar->bi_id = $bia->id;
                                $ar->bii_id = $bii->id;
                                $ar->booking_time_from = $start_time;
                                $ar->booking_time_to = $end_time;
                                $ar->contractor_id = null;
                                $ar->booking_recurring = $item_v['frequency'] == 'none' ? null : 'FREQ='.strtoupper($item_v['frequency']);
                                $ar->details = null;
                                $ar->type = 2;
                                $ar->building_id = $user['building_id'];
                                $ar->booking_time_from = $start_time;
                                $ar->booking_time_to = $end_time;
                                $ar->progress = 1;
                                $ar->status = 1;
                                $ar->save();


                                if ($isNew == true) {
                                    // update stats
                                    $building_stats = new Building_Stats();
                                    $building_stats->increase('total_inspection', 1, $user['building_id']);
                                }

                                $bii = Building_Inspection_Item::find($bii->id);
                                $bii->rid = $ar->id;
                                $bii->save();
                            }
                        }
                    }

                }

                if ($inspection_id == null || $inspection_id == '') {
                    $setting_check = new MgrSettingCheckController;
                    $setting_check::checkSettingMethod($building_id, 'inspection');

                    $building_stats = new Building_Stats();
                    $building_stats->increase('total_amenity', 1, $user['building_id']);
                }

            }
        } else {

        }
        return redirect('mgr/inspection');
    }

    /** 
     * Create or update inspection area
     * inspection items will be updated 
     * Amenity reservation will be updated due to recurring changed in inspection area
     * @param array $request form data from AJAX
     * @return json output
     */
    public function initial_create_inspections(Request $request) {
        $user = Auth::user();
        if (Auth::check()) {
            // $bia = new Building_Inspection_Area;
            $is = new InspectionService();
            $rs = new ReservationsService();

            $inspections = $request->inspections;
            $inspections['building_id'] = $building_id = $user['building_id'];

            // update or create new inspection area
            $bia = $is->saveInspectionArea($inspections);
            if ($bia->id > 0) {
                // Save or update actions
                $is->saveItems($inspections['actions'], $bia->id);
                //Update caldendar
                if ($inspections['isChangeFrequency'] == 1 && isset($inspections['id'])) {
                    // crone current reservation & add booking_end to today
                    $current_scheduler = $rs->getSchedulerByInspectionID($bia->id, $building_id)->toArray();
                    $current_id = $current_scheduler['id'];
                    $rs->setBookingEnd($current_id, now());

                    // create new reservation, get new id
                    $current_scheduler['booking_recurring'] = $rs->convertRecurringByNumber($inspections['frequency']);
                    $next_date = $rs->calculate_next_date($current_scheduler);
                    $current_scheduler['next_date'] = $next_date['next_date'];
                    $current_scheduler['booking_time_from'] = $next_date['next_booking_time_from'];
                    $current_scheduler['booking_time_to'] = $next_date['next_booking_time_to'];
                    $current_scheduler['booking_end'] = null;
                    $current_scheduler['time_id'] = null;
                    $current_scheduler['status'] = 1;
                    $current_scheduler['id'] = null;
                    $new_record = $rs->saveReservation($current_scheduler);
                    
                    // Create sub event & case 
                    $rs->createSubEventAndCase($new_record->toArray(), $user);
                }

                // update stats
                $building_stats = new Building_Stats();
                $building_stats->increase('total_inspection', 1, $user['building_id']);

                $setting_check = new MgrSettingCheckController;
                $phone_check = $setting_check::checkSettingMethod($building_id, 'inspection');
            }
            $record['success'] = true;
            $record['id'] = $bia->id;
        } 
        return response()->json($record);
    }


    public function get_inpection_items($bi_id) {
        $user = Auth::user();
        if (Auth::check()) {
            $result = Building_Inspection_Item::select('id', 'bi_area_id','asset_id', 'title', DB::raw('false as result'), DB::raw('"" as msg'))
                        ->where('status', '=', 1)
                        ->where('bi_area_id', '=', $bi_id)
                        ->get()->toArray();
        } else {

        }
        $record['success'] = true;
        $record['list'] = $result;

        return response()->json($record);
    }


    public function get_inpection_records($bih_id) {
        $user = Auth::user();
        if (Auth::check()) {
            $result = Building_Inspection_Record::select('building_inspection_record.id', 'building_inspection_record.bii_id','building_inspection_record.result', 'building_inspection_record.msg', 'bii.title')
                        ->leftJoin('building_inspection_item as bii','bii.id','=','building_inspection_record.bii_id')
                        ->where('building_inspection_record.status', '=', 1)
                        ->where('bii.status', '=', 1)
                        ->where('building_inspection_record.bih_id', '=', $bih_id)
                        ->get()->toArray();
        } else {

        }
        $record['success'] = true;
        $record['list'] = $result;

        return response()->json($record);
    }

    /**
     * Get Inspection details by building inspection area id
     * @param $bia_id building inspection id
     * @return view
     */
    public function edit_inspection($bia_id){
        $user = Auth::user();
        if (Auth::check()) {
            $b = new BuildingService();
            $building_id = $user['building_id'];
            $assets = $b->getAssetsByBuildingID($building_id);

            $is = new InspectionService();
            $bia['area'] = $is->getArea($bia_id);
            $bia['items'] = $is->getItems($bia_id);
            $bia['history'] = $is->getHistory($bia_id);
        } 
        return view('mgr.inspection_details', compact('bia','assets'));
    }

    public function del_inspection($id) {
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];

            DB::table('building_inspection_area')
            ->where('id', $id)
            ->where('building_id', '=', $building_id)
            ->update(['status' => 0 ]);

            return redirect('mgr/inspection');
        } else {

        }
    }

    public function library_list(){
        $record = array();
        $user = Auth::user();

        if (Auth::check()) {
            $files = [];
            $building_id = $user['building_id'];
            $path = public_path().'/uploads/building_lib/'.$building_id.'/';
            if (File::isDirectory($path) or File::makeDirectory($path, 0777, true, true)) {
                $d = scandir($path);

                foreach($d as $key => $value){
                    if(!is_dir($value)) {
                        $files[] = $value;
                    } 
                }
            }
            //print_r($results);
        } else {

        }

        return view('mgr.library_list', compact('files'));
    }

    public function create_lib_folder(Request $request) {
        $record = array();
        $user = Auth::user();

        if (Auth::check()) {
            $building_id = $user['building_id'];

            $fileName = $request['folder_name'];
            $path = public_path().'/uploads/building_lib/'.$building_id.'/'.$fileName;
            if (File::isDirectory($path) or File::makeDirectory($path, 0777, true, true)) {
                $record['success'] = true;
                $record['folder_name'] = $fileName;
            } else {
                $record['success'] = false;
            }
        } else {

        }
        return response()->json($record);
    }


    public function choose_lib_folder(Request $request) {
        $record = array();
        $user = Auth::user();

        if (Auth::check()) {
            $building_id = $user['building_id'];
            $files = array();
            $fileName = $request['folder_name'];
            $path = public_path().'/uploads/building_lib/'.$building_id.'/'.$fileName;
            $fileList = glob($path.'/*', GLOB_BRACE);

            $i = 0;
            foreach ($fileList as $file)  {
                $exploded = explode(".",$file);
                $extension = strtolower($exploded[count($exploded) - 1]);
                
                if ($extension == 'jpeg') {
                    $extension = 'jpg';
                }

                if ($extension == 'docx') {
                    $extension = 'doc';
                }

                if ($extension == 'xlsx') {
                    $extension = 'xls';
                }

                if ($extension == 'rar') {
                    $extension = 'zip';
                }

                if ($extension != 'docx' && $extension != 'doc' && $extension != 'jpg' && $extension != 'bmp'  && $extension != 'png'  && $extension != 'gif' && $extension != 'mp4' && $extension != 'pdf'  && $extension != 'ppt' && $extension != 'txt'  && $extension != 'wma'  && $extension != 'xlsx'  && $extension != 'xls'  && $extension != 'zip'  && $extension != 'rar' ) {
                    $extension = 'others';
                }

                $files[$i] = new \stdClass;
                $files[$i]->name = basename($file);
                $files[$i]->type = $extension;
                $files[$i]->folder = $fileName;
                $files[$i]->url = '/uploads/building_lib/'.$building_id.'/'.$fileName.'/'.basename($file);
                $i++;
            }

            $record['success'] = true;
            $record['fileList'] = $files;

        } else {

        }
        return response()->json($record);
    }


    public function del_lib_file(Request $request) {
        $user = Auth::user();

        if (Auth::check()) {
            $record = array();
            $building_id = $user['building_id'];
            //$dir = public_path().'/uploads/building_lib/'.$building_id.'/'. $request['folder'];
            $dir = '/uploads/building_lib/'.$building_id.'/'. $request['folder'];
            $dfile =  $dir."/".$request['name'];
            // $dir = $request['folder_name'];

            if (is_dir($dir)) {
                array_map('unlink', glob($dfile));
                
                $record['success'] = true;
                $record['name'] = $dfile;
            }

        } else {

        }
        return response()->json($record);
    }



    public function del_lib_folder(Request $request) {
        $user = Auth::user();

        if (Auth::check()) {
            $record = array();
            $building_id = $user['building_id'];
            $dir = public_path().'/uploads/building_lib/'.$building_id.'/'. $request['folder_name'];
            // $dir = $request['folder_name'];

            if (is_dir($dir)) {
                array_map('unlink', glob("$dir/*.*"));
                rmdir($dir);
                
                $record['success'] = true;
                $record['name'] = $dir;
            }

        } else {

        }
        return response()->json($record);
    }


    public function upload_lib(Request $request) {
        $record = array();
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];
            $folderName = $request['current_dir'];

            $fileName = time().'_'.request('doc_user_id').'_'.$request->lib_file->getClientOriginalName();
            $path = '/uploads/building_lib/'.$building_id.'/'.$folderName;
            File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
            $request->lib_file->move($path.'/', $fileName);
            

            $record['success'] = true;
            $record['name'] = $fileName;
            $record['type'] = $request->lib_file->getClientOriginalExtension();
            $record['folder'] = $folderName;
            $record['url'] = $path.'/'.basename($fileName);
            $record['action'] = 'create_lib';
        } else {

        }
        
        return response()->json($record);
    }



    public function personal_info() {
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];
 
            $user = DB::select("select ba.id, ba.name as username, ba.email, bap.first_name, bap.last_name, bap.nickname, bap.gender, bap.married_status, bap.email_signature, bap.address, bap.postcode, bap.surburb, bap.state, bap.country, bap.job_title, bap.contact_person, bap.cperson_phone, bap.cperson_mobile, bap.cperson_dob, bap.lang
                                        from building_admin ba
                                        left join building_admin_profile bap on ba.id = bap.ba_id
                                        where ba.building_id = ".$building_id."
                                        and ba.id = ".$user['id']."
                                        and bap.status = 1");

            if ($user[0]->cperson_dob != null && $user[0]->cperson_dob != '') {
                $dob = explode("-",$user[0]->cperson_dob);
                $user[0]->cperson_d = $dob[2];
                $user[0]->cperson_m = $dob[1];
                $user[0]->cperson_y = $dob[0];
            } else {
                $user[0]->cperson_d = '';
                $user[0]->cperson_m = '';
                $user[0]->cperson_y = '';
            }
        } else {

        }
        return view('mgr.personal_info', compact('user'));
    }

    public function save_user(Request $request){
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];
            $user_id = $request['user_id'];

            

            Building_Admin_Profile::where('ba_id', $user_id)
                                    ->update(['first_name' => $request['txt_firstname'], 'last_name' => $request['txt_lastname'], 'nickname' => $request['txt_nickname'], 'gender' => $request['txt_usergender'], 'married_status' => $request['txt_usermarrital'], 'address' => $request['txt_personaladd'], 'postcode' => $request['txt_personalpostcode'], 'surburb' => $request['txt_personalsurburb'], 'state' => $request['txt_personalstate'], 'country' => $request['txt_personalcountry'], 'job_title' => $request['txt_jobtitle'], 'contact_person' => $request['txt_cperson'], 'cperson_phone' => $request['txt_cpersonphone'], 'cperson_mobile' => $request['txt_cpersonmobile'], 'email_signature' => $request['txt_mailsignature'], 'cperson_dob' => $request['txt_cperson_year'].'-'.$request['txt_cperson_mon'].'-'.$request['txt_cperson_date']]);
            
                                    $setting_check = new MgrSettingCheckController;
                                    $email_signature = $setting_check::checkSettingMethod($building_id, 'email_signature');
                                    $emergency_contact_person = $setting_check::checkSettingMethod($building_id, 'emergency_contact_person');
                                    $emergency_contact_mobile = $setting_check::checkSettingMethod($building_id, 'emergency_contact_mobile');

        } else {

        }
        return redirect('mgr/personal_info');
    }

    public function report_list() {
        return view('mgr.report_list');
    }

    public function template_list(){
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];

            $list = Building_Template::select('id','title','tag','content as original_content')
                            ->where('status', '=', 1)
                            ->where('building_id', '=', $building_id)
                            ->get()->toArray();

            foreach ($list as $key => $val) {
                $list[$key]['content'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1',html_entity_decode($val['original_content']));
                $list[$key]['substr_content'] = substr(strip_tags($list[$key]['content']), 0, 120);
            }
        } else {
            
        }

        return view('mgr.template_list', compact('list'));
    }



    public function del_template($id) {
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];

            DB::table('building_template')
            ->where('id', $id)
            ->where('building_id', '=', $building_id)
            ->update(['status' => 0 ]);

            return redirect('mgr/template');
        } else {

        }
    }


    public function save_template(Request $request){
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];

            if ($request['txt_tempid'] == null ) {
                $bt = new Building_Template;

                $bt->building_id = $building_id;
                $bt->title = $request['txt_temptitle'];
                $bt->content = $request['txt_tempcontent'];
                $bt->tag = $request['txt_tag'];
                $bt->status = 1;
                $bt->save();
            } else {
                Building_Template::where('id', $request['txt_tempid'])
                                ->update(['title' => $request['txt_temptitle'], 'content' => $request['txt_tempcontent'], 'tag'=>$request['txt_tag']]);
            }
        } else {

        }

        return redirect('mgr/template');
    }

}
