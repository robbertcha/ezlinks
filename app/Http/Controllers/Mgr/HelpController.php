<?php

namespace App\Http\Controllers\Mgr;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Help;
use App\Help_details;
use Auth;

class HelpController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:building_admin');
    }
    
    public function index(){

        return view('mgr.help');
    }

    public function get_info_top($id){
        if($id == null|| $id == 'null'|| $id == ''){
            $help = new Help();
            $detail = new Help_details();
            $info_top = $help::all();
            $info_detail = $detail::all();
            return [$info_top,$info_detail];
        }else{
            return view('mgr.help');
        }
    }

    public function get_detail_info(){

        $detail = new Help_details();

        $info_detail = $detail::all();

        return $info_detail;
    }

    public function get_script($id){
       if(empty($id)){
           
       }else{
           $help_details = new Help_details();
           $info = $help_details::find($id);
           return $info;

       }
    }

    public function get_info($t,$d){
        return view('mgr.help',compact('t','d'));
    }
}
