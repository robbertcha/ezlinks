<?php

namespace App\Http\Controllers\Mgr;
use Auth;
use App\Unit;
use App\Building_Stats;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Mgr\MgrTenantController;

class MgrDeleteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:building_admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function scheduler_list(Request $request) {
        $user = Auth::user();
        $record=array();
        if (Auth::check()) {
            DB::table('amenity_reservation')
                ->whereIn('id',request('id'))
                ->update(['status' => 0 ]);

                $record['success'] = true;
                $record['id'] = request("id");
                return response()->json($record);
        }
    }


    public function annoucement_list(Request $request) {
        $user = Auth::user();
        $record=array();
        if (Auth::check()) {
            DB::table('building_announcement')
            ->whereIn('id',request('id'))
            ->update(['status' => 0 ]);

            $record['success'] = true;
            $record['id'] = request("id");
            return response()->json($record);
        }
    }


    public function setting_assets(Request $request){
        $user = Auth::user();
        $record=array();
        if(Auth::check()){
            $building_id = $user['building_id'];
            DB::table('building_assets')
                ->whereIn('id',request('id'))
                ->update(['status'=>0]);
            
            $building_stats = new Building_Stats();
            $building_stats->decrease('total_asset', count(request('id')), $user['building_id']);
            $record['success'] = true;
            $record['id'] = request("id");

            return response()->json($record);
        }
    }

    public function setting_inventory(Request $request){
        $user = Auth::user();
        $record=array();
        if(Auth::check()){
            $building_id = $user['building_id'];
            DB::table('building_inventory')
                ->whereIn('id',request('id'))
                ->update(['status'=>0]);
                
            $record['success'] = true;
            $record['id'] = request("id");

            return response()->json($record);
        }
    }

    public function setting_amenity(Request $request){
        $user = Auth::user();
        $record=array();
        if(Auth::check()){
            $building_id = $user['building_id'];
            DB::table('building_amenity')
                ->whereIn('id',request('id'))
                ->update(['status'=>0]);
                
            $record['success'] = true;
            $record['id'] = request("id");

            return response()->json($record);
        }
    }

    public function setting_inspection(Request $request){
        $user = Auth::user();
        $record=array();
        if(Auth::check()){
            $building_id = $user['building_id'];
            DB::table('building_inspection_area')
                ->whereIn('id',request('id'))
                ->update(['status'=>0]);
                
            $record['success'] = true;
            $record['id'] = request("id");

            return response()->json($record);
        }
    }

    public function setting_parking(Request $request){
        $user = Auth::user();
        $record=array();
        if(Auth::check()){
            $building_id = $user['building_id'];
            DB::table('building_parking_breaches')
                ->whereIn('id',request('id'))
                ->where('building_id', '=', $building_id)
                ->update(['status'=>0]);
                
            $record['success'] = true;
            $record['id'] = request("id");

            return response()->json($record);
        }
    }

    public function unit(Request $request){
        $user = Auth::user();
        $record = array();
        if(Auth::check()){
            // $unit = Unit::findMany(request('id'));
            // $unit->status = 0;
            // $unit->save();

            foreach (request('id') as $uid) {
                $cc = new MgrTenantController;
                $tids = $cc->delTenantsByUnitID($uid);

                $unit = DB::table('unit')
                        ->where('id','=',$uid)
                        ->update(['status'=>0]);
                   
                if ($unit > 0) {
                    $building_stats = new Building_Stats();
                    $building_stats->decrease('total_unit', 1, $user['building_id']);
                }
               
            }

             $record['success'] = true;
             $record['id'] = request("id");
        }else{

        }
        return response()->json($record);
    }

    public function vehice(Request $request){
        $user = Auth::user();
        $record = array();
        if(Auth::check()){
            
            DB::table('building_carpark')
                    ->whereIn('id',request('id'))
                    ->update(['vehicle_id' => null, 'tenant_id'=> null,'status'=>0 ]);

             $record['success'] = true;
             $record['id'] = request("id");
        }else{

        }
        return response()->json($record);
    }

    public function tenant(Request $request){
        $user = Auth::user();
        $record = array();
        if(Auth::check()){
            DB::table('tenant_detail')
                ->whereIn('id',request('id'))
                ->update(array('building_id' => 0, 'unit_id' => 0, 'carpark_id' => 0, 'link_status' => 0, 'type' => 4));
            
            $building_stats = new Building_Stats();
            $building_stats->decrease('total_unit', count(request('id')), $user['building_id']);
             $record['success'] = true;
             $record['id'] = request("id");
        }else{

        }
        return response()->json($record);
    }
}   
