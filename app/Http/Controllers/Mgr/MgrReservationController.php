<?php

namespace App\Http\Controllers\Mgr;

use Auth;
use DateTime;
use DateTimeZone;
use App\Building_Case;
use App\Building_Stats;
use App\Building_Asset;
use App\Building_Amenity;
use App\Amenity_Reservation;
use App\Contractor;
use App\Building_Inspection_Area;
use App\Unit;
use App\User;
use App\Building_Log;
use App\Building_Case_Quote;
use App\Building_Case_Invoice;
use App\Building_Inspection_Record;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Mgr\ComplaintController;
use App\Http\Controllers\Mgr\MgrNotificationController;
use App\Http\Controllers\Mgr\MgrSettingCheckController ;

// Service
use App\Services\Mgr\CommonService;
use App\Services\Mgr\Cases\CaseService;
use App\Services\Mgr\Building\BuildingService;
use App\Services\Mgr\Reservations\ReservationsService;

// enum
use App\Enums\SchedulerType;


class MgrReservationController extends Controller
{
    private $rs;
    public function __construct()
    {
        $this->middleware('auth:building_admin');
        date_default_timezone_set('Australia/Brisbane');

        $this->rs = new ReservationsService();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];

            $ba = new Building_Amenity();
            $amenity = $ba->getAllAmenitiesByBID($building_id);

            $main_month = date("Y-m-1");
            $first_day = date("Y-m-d", strtotime("- 7days", strtotime($main_month)));
            $last_day =  date("Y-m-d", strtotime("+1 month 7days", strtotime($main_month)));
            // $last_day = date("Y-m-t") ;

            $reservation = $this->rs->get_reservation_list($first_day, $last_day, '', $building_id);

            $type = $this->rs->getSchedulerType();

            $contractor = new Contractor();
            $contractor_list = $contractor->get_contractor_by_building($building_id);

            $bi = new Building_Inspection_Area();
            $bi_list = $bi->get_building_inspections_by_building($building_id);

            $u = new Unit();
            $units = $u->get_all_units($building_id);

            $user = new User();
            $tenants = $user->getTenantsByBuildingID($building_id);  

            return view('mgr.reservation_list', compact('reservation', 'amenity','tenants','type','contractor_list','bi_list', 'units'));
        } else {

        }
    }


    /**
     * Deprecated
     * Create reservation booking on case
     * parent_id > 0 && case_id > 0 && time_id = null
     * @param @request
     * @return json return via ajax
     */
    public function create_reservation_by_caseid(Request $request) {
        $user = Auth::user();
        if (Auth::check()) {
            // $date_reservation = date('Y-m-d', strtotime(request('date_reservation')));
            // $start_time = $date_reservation.' '.request('start_time');
            // $end_time = $date_reservation.' '.request('end_time');
            // $start_time =  date('Y-m-d H:i:s', strtotime($start_time ));
            // $end_time =  date('Y-m-d H:i:s', strtotime($end_time ));
            // $log_start_time =  date('Y-m-d H:i A', strtotime($start_time ));
            // $log_end_time =  date('Y-m-d H:i A', strtotime($end_time ));

            // $ar = new Amenity_Reservation;
            // $ar->blg_admin_id =  $user['id'];
            // $ar->case_id = request('case_id');
            // $ar->booking_time_from = $start_time;
            // $ar->booking_time_to = $end_time;
            // $ar->contractor_id = request('constractor_id');
            // $ar->booking_recurring = 0;
            // $ar->details = request('note');
            // $ar->type = 2;
            // $ar->building_id = $user['building_id'];
            // $ar->status = 1;

            // $ar->save();
            // $record['id'] = $ar->id;
            // $record['action'] = 'create_reservation';
            // $record['start_date'] = date('d M', strtotime(request('date_reservation')));
            // $record['start_time'] = date('H:i', strtotime($start_time));
            // $record['end_time'] = date('H:i', strtotime($end_time));
            // $record['created_at'] =  date('Y-m-d H:i:s', time());
            // $record['success'] = true;

            // if ($ar->contractor_id == null || $ar->contractor_id == '') {
            //     $record['log_content'] = $content = $user['name'].' ['.$user['id'].'] create a scheduler from '.$log_start_time.' to '.$log_end_time;
            // } else {
            //     $record['log_content'] = $content = $user['name'].' ['.$user['id'].'] create a scheduler with '.request('constractor').' from '.$log_start_time.' to '.$log_end_time;
            // }
            // $cc = new ComplaintController;
            // $cc->update_case_log(request('case_id'), '', $user['id'], $content);
        } else {

        }
        // return response()->json($record);
    }


    /**
     * Get data from building manager and do the save or update action
     * @param array $request form data
     * @return json return via ajax
     */
    public function update_reservation(Request $request) {
        $user = Auth::user();
        if (Auth::check()) {
            $record = [];
            $starttime = date('Y-m-d H:i:s', strtotime(request('start')));
            $endtime = date('Y-m-d H:i:s', strtotime(request('end')));
            
            // Save event
            $ar_id = $this->rs->saveEvent($request, $user, $starttime, $endtime);

            if (request('id') > 0) {
                $record['id'] = request('id');
                $record['action'] = 'update_reservation';
            } else {
                $record['action'] = 'create_reservation';
 
                // update stats
                $building_stats = new Building_Stats();
                if ( $request['typeId'] == 1) {
                    $building_stats->increase('total_task', 1, $user['building_id']);
                } elseif ( $request['typeId'] == 3) {
                    $building_stats->increase('total_reservation', 1, $user['building_id']);
                } elseif ( $request['typeId'] == 4) {
                    $building_stats->increase('total_inspection', 1, $user['building_id']);
                }
                $record['id'] = $ar_id;
            }
            
            // send email to tenant
            // $mgrnotifications = new MgrNotificationController;
            // $recipients = [request('newTenant')];
            // $mgrnotifications->adminSendToTenant($recipients, $action);

            // update log, deprecated
            // $log = new Building_Log;
            // $log_data = $log->generate_reservation_log($record['id'], $user['name'], $user['building_id'], $record['action'], request('typeId'), request('location'), request('bi_list'), request('amenityId'), request('car_plate'), request('progress'), $starttime, $endtime, request('recurrenceRule'));
            

            // add inspection actions
            if (request('bi_actions')) {
                $bi_actions = request('bi_actions');
                $bi_action_list_arr = [];
                for ($i = 0; $i < count($bi_actions); $i++) {
                    $bi_action_arr = [];
                    if ($bi_actions[$i]['bir_id'] == null || $bi_actions[$i]['bir_id'] == '') {
                        $bir = new Building_Inspection_Record;
                    } else {
                        $bir = Building_Inspection_Record::find($bi_actions[$i]['bir_id']);
                    }

                    $bir->building_id = $user['building_id'];
                    $bir->ar_id = request('id') > 0 ? request('id') : $ar->id;
                    $bir->bi_id = request('bi_list');
                    $bir->bii_id = $bi_actions[$i]['id'];
                    $bir->ba_id = $user['id'];
                    $bir->result = $bi_actions[$i]['result'];
                    $bir->msg = $bi_actions[$i]['msg'];
                    $bir->status = 1;
                    $bir->save();

                    $bi_action_arr[] = $bir->id;
                    $bi_action_arr[] = $bir->bii_id;
                    $bi_action_arr[] = $bir->result;
                    $bi_action_arr[] = $bir->msg;
                    $bi_action_arr = implode(",",$bi_action_arr);
                    $bi_action_list_arr[] = $bi_action_arr;
                }
                
                if (count($bi_action_list_arr) > 0) {
                    $record['inspection_actions']  = implode("|", $bi_action_list_arr);
                }
            }

            // Get current event
            $record['event'] = $this->rs->get_reservation_list(null, null, '', $user['building_id'], '', 1, 'asc', 1, $record['id']);
        } 
        $record['success'] = true;
        return response()->json($record);
    }

    /**
     * Remove event from calendar, and reduce stats
       * @param array $request - id: event id or parent event id
       * @param array $request - type: same as typeID, 1 - Job, 3 - Reservation, 4 - Parking
       * @param array $request - action: 1 - not job type, 2 - job type, delete occurence, 3 - job type, delete series
     */
    public function remove_event(Request $request) {
        $user = Auth::user();
        if (Auth::check()) {
            $request['building_id'] = $user['building_id'];
            $record = $this->rs->rmEvent($request);
            
            // update log, deprecated
            // $log = new Building_Log;
            // $log_del = $log->generate_del_log(request('id'), $user['name'], $user['building_id'], 'Reservation');

            $record['success'] =  $record['result'] == 1 ? true : false;
            $record['id'] = request('id');
            $record['action'] = 'remove_reservation';
        }
        return response()->json($record);
    }

    /** 
     * @param $request case_id
     * Get all related info for 'JOB' (job, reservation, car parking)
     */
    public function get_job_initial_data(Request $request) {
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];

            if ($request['id'] == null) {
                $record['case'] = [];
                $record['case']['id'] = null;
                $record['case']['tenant_id'] = '';
                $record['case']['unit_id'] = '';
                $record['case']['asset_id'] = '';
                $record['case']['job_area'] = 2;
            }

            $u = new Unit();
            $ba = new Building_Asset();
            $bc = new Building_Case();
            $contractor = new Contractor();
            $bi = new Building_Inspection_Area();
           
            $record['units'] = $u->get_all_units($building_id);
            $record['assets'] = $ba->getListByBuildingID($building_id);
            $record['case_type'] = $bc->get_case_type(); 
            $record['case_area'] = $bc->get_case_area();
            $record['case_progress'] = $bc->get_case_progress();
            $record['contractor_list'] = $contractor->get_contractor_by_building($building_id);
            $record['bi_list'] = $bi->get_building_inspections_by_building($building_id);
            $record['routine'] = (new CaseService())->getRoutineType();

            $record['success'] = true;
            return response()->json($record);
        }
    }

    /**
     * Get amenity list via ajax
     * @return json 
     */
    public function getAmenitiesByBuildingID() {
        $user = Auth::user();
        $record['success'] = false;
        if (Auth::check()) {
            $building_id = $user['building_id'];
            $bs = new BuildingService;
            $record['list'] = $bs->getAmenitiesByBuildingID($building_id);
            $record['success'] = true;
        } 
        return response()->json($record);
    }

    /**
     * Get contractor list via ajax
     * @return json 
     */
    public function getContractorsByBuildingID() {
        $user = Auth::user();
        $record['success'] = false;
        if (Auth::check()) {
            $building_id = $user['building_id'];
            $contractor = new Contractor();
            $record['list'] = $contractor->get_contractor_by_building($building_id);
            $record['success'] = true;
        } 
        return response()->json($record);
    }

    /**
     * Get inspections list via ajax
     * @return json 
     */
    public function getInspectionsByBuildingID() {
        $user = Auth::user();
        $record['success'] = false;
        if (Auth::check()) {
            $building_id = $user['building_id'];
            $bi = new Building_Inspection_Area();
            $record['list'] = $bi->get_building_inspections_by_building($building_id);
            $record['success'] = true;
        } 
        return response()->json($record);
    }

    /**
     * CRON - Update reservation routine
     * Run this once everyday to auto generate recurring sub-events and cases
     * @return boolean
     */
    public function dailyCheckRoutine() {
        $case = (new ReservationsService())->dailyCheckRoutine();
        return $case;
    }

    /**
     * Fire when changing view type and date in calendar 
     * @return @array reservation
     */
    public function get_reservations_by_month(Request $request) {
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];
            if (request('view') == 'month') {
                // $first_day = date("Y-m-1", strtotime(request('month')) ) ;
                // $last_day = date("Y-m-t", strtotime(request('month')) ) ;

                $main_month = date("Y-m-1", strtotime(request('month')));
                $first_day = date("Y-m-d", strtotime("- 7days", strtotime($main_month)));
                $last_day =  date("Y-m-d", strtotime("+1 month 7days", strtotime($main_month)));
            } elseif (request('view') == 'day') {
                $first_day = date("Y-m-d", strtotime(request('month')) );
                $last_day =date("Y-m-d", strtotime(request('month') .' +1 day'));
            } elseif (request('view') == 'week') {
                $unixTimestamp = strtotime(date("Y-m-d", strtotime(request('month')) ));
                $dayOfWeek = date("l", $unixTimestamp);
                $days = array('Sunday', 'Monday', 'Tuesday', 'Wednesday','Thursday','Friday', 'Saturday');
                $daycount = array_search($dayOfWeek, $days);
                $first_day_count = ($daycount*1)+1;
                $last_day_count = 7-$first_day_count;

                $first_day = date("Y-m-1", strtotime(request('month') .' -'.$first_day_count.' day'));
                $last_day =date("Y-m-d", strtotime(request('month') .' +'.$last_day_count.' day'));
            }

            $reservation = $this->rs->get_reservation_list($first_day, $last_day, '', $building_id);
        } else {

        }
        return response()->json($reservation);
    }











    public function get_all_list() {
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];

            $ba = new Building_Amenity();
            $amenity = $ba->getAllAmenitiesByBID($building_id);

            $type = $this->rs->getSchedulerType();

            $contractor = new Contractor();
            $contractor_list = $contractor->get_contractor_by_building($building_id);

            $bi = new Building_Inspection_Area();
            $bi_list = $bi->get_building_inspections_by_building($building_id);

            $u = new Unit();
            $units = $u->get_all_units($building_id);

            $user = new User();
            $tenants = $user->getTenantsByBuildingID($building_id);  


            $record['success'] = true;
            $record['amenity'] = $amenity;
            $record['type'] = $type;
            $record['contractor_list'] = $contractor_list;
            $record['bi_list'] = $bi_list;
            $record['units'] = $units;
            $tenants['units'] = $tenants;

            return response()->json($record);

        }
    }

    public function scheduler_list($page = 1, $page_qty = 12, $type = 'a', $order = 'd', $search_date = null, $search_term = null) {
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];

            $ba = new Building_Amenity();
            $amenities = $ba->getAllAmenitiesByBID($building_id);
            $stype = SchedulerType::getKeys();

            $contractor = new Contractor();
            $contractor_list = $contractor->get_contractor_by_building($building_id);

            $bi = new Building_Inspection_Area();
            $bi_list = $bi->get_building_inspections_by_building($building_id);

            $user = new User();
            $tenants = $user->getTenantsByBuildingID($building_id);
            
            $ba = new Building_Amenity();
            $amenity = $ba->getAllAmenitiesByBID($building_id);

            $u = new Unit();
            $units = $u->get_all_units($building_id);

            return view('mgr.scheduler_list', compact('page','page_qty','type','order','search_term','search_date', 'amenities', 'stype', 'contractor_list', 'bi_list','tenants', 'amenity','units'));
        } else {

        }
    }

    public function get_scheduler_list($page, $page_qty, $type, $order, $search_term, $search_date = '', $hideClosedBookings = false) {
        $user = Auth::user();
        if (Auth::check()) {
            if ($type == 'c') {
                $type = '4';
            } else if (is_numeric($type)) {
                $type = 'ba-'.$type;
            }

            if ($order == 'a') {
                $order = 'asc';
            } else {
                $order = 'desc';
            }
            // Change false from string to boolean - Hide closed booking records
            $hideClosedBookings = filter_var( $hideClosedBookings, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE );
            $reservation = $this->rs->get_reservation_list($search_date, $search_date, $type, $user['building_id'], $search_term, $page, $order, $page_qty, null, $hideClosedBookings);
            return response()->json($reservation);
        } else {

        }
    }

    public function amenity_list($p=null){
        $user = Auth::user();
        if (Auth::check()) {
            $search = new \stdClass;
            $building_id = $user['building_id'];
            if (isset($_REQUEST['input_search']) && $_REQUEST['input_search'] != '' && $_REQUEST['input_search'] != null) {
                $search->input_search = $_REQUEST['input_search'];
            }

            $amenity = Building_Amenity::select('name', 'id','booking_before','allocation_limit','hours_limit','rules','start_time', 'end_time')
                                                        ->where('status', '=', 1)
                                                        ->where('building_id', '=', $building_id)
                                                        ->when(isset($_REQUEST['input_search']) && $_REQUEST['input_search'] != '' && $_REQUEST['input_search'] != null, function ($query)  {
                                                            $query->where('name', 'like', '%' . $_REQUEST['input_search'] . '%');
                                                        })
                                                        ->groupBy('id')
                                                        ->orderBy('created_at', 'desc')
                                                        ->paginate(12, ['*'], 'page', $p);;

            if(!isset($p)){
                return view('mgr.amenity_list', compact('amenity'));
            } else {
                return response()->json(['amenity' => $amenity]);
            }
        } else {

        }
    }

    public function amenity_detail ($id) {
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];

            $amenity = Building_Amenity::select('name', 'id','booking_before','allocation_limit','hours_limit','rules','disable_date','start_time','end_time')
                                        ->where('status', '=', 1)
                                        ->where('building_id', '=', $building_id)
                                        ->where('id', '=', $id)
                                        ->get()->first();

            return view('mgr.amenity_detail', compact('amenity'));
        } else {

        }
    }


    public function new_amenity() {
        $case = new \stdClass;;
        if (Auth::check()) {
            $amenity =  new \stdClass;;
            $amenity->booking_before = 3;
            $amenity->hours_limit = 3;
            $amenity->allocation_limit = 7;
            $amenity->disable_date = null;
            $amenity->start_time = "9:00 AM";
            $amenity->end_time = "5:00 PM";
            return view('mgr.amenity_detail', compact('amenity'));
        } else {

        }
    }

    public function initial_create_amenities(Request $request) {
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];
            $amenities = request('amenities');

            for($i = 0; $i < count($amenities); $i++) {
                $amenity = new Building_Amenity;

                $amenity->building_id = $building_id;
                $amenity->name = $amenities[$i]['name'];
                $amenity->booking_before = $amenities[$i]['before'];
                $amenity->hours_limit = $amenities[$i]['duration'];
                $amenity->rules = isset($amenities[$i]['rules']) ? $amenities[$i]['rules'] : null;
                $amenity->status = 1;

                $amenity->save();
            }

            $setting_check = new MgrSettingCheckController;
            $phone_check = $setting_check::checkSettingMethod($building_id, 'amenity');

            // update stats
            $building_stats = new Building_Stats();
            $building_stats->increase('total_amenity', 1, $user['building_id']);

            $record['success'] = true;
            $record['action'] = 'create_amenities';
            $record['building_id'] = $building_id;
        } else {
            $record['success'] = false;
        }
        return response()->json($record);
    }

    public function create_amenity(Request $request) {
        date_default_timezone_set('Australia/Brisbane');
        $user = Auth::user();
        if (Auth::check()) {
            $new_amenity = request('amenity');
            $new_amenity['start_time'] = date('h:i A', strtotime($new_amenity['start_time'] ));
            $new_amenity['end_time'] = date('h:i A', strtotime($new_amenity['end_time'] ));

            if (isset($new_amenity['id'])) {
                $amenity = Building_Amenity::find($new_amenity['id']);
                $record['action'] = 'edit_amenity';
            } else {
                $amenity = new Building_Amenity;
                $amenity->status = 1;
                $record['action'] = 'create_amenity';
            }

            $amenity->building_id = $user['building_id'];
            $amenity->name = $new_amenity['name'];
            $amenity->booking_before = $new_amenity['booking_before'];
            $amenity->allocation_limit = $new_amenity['allocation_limit'];
            $amenity->hours_limit = $new_amenity['hours_limit'];
            if (isset($new_amenity['disable_date'])) {
                $amenity->disable_date = $new_amenity['disable_date'] != null && $new_amenity['disable_date'] != '' ? implode(",",$new_amenity['disable_date']) : null;
            } else {
                $amenity->disable_date = null;
            }
            $amenity->start_time = $new_amenity['start_time'];
            $amenity->end_time = $new_amenity['end_time'];
            //$amenity->able_recurring = 0;
            $amenity->rules = $new_amenity['rules'];
            $amenity->save();
            $building_id = $user['building_id'];
            


            if (isset($new_amenity['id'])) {
                $record['id'] = $new_amenity['id'];
            } else {
                $record['id'] = $amenity->id;
            }
            
            $record['success'] = true;
            $setting_check = new MgrSettingCheckController;
            $amenity_check = $setting_check::checkSettingMethod($building_id, 'amenity');

            // update stats
            $building_stats = new Building_Stats();
            $building_stats->increase('total_amenity', 1, $user['building_id']);
        } else {

        }
        return response()->json($record);
    }

    public function remove_amenity($id) {
        $user = Auth::user();
        if (Auth::check()) {
            $amenity = Building_Amenity::find($id);
            $amenity->status = 0;
            $amenity->save();

            return redirect('mgr/amenity_list');
        } else {

        }
        
    }





    
    

    




    public function update_reservation_by_inspection(Request $request) {
        date_default_timezone_set('Australia/Brisbane');
        $user = Auth::user();
        if (Auth::check()) {
            $starttime = date('Y-m-d H:i:s', strtotime(request('start')));
            $endtime = date('Y-m-d H:i:s', strtotime(request('end')));

            if (request('id') > 0) {
                $ar = Amenity_Reservation::where('id', '=', request('id'))->update(
                    array(
                        'blg_admin_id' => $user['id'], 
                        'progress' => request('progress') == null || request('progress') == '' ? 0 : request('progress'), 
                        'comment' => request('comment'), 
                        'booking_time_from' => $starttime, 
                        'booking_time_to' => $endtime, 
                        'details' => request('details'), 
                        'tenant_id' => request('newTenant'), 
                        'amenity_id' => request('amenityId'), 
                        'bi_id' => request('bi_list'), 
                        'bii_id' => request('bii_id'),
                        'location' => request('location'),
                        'task_type' => request('tasktype'),
                        'car_plate' => request('car_plate'),
                        'booking_recurring' => request('recurrenceRule'),
                        'contractor_id' => request('contractor_list'), 
                        'type' => request('typeId'), 
                        'building_id' =>  $user['building_id']
                    )
                );
                $record['success'] = true;
                $record['id'] = request('id');
                $record['action'] = 'update_reservation';

                
                $mgrnotifications = new MgrNotificationController;
                $recipients = [request('newTenant')];
                $action = 'update_scheduler';
                $mgrnotifications->adminSendToTenant($recipients, $action);

            } else {
                $ar = new Amenity_Reservation;
                $ar->building_id = $user['building_id'];
                $ar->amenity_id =  request('amenityId');
                $ar->tenant_id =  request('newTenant');
                $ar->blg_admin_id =  $user['id'];
                $ar->bii_id = request('bii_id');
                $ar->location = request('location');
                $ar->task_type = request('tasktype');
                $ar->car_plate = request('car_plate');
                $ar->booking_recurring = request('recurrenceRule');

                $ar->booking_time_from = $starttime;
                $ar->booking_time_to = $endtime;
                $ar->details = request('details');
                $ar->progress = request('progress');
                $ar->comment = request('comment');
                $ar->type = request('typeId');
                $ar->contractor_id = request('contractor_list');
                $ar->bi_id = request('bi_list');
                $ar->status = 1;
                $ar->save();

                // update stats
                $building_stats = new Building_Stats();
                if ($ar->type == 1) {
                    $building_stats->increase('total_task', 1, $user['building_id']);
                } elseif ($ar->type == 2) {
                    $building_stats->increase('total_maintenance', 1, $user['building_id']);
                } elseif ($ar->type == 3) {
                    $building_stats->increase('total_reservation', 1, $user['building_id']);
                } elseif ($ar->type == 4) {
                    $building_stats->increase('total_inspection', 1, $user['building_id']);
                }

            

                $record['id'] = $ar->id;
                $record['action'] = 'create_reservation';
                $record['event_desc'] = substr($ar->details, 0 , 40);
                $record['success'] = true;

                $mgrnotifications = new MgrNotificationController;
                $recipients = [request('newTenant')];
                $action = 'update_scheduler';
                $mgrnotifications->adminSendToTenant($recipients, $action);
            }
        } else {

        }  
        return response()->json($record);
    }



    public function update_current_occurence(Request $request) {
        date_default_timezone_set('Australia/Brisbane');
        $user = Auth::user();
        if (Auth::check()) {
            $starttime = date('Y-m-d H:i:s', strtotime(request('start')));
            $endtime = date('Y-m-d H:i:s', strtotime(request('end')));


                $ar = new Amenity_Reservation;
                $ar->building_id = $user['building_id'];
                $ar->amenity_id =  request('amenityId');
                $ar->tenant_id =  request('newTenant');
                $ar->blg_admin_id =  $user['id'];
                $ar->bii_id = request('bii_id');
                $ar->location = request('location');
                $ar->task_type = request('tasktype');
                $ar->car_plate = request('car_plate');
                $ar->booking_recurring = request('recurrenceRule');

                $ar->booking_time_from = $starttime;
                $ar->booking_time_to = $endtime;
                $ar->details = request('details');
                $ar->progress = request('progress');
                $ar->comment = request('comment');
                $ar->type = request('typeId');
                $ar->contractor_id = request('contractor_list');
                $ar->bi_id = request('bi_list');
                $ar->status = 1;
                $ar->save();

                // update stats
                $building_stats = new Building_Stats();
                if ($ar->type == 1) {
                    $building_stats->increase('total_task', 1, $user['building_id']);
                } elseif ($ar->type == 2) {
                    $building_stats->increase('total_maintenance', 1, $user['building_id']);
                } elseif ($ar->type == 3) {
                    $building_stats->increase('total_reservation', 1, $user['building_id']);
                } elseif ($ar->type == 4) {
                    $building_stats->increase('total_inspection', 1, $user['building_id']);
                }

                $record['id'] = $ar->id;
                $record['action'] = 'update_current_occurence';
                $record['event_desc'] = substr($ar->details, 0 , 40);
                $record['success'] = true;

                // $mgrnotifications = new MgrNotificationController;
                // $recipients = [request('newTenant')];
                // $action = 'update_scheduler';
                // $mgrnotifications->adminSendToTenant($recipients, $action);
            
        } else {

        }  
        return response()->json($record);
    }

    public function remove_current_occurence(Request $request) {
        $user = Auth::user();
        if (Auth::check()) {
            $ar = $this->add_booking_exception(request('id'), $request['start_time']);

            $record['success'] = true;
            $record['id'] = request('id');
            $record['action'] = 'remove_current_occurence';
        }

        return response()->json($record);
    }


    public function add_booking_exception($id, $time) {
        $booking_exception = Amenity_Reservation::get_bookingexception_by_id($id);
        $booking_exception = $booking_exception[0];
        $exception_arr = explode(",",$booking_exception);
        $exception_arr[] = $time;
        $exception = implode(",", $exception_arr);

        return Amenity_Reservation::where('id', '=', $id)->update(array('booking_exception' => $exception));
    }

    public function task_list() {
        date_default_timezone_set('Australia/Brisbane');

        $input_search = '';
        $search = new \stdClass;
        $user = Auth::user();

        if (Auth::check()) {
            if (isset($_REQUEST['input_search']) && $_REQUEST['input_search'] != '' && $_REQUEST['input_search'] != null) {
                $search->input_search = $input_search = $_REQUEST['input_search'];
            }

            $reservation = $this->rs->get_reservation_list('', '', '1', $user['building_id'], $input_search);
        } else {

        }
        return view('mgr.task_list', compact('reservation','search'));
    }

    public function create_task() {
        return view('mgr.task_details');
    }

    public function maintenance_list() {
        date_default_timezone_set('Australia/Brisbane');

        $input_search = '';
        $search = new \stdClass;
        $user = Auth::user();

        if (Auth::check()) {
            if (isset($_REQUEST['input_search']) && $_REQUEST['input_search'] != '' && $_REQUEST['input_search'] != null) {
                $search->input_search = $input_search = $_REQUEST['input_search'];
            }

            $reservation = $this->rs->get_reservation_list('', '', '2', $user['building_id'], $input_search);
        } else {

        }
        return view('mgr.maintenance_list', compact('reservation','search'));
    }


    public function booking_list() {
        date_default_timezone_set('Australia/Brisbane');

        $input_search = '';
        $search = new \stdClass;
        $user = Auth::user();

        if (Auth::check()) {
            if (isset($_REQUEST['input_search']) && $_REQUEST['input_search'] != '' && $_REQUEST['input_search'] != null) {
                $search->input_search = $input_search = $_REQUEST['input_search'];
            }

            $reservation = $this->rs->get_reservation_list('', '', '3', $user['building_id'], $input_search);
        } else {

        }
        return view('mgr.booking_list', compact('reservation','search'));
    }

    public function inspection_booking_list() {
        date_default_timezone_set('Australia/Brisbane');

        $input_search = '';
        $search = new \stdClass;
        $user = Auth::user();

        if (Auth::check()) {
            if (isset($_REQUEST['input_search']) && $_REQUEST['input_search'] != '' && $_REQUEST['input_search'] != null) {
                $search->input_search = $input_search = $_REQUEST['input_search'];
            }

            $reservation = $this->rs->get_reservation_list('', '', '4', $user['building_id'], $input_search);
        } else {

        }
        return view('mgr.inspection_booking_list', compact('reservation','search'));
    }

    public function create_maintenance() {
        return view('mgr.maintenance_details');
    }




    public function reminder_list() {
        return view('mgr.reminder_list');
    }

}
