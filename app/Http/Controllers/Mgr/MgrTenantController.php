<?php

namespace App\Http\Controllers\Mgr;

use Auth;
use File;
use App\Tenant_Pet;
use App\Tenant_Vehicle;
use App\Tenant_Detail;
use App\Tenant_Note;
use App\Building_Case;
use App\Building_Stats;
use App\Building_Parking_Breaches;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Tag;

class MgrTenantController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:building_admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($page = 1, $ttype = 4, $torder = 'a', $torderby = 'first_name', $search_term = null)
    {
        $user = Auth::user();
        if (Auth::check()) {
            $search = new \stdClass;
            $building_id = $user['building_id'];
            $search_tag = null;

            $t = new Tag();
            $tag = $t::get_unit_tag($building_id);

            // if (isset($_REQUEST['input_search']) && $_REQUEST['input_search'] != '' && $_REQUEST['input_search'] != null) {
            //     $search->input_search = $_REQUEST['input_search'];
            // }


            // $tenant = DB::table('tenant_detail')
            //                     ->select('tenant_detail.id', 'tenant_detail.user_id', 'tenant_detail.building_id', 'tenant_detail.unit_id','tenant_detail.carpark_id','tenant_detail.tenant_code','tenant_detail.type','tenant_detail.first_name','tenant_detail.last_name','tenant_detail.preferred_lang')
            //                     ->leftJoin('users','users.id','=','tenant_detail.user_id')
            //                     ->where('tenant_detail.building_id', '=', $building_id)
            //                     ->when(isset($_REQUEST['input_search']), function ($query)  {
            //                         $query->where(function ($q) {
            //                             $q->where('tenant_detail.first_name', 'like', '%' . $_REQUEST['input_search'] . '%');
            //                             $q->orWhere('tenant_detail.last_name', 'like', '%' . $_REQUEST['input_search'] . '%');
            //                             $q->orWhere('tenant_detail.tenant_code', 'like', '%' . $_REQUEST['input_search'] . '%');
            //                         });
            //                     })
            //                     ->orderBy('tenant_detail.created_at', 'desc')
            //                     ->paginate(10);

            // return view('mgr.tenant_list', compact('tenant','search'));
            return view('mgr.unit_list', compact('page','ttype','torder','torderby','search_term','search_tag', 'tag'));
        } else {

        }
    }

    public function get_tenant_list($page, $ttype, $torder, $orderby, $search_term) {
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];

            if ($torder == 'a') {
                $torder = 'asc';
            } else {
                $torder = 'desc';
            }

            if ($orderby == 'first_name') {
                $orderby = 'tenant_detail.first_name';
            } elseif ($orderby == 'tenant_code') {
                $orderby = 'tenant_detail.tenant_code';
            }elseif ($orderby == 'unit_title') {
                $orderby = 'unit.unit_title';
            }

            $tenant = DB::table('tenant_detail')
                                ->select('tenant_detail.id', 'tenant_detail.user_id', 'tenant_detail.building_id', 'tenant_detail.unit_id','tenant_detail.carpark_id','tenant_detail.tenant_code','tenant_detail.type','tenant_detail.first_name','tenant_detail.last_name','tenant_detail.preferred_lang','unit.unit_title', 'users.email', 'building_tenant.move_in', 'building_tenant.move_out', DB::raw('MAX(CAST(building_tenant.id AS CHAR)) as latest_movein_id'),
                                DB::raw("
                                    (CASE 
                                        WHEN tenant_detail.link_status = '0' THEN ''
                                        WHEN tenant_detail.link_status = '1' THEN 'Verified'
                                        WHEN tenant_detail.link_status = '2' THEN 'Unverified'
                                    END) AS link_status
                                "))
                                ->leftJoin('users','users.id','=','tenant_detail.user_id')
                                ->leftJoin('unit','unit.id','=','tenant_detail.unit_id')
                                ->leftJoin('building_tenant','tenant_detail.user_id','=','building_tenant.tenant_id')
                                ->where('tenant_detail.building_id', '=', $building_id)
                                ->where('tenant_detail.type', '=', $ttype)
                                ->where('users.status', '=', 1)
                                ->when($search_term != null && $search_term != '' && $search_term != 'null', function ($query) use ($search_term) {
                                    $query->where(function ($q) use ($search_term) {
                                        $q->where('tenant_detail.first_name', 'like', '%' . $search_term . '%');
                                        $q->orWhere('tenant_detail.last_name', 'like', '%' . $search_term . '%');
                                        $q->orWhere('tenant_detail.tenant_code', 'like', '%' . $search_term . '%');
                                    });
                                })
                                ->orderBy($orderby, $torder)
                                ->groupBy('tenant_detail.id')
                                ->paginate(12, ['*'], 'page', $page);  
             
            return response()->json($tenant);
        } else {

        }
    }


    public function get_carpark_list($page) {
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];

            $cp = DB::table('building_carpark')
                                ->select('building_carpark.*','tenant_detail.first_name','tenant_detail.last_name','unit.unit_title','tenant_vehicle.make','tenant_vehicle.model','tenant_vehicle.plate_no')
                                        ->leftJoin('tenant_detail','tenant_detail.user_id','=','building_carpark.tenant_id')
                                        ->leftJoin('unit','unit.id','=','building_carpark.unit_id')
                                        ->leftJoin('tenant_vehicle','tenant_vehicle.id','=','building_carpark.vehicle_id')
                                        ->where('building_carpark.building_id', '=', $building_id)
                                        ->where('building_carpark.status', '=', 1)
                                        ->paginate(12, ['*'], 'page', $page);

            return response()->json($cp);
        } else {

        }
    }

    public function create_parking_breaches() {
        $user = Auth::user();
        if (Auth::check()) {
            $pb_id = null;
            $pb = [];
            $building_id = $user['building_id'];
            

            return view('mgr.create_parking_breaches', compact('pb_id', 'pb'));
        } else {

        }
    }

    public function del_parking_breach($id) {
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];

            DB::table('building_parking_breaches')
                    ->where('id', $id)
                    ->where('building_id', '=', $building_id)
                    ->update(['status' => 0 ]);

            return redirect('mgr/parking_breaches');
        } else {

        }
    }

    public function edit_parking_breaches($pb_id) {
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];
            $pb = Building_Parking_Breaches::select('id','rego_no', 'vehicle_make','vehicle_model','vehicle_color','breach','photo', DB::raw("(DATE_FORMAT(date_breach,'%M %d %Y')) as date_breach"))
                                    ->where('status', 1)
                                    ->where('building_id', $building_id)
                                    ->where('id', $pb_id)->first();
                                    
            return view('mgr.create_parking_breaches', compact('pb_id', 'pb'));
        } else {

        }
    }

    public function save_parking_breaches(Request $request) {
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];
            
            $pb = new Building_Parking_Breaches;
            $pb->building_id = $building_id;
            $pb->rego_no = request('txt_vehiclerego');
            $pb->vehicle_make = request('txt_vehiclemake');
            $pb->vehicle_model = request('txt_vehiclemodel');
            $pb->vehicle_color = request('txt_vehiclecolor');
            $pb->date_breach = request('txt_datebreach');
            $pb->breach = request('txt_breach');
            $pb->status = 1;

            // add photo
            if (isset($request->pb_photo) && $request->pb_photo != null && $request->pb_photo != '') {
                date_default_timezone_set('Australia/Brisbane');
                $fileName = time().'_'.$building_id.'_'.$request->pb_photo->getClientOriginalName();
                $path = public_path().'/uploads/building_pb/'.date('Y_m_d');
                File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
                $request->pb_photo->move(public_path().'/uploads/building_pb/'.date('Y_m_d').'/', $fileName);
                $pb->photo = '/uploads/building_pb/'.date('Y_m_d').'/'.$fileName;
            }
            $pb->save();

            return redirect('mgr/parking_breaches');
        } else {

        }
    }

    

    public function parking_breaches() {
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];
            $parking_breaches = Building_Parking_Breaches::select('id','rego_no', 'vehicle_make','vehicle_model','vehicle_color','breach','photo', DB::raw("(DATE_FORMAT(date_breach,'%M %d %Y')) as date_breach"))
                                    ->where('status', 1)
                                    ->where('building_id', $building_id)
                                    ->orderBy('created_at', 'desc')
                                    ->paginate(10);

            return view('mgr.parking_breaches_list', compact('parking_breaches'));
        } else {

        }
    }

    public function createTenantNote(Request $request) {
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];
            
            $note = new Tenant_Note;
            $note->building_id = $building_id;
            $note->tenant_id = $this->getUserIDByTID(request('tenant_id'));
            $note->content = request('note');
            $note->status = 1;
            $note->save();

            $record['success'] = true;
            $record['data']['id'] = $note->id;
            $record['data']['created_at'] = date("M d Y"); 
            $record['data']['content'] = request('note');

            return response()->json($record);
        } else {

        }
    }

    public function getUserIDByTID($tid) {
        $record = DB::table('tenant_detail')
                    ->select('tenant_detail.user_id')
                    ->where('tenant_detail.id', '=', $tid)
                    ->get()->first();
        return $record->user_id;
    }

    public function getCPDetailsByID($cp_id) {
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];
            $id = explode(',',$cp_id);
            
            $record['vehicles'] = [];
            $record['info'] =  DB::table('building_carpark')
                                ->select('building_carpark.*','unit.unit_title','tenant_vehicle.make','tenant_vehicle.model','tenant_vehicle.color','tenant_vehicle.plate_no')
                                ->leftJoin('unit','unit.id','=','building_carpark.unit_id')
                                ->leftJoin('tenant_vehicle','tenant_vehicle.id','=','building_carpark.vehicle_id')
                                ->whereIn('building_carpark.id', $id)
                                ->where('building_carpark.status', '=', 1)
                                ->get()->first();

            if ($record['info']->unit_id > 0) {
                $record['vehicles'] = $this->get_vehicles_by_unitID( $record['info']->unit_id);      
            }             
            return response()->json($record);
        } else {

        }
    }

    public function getTenantDetailsByID($tid) {
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];

            $record['info'] = DB::table('tenant_detail')
                                ->select('tenant_detail.id', 'tenant_detail.user_id', 'tenant_detail.building_id', 'tenant_detail.tenant_code','tenant_detail.unit_id','tenant_detail.carpark_id','tenant_detail.tenant_code','tenant_detail.type','tenant_detail.first_name','tenant_detail.last_name','tenant_detail.preferred_lang', 'unit.unit_title', 'users.email', DB::raw("DATE_FORMAT(building_tenant.move_in , '%M %d %Y') as move_in"),'tenant_detail.contact','tenant_detail.mobile',
                                DB::raw("CONCAT(tenant_detail.emergency_no,' - ',tenant_detail.emergency_person) as emerg_person"),
                                DB::raw("
                                            (CASE 
                                                WHEN tenant_detail.type = '3' THEN 'Owner'
                                                WHEN tenant_detail.type = '4' THEN 'Tenant'
                                                WHEN tenant_detail.type = '5' THEN 'Invester'
                                            END) AS type
                                        ")

                                )
                                ->leftJoin('users','users.id','=','tenant_detail.user_id')
                                ->leftJoin('unit','unit.id','=','tenant_detail.unit_id')
                                ->leftJoin('building_tenant','building_tenant.tenant_id','=','users.id')
                                ->where('tenant_detail.id', '=', $tid)
                                ->get()->first();
            
            $record['vehicles'] = Tenant_Vehicle::where('status', 1)->where('tenant_id', $record['info']->user_id)->get()->toArray();
            $record['pets'] = Tenant_Pet::where('status', 1)->where('tenant_id', $record['info']->user_id)->get()->toArray();

            $building_case = new Building_Case();
            $record['cases'] = $building_case->get_case_detail_by_tid($record['info']->user_id);
            // $record['notes'] =  DB::table('building_tenant_note')->select('tenant_id','content',DB::raw("DATE_FORMAT(created_at , '%M %d %Y') as created_at"))->where('status', 1)->where('tenant_id', $record['info']->user_id)->get()->toArray();

            return response()->json($record);
        } else {

        }
    }

    public function getTenantListByUnitID($unit_id) {
        return DB::table('tenant_detail')
                                    ->select("tenant_detail.id","tenant_detail.user_id","tenant_detail.first_name","tenant_detail.last_name", "u.email","tenant_detail.type as type_id", "tv.vehicles",
                                    DB::raw("
                                        (CASE 
                                            WHEN tenant_detail.type = '3' THEN 'Owner'
                                            WHEN tenant_detail.type = '4' THEN 'Tenant'
                                            WHEN tenant_detail.type = '5' THEN 'Invester'
                                        END) AS type
                                    "),
                                    DB::raw("
                                        (CASE 
                                            WHEN tenant_detail.link_status = '0' THEN ''
                                            WHEN tenant_detail.link_status = '1' THEN 'Verified'
                                            WHEN tenant_detail.link_status = '2' THEN 'Unverified'
                                        END) AS link_status
                                    "),
                                    DB::raw("
                                        (CASE 
                                            WHEN tenant_detail.link_status = '0' THEN ''
                                            WHEN tenant_detail.link_status = '1' THEN tenant_detail.mobile
                                            WHEN tenant_detail.link_status = '2' THEN ''
                                        END) AS mobile
                                    "),
                                    DB::raw("
                                        (CASE 
                                            WHEN tenant_detail.link_status = '0' THEN ''
                                            WHEN tenant_detail.link_status = '1' THEN tenant_detail.contact
                                            WHEN tenant_detail.link_status = '2' THEN ''
                                        END) AS contact
                                    "),
                                    DB::raw("
                                        (CASE 
                                            WHEN tenant_detail.link_status = '0' THEN ''
                                            WHEN tenant_detail.link_status = '1' THEN tenant_detail.emergency_person
                                            WHEN tenant_detail.link_status = '2' THEN ''
                                        END) AS emergency_person
                                    "),
                                    DB::raw("
                                        (CASE 
                                            WHEN tenant_detail.link_status = '0' THEN ''
                                            WHEN tenant_detail.link_status = '1' THEN tenant_detail.emergency_no
                                            WHEN tenant_detail.link_status = '2' THEN ''
                                        END) AS emergency_no
                                    "))
                                    ->join('users as u',function($join){
                                        $join->on('tenant_detail.user_id','=','u.id');
                                    }, null,null,'left')
                                    ->leftJoin(DB::raw("(select tenant_id, GROUP_CONCAT( CONCAT(id,'-',make,'-',model,'-',plate_no) SEPARATOR '|') as vehicles from `tenant_vehicle` where status = 1 group by id) as tv"),function($join){
                                        $join->on("tv.tenant_id","=","tenant_detail.user_id");
                                    })
                                    ->where('tenant_detail.unit_id', '=', $unit_id)
                                    ->where('u.status', '=', 1)
                                    ->where('tenant_detail.link_status', '=', 1)
                                    ->get()->toArray();

}

    public function getTenantListByBuildingID($building_id) {
            return DB::table('tenant_detail')
                                        ->select("tenant_detail.id","tenant_detail.user_id","tenant_detail.first_name","tenant_detail.last_name", "u.email","tenant_detail.type as type_id", "tv.vehicles",
                                        DB::raw("
                                            (CASE 
                                                WHEN tenant_detail.type = '3' THEN 'Owner'
                                                WHEN tenant_detail.type = '4' THEN 'Tenant'
                                                WHEN tenant_detail.type = '5' THEN 'Invester'
                                            END) AS type
                                        "),
                                        DB::raw("
                                            (CASE 
                                                WHEN tenant_detail.link_status = '0' THEN ''
                                                WHEN tenant_detail.link_status = '1' THEN 'Verified'
                                                WHEN tenant_detail.link_status = '2' THEN 'Unverified'
                                            END) AS link_status
                                        "),
                                        DB::raw("
                                            (CASE 
                                                WHEN tenant_detail.link_status = '0' THEN ''
                                                WHEN tenant_detail.link_status = '1' THEN tenant_detail.mobile
                                                WHEN tenant_detail.link_status = '2' THEN ''
                                            END) AS mobile
                                        "),
                                        DB::raw("
                                            (CASE 
                                                WHEN tenant_detail.link_status = '0' THEN ''
                                                WHEN tenant_detail.link_status = '1' THEN tenant_detail.contact
                                                WHEN tenant_detail.link_status = '2' THEN ''
                                            END) AS contact
                                        "),
                                        DB::raw("
                                            (CASE 
                                                WHEN tenant_detail.link_status = '0' THEN ''
                                                WHEN tenant_detail.link_status = '1' THEN tenant_detail.emergency_person
                                                WHEN tenant_detail.link_status = '2' THEN ''
                                            END) AS emergency_person
                                        "),
                                        DB::raw("
                                            (CASE 
                                                WHEN tenant_detail.link_status = '0' THEN ''
                                                WHEN tenant_detail.link_status = '1' THEN tenant_detail.emergency_no
                                                WHEN tenant_detail.link_status = '2' THEN ''
                                            END) AS emergency_no
                                        "))
                                        ->join('users as u',function($join){
                                            $join->on('tenant_detail.user_id','=','u.id');
                                        }, null,null,'left')
                                        ->leftJoin(DB::raw("(select tenant_id, GROUP_CONCAT( CONCAT(id,'-',make,'-',model,'-',plate_no) SEPARATOR '|') as vehicles from `tenant_vehicle` where status = 1 group by id) as tv"),function($join){
                                            $join->on("tv.tenant_id","=","tenant_detail.user_id");
                                        })
                                        ->where('tenant_detail.building_id', '=', $building_id)
                                        ->where('u.status', '=', 1)
                                        ->get()->toArray();

    }


    public function getTenantsByUnitID($uid) {
        return DB::table('tenant_detail')
                ->join('users as u',function($join){
                    $join->on('tenant_detail.user_id','=','u.id');
                }, null,null,'left')
                ->where('tenant_detail.unit_id', '=', $uid)
                ->where('u.status', '=', 1)
                ->pluck('user_id')->toArray();
    }

    public function getTenantUnitID($id) {
        return DB::table('tenant_detail')
                ->where('tenant_detail.unit_id', '=', $id)
                ->where('tenant_detail.link_status', '=', 1)
                ->select('first_name','last_name','user_id')
                ->get();
    }


    public function delTenantsByUnitID($uid) {
        $user = Auth::user();
        if (Auth::check()) {
            $tid = DB::table('tenant_detail')
                        ->join('users as u',function($join){
                            $join->on('tenant_detail.user_id','=','u.id');
                        }, null,null,'left')
                        ->where('tenant_detail.unit_id', '=', $uid)
                        ->where('u.status', '=', 1)
                        ->update(array('building_id' => 0, 'unit_id' => 0, 'carpark_id' => 0, 'link_status' => 0));

            // update stats
            $building_stats = new Building_Stats();
            $building_stats->decrease('total_tenant', $tid, $user['building_id']);
            return $tid;
        } else {

        }
    }

    public function getTenantsLog($id){
        $n_id = explode(',',$id);
        $user = Auth::user();
        if(Auth::check()){
            $info = DB::table('building_tenant')
                    ->whereIn('tenant_id',$n_id)
                    ->select('move_in','move_out')
                    ->get();
        }
        return $info;
    }

    public function getTenantsLogName($id){
        // $n_id = explode(',',$uid);
        $user = Auth::user();
        if(Auth::check()){
            $info = DB::table('building_tenant')
                    ->join('tenant_detail','tenant_detail.unit_id','=','building_tenant.unit_id')
                    ->where('building_tenant.unit_id',$id)
                    ->select('tenant_detail.first_name','tenant_detail.last_name','building_tenant.move_in','building_tenant.move_out')
                    ->get();
        }
        return $info;
    }

    public function getUnitCarParks($id){
        $n_id = explode(',',$id);
        $user = Auth::user();
        if(Auth::check()){
            $info = DB::table('building_carpark')
                    ->whereIn('id',$n_id)
                    ->select('carpark_title')
                    ->get();
        }
        return $info;
    }

    public function getOwner($id){
        $user = Auth::user();
        if(Auth::check()){
            $info = DB::table('tenant_detail')
                    ->where('unit_id',$id)
                    ->where('type',3)
                    ->select('first_name','last_name','gender','dob','contact','mobile')
                    ->get();
        }
        return $info;
    }


    public function get_vehicles_by_unitID($unit_id) {
        $user =Auth::user();
        if(Auth::check()){
            $info = DB::table('tenant_vehicle')
                        ->select('tenant_vehicle.id', 'tenant_vehicle.make', 'tenant_vehicle.model')
                        ->leftJoin('tenant_detail','tenant_detail.user_id','=','tenant_vehicle.tenant_id')
                        ->where('tenant_detail.unit_id', $unit_id)
                        ->where('tenant_vehicle.status', 1)
                        ->get();
            
            return $info;
        }
    }

    public function getCarparkByUnitId($id){
        $user =Auth::user();
        if(Auth::check()){
            $info = DB::table('building_carpark')
                        ->where('unit_id',$id)
                        ->select('id','carpark_title')
                        ->get();
            
            return $info;
        }
        
    }
}
