<?php

namespace App\Http\Controllers\Mgr;

use Auth;
use App\Courier;
use Illuminate\Http\Request;
use App\Tenant_Detail;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Building_FAQ;

class InstructionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:building_admin');
    }

    public function index($slug){

        $instruction = new Building_FAQ();
        $ins = $instruction::getFAQListsBySlug($slug);
        return $ins;
    }

}