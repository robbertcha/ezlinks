<?php

namespace App\Http\Controllers\Mgr;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Tag;

class MgrTagController extends Controller {
   

   public function __construct() {
       $this->middleware('auth:building_admin');
   }

   public static function get_unit_tag() {

        $user = Auth::user();
        if(Auth::check()) {

            $building_id = $user['building_id'];
            $model = new Tag();
            $model = $model::get_unit_tag($building_id);
            return $model;
        }
   }

   public static function get_announcement_tag() {

        $user = Auth::user();

        if(Auth::check()) {

            $building_id = $user['building_id'];
            $model = new Tag();
            $model = $model::get_announcement_tag($building_id);
            return $model;
        }
    }   
}

