<?php

namespace App\Http\Controllers\Mgr;

use Auth;
use App\Unit;
use App\Announcement;
use App\Announcement_Type;
use App\Building_Announcement_Participant;
use App\Tag;
use App\Building_Template;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Mgr\MgrNotificationController;
use \Carbon\Carbon;

// Service
use App\Services\Mgr\CommonService;
use App\Services\Mgr\Building\TenantService;

class MgrAnnouncementController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:building_admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($p=null)
    {  
        $user = Auth::user();
        if (Auth::check()) {
            // $announcement type
            $search = new \stdClass;
            $building_id = $user['building_id'];

            if (isset($_REQUEST['input_search']) && $_REQUEST['input_search'] != '' && $_REQUEST['input_search'] != null) {
                $search->input_search = $_REQUEST['input_search'];
            }

            if (isset($_REQUEST['input_type']) && $_REQUEST['input_type'] != '' && $_REQUEST['input_type'] != null) {
                $search->input_type = $_REQUEST['input_type'];
            }
        

            // $announcement_type = Announcement_Type::where('status', '=', 1)
            //                                         ->where(function ($query) use ($building_id) {
            //                                             $query->where('building_id', '=', $building_id)->orWhere('building_id', '=', "0");
            //                                         })->get()->toArray();
            

            $announcement = DB::table('building_announcement')
                                ->select('building_announcement.id', 'building_announcement.admin_id', 'building_announcement.building_id', 'building_announcement.type', 'building_announcement.tag',  'building_announcement.title as title',  'building_announcement.content as content', 'building_announcement.created_at', 'building_announcement.total_participants' , 'building_announcement.recipients', 'building_announcement.show_event_btn',DB::raw("
                                (CASE 
                                    WHEN building_announcement.type = '1' THEN 'Announcement'
                                    WHEN building_announcement.type = '2' THEN 'Event'
                                    WHEN building_announcement.type = '3' THEN 'Maintenance/Repair'
                                    WHEN building_announcement.type = '4' THEN 'Advertisement'
                                    WHEN building_announcement.type = '5' THEN 'Newsletter'
                                    WHEN building_announcement.type = '6' THEN 'Other'
                                END) AS type_title
                                "))
                                ->where('building_announcement.status', '=', 1)
                                ->where('building_announcement.building_id', '=', $user['building_id'])
                                ->when(isset($_REQUEST['input_search']), function ($query)  {
                                    $query->where(function ($q) {
                                        $q->where('building_announcement.content', 'like', '%' . $_REQUEST['input_search'] . '%');
                                        $q->orWhereRaw('(SELECT GROUP_CONCAT(tag.title SEPARATOR ",") as tags from tag where tag.status = 1  and FIND_IN_SET(tag.id, building_announcement.tag)) like "%'. $_REQUEST['input_search'].'%"');
                                    });
                                })
                                ->when(isset($_REQUEST['input_type']) && ($_REQUEST['input_type'] != 7), function ($query)  {
                                    $query->where('building_announcement.type', '=', $_REQUEST['input_type']);
                                   
                                })
                                ->when(isset($_REQUEST['input_type']) && ($_REQUEST['input_type'] == 7), function ($query)  {
                                    $query->where('building_announcement.type', '=', 1)
                                        ->orWhere('building_announcement.type', '=', 2)
                                        ->orWhere('building_announcement.type', '=', 3)
                                        ->orWhere('building_announcement.type', '=', 4)
                                        ->orWhere('building_announcement.type', '=', 5)
                                        ->orWhere('building_announcement.type', '=', 6);
                                   
                                })
                                ->groupBy('building_announcement.id')
                                ->orderBy('created_at', 'desc')
                                ->paginate(12, ['*'], 'page', $p);

            // get recipients details
            $recipient_arr= [];
            foreach ($announcement as $key => $val) {
                $arr = explode(',', $val->recipients);
                $recipient_arr = array_merge($recipient_arr, $arr);
                $val->recipients = $arr;

                $val->content = preg_replace('/(<[^>]+) style=".*?"/i', '$1',html_entity_decode($val->content));
                $val->content = substr(strip_tags($val->content), 0, 120);

                $val->tags = DB::table('tag')
                                    ->select("tag.id", "tag.title")
                                    ->where('status', '=', 1)
                                    ->whereIn('tag.id', explode(",", $val->tag))
                                    ->where('building_id', '=', $user['building_id'])
                                    ->get()->keyBy('id');
            }
            
            $recipients =   DB::table('users')
                            ->select('users.id', 'users.name', 'users.email', 'unit.unit_title', 'tenant_detail.thumb', 'tenant_detail.first_name' , 'tenant_detail.last_name')
                            ->leftJoin('tenant_detail','tenant_detail.user_id','=','users.id')
                            ->leftJoin('unit','tenant_detail.unit_id','=','unit.id')
                            ->where(function($query) {
                                $query->where('users.type', '4')
                                ->orWhere('users.type', '3')
                                ->orWhere('users.type', '5');
                            })
                            ->whereIn('users.id', $recipient_arr)
                            ->where('tenant_detail.building_id', '=', $user['building_id'])
                            ->get()->keyBy('id');

            foreach ($announcement as $key => $val) {
                $new_arr = [];
                foreach ($val->recipients as $k => $v) {
                    if (isset($recipients[$v]) && count($new_arr) < 4) {
                        $new_arr[$v] = $recipients[$v];
                    }
                }
                $val->recipients = $new_arr;
            }
            // get recipients details

            $reci = [];
            $all_reci = Unit::orderBy('unit_title', 'asc')->with('tenant')->get()->groupBy('unit_title');
            foreach($all_reci as $key => $val) {
                foreach($val as $item => $item_val) {
                    foreach($item_val['tenant'] as $tenant_key) {
                        $arr = [];
                        $arr['building_id'] = $tenant_key->building_id;
                        $arr['unit_id'] = $tenant_key->unit_id;
                        $arr['user_id'] = $tenant_key->user_id;
                        $arr['first_name'] = $tenant_key->first_name;
                        $arr['last_name'] = $tenant_key->last_name;
                        $arr['gender'] = $tenant_key->gender;
                        $arr['type'] = $tenant_key->type;
                        $reci[$tenant_key->type][] = $arr;
                    }
                }
            }
            // print_r($reci);
            if(empty($p)){
                return view('mgr.announcement_list', compact('announcement', 'all_reci', 'reci','search'));
            }else{
                return response()->json(['announcement' => $announcement,'all_reci' => $all_reci,'reci' => $reci,'search' => $search]);
            }
        } else {

        }
    }


    public function create_announcement(){
        $user = Auth::user();
        if (Auth::check()) {
            $aid = null;
            $building_id = $user['building_id'];
            $CommonService = new CommonService();
            $TenantService = new TenantService();

            $announcement_tag = $CommonService->getTagByType('announcement', $building_id);
            $recipientTags = $CommonService->getUnitTags($building_id);
            $recipients = $TenantService->getTenantsByBuildingID($building_id);

            // $recipients = $this->getRecipientsByTagIDs($building_id);
            $template_list = $this->get_template_list($building_id);

            return view('mgr.create_announcement', compact('recipients', 'recipientTags', 'aid','announcement_tag','template_list'));
        } 
    }

    public function edit_announcement($aid){
        $user = Auth::user();
        //$record = array();
        if (Auth::check()) {
            $building_id = $user['building_id'];
            $CommonService = new CommonService();
            $TenantService = new TenantService();

            $announcement_tag = $CommonService->getTagByType('announcement', $building_id);
            $recipientTags = $CommonService->getUnitTags($building_id);
            $recipients = $TenantService->getTenantsByBuildingID($building_id);

            //$recipients = $this->getRecipientsByTagIDs($building_id);
            $template_list = $this->get_template_list($building_id);

            return view('mgr.create_announcement', compact('recipients', 'recipientTags', 'aid','announcement_tag','template_list'));
        } 
    }

    public function get_template_list($building_id) {
        $list = Building_Template::select('id','title', 'content as original_content' )
                            ->where('status', '=', 1)
                            ->where('building_id', '=', $building_id)
                            ->get()->toArray();
        
        foreach ($list as $key => $val) {
            $list[$key]['content'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1',html_entity_decode($val['original_content']));
            $list[$key]['substr_content'] = substr(strip_tags($list[$key]['content']), 0, 120);
        }
        return $list;
    }

    public function create_announcement_type() {
        $user = Auth::user();
        if (Auth::check()) {
            $title = request('title');
            $announcement_type = new Announcement_Type;
            $announcement_type->title = $title;
            $announcement_type->building_id = $user['building_id'];
            $announcement_type->status = 1;

            $announcement_type->save();
            $record['success'] = true;
            $record['new_type']['id'] = $announcement_type->id;
            $record['new_type']['building_id'] = $user['building_id'];
            $record['new_type']['title'] = request('title');
            $record['new_type']['status'] = 1;
        } else {

        }
        return response()->json($record);
    }





    public function getRecipientsByTagIDs($building_id) {
        return DB::select("SELECT tag.id, tag.title, ifnull(GROUP_CONCAT( 
                                CONCAT(	u.id, '-', 
                                    u.unit_title, '-', 
                                    (
                                        SELECT ifnull(
                                                GROUP_CONCAT( 
                                                    CONCAT(tenant_detail.user_id, ':', tenant_detail.first_name, ' ', tenant_detail.last_name, ':', tenant_detail.type) 
                                                SEPARATOR '|'), 
                                            '') as tenants 
                                        FROM tenant_detail 
                                        where u.id = tenant_detail.unit_id  
                                        AND tenant_detail.link_status = 1
                                    ) 
                                ) SEPARATOR ','
                            ), '') as unit_tenant
                    from unit as u
                    left join tag on ((FIND_IN_SET(tag.id, u.tag_ids) and tag.type = 'unit') OR u.tag_ids = null OR u.tag_ids = '') 
                    where u.building_id = ".$building_id."
                    group by tag.id
                    order by tag.id desc");
    }

    public function del_announcement($aid) {
        $user = Auth::user();
        if (Auth::check()) {
            DB::table('building_announcement')
            ->where('id', $aid)
            ->update(['status' => 0 ]);

            return redirect('mgr/announcement');
        } else {

        }
    }

    public function get_announcement_participants($aid) {
        $user = Auth::user();
        $record = array();
        if (Auth::check()) {
            $record['data'] = DB::table('building_announcement as ba')
                                ->select('bap.created_at', 'td.first_name', 'td.last_name', 'u.unit_title', 'bap.message')
                                ->leftJoin('building_announcement_participant as bap','bap.ba_id','=','ba.id')
                                ->leftJoin('tenant_detail as td','td.user_id','=','bap.user_id')
                                ->leftJoin('unit as u','u.id','=','td.unit_id')
                                ->where('ba.status', '=', 1)
                                ->where('bap.status', '=', 1)
                                ->where('ba.id', '=', $aid)
                                ->get()->toArray();
          
            $record['success'] = true;
            return response()->json($record);
        } else {

        }
    }

    // ajax return
    public function get_announcement($aid) {
        $user = Auth::user();
        $record = array();
        if (Auth::check()) {
            $record = Announcement::find($aid);
            $record['content'] =  html_entity_decode($record['content'] );
            // $announcement_type = Announcement_Type::find($record['typeID']);
            // $record['type'] = $record['title'];
            $record['tag'] = DB::table('tag')
                                ->select('tag.title', 'tag.id','tag.description')
                                ->where('status', '=', 1)
                                ->whereIn('tag.id', explode(",", $record['tag']))
                                ->where('building_id', '=', $user['building_id'])
                                ->get()->toArray();
            $record['start_time'] = $record['start_time'];
            $record['end_time'] = $record['end_time'];
            $record['recipients'] = explode(",", $record['recipients']);

            $recipients = DB::table('tenant_detail')
                                ->select('tenant_detail.first_name', 'tenant_detail.last_name', 'tenant_detail.type', 'tenant_detail.user_id','tenant_detail.unit_id')
                                ->whereIn('tenant_detail.user_id', $record['recipients'])
                                ->get();

            $record['recipients'] = $recipients;
            return $record;
        } else {

        }
    }

    public function create_tag(Request $request) {
        $user = Auth::user();
        $record = array();
        if (Auth::check()) {
            // $tag = Tag::firstOrNew(['building_id'=>1, 'status'=>1, 'title'=>request('title')]);
            
            if (Tag::where('building_id', '=', $user['building_id'])->where('status', '=', 1)->where('title', '=', request('title'))->count() > 0) {
                $record['success'] = 2;
                $tag_id = Tag::where('building_id', '=', $user['building_id'])->where('status', '=', 1)->where('title', '=', request('title'))->first();
                $record['new_tag']['id'] = $tag_id['id'];
            } else {
                $tag = new Tag;
                $tag->title = request('title');
                $tag->type = request('type');
                $tag->building_id = $user['building_id'];
                $tag->status = 1;
                $tag->save();

                $record['success'] = true;
                $record['new_tag']['id'] = $tag->id;
            }
            
            $record['new_tag']['title'] = request('title');
            $record['new_tag']['type'] = request('type');
        } else {

        }
        return response()->json($record);
    }

    public function del_tag($tag_id) {
        $user = Auth::user();
        $record = array();
        if (Auth::check()) {
            DB::table('tag')
                ->where('id', $tag_id)
                ->where('type', 'announcement')
                ->update(['status' => 0 ]);

            $record['success'] = true;
            $record['tag_id'] = $tag_id;
        } else {

        }
        return response()->json($record);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $user = Auth::user();
  
        if (Auth::check()) {
            $tags = '';
            if (!empty(request('announcement_tags'))) {
                //$announcement_tags =  html_entity_decode(request('announcement_tags'));
                // $announcement_tags = json_decode(request('announcement_tags'), true);
                $announcement_tags = request('announcement_tags');
                $tags = array();
                for ($i = 0; $i < count($announcement_tags) ; $i++ ) {
                    
                    $tags[$i] = $announcement_tags[$i]['id'];
                }
                $tags = implode(",",$tags);
            }

            $announcement_type = request('announcement_type');
            if (is_null(request('aid'))) {
                $announcement = new Announcement;
                $announcement->status = 1;
            } else {
                $announcement = Announcement::find(request('aid'));
            }

            $announcement->admin_id = $user['id'];
            $announcement->building_id = $user['building_id'];
            $announcement->title = request('announcement_title');
            $announcement->content = request('announcement_content');
            $announcement->tag = $tags;
            $announcement->recipients = implode(",", request('recipient_list'));
            $announcement->start_time =request('announcement_startdate');
            $announcement->end_time = request('announcement_enddate');
            $announcement->level = request('announcement_status');
            $announcement->event_enddate = request('event_enddate');
            $announcement->type = $announcement_type;
           

            $announcement->show_event_btn = 0;
            if (request('allow_participant') == 1) {
                $announcement->show_event_btn = 1;
                $announcement->btn_words = 'Join';
            } else {
                $announcement->btn_words = '';
            }
            $announcement->save();


            if (isset($announcement->id)) {
                $record['id'] = $announcement->id;
            } else {
                $record['id'] = request('aid');
            }
            $record['success'] = true;
            // $notification = new MgrNotificationController;
            // $notification->push_msg_mail('announcement', request('recipient_list'), $announcement, true);
        } else {

        }
        return response()->json($record);
        // return redirect('mgr/announcement');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Announcement $announcement)
    {
        //
    }
}
