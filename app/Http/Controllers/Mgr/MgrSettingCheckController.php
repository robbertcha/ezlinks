<?php

namespace App\Http\Controllers\Mgr;

use Auth;
use App\Service;
use App\MgrAdmin;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Building_Setting_Check;
use App\Building_Setting;

class MgrSettingCheckController extends Controller {

    

    public static function unfinalizeSetting($bid, $slug) {
        $user = Auth::user();

        if (Auth::check()) {
            $setting_check = new Building_Setting_Check();
            $setting_check = $setting_check::unfinalizeSetting($bid, $slug);;
            
        }

        return $setting_check;
    }

    public static function firstTimeCheck($bid) {
        $user = Auth::user();

        if (Auth::check()) {
            $setting = new Building_Setting();
            $setting_content = $setting::getSettingContent();
            $setting_check = new Building_Setting_Check();
            $data = $setting_check::copySettingCheck($bid, $setting_content);
            
        } else {
            return;
        }
    }

    public static function settingDone($bid, $slug) {
        $user = Auth::user();

        if (Auth::check()){
            $setting_check = new Building_Setting_Check();
            $setting_check = $setting_check::slugCheck($bid, $slug);
            if ($setting_check) {
                return TRUE;
            }
        } else {
            return;
        }
    }

    public static function checkSettingMethod($bid, $slug) {
        $user = Auth::user();
        if (Auth::check()) {
            
            $setting = new Building_Setting();
            $setting_check = new Building_Setting_Check();
            $setting_method = $setting::getSettingMethod($slug)[0]['setting_method'];
            
            $setting_table = $setting::getSettingMethod($slug)[0]['setting_table'];
            $setting_field = $setting::getSettingMethod($slug)[0]['setting_field'];
            
            if ($setting_method == '1') {
                $result = DB::table($setting_table)   
                    ->when($setting_table == 'building', function ($query) use($bid, $setting_field){
                        $query->select($setting_field, 'building.id')
                              ->where('building.id', '=', $bid)  
                              ->where('status', '=', 1);
                              
                    })
                    ->when($setting_table == 'building_admin_profile', function ($query) use($bid, $setting_field){
                        $query->select($setting_field)
                              ->leftJoin('building_admin', 'building_admin.id', '=', 'building_admin_profile.ba_id')
                              ->leftJoin('building', 'building.id', '=', 'building_admin.building_id')
                              ->where('building.id', '=', $bid)  
                              ->where('building_admin_profile.status', '=', 1);
                              
                    })
                    ->pluck($setting_field);
                $result = $result[0];
                
                if ($result != null || $result != '') {
                    $check_result = self::settingDone($bid, $slug);
                } else {
                    $check_result = $setting_check::unfinalizeSetting($bid, $slug);
                }
            } elseif ($setting_method == '2') {
                
                $result = DB::table($setting_table)
                            ->select($setting_field)
                            ->where('building_id', '=', $bid)
                            ->where('status', '=', 1)
                            ->get()->ToArray();
                
                $result = count($result);
                
                
                if ($result > 0) {
                    $check_result = self::settingDone($bid, $slug);
                } elseif ($result == 0){
                    $check_result = $setting_check::unfinalizeSetting($bid, $slug);
                } else {
                    return;
                }
            } else {
                return;
            }
            
            
            return response()->json($check_result);
        }
    }
    
}