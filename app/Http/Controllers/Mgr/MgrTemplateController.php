<?php

namespace App\Http\Controllers\Mgr;

use Auth;
use App\Building_Template;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\User;

use Illuminate\Support\Facades\Input;

class MgrTemplateController extends Controller {

    public function __construct() { 
        $this->middleware('auth:building_admin');
    }

    public function get_template() {
        $user = Auth::user();
        

        if(Auth::check()){
            
            $model = new Building_Template();
            $model = $model::get_template_by_tag($user->building_id);
            return $model;
            
        } else {
            return null;
        }
    }

    public function get_tag_by_building_id(){
        $user = Auth::user();
        

        if(Auth::check()){
            $model = new Building_Template();
            $model = $model::get_tag($user->building_id);
            return $model;
            
        } else {
            return null;
        }
    }
}