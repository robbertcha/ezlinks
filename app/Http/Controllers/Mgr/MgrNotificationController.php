<?php

namespace App\Http\Controllers\Mgr;

use Auth;
use Mail; 
use Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

   
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Mail\NotificationMail;
use App\Mail\CaseMail;   
use App\Mail\ParcelMail;   
use App\Mail\SchedulerMail;  
use App\Mail\TenantVerifyMail;

use App\User;
use App\Building_Admin;
use App\Tenant_Detail;
use App\Building_Amenity;
use App\Mail_Modify;

use App\Send_To_Contractor;
use App\Push_Subscriptions;
use App\Notifications\PushDemo;



class MgrNotificationController extends Controller {

      public function __construct(){
            $this->middleware('auth:building_admin');
      }

// maybe able to delete
      /*
            $type - format: (Int) [0,1,2] (Have to be list, otherwise error)
                  - 0：all 
                  - 1: email
                  - 2: notification
                  - 3: sms
            
            $recipients - format: (Array) [{'id'=>1, 'first_name'=>'aaa', 'last_name'=>'bbb'}]
                        - composary fields: id, first_name, last_name
                        - tenant non-composary fields: building_id, unit_id
            
            $recipient_type - format: (Int) either 1 or 2 or 3
                            - 1: admin
                            - 2: tenant
                            - 3: contractor  

            $action - format: - (String) 'new_tenant_welcome_mail'
                              - All lowercase and connect with '_'
      */    
      public function send_middleware(Request $request) {
            $user = Auth::user();
            if(Auth::check()) {
                  $bm_sender = [];
                  $type = $request->type;
                  $recipients = $request->recipients;
                  $recipient_type = $request->recipient_type;
                  $action = $request->action;
                  $announcement_content = $request->announcement_content;

                  if (isset($request->use_bm_sender)) {
                        $bm_sender['use_bm_sender'] = 1;
                        $bm_sender['name'] =  $user['name'];
                        $bm_sender['email'] =  $user['email'];                        
                  } else {
                        $bm_sender['use_bm_sender'] = 0;
                        $bm_sender['name'] =  'EzLinks Admin';
                        $bm_sender['email'] =  'admin@ezlinks.com.au';    
                  }
                  
                  self::send($type, $recipients, $recipient_type, $action,$announcement_content,$bm_sender);
            }
            
      }

      public function send($type, $recipients, $recipient_type, $action,$announcement_content,$arr = []) {
            $user = Auth::user();
            
            if (Auth::check()) {
                  $building_id = $user['building_id'];
                  
                  if (in_array("1", $type)) {
                        $email_list = $this->getEmailListByID($recipient_type, $recipients);    
                  }
                  
                  if (count($email_list) > 0) {
                        $result = $this->sendEmail($action, $email_list, $recipient_type,$announcement_content,$arr);
                  } else {
                        
                  }
                  // return response()->json($record);
            } else {
                  
            }
      }
// maybe able to delete


      // send emails to contractors
      public function sendEmailToContractors($action, $address, $email_title, $email_content, $sender) {
            $mail_modify = new Mail_Modify;
            $id_key['contact_email'] = $address;
            $content_arr['sender_name'] = $sender->name;
            $content_arr['sender_email'] = $sender->email;

            $new_email_content = $mail_modify->init($email_content,$id_key);
            $content = $this->getEmailTemplate($action, $email_title, $address, $new_email_content, $content_arr);
            Mail::to($address)->queue($content);
      }

      // get email templates
      public function getEmailTemplate($action, $email_title, $recipients,$content, $content_arr) {
            if ($action == 'create_case') {
                  return new CaseMail($action, $email_title, $recipients,$content,$content_arr);
            }
            
            if ($action == 'create_announcement') {
                  return new NotificationMail($action, 'You have a new announcement', $recipients,$content, $content_arr);
            }

            if ($action == 'new_parcel') {
                  return new ParcelMail($action, 'You have a new parcel', $recipients, $content_arr);
            }

            if ($action == 'update_scheduler') {
                  return new SchedulerMail($action, 'Your reservation has been updated', $recipients, $content_arr);
            }


            if ($action == 'verify_tenant') {
                  return new TenantVerifyMail($content_arr['tenant'], $content_arr['unit_title'], $content_arr['token']);
            }
            
      }











      public function adminSendToTenant($recipients, $action, $arr = []){
            $user = Auth::user();
            
            if(Auth::check()) {
                  $this->send([1], $recipients, 'tenant', $action, $arr);
            }
      }


      // public function adminSendToContractor($recipients, $action) {
      //       $user = Auth::user();
            
      //       if(Auth::check()) {
      //             $this->send([1], $recipients, 'contractor', $action);
      //       }
      // }

      



      public function getEmailListByID($recipient_type, $recipients) {

            $user = Auth::user();
            if (Auth::check()) {
                  if ($recipient_type == 'admin') {
                        return DB::table('building_admin as ba')
                                          ->select("ba.email","bap.first_name","bap.last_name")
                                          ->join('building_admin_profile as bap',function($join){
                                                $join->on('bap.ba_id','=','ba.id');
                                          }, null,null,'left')
                                          ->whereIn('ba.id', $recipients)
                                          ->get()->toArray();
                  } else if ($recipient_type == 'tenant') {
                        
                        return DB::table('tenant_detail')
                        ->select('tenant_detail.id', 'tenant_detail.user_id', 'tenant_detail.building_id','tenant_detail.first_name','tenant_detail.last_name', 'users.email')
                              ->leftJoin('users','users.id','=','tenant_detail.user_id')
                              ->whereIn('users.id', $recipients)
                              ->get()->toArray();
                        

                  } else if ($recipient_type == 'contractor') {
                        
                        $contractor_email = new Send_To_Contractor();
                        
                        $contractor_email = $contractor_email->get_contractor_email($recipients);

                        return $contractor_email;
                  }
            } else {
                  return false;
            }
      }



      public function sendEmailAddress(Request $request) {
            $user = Auth::user();

            if(Auth::check()) {
                  $address = $request->email_address;
                  $action = $request->action;
                  $content = $request->email_content;
                  $mail_modify =new Mail_Modify;
                  $id_key['contact_email']=$address;
                  $new_email_content = $mail_modify->init($content,$id_key);
                  $content = $this->getEmailTemplate($action, 'the newest email testing', $address,  $new_email_content,[]);
                  Mail::to($address)->send($content);
                  $record = [];
                  $record['success'] = 'true';
                  return response()->json($record);
            }
      }
      

      public function sendEmail($action, $recipients, $recipient_type,$mail_content,$arr = []) {
            try {
                  foreach ($recipients as $key => $val) {
                        if($recipient_type == 'contractor'){
                              $content = $this->getEmailTemplate($action, 'You got a new case', $val['contact_email'],[]);
                              Mail::to($val['contact_email'])->queue($content);
                        } else {
                              $mail_modify = new Mail_Modify;
                              $id_key['user_id']= $val->user_id;
                              
                              if ($action != 'verify_tenant') {
                                    $new_email_content = $mail_modify->init($mail_content,$id_key);
                                    $content = $this->getEmailTemplate($action, 'Ezlinks Notification', $val->email, $new_email_content,$arr);
                              } else {
                                    $content = $this->getEmailTemplate($action, 'Ezlinks Notification', $val->email, '',$mail_content);
                              }
                              
                              Mail::to($val->email)->queue($content);
                        }
                  }
            } catch (Exception $e) {
                  // Log error
                  // continue;
            }
            $record = [];
            $record['success'] = true;
            return response()->json($record);
      }

      // check is it auto log in app/listeners/logsentmessage.php
      public function log() {

      }


      public function getAdminPusherListByID($recipients) {

      }

      public function sendNotification() {

      }

      public function getNotificationContent() {

      }


      /* this is sample
      public function push_msg_mail($action, $recipient_list, $object_arr, $sent_email = false) {
            $recipients_arr = explode(",", $recipient_list);
            $pushers = DB::table('building_admin as ba')
                            ->leftJoin('push_subscriptions as ps','ps.building__admin_id','=','ba.id')
                            ->whereIn('ps.building__admin_id', $recipients_arr)
                            ->pluck('ps.building__admin_id')->toArray();;

            if (count($pushers) > 0) {
            //     $object_arr->content = preg_replace('/(<[^>]+) style=".*?"/i', '$1',html_entity_decode($object_arr->content));
            //     $object_arr->substr_content = substr(strip_tags($object_arr->content), 0, 120);
                  $object_arr = $this->generate_notification_content($action, $object_arr);
                  Notification::send(Building_Admin::find($pushers),new PushDemo($action, $object_arr, $object_arr->id));

                  if ($sent_email == true) {
                        $email_users = DB::table('building_admin as ba')
                                          ->whereIn('ba.id', $recipients_arr)
                                          ->pluck('ba.email')->toArray();
                                          print_r($email_users);
                        foreach($email_users as $key) {
                              $a[] = $key;
                              Mail::to($a)->send(new NotificationMail($object_arr));
                        }
                  }
            } 
            
      }


      public function generate_notification_content($action, $object_arr) {
            if ($action == "create_booking") {
                  $tenant = new Tenant_Detail();
                  $tdetails = $tenant->getTenantDetailsByTID($object_arr->tenant_id);

                  $amenity = new Building_Amenity();
                  $adetails = $amenity->getAmenityDetailsByAID($object_arr->amenity_id);

                  $object_arr->title = "A booking request [id: ".$object_arr->id."] from ".$tdetails->tenant_text;
                  $object_arr->content = $tdetails->tenant_text." is making a booking request - ".$adetails->name.", from ". $object_arr->booking_time_from." to ".$object_arr->booking_time_to;
            }
            return  $object_arr;
      }
      this is sample */
            

}
?>