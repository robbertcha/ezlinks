<?php

namespace App\Http\Controllers\Mgr;

use App\EmergencyBell;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EmergencyBellController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:building_admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('mgr.emergencybell_list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmergencyBell  $emergencyBell
     * @return \Illuminate\Http\Response
     */
    public function show(EmergencyBell $emergencyBell)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EmergencyBell  $emergencyBell
     * @return \Illuminate\Http\Response
     */
    public function edit(EmergencyBell $emergencyBell)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EmergencyBell  $emergencyBell
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmergencyBell $emergencyBell)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EmergencyBell  $emergencyBell
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmergencyBell $emergencyBell)
    {
        //
    }
}
