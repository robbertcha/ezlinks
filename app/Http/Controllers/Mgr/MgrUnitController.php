<?php

namespace App\Http\Controllers\Mgr;

use Auth;
use File;
use Mail; 
use App\Model\Tenant\Tenant_Pet;
use App\Unit_Key;
use App\Unit_Key_Log;
use App\Unit;
use App\Unit_Group;
use Carbon\Carbon;
use App\Model\Tenant\Tenant_Detail;
use App\Tag;
use App\Model\Tenant\Tenant_Vehicle;
use App\Building_Unit_Custom_Field;
use App\Building_Stats;
use App\Model\Tenant\Unit_Document;
use App\TenantVerification;
use App\Building_Carpark;
use App\Building_Tenant;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\TenantVerifyMail;
use App\Http\Controllers\Mgr\MgrNotificationController;
use App\Http\Controllers\Mgr\MgrTagController;
use App\Http\Controllers\Mgr\MgrSettingCheckController;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Response;

class MgrUnitController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:building_admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if (Auth::check()) {
            // $units = DB::table('unit')
            //             ->select("unit.*", 
            //                     DB::raw("(SELECT GROUP_CONCAT( CONCAT(tenant_detail.id, '|', tenant_detail.first_name, ' ', tenant_detail.last_name) SEPARATOR ',') as tenant_name FROM tenant_detail where unit.id = tenant_detail.unit_id) as tenant_info"),
            //                     DB::raw("(SELECT GROUP_CONCAT(CONCAT(bc.id, '|',bc.carpark_title,'|',td.first_name, ' ', td.last_name,'|', tv.id,'|',tv.plate_no) SEPARATOR ',') as carpark_list FROM building_carpark as bc left join tenant_detail as td on td.id = bc.tenant_id  left join tenant_vehicle as tv on td.id = tv.tenant_id where unit.id = bc.unit_id and bc.type = 1 and bc.status = 1) as carpark_list"),
            //                     DB::raw("(SELECT GROUP_CONCAT(CONCAT(bk.id, '|',bk.key_holder,'|',bk.type, '|', bk.comment,'|', bk.key_status) SEPARATOR ',') as key_list FROM building_key as bk where unit.id = bk.unit_id  and bk.status = 1) as key_list")
                                
            //             )
            //             ->where('unit.status', 1)
            //             ->where('unit.building_id', '=', $user['building_id'])
            //             ->orderBy('unit.id', 'asc')
            //             ->paginate(12);


            // $units = DB::table('unit')
            //             ->select("unit.*", DB::raw("GROUP_CONCAT( CONCAT(tenant_detail.type, '|', tenant_detail.user_id, '|', tenant_detail.first_name, ' ', tenant_detail.last_name) SEPARATOR ',') as tenant_info")
            //             )
            //             ->leftJoin('tenant_detail','unit.id','=','tenant_detail.unit_id')
            //             ->where('unit.status', 1)
            //             ->where('unit.building_id', '=', $user['building_id'])
            //             ->orderBy('unit.id', 'asc')
            //             ->groupBy('unit.id')
            //             ->paginate(12);
                        // print_r($units);

            $page = 1;
            $ttype = null;
            $torder = 'a';
            $torderby = 'unit_title';
            $search_term = null;
            $search_tag = null;
            $tag = new MgrTagController();
            $tag = $tag::get_unit_tag();
  
            return view('mgr.unit_list', compact('page','ttype','torder','torderby','search_term','search_tag', 'tag'));
        } else {

        }
    }

    public function get_unit_list($page, $order, $orderby, $search_term, $search_tag, $qty_per_page = 12) {
        $user = Auth::user();
        if (Auth::check()) {
            if ($order == 'a') {
                $order = 'asc';
            } else {
                $order = 'desc';
            }

            if ($orderby == 'unit_title') {
                $orderby = 'unit.unit_title';
            }
            //, DB::raw("GROUP_CONCAT( CONCAT(building_carpark.carpark_title) SEPARATOR ',') as carpark_info ") 

            // $unit_cid = DB::table('unit')->select()
            
            $units = DB::table('unit')
                            ->select("unit.*", 
                                        DB::raw("GROUP_CONCAT(DISTINCT CONCAT(tenant_detail.type, '|', tenant_detail.user_id, '|', tenant_detail.first_name, ' ', tenant_detail.last_name) SEPARATOR ',') as tenant_info"), 
                                        DB::raw("GROUP_CONCAT(DISTINCT CONCAT(building_carpark.carpark_title) SEPARATOR ',') as carpark_info"), 
                                        DB::raw("(SELECT GROUP_CONCAT( CONCAT(tag.id, '|',tag.title) SEPARATOR ',') as tags FROM tag where tag.type = 'unit' and tag.building_id = '".$user['building_id']."' and tag.status = 1 and FIND_IN_SET(tag.id, unit.tag_ids)) as tags ")
                            )
                            ->leftJoin('tenant_detail','unit.id','=','tenant_detail.unit_id')
                            // ->leftJoin('tag', 'unit.tag_ids','=','tag.id')
                            // ->leftJoin('building_carpark',DB::raw("FIND_IN_SET(building_carpark.id,unit.carpark_id)"),">",DB::raw("'0'"))
                            ->leftJoin('building_carpark','unit.id','=','building_carpark.unit_id')
                            ->where('unit.status', 1)
                            ->where('unit.building_id', '=', $user['building_id'])
                            ->when($search_term != null && $search_term != '' && $search_term != 'null', function ($query) use ($search_term) {
                                $query->where(function ($q) use ($search_term) {
                                    $q->where('tenant_detail.first_name', 'like', '%' . $search_term . '%');
                                    $q->orWhere('tenant_detail.last_name', 'like', '%' . $search_term . '%');
                                    $q->orWhere('unit.unit_title', 'like', '%' . $search_term . '%');
                                });
                            })
                            ->when($search_tag != null && $search_tag != '' && $search_tag != 'null', function ($query) use ($search_tag) {
                                $query->where('unit.tag_ids', 'like', '%' . $search_tag . '%');
                            })
                            ->orderBy($orderby, $order)
                            ->groupBy('unit.id')
                            ->paginate($qty_per_page, ['*'], 'page', $page);


            return response()->json($units);
        } else {

        }
    }


    public function del_tag($tag_id) {
        $user = Auth::user();
        $record = array();
        if (Auth::check()) {
            DB::table('tag')
                ->where('id', $tag_id)
                ->where('type', 'unit')
                ->update(['status' => 0 ]);

            $record['success'] = true;
            $record['tag_id'] = $tag_id;
        } else {

        }
        return response()->json($record);
    }


    public function create_unit() {
        $uid = 0;
        $unit = array();
        $user = Auth::user();

        if (Auth::check()) {
            $unit['tags'] = Tag::where('building_id', '=', $user['building_id'])->where('type', '=', 'unit')->where('status', '=', '1')->get()->toArray();

            $u = new Unit();
            $units = $u->get_all_units($user['building_id']);

            return view('mgr.create_unit', compact('uid','unit','units'));
        } else {

        }
    }

    public function edit_unit($uid) {
        $user = Auth::user();
        if (Auth::check()) {
            // get tenant ids
            $tenant_ids = Tenant_Detail::select("tenant_detail.user_id")
                                            ->where('tenant_detail.unit_id', '=', $uid)
                                            ->where('tenant_detail.link_status', '=', 1)
                                            ->get()->pluck('user_id')->all();
            
            $unit['tags'] = Tag::where('building_id', '=', $user['building_id'])->where('type', '=', 'unit')->where('status', '=', '1')->get()->toArray();
            $unit['selected_tags'] = DB::select("select tag.id, tag.title
                                                from tag, unit
                                                where tag.building_id = ".$user['building_id']."
                                                and FIND_IN_SET(tag.id, unit.tag_ids)
                                                and tag.type = 'unit'
                                                and unit.id = ".$uid."
                                                group by tag.id
                                                order by tag.id desc");

            // get unit information
            $unit['info'] = Unit::where('id', '=', $uid)->first()->toArray();

            // get all pets
            $unit['pet_types'] = Tenant_Pet::getAllPetTypes();

             // get all carparks
            $unit['carparks'] = Building_Carpark::where('status', '=', 1)
                                                ->where('type', '=', 1)
                                                ->where(function($query)  {
                                                    $query->where('vehicle_id', '=', 0)
                                                        ->orWhere('vehicle_id', null);
                                                })
                                                ->where(function($query)  {
                                                    $query->where('tenant_id', '=', 0)
                                                        ->orWhere('tenant_id', null);
                                                })
                                                ->get()->toArray();

            // get tenant informations emergency_contact
            $unit['tenants'] = DB::table('tenant_detail')
                                        ->select("tenant_detail.id","tenant_detail.user_id","tenant_detail.first_name","tenant_detail.last_name", "u.email","tenant_detail.type as type_id",
                                        DB::raw("
                                            (CASE 
                                                WHEN tenant_detail.type = '3' THEN 'Owner'
                                                WHEN tenant_detail.type = '4' THEN 'Tenant'
                                                WHEN tenant_detail.type = '5' THEN 'Invester'
                                            END) AS type
                                        "),
                                        DB::raw("
                                            (CASE 
                                                WHEN tenant_detail.link_status = '0' THEN ''
                                                WHEN tenant_detail.link_status = '1' THEN 'Verified'
                                                WHEN tenant_detail.link_status = '2' THEN 'Unverified'
                                            END) AS link_status
                                        "),
                                        DB::raw("
                                            (CASE 
                                                WHEN tenant_detail.link_status = '0' THEN ''
                                                WHEN tenant_detail.link_status = '1' THEN tenant_detail.mobile
                                                WHEN tenant_detail.link_status = '2' THEN ''
                                            END) AS mobile
                                        "),
                                        DB::raw("
                                            (CASE 
                                                WHEN tenant_detail.link_status = '0' THEN ''
                                                WHEN tenant_detail.link_status = '1' THEN tenant_detail.contact
                                                WHEN tenant_detail.link_status = '2' THEN ''
                                            END) AS contact
                                        "),
                                        DB::raw("
                                            (CASE 
                                                WHEN tenant_detail.link_status = '0' THEN ''
                                                WHEN tenant_detail.link_status = '1' THEN tenant_detail.emergency_person
                                                WHEN tenant_detail.link_status = '2' THEN ''
                                            END) AS emergency_person
                                        "),
                                        DB::raw("
                                            (CASE 
                                                WHEN tenant_detail.link_status = '0' THEN ''
                                                WHEN tenant_detail.link_status = '1' THEN tenant_detail.emergency_no
                                                WHEN tenant_detail.link_status = '2' THEN ''
                                            END) AS emergency_no
                                        ")
                                        )
                                        ->join('users as u',function($join){
                                            $join->on('tenant_detail.user_id','=','u.id');
                                        }, null,null,'left')
                                        ->where('tenant_detail.unit_id', '=', $uid)
                                        // ->where('tenant_detail.link_status', '=', 1)
                                        ->where('u.status', '=', 1)
                                        ->get()->keyBy('user_id');
                                        // ->get()->toArray();

                                        
            // get unit & tenant assets
            $unit['unit_doc'] = DB::table('unit_document')->select("unit_document.id","unit_document.tenant_id","unit_document.title", "unit_document.size", "unit_document.type")
                                                            ->where('status', 1)
                                                            ->where('unit_id', '=', $uid)
                                                            ->where(function($query) use ($tenant_ids) {
                                                                $query->whereIn('tenant_id', $tenant_ids)
                                                                        ->orWhere('tenant_id', null);
                                                            })
                                                            ->get()->toArray();
            $unit['unit_cfield'] = DB::table('unit_custom_field')
                                        ->select("unit_custom_field.id","unit_custom_field.key", "unit_custom_field.value")
                                        ->where('status', 1)
                                        ->where('unit_id', '=', $uid)
                                        ->get()->toArray();

            $unit['tenant_vehicles'] = DB::table('tenant_vehicle')
                                        ->select("tenant_vehicle.id", "tenant_vehicle.tenant_id","tenant_vehicle.year","bc.id as parking_id","tenant_vehicle.make","tenant_vehicle.model","tenant_vehicle.color","tenant_vehicle.plate_no","tenant_vehicle.state","tenant_vehicle.tag","bc.carpark_title")
                                        ->leftJoin('building_carpark as bc','bc.vehicle_id','=','tenant_vehicle.id')
                                        ->where('tenant_vehicle.status', 1)
                                        ->whereIn('tenant_vehicle.tenant_id', $tenant_ids)
                                        ->get()->toArray();

            $unit['tenant_pets'] = Tenant_Pet::where('status', 1)->whereIn('tenant_id', $tenant_ids)->get()->toArray();
            $unit['tenant_keys'] = Unit_Key::where('status', 1)->where('unit_id', '=',$uid)->get()->toArray();
            // print_r($unit['tenants']); 

            $u = new Unit();
            $units = $u->get_all_units($user['building_id']);

            return view('mgr.unit_details', compact('uid','unit','units'));
        } else {

        }
    }

    public function update_tenant_type(Request $request) {
        $user = Auth::user();
        if (Auth::check()) {
            $edit_tenant_response = request('edit_tenant_response');

            $tenant = Tenant_Detail::find($edit_tenant_response['id']);
            $tenant->type = $edit_tenant_response['type_id'];

            $tenant->save();
            $record['success'] = true;
            $record['id'] = $edit_tenant_response['id'];
            $record['user_id'] = $edit_tenant_response['user_id'];
            $record['type'] = $this->get_tenant_type($edit_tenant_response['type_id']);
            $record['action'] = 'update_tenant_type';
        } else {

        }
        return response()->json($record);
    }


    public function get_tenant_type($type_id) {
        if ($type_id == 3) {
            return 'Owner';
        } elseif ($type_id == 4) {
            return 'Tenant';
        } elseif ($type_id == 5) {
            return 'Invester';
        }
    }
    
    public function get_vehicle_by_tid($tid) {
        $user = Auth::user();
        if (Auth::check()) {
            $record = array();
            $record['success'] = true;
            $record['vehicles'] = Tenant_Vehicle::where('status', 1)->where('tenant_id', $tid)->get()->toArray();

        } else {

        }
        return response()->json($record);
    }



    public function create_key(Request $request) {
        $record = array();
        $user = Auth::user();
        $new_key = request('key');

        if (Auth::check()) {
            $key = new Unit_Key;
            $key->building_id = $user['building_id'];
            $key->unit_id = $new_key['unit_id'];
            $key->key_holder = '-';
            $key->type = $new_key['type'];
            $key->comment = $new_key['comment'];
            $key->key_status = 1;
            $key->status = 1;
            $key->save();

            // update stats
            $building_stats = new Building_Stats();
            $building_stats->increase('total_key', 1, $user['building_id']);

            $record['success'] = true;
            $record['id'] = $key->id;
            $record['unit_id'] = $new_key['unit_id'];
            $record['type'] = $new_key['type'];
            $record['comment'] = $new_key['comment'];
            $record['key_holder'] = '-';
            $record['key_status'] = 1;
            $record['action'] ="create_key";
            $record['log_open'] = false;


        } else {

        }
        
        return response()->json($record);
    }


    public function create_key_log(Request $request) {
        $record = array();
        $user = Auth::user();
        $new_key = request('key');

        if (Auth::check()) {
            $key = new Unit_Key_Log;
            $key->unit_id = $new_key['unit_id'];
            $key->key_id = $new_key['key_id'];
            $key->key_taker = $new_key['key_taker'];
            $key->action = '-';
            $key->comment = '-';
            $key->start_at = time();
            $key->return_at = null;
            $key->status = 1;
            $key->save();

            // update stats
            $building_stats = new Building_Stats();
            $building_stats->increase('total_key_away', 1, $user['building_id']);

            $record['success'] = true;
            $record['id'] = $key->id;
            $record['start_at'] = date("m-d h:i");
            $record['return_at'] = '';
            $record['key_taker'] = $new_key['key_taker'];


        } else {

        }
        
        return response()->json($record);
    }


    public function update_key_log(Request $request) {
        $user = Auth::user();
        $key = request('key');

        if (Auth::check()) {
            $key_log = Unit_Key_Log::where('id', '=', $key['key_log_id'])->update(array('return_at' => time()));

            // update stats
            $building_stats = new Building_Stats();
            $building_stats->decrease('total_key_away', 1, $user['building_id']);

            $record['success'] = true;
            $record['return_at'] = date("m-d h:i");
        } else {

        }
        return response()->json($record);
    }



    public function get_key_log($key_id) {
        $user = Auth::user();
        if (Auth::check()) {
            $record = array();
            $record['success'] = true;
            $record['keys'] = Unit_Key_Log::select(DB::raw('from_unixtime(start_at,"%m-%d %h:%i") as start_at'), DB::raw('from_unixtime(return_at,"%m-%d %h:%i") as return_at'), "id", "key_taker")->where('status', 1)->where('key_id', $key_id)->get()->toArray();

        } else {

        }
        return response()->json($record);

    }


    public function link_tenant(Request $request) {
        $user = Auth::user();
        if (Auth::check()) {
            $new_tenant_code = request('new_tenant_code');
            $new_tenant_type = request('new_tenant_type');

            $tenant = DB::table('tenant_detail')->select("tenant_detail.id","tenant_detail.user_id", "tenant_detail.tenant_code","tenant_detail.unit_id","u.email","tenant_detail.contact","tenant_detail.mobile","tenant_detail.first_name","tenant_detail.emergency_no","tenant_detail.last_name",
                            DB::raw("
                                (CASE 
                                    WHEN tenant_detail.type = '3' THEN 'Owner'
                                    WHEN tenant_detail.type = '4' THEN 'Tenant'
                                    WHEN tenant_detail.type = '5' THEN 'Invester'
                                END) AS type
                            "),
                            DB::raw("
                                (CASE 
                                    WHEN tenant_detail.link_status = '0' THEN ''
                                    WHEN tenant_detail.link_status = '1' THEN 'Verified'
                                    WHEN tenant_detail.link_status = '2' THEN 'Unverified'
                                END) AS link_status
                            "))
                                        ->join('users as u',function($join){
                                            $join->on('tenant_detail.user_id','=','u.id');
                                        }, null,null,'left')
                                        ->where(function($query) use ($new_tenant_code) {
                                            $query->where('tenant_detail.tenant_code', '=', $new_tenant_code)
                                                    ->orWhere('u.email', '=', $new_tenant_code);
                                        })
                                        ->where(function($query) use ($new_tenant_code) {
                                            $query->where('tenant_detail.unit_id', '=', 0)
                                                    ->orWhere('tenant_detail.unit_id', null);
                                        })
                                        ->first();

            if (isset($tenant->user_id)) {
                $verification_token = str_random(40);
                $TenantVerification = TenantVerification::create([
                    'tenant_id' => $tenant->user_id,
                    'building_id' => $user['building_id'],
                    'unit_id' => request('unit_id'),
                    'token' => $verification_token,
                    'token_verified' => false
                ]);

                
                // update tenant type & link status
                $update_tenant = Tenant_Detail::where('user_id', '=', $tenant->user_id)->update(array( 'link_status' => 2, 'type' => $new_tenant_type, 'building_id' => $user['building_id'], 'unit_id' => request('unit_id')));

                // insert move in time 
                $bt = new Building_Tenant;
                $bt->building_id = $user['building_id'];
                $bt->tenant_id = $tenant->user_id;
                $bt->unit_id = request('unit_id');
                $bt->move_in = request('move_in');
                $bt->status = 1;
                $bt->save();

                // update stats
                $building_stats = new Building_Stats();
                $building_stats->increase('total_tenant', 1, $user['building_id']);

                // send activation email to tenant
                $tenant_arr = [];
                $mgrnotifications = new MgrNotificationController;
                $recipients = [$tenant->user_id];
                $action = 'verify_tenant';
                $tenant_arr['token'] = $verification_token;
                $tenant_arr['unit_title'] = request('unit_title');
                $tenant_arr['tenant'] = $tenant;

                $mgrnotifications->adminSendToTenant($recipients, $action, $tenant_arr);

                $record['success'] = true;
                $record['id'] = $tenant->id;
                $record['user_id'] = $tenant->user_id;
                $record['unit_id'] =  request('unit_id');
                $record['tenant_code'] =  $tenant->tenant_code;
                $record['type'] = $tenant->type;
                $record['email'] = $tenant->email;
                $record['contact'] = '-';
                $record['mobile'] = '-';
                $record['first_name'] = $tenant->first_name;
                $record['last_name'] = $tenant->last_name;
                $record['emergency_person'] = '-';
                $record['emergency_no'] = '';
                $record['link_status'] = 'Unverified';
            } else {
                $record['success'] = false;
                $record['message'] = "This Tenant is not exist or currently link to another unit.";
            }
            $record['action'] = 'send_tenant_activation_email';
        }else {

        }
        return response()->json($record);
    }

    public function unlink_tenant(Request $request) {
        $user = Auth::user();
        if (Auth::check()) {

            // unlink tenant
            $tenant = Tenant_Detail::where('user_id', '=', request('tenant_id'))->update(array('building_id' => 0, 'unit_id' => 0, 'carpark_id' => 0, 'link_status' => 0));

            // update move out time 
            $bt = Building_Tenant::where('tenant_id', '=', request('tenant_id'))->update(array('move_out' => date('Y-m-d')));

            // update stats
            $building_stats = new Building_Stats();
            $building_stats->decrease('total_tenant', 1, $user['building_id']);

            $record['success'] = true;
            $record['tenant_id'] = request('tenant_id');
            $record['action'] = 'unlink_tenant';
        } else {

        }
        return response()->json($record);
    }

    public function save_carpark(Request $request){
        $record = array();
        $user = Auth::user();
        if (Auth::check()) {
            $bc = new Building_Carpark;
            $bc->carpark_title = request('carpark_title');
            $bc->unit_id = request('unit_id');
            $bc->vehicle_id = request('vehicle_id');
            $bc->building_id = $user['building_id'];
            $bc->type = request('carpark_type');
            $bc->status = 1;
            $bc->save();

            $building_stats = new Building_Stats();
            $building_stats->increase('total_carpark', 1, $user['building_id']);

            $record['success'] = true;
            $record['id'] = $bc->id;


            // if(!empty(request('cp_title'))){
            //     for ($i = 0; $i < count(request('cp_title')); $i++) {
            //         if (request('cp_title')[$i]['title'] != null && request('cp_title')[$i]['title'] != '') {

            //             $cp_title = Building_Carpark::where('carpark_title', '=', request('cp_title')[$i]['title'])->where('building_id', '=', $user['building_id'])->where('status', '=', 1)->first();
            //             if (empty($cp_title)) {
            //                 $bc = new Building_Carpark;
            //                 $bc->carpark_title = request('cp_title')[$i]['title'];
            //                 $bc->building_id = $user['building_id'];
            //                 $bc->type = request('cp_type');
            //                 $bc->status = 1;
            //                 $bc->save();
                
            //                 $new_cp = new \stdClass();
            //                 $new_cp->id = $bc->id;
            //                 $new_cp->unit_title =  request('cp_title')[$i]['title'];
            //                 $record['success'] = true;
            //                 $record['carparks'][] = $new_cp;
            //             }
            //         }
            //     }
            // }else{
            //     // $record['unit_title'] = request('unit_title');
            //     // $record['carpark_type'] = request('carpark_type');
            //     // $record['carpark_title'] = request('carpark_title');
            //     $t_carpark = DB::table('building_carpark')
            //                 ->where('carpark_title',request('carpark_title'))
            //                 ->select('carpark_title')
            //                 ->get();
            //     //dd($t_carpark);
            //     if(count($t_carpark)===0){
            //         $data=array('building_id'=> $user['building_id'],'carpark_title'=>request('carpark_title'),'type'=>request('carpark_type'),'unit_id'=>request('unit_id'),'status'=>1);
            //         DB::table('building_carpark')->insert($data);
            //         $building_stats = new Building_Stats();
            //         $building_stats->increase('total_carpark', 1, $user['building_id']);
            //         $record['success'] = true;
            //     }else{
            //         $record['info'] = 'CarPark Existed';
            //     }
            // }
           
        } else {
            
        }
        
        return response()->json($record);
    }

    public function save_unit(Request $request){
        //
        $record = array();
        $user = Auth::user();
        if (Auth::check()) {
            

                $tags = [];
                // count(request('unit_tags')) > 0
                if (!empty(request('unit_tags'))) {
                    $unit_tags = html_entity_decode(request('unit_tags'));
                    $unit_tags = json_decode($unit_tags);
                    for ($i = 0; $i < count($unit_tags) ; $i++ ) {
                        $tags[] = $unit_tags[$i]->id;
                    }
                    if (count($tags) > 0) {
                        $tags = implode(",",$tags);
                    } else {
                        $tags = NULL;
                    }
                } else {
                    $tags = NULL;
                }

                if (request('unit_action') == 'update_unit') {
                    $unit = Unit::find(request('unit_id'));
                    $unit->admin_id = $user['id'];
                    $unit->building_id = $user['building_id'];
                    $unit->company_id = 1;
                    $unit->unit_title = request('unit_title');
                    $unit->comment = request('unit_comment');
                    $unit->tag_ids = $tags;
                    $unit->save();

                    $record['success'] = true;
                    $record['id'] = $unit->id;
                    $record['unit_title'] = request('unit_title');


                } else {
                    
                    
                    for ($i = 0; $i < count(request('unit_title')); $i++) {
                        if (request('unit_title')[$i]['title'] != null && request('unit_title')[$i]['title'] != '') {

                            $unit_title = Unit::where('unit_title', '=', request('unit_title')[$i]['title'])->where('building_id', '=', $user['building_id'])->where('status', '=', 1)->first();
                            if (empty($unit_title)) {
                                $unit = new Unit;
                                $unit->status = 1;
                                $unit->admin_id = $user['id'];
                                $unit->building_id = $user['building_id'];
                                $unit->company_id = 1;
                                $unit->unit_title = request('unit_title')[$i]['title'];
                                $unit->comment = request('unit_comment');
                                $unit->tag_ids = $tags;
                                $unit->save();

                                // 
                                $new_unit = new \stdClass();
                                $new_unit->id = $unit->id;
                                $new_unit->unit_title =  request('unit_title')[$i]['title'];
                                $record['success'] = true;
                                $record['units'][] = $new_unit;
                
                                // update stats
                                $building_stats = new Building_Stats();
                                $building_stats->increase('total_unit', 1, $user['building_id']);
                            }

                           
                        }
                    }
                }
        } else {

        }
        
        return response()->json($record);
    }


    public function initial_create_units(Request $request){
        $record = array();
        $user = Auth::user();
        if (Auth::check()) {
            $tags = [];
            if (count(request('unit_tags')) > 0) {
                    for ($i = 0; $i < count($request->unit_tags) ; $i++ ) {
                        $tags[] = $request->unit_tags[$i]['id'];
                    }
                    if (count($tags) > 0) {
                        $tags = implode(",",$tags);
                    } else {
                        $tags = NULL;
                    }
            } else {
                    $tags = NULL;
            }

            for ($i = 0; $i < count(request('new_units')); $i++) {
                if (request('new_units')[$i]['name'] != null && request('new_units')[$i]['name'] != '') {

                    $unit_title = Unit::where('unit_title', '=', request('new_units')[$i]['name'])->where('building_id', '=', $user['building_id'])->where('status', '=', 1)->first();
                    if (empty($unit_title)) {
                        $unit = new Unit;
                        $unit->status = 1;
                        $unit->admin_id = $user['id'];
                        $unit->building_id = $user['building_id'];
                        $unit->company_id = 1;
                        $unit->unit_title = request('new_units')[$i]['name'];
                        $unit->comment = null;
                        $unit->tag_ids = $tags;
                        $unit->save();

                        // carparks
                        $carparks = request('new_units')[$i]['carparks'];
                        $carparks = explode(",",$carparks);

                        for ($q = 0; $q < count($carparks) ; $q++) {
                            $bc = new Building_Carpark;
                            $bc->carpark_title = $carparks[$q];
                            $bc->building_id = $user['building_id'];
                            $bc->unit_id = $unit->id;
                            $bc->type = 1;
                            $bc->status = 1;
                            $bc->save();
                        }

                        // update stats
                        $building_stats = new Building_Stats();
                        $building_stats->increase('total_carpark', count($carparks), $user['building_id']);

                    }
                }
            }

            // update stats
            $building_stats = new Building_Stats();
            $building_stats->increase('total_unit', count(request('new_units')), $user['building_id']);

            $setting_check = new MgrSettingCheckController;
            $logo_check = $setting_check::checkSettingMethod($user['building_id'], 'unit');


            $record['success'] = true;
        } else {
            $record['success'] = false;
        }
        
        return response()->json($record);
    }


    public function create_carpark(Request $request) {
        $record = array();
        $user = Auth::user();
        $new_carpark = request('carpark');

        if (Auth::check()) {
            $carpark = new Building_Carpark;
            $carpark->tenant_id = $new_carpark['tid'];
            $carpark->building_id = $user['building_id'];
            //$carpark->unit_id = $new_carpark['unit_id'];
            $carpark->vehicle_id = $new_carpark['vid'];
            $carpark->carpark_title = $new_carpark['title'];
            $carpark->type = 1;
            $carpark->status = 1;
            $carpark->save();

            $tenant_name = Tenant_Detail::select('first_name','last_name')->where('id', '=', $new_carpark['tid'])->first();
            $plate_no = Tenant_Vehicle::select('plate_no')->where('id', '=', $new_carpark['vid'])->first();

            $record['success'] = true;
            $record['id'] = $carpark->id;
            $record['tenant_id'] = $new_carpark['tid'];
            $record['vehicle_id'] = $new_carpark['vid'];
            $record['carpark_title'] = $new_carpark['title'];
            $record['name'] = $tenant_name->first_name." ".$tenant_name->last_name;
            $record['plate_no'] = $plate_no->plate_no;

        } else {

        }
        
        return response()->json($record);
    }


    public function del_carpark(Request $request) {
        $user = Auth::user();
        $record = array();
        if (Auth::check()) {
            $result = $this->unlink_vehicle_from_carpark(request('cp_id'), request('vehicle_id'));

            DB::table('building_carpark')
                ->where('id', request('cp_id'))
                ->update(['status' => 0 ]);

            $record['success'] = true;
            $record['id'] = request('cp_id');
        } else {

        }
        return response()->json($record);
    }

    public function del_carpark_uid(Request $request) {
        $user = Auth::user();
        $record = array();
        if (Auth::check()) {
            $result = $this->unlink_vehicle_from_carpark(request('cp_id'), request('vehicle_id'));

            DB::table('building_carpark')
                ->where('id', request('cp_id'))
                ->update(['unit_id' => NULL ]);

            $record['success'] = true;
            $record['id'] = request('cp_id');
        } else {

        }
        return response()->json($record);
    }

    public function update_carpark(Request $request) {
        $user = Auth::user();
        $record = array();
        if (Auth::check()) {
            DB::table('building_carpark')
                ->where('id', request('cp_id'))
                ->update(['vehicle_id' => request('vehicle_id'), 'unit_id'=> request('unit_id'), 'type'=> request('carpark_type'), 'carpark_title'=> request('carpark_title') ]);

            $record['success'] = true;
            $record['id'] = request('cp_id');
            $record['unit_id'] = request('unit_id');
            $record['vehicle_id'] = request('vehicle_id');
        } else {

        }
        return response()->json($record);
    }

    public function unlink_vehicle(Request $request) {
        $user = Auth::user();
        $record = array();
        if (Auth::check()) {
            $result = $this->unlink_vehicle_from_carpark(request('cp_id'), request('vehicle_id'));

            $record['success'] = true;
            $record['id'] = request('cp_id');
        } else {

        }
        return response()->json($record);
    }

    public function unlink_vehicle_from_carpark($cp_id, $vehicle_id) {
        return DB::table('building_carpark')
                    ->where('id',$cp_id)
                    ->where('vehicle_id', $vehicle_id)
                    ->update(['vehicle_id' => null, 'tenant_id'=> null ]);
    }

    public function get_unit_id_by_tid($tid) {
        return Tenant_Detail::select('unit_id')->where('id', '=', $tid)->first();
    }


    public function create_vehicle(Request $request) {
        $record = array();
        $user = Auth::user();
        $edit_tenant_vehicle = request('edit_tenant_vehicle');

        if (Auth::check()) {
            
            // update
            if ($edit_tenant_vehicle['id'] > 0) {
                $vehicle = Tenant_Vehicle::find($edit_tenant_vehicle['id']);
                $vehicle->tenant_id = $edit_tenant_vehicle['tenant_id'];
                //$vehicle->parking_id = $edit_tenant_vehicle['parking_id'];
                $vehicle->year = $edit_tenant_vehicle['year'];
                $vehicle->make = $edit_tenant_vehicle['make'];
                $vehicle->model = $edit_tenant_vehicle['model'];
                $vehicle->color = $edit_tenant_vehicle['color'];
                $vehicle->plate_no = $edit_tenant_vehicle['plate_no'];
                $vehicle->state = $edit_tenant_vehicle['state'];
                $vehicle->status = 1;
                $vehicle->save();

                DB::table('building_carpark')
                        ->where('id', $edit_tenant_vehicle['parking_id'])
                        ->update(['vehicle_id' => $edit_tenant_vehicle['id'], 'tenant_id' => $edit_tenant_vehicle['tenant_id'] ]);

                $record['id'] = $edit_tenant_vehicle['id'];
                $record['action'] = 'update_vehicle';
            } else {
                // new 
                $vehicle = new Tenant_Vehicle;
                $vehicle->tenant_id = $edit_tenant_vehicle['tenant_id'];
                //$vehicle->parking_id = $edit_tenant_vehicle['parking_id'];
                $vehicle->year = $edit_tenant_vehicle['year'];
                $vehicle->make = $edit_tenant_vehicle['make'];
                $vehicle->model = $edit_tenant_vehicle['model'];
                $vehicle->color = $edit_tenant_vehicle['color'];
                $vehicle->plate_no = $edit_tenant_vehicle['plate_no'];
                $vehicle->state = $edit_tenant_vehicle['state'];
                $vehicle->status = 1;
                $vehicle->save();

                DB::table('building_carpark')
                        ->where('id', $edit_tenant_vehicle['parking_id'])
                        ->update(['vehicle_id' => $vehicle->id, 'tenant_id' => $edit_tenant_vehicle['tenant_id'] ]);
                $record['id'] = $vehicle->id;
                $record['action'] = 'create_vehicle';
            }

            $carpark_title = Building_Carpark::select('carpark_title')->where('id', '=', $edit_tenant_vehicle['parking_id'])->first();

            $record['success'] = true;
            $record['tenant_id'] = $edit_tenant_vehicle['tenant_id'];
            $record['carpark_title'] = $carpark_title->carpark_title;
            $record['year'] = $edit_tenant_vehicle['year'];
            $record['make'] = $edit_tenant_vehicle['make'];
            $record['model'] = $edit_tenant_vehicle['model'];
            $record['color'] = $edit_tenant_vehicle['color'];
            $record['plate_no'] = $edit_tenant_vehicle['plate_no'];
            $record['state'] = $edit_tenant_vehicle['state'];

        } else {

        }
        
        return response()->json($record);
    }


    public function del_vehicle(Request $request) {
        $record = array();
        $user = Auth::user();

        if (Auth::check()) {
            $result = $this->unlink_vehicle_from_carpark(request('cp_id'), request('vehicle_id'));

            $vehicle = Tenant_Vehicle::find(request("vehicle_id"));
            $vehicle->status = 0;
            $vehicle->save();


            $record['success'] = true;
            $record['id'] = request("vehicle_id");
        } else {

        }
        return response()->json($record);
    }

    public function del_key(Request $request) {
        $record = array();
        $user = Auth::user();

        if (Auth::check()) {
            $key = Unit_Key::find(request("key_id"));
            $key->status = 0;
            $key->save();

            // update stats
            $building_stats = new Building_Stats();
            $building_stats->decrease('total_key', 1, $user['building_id']);


            $record['success'] = true;
            $record['id'] = request("key_id");
        } else {

        }
        return response()->json($record);
    }


    public function del_unit(Request $request) {
        $record = array();
        $user = Auth::user();

        if (Auth::check()) {
            $unit = Unit::find(request("unit_id"));
            $unit->status = 0;

            // update stats
            $building_stats = new Building_Stats();
            $building_stats->decrease('total_unit', 1, $user['building_id']);

            $unit->save();
            $record['success'] = true;
            $record['unit_id'] = request("unit_id");
        } else {

        }
        return response()->json($record);
    }


    public function create_key_action(Request $request) {
        $record = array();
        $user = Auth::user();
        $unit_id = request('unit_id');
        $action = request('action');
        $key_id = request('key_id');
        $key_to = request('key_to');
        $key_note = request('key_note');


        if (Auth::check()) {
            $building_id = $user['building_id'];
            $key_log = new Unit_Key_Log;
            $key_log->key_id = $key_id;
            $key_log->unit_id = $unit_id;
            $key_log->key_taker = $key_to;
            $key_log->action = $action;
            $key_log->comment = $key_note;
            $key_log->save();

            $key = Unit_Key::find($key_id);
            if ($action == 'Key To') {
                $key->key_status = 2;
            } else {
                $key->key_status = 1;
            }
            $key->save();

            $record['id'] = $key_log->id;
            $record['success'] = true;
            $record['action'] = 'create_key_to';
        } else {

        }
        
        return response()->json($record);
        
    }

    // public function create_key(Request $request) {
    //     $record = array();
    //     $user = Auth::user();
    //     $edit_tenant_key = request('edit_tenant_key');

    //     if (Auth::check()) {
    //         $building_id = $user['building_id'];
    //         if ($edit_tenant_key['id'] > 0) {
    //             $key = Unit_Key::find($edit_tenant_key['id']);
    //             $key->building_id = $building_id;
    //             $key->unit_id = $edit_tenant_key['unit_id'];
    //             $key->key_holder = $edit_tenant_key['key_holder'];
    //             $key->type = $edit_tenant_key['type'];
    //             $key->comment = $edit_tenant_key['comment'];
    //             $key->status = 1;

    //             $key->save();
    //             $record['id'] = $edit_tenant_key['id'];
    //             $record['action'] = 'update_key';
    //         } else {
    //             $key = new Unit_Key;
    //             $key->building_id = $building_id;
    //             $key->unit_id = $edit_tenant_key['unit_id'];
    //             $key->key_holder = $edit_tenant_key['key_holder'];
    //             $key->type = $edit_tenant_key['type'];
    //             $key->comment = $edit_tenant_key['comment'];
    //             $key->status = 1;

    //             $key->save();
    //             $record['id'] = $key->id;
    //             $record['action'] = 'create_key';
    //         }

    //         $record['success'] = true;
    //         $record['unit_id'] = $edit_tenant_key['unit_id'];
    //         $record['key_holder'] = $edit_tenant_key['key_holder'];
    //         $record['type'] = $edit_tenant_key['type'];
    //         $record['comment'] = $edit_tenant_key['comment'];
           
    //     } else {

    //     }
        
    //     return response()->json($record);
    // }

    public function create_pet(Request $request) {
        $record = array();
        $user = Auth::user();
        $edit_tenant_pet = request('edit_tenant_pet');

        if (Auth::check()) {
            if ($edit_tenant_pet['id'] > 0) {
                $pet = Tenant_Pet::find($edit_tenant_pet['id']);
                $pet->tenant_id = $edit_tenant_pet['tenant_id'];
                $pet->type = $edit_tenant_pet['type'];
                $pet->variety = $edit_tenant_pet['variety'];
                $pet->color = $edit_tenant_pet['color'];
                $pet->borned_in = $edit_tenant_pet['borned_in'];
                $pet->status = 1;

                $pet->save();
                $record['id'] = $edit_tenant_pet['id'];
                $record['action'] = 'update_pet';
            } else {
                $pet = new Tenant_Pet;
                $pet->tenant_id = $edit_tenant_pet['tenant_id'];
                $pet->type = $edit_tenant_pet['type'];
                $pet->variety = $edit_tenant_pet['variety'];
                $pet->color = $edit_tenant_pet['color'];
                $pet->borned_in = $edit_tenant_pet['borned_in'];
                $pet->status = 1;

                $pet->save();
                $record['id'] = $pet->id;
                $record['action'] = 'create_pet';
            }

            $record['success'] = true;
            $record['tenant_id'] = $edit_tenant_pet['tenant_id'];
            $record['type'] = $edit_tenant_pet['type'];
            $record['variety'] = $edit_tenant_pet['variety'];
            $record['color'] = $edit_tenant_pet['color'];
            $record['borned_in'] = $edit_tenant_pet['borned_in'];

        } else {

        }
        
        return response()->json($record);
    }

    public function del_pet(Request $request) {
        $record = array();
        $user = Auth::user();

        if (Auth::check()) {
            $pet = Tenant_Pet::find(request("pet_id"));
            $pet->status = 0;

            $pet->save();
            $record['success'] = true;
            $record['id'] = request("pet_id");
        } else {

        }
        return response()->json($record);
    }

    public function create_custom_field(Request $request) {
        $record = array();
        $user = Auth::user();
        if (Auth::check()) {
            $new_cfields = request('new_cfields');

            foreach($new_cfields as $cfield_key => $cfield_val) {
                $Custom_Field = new Building_Unit_Custom_Field;
                $Custom_Field->unit_id = request('current_uid');
                $Custom_Field->key = $cfield_val['key'];
                $Custom_Field->value = $cfield_val['value'];
                $Custom_Field->status = 1;
                $Custom_Field->save();
            }

            $record['success'] = true;
            $record['cfields'] = $new_cfields;
            $record['action'] = 'create_custom_field';
        } else {

        }
        
        return response()->json($record);
    }

    public function del_cfield(Request $request) {
        $record = array();
        $user = Auth::user();

        if (Auth::check()) {
            $cfield = Building_Unit_Custom_Field::find(request("cfield_id"));
            $cfield->status = 0;

            $cfield->save();
            $record['success'] = true;
            $record['id'] = request("cfield_id");
        } else {

        }
        return response()->json($record);
    }


    public function upload_doc(Request $request) {
        $record = array();
        $user = Auth::user();
        if (Auth::check()) {
            date_default_timezone_set('Australia/Brisbane');
            $fileName = time().'_'.request('doc_user_id').'_'.$request->doc_file->getClientOriginalName();

            $path = public_path().'/uploads/tenant_docs/'.date('Y_m_d');
            File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);

            $request->doc_file->move(public_path().'/uploads/tenant_docs/'.date('Y_m_d').'/', $fileName);
            
            $Unit_Doc = new Unit_Document;
            $Unit_Doc->unit_id = request('current_uid');
            $Unit_Doc->tenant_id = request('doc_user_id');
            $Unit_Doc->title = $fileName;
            $Unit_Doc->path = public_path('uploads/tenant_docs');
            $Unit_Doc->size = $request->doc_file->getClientSize();
            $Unit_Doc->type = $request->doc_file->getClientOriginalExtension();
            $Unit_Doc->status = 1;
            $Unit_Doc->save();

            $record['success'] = true;
            $record['name'] = $fileName;
            $record['tenant_id'] = request('doc_user_id');
            $record['type'] = $request->doc_file->getClientOriginalExtension();
            $record['action'] = 'create_doc';
        } else {

        }
        
        return response()->json($record);
    }

    public function del_doc(Request $request) {
        $record = array();
        $user = Auth::user();

        if (Auth::check()) {
            $doc = Unit_Document::find(request("doc_id"));
            $doc->status = 0;

            $doc->save();
            $record['success'] = true;
            $record['id'] = request("doc_id");
        } else {

        }
        return response()->json($record);
    }

    public function download_doc($id){
        $dl  = Unit_Document::find($id);
        $c = $dl->created_at;
        $filename = $dl->title;
        $t = $c->toDateTimeString();
        $t_array = preg_split('/ +/',$t);
        $time = str_replace('-','_',$t_array[0]);
        $file = "/uploads/tenant_docs/".$time."/".$filename;
        return response()->json($file);
    }  

}
