<?php

namespace App\Http\Controllers\Mgr;

use Auth;
use File;
use Mail; 
use App\Unit;
use App\User;
use App\Contractor;
use App\Building;
use App\Building_Admin_Profile;
use App\Building_Log;
use App\Building_Case;
use App\Building_Asset;
use App\Tenant_Detail;
use App\Case_Progress;
use App\Building_Case_Attachment;
use App\Building_Stats;
use App\Building_Case_Quote;
use App\Building_Case_Invoice;
use App\Building_Case_Comment;
use App\Building_Template;
use App\Building_Amenity;
use App\Amenity_Reservation;
use App\Building_Inspection_Area;
// model
use App\Model\Mgr\Cases\BuildingCase;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Mgr\MgrContractorController;
use App\Http\Controllers\Mgr\MgrSettingController;
use App\Http\Controllers\Mgr\MgrReservationController;
use App\Http\Controllers\Mgr\MgrNotificationController;

// Service
use App\Services\Mgr\Cases\CaseService;
use App\Services\Mgr\Building\TenantService;
use App\Services\Mgr\Building\BuildingService;
use App\Services\Mgr\Building\BuildingAdminService;
use App\Services\Mgr\Building\ContractorService;
use App\Services\Mgr\Inspections\InspectionService;
use App\Services\Mgr\Reservations\ReservationsService;

// enum
use App\Enums\BuildingCase\RoutineType;
use App\Enums\BuildingCase\CaseProgressType;
use App\Enums\PriorityType;
use App\Enums\SchedulerType;


class ComplaintController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:building_admin');
        date_default_timezone_set('Australia/Brisbane');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($page = 1, $search_term = null){
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];
            // $mgrreservation = new MgrReservationController;

            $ba = new Building_Amenity();
            $amenities = $ba->getAllAmenitiesByBID($building_id);
            $stype = (new ReservationsService())->getSchedulerType();

            $contractor = new Contractor();
            $contractor_list = $contractor->get_contractor_by_building($building_id);

            $bi = new Building_Inspection_Area();
            $bi_list = $bi->get_building_inspections_by_building($building_id);

            $user = new User();
            $tenants = $user->getTenantsByBuildingID($building_id);
            
            $ba = new Building_Amenity();
            $amenity = $ba->getAllAmenitiesByBID($building_id);

            $u = new Unit();
            $units = $u->get_all_units($building_id);
        } else {

        }

        return view('mgr.case_list', compact('page','search_term', 'amenities', 'stype', 'contractor_list', 'bi_list','tenants', 'amenity','units'));
    }


    /**
     * AJAX - Get case list
     * @param int $page - page number
     * @param string $ttype - upcoming, on-going, history
     * @param string $search_term - search terms
     * @param boolean $hideClosedJobs
     * @return array case list
     */
    public function get_case_list($page, $ttype, $search_term, $hideClosedJobs = false){
        $user = Auth::user();
        if (Auth::check()) {
            $bi = [];
            $building_id = $user['building_id'];
            $ar = new Amenity_Reservation();

            if ($ttype == 'h') {
                $bi = $ar->getJobsByBuildingID($building_id, $page, 'h', $search_term, $hideClosedJobs);
            } else if ($ttype == 'o') {
                $bi = $ar->getJobsByBuildingID($building_id, $page, 'o', $search_term, $hideClosedJobs);
            } else if ($ttype == 'u') {
                $bi = $ar->getJobsByBuildingID($building_id, $page, 'u', $search_term, $hideClosedJobs);
            }
        }
        return response()->json($bi);
    }

    /**
     * Update inspection records by case
     * @param json $request
     * @return array 
     */
    public function update_case_inspections(Request $request) {
        $user = Auth::user();
        if (Auth::check()) {
            $is = new InspectionService();
            $record['success'] = $is->updateInspectionsByCase(request('inspectionResult'));

            if ($record['success'] == true) {
                $case = new CaseService();
                // Update inspection status in case
                $case->updateInspectionsStatus(request('inspectionResult')[0]['case_id']);
            }
        } else {
            $record['success'] = false;
        }
        
        $record['action'] = 'update_case_inspections';
        return response()->json($record);
    }

    public function get_all_case($c=null){
        $user = Auth::user();
        if (Auth::check()) {
            $search = new \stdClass;
            $bc = new Building_Case();
            
            $case_type = $bc->get_case_type(); 
            $building_id = $user['building_id'];

            if (isset($_REQUEST['input_search']) && $_REQUEST['input_search'] != '' && $_REQUEST['input_search'] != null) {
                $search->input_search = $_REQUEST['input_search'];
            }

            if (isset($_REQUEST['input_type']) && $_REQUEST['input_type'] != '' && $_REQUEST['input_type'] != null) {
                $search->input_type = $_REQUEST['input_type'];
            }

            $cases = Building_Case::select("cases.id","cases.title", "cases.due_date","cases.location","cases.routine", "cases.content","cases.tenant_id","cases.case_type","cases.created_at", "cases.priority","cases.progress","cases.reporter","contractor.comp_name",
                                        DB::raw("
                                            (CASE 
                                                WHEN cases.progress = '1' THEN 'New'
                                                WHEN cases.progress = '2' THEN 'In Progress'
                                                WHEN cases.progress = '3' THEN 'Completed'
                                                WHEN cases.progress = '4' THEN 'Closed'
                                                WHEN cases.progress = '5' THEN 'Awaiting Contractor'
                                                WHEN cases.progress = '6' THEN 'Awaiting Approval'
                                                WHEN cases.progress = '7' THEN 'Commitee Approval'
                                                WHEN cases.progress = '8' THEN 'Admin Approval'
                                                WHEN cases.progress = '9' THEN 'Building Manager Discretion'
                                            END) AS progress_txt
                                        ")
                                    )
                                    ->leftJoin('contractor','cases.assignee_id','=','contractor.id')
                                    ->where('cases.building_id', '=', $building_id)
                                    ->where('cases.status', '=', 1)
                                    ->when(isset($_REQUEST['input_search']), function ($query)  {
                                        $query->where('cases.content', 'like', '%' . $_REQUEST['input_search'] . '%');
                                    })
                                    ->when(isset($_REQUEST['input_type']), function ($query)  {
                                        $query->where('cases.case_type', '=', $_REQUEST['input_type']);
                                    })
                                    ->when($c == false || $c == 'false', function ($query, $c) {
                                        $query->where('cases.progress', '!=', 4);
                                    })
                                    ->orderBy('cases.created_at', 'desc')
                                    ->paginate(12, ['*'], 'page', null);
            foreach ($cases as $key => $val) {
                $val->content = preg_replace('/(<[^>]+) style=".*?"/i', '$1',html_entity_decode($val->content));
                $val->content = substr(strip_tags($val->content), 0, 120);
                // $val->progress = CaseProgressType::getKey($val->progress);
                $val->routine = RoutineType::getKey($val->routine);
                $val->priority = PriorityType::getKey($val->priority);
            }
            if(empty($p)){
                // return view('mgr.case_list', compact('cases','search','case_type'));
                return response()->json($cases);
            }else{
                // return response()->json(['cases' => $cases, 'search' => $search, 'case_type' => $case_type]);
                return response()->json($cases);
            }
        } else {

        }
    }
    
    // public function testRoutine() {
    //     $case = (new CaseService())->dailyCheckRoutineCase();
        
    //     return $case;
    // }

    public function email_contrator(Request $request) {
        $user = Auth::user();
        if (Auth::check()) {
            $sender = new \stdClass;
            $sender->email = $user['email'];
            $sender->name = $user['name'];

            $selectedContractor = request("selectedContractor");
            $case_id = request("case_id");
            $email_title = request("email_title"); 
            $email_content = request("email_content"); 

            $mgrnotifications = new MgrNotificationController;
            $mgrnotifications->sendEmailToContractors('create_case', $selectedContractor, $email_title, $email_content, $sender);

            $record['log_content'] = $content = 'An email has been sent to '.$selectedContractor.' by '.$user['name'].' ['.$user['id'].']';
            $log = $this->update_case_log($case_id, '', $user['id'], $content);

            $record['created_at'] =  date('Y-m-d H:i:s', time());
            $record['success'] = true;
        } else {
            $record['success'] = false;
        }
        
        $record['action'] = 'case_send_contractor_email';
        return response()->json($record);
    }



    public function saveCaseAttachment(Request $request) {
        if (Auth::check()) {
            $user = Auth::user();
            $building_id = $user['building_id'];
            $userInf = [];
            $userInf['buildingId'] = $building_id;
            $userInf['userId'] = $user['id'];
            $result= [];
            $result['success'] = (new CaseService())->saveAttachments($request['caseId'], $request, $userInf);
            
            return response()->json($result);
        }
    }

    /**
     * Save the new cases.
     * 
     * @param  Request $request.
     * @return redirect to case list page.
     */
    public function saveCase(Request $request){
        
        if (Auth::check()) {    
            $user = Auth::user();
            $building_id = $user['building_id'];
            $success = null;
            $caseService = new CaseService();
            $request['building_id'] = $building_id;
            
            $saveCase = $caseService->saveCase($request);
            
            if (request('id')) {
                $userInf = [];
                $userInf['name'] = $user['name'];
                $userInf['userId'] = $user['id'];

                $caseId = request('id');
                $action = 'update';
                
                if(isset($request->comment)) {
                    $saveComment = $caseService->saveComments($request->id, $request->comment, $userInf);
                    $success = TRUE && $saveComment;
                } else {
                    $success = TRUE;
                }
                
            } else {
                $action = 'create';
                $caseId = $saveCase['id'];
                if ($caseId) {
                    $success = TRUE;
                }
                
                // update stats
                $building_stats = new Building_Stats();
                $building_stats->increase('total_case', 1, $building_id);
            }

            if ($saveCase['progress'] == 3 || $saveCase['progress'] == 4) {
                // update stats
                $building_stats = new Building_Stats();
                $building_stats->decrease('total_case', 1, $building_id);
            }
            
            // create log
            if ( request('asset_id') || request('unit_id') ) {
                if (request('asset_id') ) {
                    $object = 'building_assets';
                    $object_id = request('asset_id');
                } else {
                    $object = 'unit';
                    $object_id = request('unit_id');
                }

                $log = new Building_Log;
                $log_bi_data = $log->generate_case_log($caseId, $user['name'], $user['building_id'], $action, $object, $object_id, request('case_progress'));
            } 
                
            $content = 'Case '.$caseId.' has been updated by '.$user['name'].' ['.$user['id'].']';
            $log = $this->update_case_log($caseId, '', $user['id'], $content);
        } else {

        }
        
        $result = [];
        $result['success'] = $success;
        $result['id'] = $caseId;
        return response()->json($result);
       
        
    }

    /**
     * 
     */
    public function getAttachments() {
        if (Auth::check()) {
            return (new CaseService())->getAttachments();
        }
    }

    
    public function create_quote_by_caseid(Request $request) {
        $user = Auth::user();

        if (Auth::check()) {
            date_default_timezone_set('Australia/Brisbane');
            $building_id = $user['building_id'];

            // upload doc file
            //foreach (request('quote_doc') as $key => $val) {
            if (request('quote_doc') != 'null' && request('quote_doc') != null && request('quote_doc') != '') { 
                $fileName = time().'_'.$building_id.'_'.request('quote_doc')->getClientOriginalName();
                $path = public_path().'/uploads/building_cases/'.date('Y_m_d');
                File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
                request('quote_doc')->move(public_path().'/uploads/building_cases/'.date('Y_m_d').'/', $fileName);
            }
            //}

            $cq = new Building_Case_Quote;
            $cq->case_id = request('case_id');
            $cq->contractor_id = request('contractor_id');
            $cq->quote_value = request('quote_val');
            $cq->quote_doc = request('quote_doc') != 'null' && request('quote_doc') != null && request('quote_doc') != '' ? '/uploads/building_cases/'.date('Y_m_d').'/'.$fileName : '';
            $cq->status = 1;

            $cq->save();
            $record['id'] = $cq->id;
            $record['date'] = date("d M");
            $record['quote_doc'] = request('quote_doc') != 'null' && request('quote_doc') != null && request('quote_doc') != '' ? $cq->quote_doc : '';
            $record['quote_value'] = request('quote_val');
            $record['action'] = 'create_quote';
            $record['success'] = true;

            $content = 'Quote '.$cq->id.' has been created by '.$user['name'].' ['.$user['id'].']';
            $log = $this->update_case_log($request->case_id, $request->id, $user['id'], $content);
            $record['log']['id'] = $log;
            $record['log']['message'] = $content;
            $record['log']['created_at'] = date('l M d Y');
        } else {

        }
        return response()->json($record);
    }



    public function create_invoice_by_caseid(Request $request) {
        $user = Auth::user();

        if (Auth::check()) {
            date_default_timezone_set('Australia/Brisbane');
            $building_id = $user['building_id'];

            // upload doc file
            if (request('case_invoice') != 'null' && request('case_invoice') != null && request('case_invoice') != '') { 
                $fileName = time().'_'.$building_id.'_'.request('case_invoice')->getClientOriginalName();
                $path = public_path().'/uploads/building_cases/'.date('Y_m_d');
                File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
                request('case_invoice')->move(public_path().'/uploads/building_cases/'.date('Y_m_d').'/', $fileName);
            }

            $ci = new Building_Case_Invoice;
            $ci->case_id = request('case_id');
            $ci->contractor_id = request('contractor_id');
            $ci->invoice_no = request('invoice_no');
            $ci->invoice_value = request('invoice_val');
            $ci->invoice_fee = request('invoice_fee');
            $ci->total_value = request('total_val');
            $ci->invoice_doc = request('case_invoice') != 'null' && request('case_invoice') != null && request('case_invoice') != '' ? '/uploads/building_cases/'.date('Y_m_d').'/'.$fileName : '';
            $ci->status = 1;

            $ci->save();
            
            $record['id'] = $ci->id;
            $record['date'] = date("d M");
            $record['invoice_doc'] = request('case_invoice') != 'null' && request('case_invoice') != null && request('case_invoice') != '' ? $ci->invoice_doc : '';
            $record['invoice_no'] = request('invoice_no');
            $record['invoice_value'] = request('invoice_val');
            $record['action'] = 'create_invoice';
            $record['success'] = true;

            $content = 'Invoice '.$ci->id.' has been created by '.$user['name'].' ['.$user['id'].']';
            $log = $this->update_case_log($request->case_id, $request->id, $user['id'], $content);
            $record['log']['id'] = $log;
            $record['log']['message'] = $content;
            $record['log']['created_at'] = date('l M d Y');

        } else {

        }
        return response()->json($record);
    }

    // deprecated
    public function cancel_scheduler (Request $request) {
        $user = Auth::user();
        $record = array();
        if (Auth::check()) {
            DB::table('amenity_reservation')
                ->where('id', $request->ar_id)
                ->update(['status' => 0 ]);

            $content = 'Scheduler '.$request->ar_id.' has been removed permanently by '.$user['name'].' ['.$user['id'].']';
            $log = $this->update_case_log($request->case_id, $request->id, $user['id'], $content);
            $record['success'] = true;
            $record['id'] = $request->ar_id;
            $record['log']['id'] = $log;
            $record['log']['message'] = $content;
            $record['log']['created_at'] = date('l M d Y');
        } else {

        }
        return response()->json($record);
    }

    public function createCaseById($id) {
        
        $user = Auth::user();
        if (Auth::check()) {
            return $this::create_case($id);
        }
    }

    /**
     * Create Case by id or null
     */
    public function create_case ($id = null) {
        $user = Auth::user();
        if (Auth::check()) {
       
            if ($id) {
                $caseService = new CaseService();
                $caseDetails = $caseService->getCaseById($id);
                
                $caseAttachments = $caseService->getAttachments($id);
                $case = $caseDetails;
                $case['id'] = null;
                
                $case_images = $caseAttachments['photos']?$caseAttachments['photos']:[];
                $case_docs = $caseAttachments['docs']?$caseAttachments['docs']:[];
            } else {
                $case_images = [];
                $case_docs = [];
                $case = [];
                $case['id'] = null;
                $case['tenant_id'] = '';
                $case['unit_id'] = '';
                $case['asset_id'] = '';
                $case['job_area'] = 2;

            }
            $case_scheduler = [];
            $case_quotes = [];
            $case_invoices = [];
            $case_comments = [];
            $case_log = [];

            $routine = (new CaseService())->getRoutineType();
            $u = new Unit();
            $ba = new Building_Asset;
            $bc = new Building_Case();
            $tenant = new Tenant_Detail();
            $contractor = new Contractor();
  
            $bi = new Building_Inspection_Area();
            $mgrsetting = new MgrSettingController;
   
            $building_id = $user['building_id'];

            $case_type = $bc->get_case_type(); 
            $case_area = $bc->get_case_area();
            $case_cate = $mgrsetting->get_catelist_by_mainID(3);
            $case_progress = $bc->get_case_progress();
            $contractor_list = $contractor->get_contractor_by_building($building_id);
            $contractor_detail = new \stdClass;;
            $tenant_list = $tenant->getTenantsByBuildingID($building_id);
    
            $admin['id'] = $user['id'];
            $admin_profile = Building_Admin_Profile::getAdminInfoByBAID($admin['id']);
            $admin['name'] = $admin_profile->first_name." ".$admin_profile->last_name;
            $admin['phone'] = $admin_profile->cperson_phone;
            
            $bi_list = $bi->get_building_inspections_by_building($building_id);
            
            $template_list = $this->get_template_list($building_id);

            $units = $u->get_all_units($building_id);
            $assets = $ba->getListByBuildingID($building_id);

            return view('mgr.case_detail', compact('case','units','assets', 'case_type', 'case_area',
                'case_cate','case_progress','case_scheduler','case_images','case_docs','case_quotes','case_invoices',
                'case_log','contractor_list','tenant_list','template_list','admin','bi_list',
                'contractor_detail', 'case_comments', 'routine'));
        } else {

        }
    }

    /**
     * Get case details and render into vue template via ajax
     * Note: Reservation event created on case -> parent_id = null && case_id > 0 && time_id = null
     * @param int $cid
     */
    public function case_detail($cid) {
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];

            // service setup
            $CaseService = new CaseService();
            $TenantService = new TenantService();
            $BuilbingService = new BuildingService();
            $BuilbingAdminService = new BuildingAdminService();
            $ReservationsService =new ReservationsService();
            $ContractorService =new ContractorService();
            $InspectionService = new InspectionService();

            // get admin details
            $admin = $BuilbingAdminService->getInfoByBuildingAdminID($user['id'], $user['email']);
            $admin->building = $BuilbingService->getBuildingInfoById($building_id);

            // case details
            $case = $CaseService->getCaseDetailsByBuildingID($cid, $building_id);
            $case->job_area_name = $case->job_area == 2 && $case->area_detail != null &&  $case->area_detail != '' ?  $case->job_area_name .': '.$case->area_detail : $case->job_area_name;
            $case->asset = $BuilbingService->getAssetDetailsByID($case->asset_id);
            $case->unit = $BuilbingService->getUnitDetailsByID($case->unit_id);
            $case->bi_items = $InspectionService->getInspectionItemsByAreaID($case->bi_id, $cid);

            // case
            $case_type = $CaseService->getCaseType(); 
            $case_area = $CaseService->getCaseArea();
            $case_progress = $CaseService->getCaseProgress();
            $case_comments = $CaseService->getCaseCommentsByCaseID($cid);
            $case_images = $CaseService->getCaseAttachmentsByCaseID($cid, 1);
            $case_docs = $CaseService->getCaseAttachmentsByCaseID($cid, 2);
            $case_quotes = $CaseService->getCaseQuotesByCaseID($cid);
            $case_invoices = $CaseService->getCaseInvoices($cid);
            $case_log = $CaseService->getCaseLogs($cid);

            // required information
            $case_scheduler = $ReservationsService->getEventsByCaseID($cid);
            $contractor_list = $ContractorService->getContractorsByBuildingID($building_id);
            $units = $BuilbingService->getUnitsByBuildingID($building_id);
            $assets = $BuilbingService->getAssetsByBuildingID($building_id);
            $bi_list = $InspectionService->getInspectionsByBuildingID($building_id);
            $template_list = $BuilbingService->getEmailTemplateList($building_id);
            $tenant_list = $TenantService->getTenantsByBuildingID($building_id);

            return view('mgr.case_detail', compact('case','units','assets', 'case_type', 'case_area' ,'case_progress','case_scheduler','case_images','case_docs','case_quotes','case_invoices','case_log','contractor_list','tenant_list','template_list','admin','bi_list', 'case_comments'));
        } 
    }

    

    public function update_contractor_list() {
        $user = Auth::user();
        $record = array();
        if (Auth::check()) {
            $building_id = $user['building_id'];
            $contractor = new Contractor();
            $record['list'] = $contractor->get_contractor_by_building($building_id);

            $record['success'] = true;
        } else {

        }
        return response()->json($record);
    }

   



    public function remove_case_docs(Request $request) {
        $user = Auth::user();
        $record = array();
        if (Auth::check()) {
            DB::table('case_attachment')
                ->where('id', $request->doc_id)
                ->update(['status' => 0 ]);

            $content = 'Document '.$request->doc_id.' has been removed permanently by '.$user['name'].' ['.$user['id'].']';
            $log = $this->update_case_log($request->case_id, $request->doc_id, $user['id'], $content);
            $record['success'] = true;
            $record['id'] = $request->doc_id;
            $record['log']['id'] = $log;
            $record['log']['message'] = $content;
            $record['log']['created_at'] = date('l M d Y');
        } else {

        }
        return response()->json($record);
    }

    public function remove_case_img(Request $request) {
        $user = Auth::user();
        $record = array();
        if (Auth::check()) {
            DB::table('case_attachment')
                ->where('id', $request->img_id)
                ->update(['status' => 0 ]);

            $content = 'Image '.$request->img_id.' has been removed permanently by '.$user['name'].' ['.$user['id'].']';
            $log = $this->update_case_log($request->case_id, $request->img_id, $user['id'], $content);
            $record['success'] = true;
            $record['id'] = $request->img_id;
            $record['log']['id'] = $log;
            $record['log']['message'] = $content;
            $record['log']['created_at'] = date('l M d Y');
        } else {

        }
        return response()->json($record);
    }


    public function remove_quote (Request $request) {
        $user = Auth::user();
        $record = array();
        if (Auth::check()) {
            DB::table('case_quote')
                ->where('id', $request->id)
                ->update(['status' => 0 ]);

            $content = 'Quote '.$request->id.' has been removed permanently by '.$user['name'].' ['.$user['id'].']';
            $log = $this->update_case_log($request->case_id, $request->id, $user['id'], $content);
            $record['success'] = true;
            $record['id'] = $request->id;
            $record['log']['id'] = $log;
            $record['log']['message'] = $content;
            $record['log']['created_at'] = date('l M d Y');
        } else {

        }
        return response()->json($record);
    }


    public function remove_invoice (Request $request) {
        $user = Auth::user();
        $record = array();
        if (Auth::check()) {
            DB::table('case_invoice')
                ->where('id', $request->id)
                ->update(['status' => 0 ]);

            $content = 'Invoice '.$request->id.' has been removed permanently by '.$user['name'].' ['.$user['id'].']';
            $log = $this->update_case_log($request->case_id, $request->id, $user['id'], $content);
            $record['success'] = true;
            $record['id'] = $request->id;
            $record['log']['id'] = $log;
            $record['log']['message'] = $content;
            $record['log']['created_at'] = date('l M d Y');
        } else {

        }
        return response()->json($record);
    }


    public function create_case_docs(Request $request) {
        // if (isset($request->asset_photo) && $request->asset_photo != null && $request->asset_photo != '') {
        //     date_default_timezone_set('Australia/Brisbane');
        //     $fileName = time().'_'.$building_id.'_'.$request->asset_photo->getClientOriginalName();
        //     $path = public_path().'/building_assets/'.date('Y_m_d');
        //     File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
        //     $request->asset_photo->move(public_path().'/building_assets/'.date('Y_m_d').'/', $fileName);
        //     $asset->photo = '/building_assets/'.date('Y_m_d').'/'.$fileName;
        // }

        return response()->json($request);
    }

    public function update_case_log($case_id, $assignee_id, $user_id, $content) {
        $user = Auth::user();
        if (Auth::check()) {
                $cp = new Case_Progress;
                $cp->case_id =  $case_id;
                $cp->admin_id = $user_id;
                $cp->assignee_id = $assignee_id;
                $cp->message = $content;
                $cp->save();

                if ($cp->id > 0) {
                    return $cp->id;
                } else {
                    return false;
                }
        } else {

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Complaint  $complaint
     * @return \Illuminate\Http\Response
     */
    public function destroy(Complaint $complaint)
    {
        //
    }
}
