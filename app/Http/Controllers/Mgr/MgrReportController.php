<?php

namespace App\Http\Controllers\Mgr;

use Auth;
use Mail; 
use Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Mgr\MgrReservationController;
use App\Http\Controllers\Mgr\ComplaintController;
use Dompdf\Dompdf;
use PhpOffice\PhpWord\PhpWord;
use File;
use PDF;
use View;
use Illuminate\Support\Facades\Input;
use Dompdf\Options;
use App;
use App\User;

use App\Building_Admin;
use App\Tenant_Detail;
use App\Amenity_Reservation;
use App\Building_Case;
use App\Building_report;
use App\Building_Inspection_Area;

class MgrReportController extends Controller {
    
    public function __construct(){
        $this->middleware('auth:building_admin');
  }

    public static function index() {
        $user = Auth::user();
        if (Auth::check()) {
            return view('mgr/report');
        }
    }

    public static function get_maintenance_list($date_from, $date_to) {
        $user = Auth::user();
        if( Auth::check() ){
            $reservation_controller = new MgrReservationController;
            $building_id = $user['building_id'];
            $maintenance_list = $reservation_controller::get_reservation_list($date_from, $date_to, '2', $building_id, $order = 'asc');
        } else {

        }
        return response()->json($maintenance_list);
    }

    public static function get_daily_task_list($date_from, $date_to) {
        $user = Auth::user();
        if( Auth::check() ){
            $reservation_controller = new MgrReservationController;
            $building_id = $user['building_id'];
            $daily_task_list = $reservation_controller::get_reservation_list($date_from, $date_to, '1', $building_id, $order = 'asc');
        } else {

        }
        return response()->json($daily_task_list);

    }


    public static function select_content($date_from, $date_to) {
        $user = Auth::user();
        if( Auth::check() ){
            $date_from = $date_from;
            $date_to = $date_to;
            $content = [];
        
            $inspection = self::get_inspection_list($date_from, $date_to);
            $case = self::get_case_list($date_from, $date_to);
            $daily_task = self::get_daily_task($date_from, $date_to);

            $content['inspection'] = $inspection;
            $content['case'] = $case;
            $content['daily_task'] = $daily_task;
            
            // $report = self::get_inspection_list($date_from, $date_to);
        } else {

        }
        return response()->json($content);

    }

    public static function get_case_type() {
        $user = Auth::user();
        if( Auth::check() ){
            $case_controller = new ComplaintController;
            $case_type = $case_controller::get_case_type();
            $content = [];
            $content['case_type'] = $case_type;
        }
        return response()->json($content);
        
    }

    public static function get_inspection_list($date_from, $date_to) {
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];
            $date_from = $date_from;
            $date_to = $date_to;
            $inspection = new Building_Inspection_Area;
            $inspection = $inspection::get_building_inspections_by_date($building_id, $date_from, $date_to);
            
            return $inspection;
            
        } else {

        }
    }

    public static function get_case_list($date_from, $date_to) {
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];
            $date_from = $date_from;
            $date_to = $date_to;
            $case_list = new Building_Case;
            $case_list = $case_list::get_case_by_date($building_id, $date_from, $date_to);

            return $case_list;
        }
    }

    public static function get_daily_task($date_from, $date_to) {
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];
            $date_from = $date_from;
            $date_to = $date_to;
            $daily_task_list = new Amenity_Reservation;
            $daily_task_list = $daily_task_list::get_task_by_date($building_id, $date_from,$date_to);

            return $daily_task_list;
        }
    }

    public static function download_report($report_id) {
        $user = Auth::user();
        $record = array();
        if (Auth::check()) {
            $report_id = $report_id;
            $building_report = new Building_report;

            $file_path = $building_report::get_file_path($report_id);
            $file_path = str_replace('\\', '', $file_path);
           
            $report_file = $file_path;
            return $file_path;
        }
    }

    public static function next_service_calculate($current_date, $frequency) {
        switch ($frequency) {
            case 'none':
                return '-';
            case 'daily':
                return date('Y-m-d', strtotime($current_date . "+1 day"));
            case 'weekly': 
                return date('Y-m-d', strtotime($current_date . "+1 week"));
            case 'fortnightly':
                return date('Y-m-d', strtotime($current_date . "+15 day"));
            case 'monthly': 
                return date('Y-m-d', strtotime($current_date . "+1 month"));
            case 'quarterly': 
                return date('Y-m-d', strtotime($current_date . "+3 month"));
            case 'twice a year':
                return date('Y-m-d', strtotime($current_date . "+6 month"));
            case 'yearly':
                return date('Y-m-d', strtotime($current_date . "+1 year")); 
        }
    }

    public static function generate_report_directly(Request $request) {
        $user = Auth::user();
       
        
        if (Auth::check()) { 
            $date_from = $request['date_from'];
            $date_to = $request['date_to']; 
            $inspection_bool = $request['inspection'];
            $daily_task_bool = $request['daily_task'];
            $case_bool = $request['case'];
            $building_id = $user['building_id'];
            $word = new PhpWord();

            $word->addTitleStyle('h1', array('name'=>'HelveticaNeueLT Std Med', 'size'=>16, 'bold'=>true)); //h1
            //h2
            $word->addTitleStyle('h3', array('name'=>'HelveticaNeueLT Std Med', 'size'=>12, 'bold'=>true)); //h3


            if ($inspection_bool) {
                $inspection = self::get_inspection_list($date_from, $date_to);
                $inspection_section = $word->addSection();
                
                $inspection_list = self::generate_inspection_list($inspection, $inspection_section);
                
                // $inspection_section = $inspection_list;
            }
            
            if ($daily_task_bool) {
                $daily_task = self::get_daily_task($date_from, $date_to);
                if (count($daily_task) != 0) {
                    $daily_task_section = $word->addSection();
                    $daily_task_list = self::generate_daily_task_list($daily_task, $daily_task_section);
                }
    
            }
            
            if(count($case_bool) != 0) {
                $case = self::get_case_list($date_from, $date_to);
                $new_case = [];
                foreach($case as $key => $val) {
                    
                    if (in_array($val['case_type'], $case_bool)) {
                        array_push($new_case,$val);
                    }
                }
                
                if (count($case) != 0) {
                    $case_section = $word->addSection();
                    $case_list = self::generate_case_list($new_case, $case_section);
                }
            }

            $file_name = 'building' . $building_id . "_" . date("Ymdhis") . "_report.docx" ;
            $building_report_dir_path = './uploads/building_lib/' . $building_id . '/report';
            $file_path = $building_report_dir_path . '/' . $file_name;
            if (!File::isDirectory($building_report_dir_path)) {
                File::makeDirectory($building_report_dir_path, 0777, true, true);
            } 
            
            $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($word, 'Word2007');
            $file = $objWriter->save($file_path);
           
            $data['building_id'] = $user['building_id'];
            $data['file_name'] = $file_name;
            $data['file_path'] = $file_path;
            
            $building_report = new Building_report;
            $save_contents = $building_report::save_contents($data);
           
            if ($save_contents['id'] > 0) {
                $record['success'] = true;
                // $record['type'] = $type;
                $record['report_id'] = $save_contents['id'];
                return response()->json($record);
            }
        }
    }
    // generate inspection section
    public static function generate_inspection_list($inspection, $inspection_section) {
        $word = new PhpWord();
        
        $tableStyle = array(
                    
            'borderSize' => 2,
            'borderColor' => 'black',
            'cellMargin' => 100,
            
        );
        $firstRowStyle = array(
            'bgColor' => 'grey',
            
        );
        $styleCell = array('borderTopSize'=>1 ,'borderTopColor' =>'black','borderLeftSize'=>1,'borderLeftColor' =>'black','borderRightSize'=>1,'borderRightColor'=>'black','borderBottomSize' =>1,'borderBottomColor'=>'black' );
        // $section = $word->addSection();
        $frequency = ['none', 'daily', 'weekly', 'fortnightly', 'monthly', 'quarterly', 'twice a year', 'yearly'];

        $word -> addTableStyle('table', $tableStyle, $firstRowStyle);
        $word -> addFontStyle('h2', array('name'=>'HelveticaNeueLT Std Med', 'size'=>14, 'bold'=>true)); 
        $inspection_section->addText('Scheduled Maintenance', 'h2');
        $table = $inspection_section->addTable('table');
        $header = $inspection_section->addHeader();
        $footer = $inspection_section->addFooter();
        $TfontStyle = array('bold'=>true, 'italic'=> false, 'size'=>12, 'name' => 'Times New Roman', 'afterSpacing' => 0, 'Spacing'=> 0, 'cellMargin'=>0);
        $fontStyle = array('bold'=>false, 'italic'=> false, 'size'=>10, 'name' => 'Times New Roman', 'afterSpacing' => 0, 'Spacing'=> 0, 'cellMargin'=>0);


        $table->addRow(-0.5, array('exactHeight' => -5));
        $table->addCell(2500,$styleCell)->addText('Service Detail',$TfontStyle);
        $table->addCell(2500,$styleCell)->addText('Frequency',$TfontStyle);
        $table->addCell(2500,$styleCell)->addText('Last Service',$TfontStyle);
        $table->addCell(2500,$styleCell)->addText('Next Service',$TfontStyle);
        
        foreach($inspection as $key => $val) {
            $table->addRow(-0.5, array('exactHeight' => -5));
            $table->addCell(2500,$styleCell)->addText($val['title'],$fontStyle);
            // $table->addCell(2500,$styleCell)->addText($val['title'],$TfontStyle);
            
            if ($val['items'] != null) {
               explode('-',explode('|',$val['items'])[0])[2];
               $table->addCell(1500,$styleCell)->addText($frequency[explode('-',explode('|',$val['items'])[0])[2]],$fontStyle);
            } else {
                $table->addCell(2500,$styleCell)->addText('-',$fontStyle);
            }
            
            $table->addCell(2500,$styleCell)->addText(substr($val['created_at'], 0, 10),$fontStyle);
            
            if  ($val['items'] != null) {
                explode('-',explode('|',$val['items'])[0])[2];
                $table->addCell(2500,$styleCell)->addText(self::next_service_calculate(substr($val['created_at'], 0, 10), $frequency[explode('-',explode('|',$val['items'])[0])[2]]), $fontStyle);
            } else {
                $table->addCell(2500,$styleCell)->addText('-',$fontStyle);
            }
        }
        
        // return $section;

    }
    // generate daily task section
    public static function generate_daily_task_list($daily_task, $daily_task_section) {
        $word = new PhpWord();
        $tableStyle = array(
                    
            'borderSize' => 2,
            'borderColor' => 'black',
            'cellMargin' => 50,
        );
        $firstRowStyle = array(
            'bgColor' => 'grey',
            
        );
        $styleCell = array('borderTopSize'=>1 ,'borderTopColor' =>'black','borderLeftSize'=>1,'borderLeftColor' =>'black','borderRightSize'=>1,'borderRightColor'=>'black','borderBottomSize' =>1,'borderBottomColor'=>'black' );
        
        $word -> addTableStyle('table', $tableStyle, $firstRowStyle);
        $word->addFontStyle('h2', array('name'=>'HelveticaNeueLT Std Med', 'size'=>14, 'bold'=>true)); 
        $daily_task_section->addText('Daily Task', 'h2');
        $table = $daily_task_section->addTable('table');
        $header = $daily_task_section->addHeader();
        $footer = $daily_task_section->addFooter();
        $TfontStyle = array('bold'=>true, 'italic'=> false, 'size'=>12, 'name' => 'Times New Roman', 'afterSpacing' => 0, 'Spacing'=> 0, 'cellMargin'=>0);
        $fontStyle = array('bold'=>false, 'italic'=> false, 'size'=>10, 'name' => 'Times New Roman', 'afterSpacing' => 0, 'Spacing'=> 0, 'cellMargin'=>0);

        $table->addRow(-0.5, array('exactHeight' => -5));
        $table->addCell(2500,$styleCell)->addText('Date',$TfontStyle);
        $table->addCell(2500,$styleCell)->addText('Location',$TfontStyle);
        $table->addCell(2500,$styleCell)->addText('Type',$TfontStyle);
        $table->addCell(2500,$styleCell)->addText('Comment',$TfontStyle);
        $table->addCell(2500,$styleCell)->addText('Details',$TfontStyle);
        $date_duplicate = null;
        
        foreach($daily_task as $key => $val) {
            $table->addRow(-0.5, array('exactHeight' => -5));
            if ($date_duplicate == substr($val['created_at'], 0, 10)) {
                $table->addCell(2500,$styleCell)->addText('',$fontStyle);
            } else {
                $table->addCell(2500,$styleCell)->addText(substr($val['created_at'], 0, 10),$fontStyle);
                $date_duplicate = substr($val['created_at'], 0, 10);
            }
            $table->addCell(2500,$styleCell)->addText($val['location'] == null ? '-': $val['location'],$fontStyle);
            $table->addCell(2500,$styleCell)->addText($val['task_type'] == null ? '-': $val['task_type'],$fontStyle);
            $table->addCell(2500,$styleCell)->addText($val['comment'] == null ? '-': $val['comment'] ,$fontStyle);
            $table->addCell(2500,$styleCell)->addText($val['details'] == null ? '-': $val['details'],$fontStyle);
        }
    
    }

    public static function generate_case_list($case, $case_section) {
        $word = new PhpWord();
        $caseProgress = ['New', 'In Progress', 'Completed', 'Closed', 
        'Awaiting Contractor', 'Awaiting Approval', 'Commitee Approval', 
        'Admin Approval', 'Building Manager Discretion'];
        $tableStyle = array(    
            'borderSize' => 2,
            'borderColor' => 'black',
            'cellMargin' => 50,
        );

        $firstRowStyle = array(
            'bgColor' => 'grey',
        );

        $styleCell = array('borderTopSize'=>1 ,'borderTopColor' =>'black','borderLeftSize'=>1,'borderLeftColor' =>'black','borderRightSize'=>1,'borderRightColor'=>'black','borderBottomSize' =>1,'borderBottomColor'=>'black' );
        
        $word -> addTableStyle('table', $tableStyle, $firstRowStyle);
        $word->addFontStyle('h2', array('name'=>'HelveticaNeueLT Std Med', 'size'=>14, 'bold'=>true)); 
        $case_section->addText('Case', 'h2');
        
        $table = $case_section->addTable('table');
        $header = $case_section->addHeader();
        $footer = $case_section->addFooter();
        $TfontStyle = array('bold'=>true, 'italic'=> false, 'size'=>12, 'name' => 'Times New Roman', 'afterSpacing' => 0, 'Spacing'=> 0, 'cellMargin'=>0);
        $fontStyle = array('bold'=>false, 'italic'=> false, 'size'=>10, 'name' => 'Times New Roman', 'afterSpacing' => 0, 'Spacing'=> 0, 'cellMargin'=>0);

        $table->addRow(-0.5, array('exactHeight' => -5));
        $table->addCell(2500,$styleCell)->addText('Date',$TfontStyle);
        // $table->addCell(2500,$styleCell)->addText('Time',$TfontStyle);
        $table->addCell(2500,$styleCell)->addText('Type',$TfontStyle);
        $table->addCell(2500,$styleCell)->addText('Location',$TfontStyle);
        // $table->addCell(2500,$styleCell)->addText('Description',$TfontStyle);
        $table->addCell(2500,$styleCell)->addText('Action',$TfontStyle);
        $table->addCell(2500,$styleCell)->addText('Outcome',$TfontStyle);
        
        foreach($case as $key => $val) {
            $table->addRow(-0.5, array('exactHeight' => -5));
            $table->addCell(2500,$styleCell)->addText(substr($val['created_at'], 0,10),$fontStyle);
            // $table->addCell(2500,$styleCell)->addText(substr($val['created_at'], 10, 19),$TfontStyle);
            $table->addCell(2000,$styleCell)->addText($val['case_type'] == null ? '-': $val['case_type'] , $fontStyle);
            $table->addCell(1500,$styleCell)->addText('', $fontStyle);
            // $table->addCell(2000,$styleCell)->addText($val['title'] == null ? '-': $val['title'], $fontStyle);
            $table->addCell(2500,$styleCell)->addText($val['content'] == null ? '-': $val['content'] , $fontStyle);
            $table->addCell(2500,$styleCell)->addText($caseProgress[$val['progress'] - 1], $fontStyle);
        }
    }

    public static function get_report_list() {
        $record = array();
        $user = Auth::user();

        if (Auth::check()) {
            $building_id = $user['building_id'];
            $files = array();
            $fileName = 'report';
            $path = public_path().'/uploads/building_lib/'.$building_id.'/'.$fileName;
            $fileList = glob($path.'/*', GLOB_BRACE);

            $i = 0;
            foreach ($fileList as $file)  {
                $exploded = explode(".",$file);
                $extension = strtolower($exploded[count($exploded) - 1]);
                
                if ($extension == 'jpeg') {
                    $extension = 'jpg';
                }

                if ($extension == 'docx') {
                    $extension = 'doc';
                }

                if ($extension == 'xlsx') {
                    $extension = 'xls';
                }

                if ($extension == 'rar') {
                    $extension = 'zip';
                }

                if ($extension != 'docx' && $extension != 'doc' && $extension != 'jpg' && $extension != 'bmp'  && $extension != 'png'  && $extension != 'gif' && $extension != 'mp4' && $extension != 'pdf'  && $extension != 'ppt' && $extension != 'txt'  && $extension != 'wma'  && $extension != 'xlsx'  && $extension != 'xls'  && $extension != 'zip'  && $extension != 'rar' ) {
                    $extension = 'others';
                }

                $files[$i] = new \stdClass;
                $files[$i]->name = basename($file);
                $files[$i]->type = $extension;
                $i++;
            }

            $record['success'] = true;
            $record['fileList'] = $files;
            $record['building_id'] = $building_id;

        } else {

        }
        return response()->json($record);
    }

    public static function generate_report(Request $request) {
        $user = Auth::user();
        
        if (Auth::check()) {
        
            $record = array();
            $data = [];
            $report = $request->report;
            $building_id = $user['building_id'];

            $type = $request->type;
            
            if ($type == 'pdf') {
                $file_name = 'building' . $building_id . "_" . date("Ymdhis") . "_report.pdf" ;
                $building_report_dir_path = './uploads/building_lib/' . $building_id . '/report';
                $file_path = $building_report_dir_path . '/' . $file_name;

                $dompdf = new Dompdf();
                $dompdf->loadHtml($report);
                $dompdf->render();
                $output = $dompdf->output();

                if (!File::isDirectory($building_report_dir_path)) {
                    File::makeDirectory($building_report_dir_path, 0777, true, true);
                } 
    
                $report_file = file_put_contents($file_path, $output);

            } elseif ($type == 'word') {

                
                $file_name = 'building' . $building_id . "_" . date("Ymdhis") . "_report.docx" ;
                $building_report_dir_path = './uploads/building_lib/' . $building_id . '/report';
                $file_path = $building_report_dir_path . '/' . $file_name;
                if (!File::isDirectory($building_report_dir_path)) {
                    File::makeDirectory($building_report_dir_path, 0777, true, true);
                } 

                $word = new PhpWord();
               
                $tableStyle = array(
                    
                    'borderSize' => 2,
                    'borderColor' => 'black',
                );
                $firstRowStyle = array(
                    'bgColor' => 'grey',
                    
                );
                
                $section = $word->addSection();
                
                $word -> addTableStyle('table', $tableStyle, $firstRowStyle);
                $table = $section->addTable('table');
                
                $word->addTitleStyle(1, array('name'=>'HelveticaNeueLT Std Med', 'size'=>16, 'bold'=>true)); //h1
                $word->addTitleStyle(2, array('name'=>'HelveticaNeueLT Std Med', 'size'=>14, 'bold'=>true)); //h2
                $word->addTitleStyle(3, array('name'=>'HelveticaNeueLT Std Med', 'size'=>12, 'bold'=>true)); //h3

                $doc = new \DOMDocument();
                
                $doc->loadHTML($report);
                $doc->saveHTML();
                
                \PhpOffice\PhpWord\Shared\Html::addHtml($section, $doc->saveHtml(), true);
               
                $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($word, 'Word2007');
                $file = $objWriter->save($file_path);
                
               
            } else {

            }
            
            $data['building_id'] = $user['building_id'];
            $data['file_name'] = $file_name;
            $data['file_path'] = $file_path;
            
            $building_report = new Building_report;
            $save_contents = $building_report::save_contents($data);
           
            if ($save_contents['id'] > 0) {
                $record['success'] = true;
                // $record['type'] = $type;
                $record['report_id'] = $save_contents['id'];
                return response()->json($record);
            }

        }
    }

    public static function delete_report(Request $request) {
        $user = Auth::user();
        $record = [];

        if (Auth::check()) {
            $building_id = $user['building_id'];
            $file_name = $request['file_name'];
            $building_report = new Building_report;
            $delete = $building_report::delete_file($building_id, $file_name);
            if ($delete) {
                
                $record['success'] = true;

                
            }

        }

        return response() -> json($record);

    }
 }