<?php

namespace App\Http\Controllers\Mgr;

use Auth;
use DateTime;
use DateTimeZone;
use DatePeriod;
use DateInterval;

use App\Building_Amenity;;
use App\Amenity_Reservation;
use App\Admin_Task;
use App\Tag;
use App\Term;
use App\Unit;
use App\Contractor;
use App\Building;
use App\Building_Inspection_Area;
use App\Building_Dashboard;
use App\Building_Stats;
use App\Building_Setting_Check;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Mgr\MgrNotificationController;
use App\Http\Controllers\Mgr\MgrSettingCheckController ;

// service
use App\Services\Mgr\Reservations\ReservationsService;

class MgrDashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:building_admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];
            $first_day = date("Y-m-d") ;
            $last_day = date("Y-m-t") ;
            $rs = new ReservationsService();

            $setting_check = new MgrSettingCheckController;
            $setting_check = $setting_check::firstTimeCheck($user['building_id']);

            $sc = new Building_Setting_Check();
            $setting_check = $sc::getSettingCheckByBid($building_id);
            $setting_check = count($setting_check) == 0 ? false : $setting_check;                                   // get which is not done
            $setting_count = $sc::getSettingCountByBid($building_id);                                               // total

            if ($setting_check != false) {
                $tags = Tag::where('building_id', '=', $building_id)->where('type', '=', 'unit')->where('status', '=', '1')->get()->toArray();
                $building_info = Building::getBuildingInfoById($building_id);

                return view('mgr.initial_setting', compact('setting_check', 'setting_count','tags','building_info'));
            } else {
                
                $amenity = Building_Amenity::select('name as text', 'id as value','color')
                                                            ->where('status', '=', 1)
                                                            ->where('building_id', '=', $building_id)
                                                            ->get()->toArray();


                $reservation = $rs->get_reservation_list($first_day, $last_day, '', $building_id);
                $type = $rs->getSchedulerType();
                

                $contractor = new Contractor();
                $contractor_list = $contractor->get_contractor_by_building($building_id);

                $bi = new Building_Inspection_Area();
                $bi_list = $bi->get_building_inspections_by_building($building_id);

                $u = new Unit();
                $units = $u->get_all_units($building_id);

                // $admintask = $this->get_admintask_list($first_day, $last_day, $building_id);
                // $reservation = array_merge($reservation, $admintask);

                $term = Term::select("id as Id","title")
                                ->where('status', '=', 1)
                                ->where('slug', '=', 'admin_task_cate')
                                ->where(function ($query) use ($building_id) {
                                    $query->where('building_id', '=', $building_id)->orWhere('building_id', '=', "0");
                                })->get()->toArray();

                $tenants =   DB::table('users')
                                ->select('users.id as value', DB::raw("CONCAT(unit.unit_title, ' - ',tenant_detail.first_name ,' ', tenant_detail.last_name)  as text"), DB::raw("'' as color"))
                                ->leftJoin('tenant_detail','tenant_detail.user_id','=','users.id')
                                ->leftJoin('unit','tenant_detail.unit_id','=','unit.id')
                                ->where(function($query) {
                                    $query->where('users.type', '4')
                                    ->orWhere('users.type', '3')
                                    ->orWhere('users.type', '5');
                                })
                                ->where('users.status', '=', 1)
                                ->where('tenant_detail.building_id', '=', $building_id)
                                ->get()->toArray();

                
                $stats = Building_Stats::select("id","total_unit","total_owner","total_tenant","total_investor","total_asset","total_amenity","total_case","total_key","total_key_away","total_task","total_maintenance","total_reservation","total_inspection", "total_leasing", "total_carpark","total_inventory","total_inspection")
                                        ->where('status', '=', 1)
                                        ->where('building_id', '=', $building_id)
                                        ->get()->first();

                $stats['task_today'] = $this->get_scheduler_count_today(1, $building_id);
                // $stats['maintenance_today'] = $this->get_scheduler_count_today(2, $building_id);
                $stats['reservation_today'] = $this->get_scheduler_count_today(3, $building_id);
                $stats['inspection_today'] = $this->get_scheduler_count_today(2, $building_id);
                $stats['breaches_today'] = DB::table('building_parking_breaches')
                                                ->where('building_id', '=', $building_id)
                                                ->where('status','=',1)
                                                ->count();

                
                return view('home',compact(['building_id'],'user', 'reservation', 'amenity','tenants','term','stats', 'type','contractor_list','bi_list','user', 'setting_check', 'setting_count', 'units'));
            }
        } else {

        }

    }

    public function get_notification_list(Request $request) {
        $user = Auth::user();
        $record = array();
        if (Auth::check()) {
            $building_id = $user['building_id'];
            $records = DB::table('log_notifications as ln')
                                ->select("id", "url", 'action','title','content','created_at')
                                                ->where('status', '=', 1)
                                                ->where('recipient_type', '=', 1)
                                                ->where('type', '=', 'notification')
                                                ->where('id', '>', $request->latest_nid)
                                                ->where('building_id', '=', $building_id)
                                                ->orderBy('id', 'desc')
                                                ->get()->toArray();
            return response()->json($records);
        }
    }


    public function get_scheduler_count_today($type, $building_id) {
        $start_time = date('Y-m-d H:i:s', strtotime('today midnight'));
        $end_time = date('Y-m-d H:i:s', strtotime('23:59:59'));

        return DB::table('amenity_reservation as ar')
                                                ->select("ar.id")
                                                ->where('ar.status', '=', 1)
                                                ->where('ar.type', '=', $type)
                                                ->where('ar.building_id', '=', $building_id)
                                                // ->whereDate('booking_time_from', DB::raw('CURDATE()'))
                                                ->whereBetween('booking_time_from', [$start_time, $end_time])
                                                ->count();
    }


    
    // public function get_reservations_tasks_by_month(Request $request) {
    //     $user = Auth::user();
    //     if (Auth::check()) {
    //         $building_id = $user['building_id'];
    //         $first_day = date("Y-m-1", strtotime(request('month')) ) ;
    //         $last_day = date("Y-m-t", strtotime(request('month')) ) ;

    //         $reservation = $this->get_reservation_list($first_day, $last_day, $building_id);
    //         $admintask = $this->get_admintask_list($first_day, $last_day, $building_id);
    //         $reservation = array_merge($reservation, $admintask);
    //     } else {

    //     }
    //     return response()->json($reservation);
    // }

    // public function get_reservation_list($first_day, $last_day, $building_id) {
    //     return  DB::table('amenity_reservation as ar')
    //                 ->select("ar.id", "ar.tenant_id", DB::raw("CONCAT(u.unit_title, ' - ',td.first_name,' ', td.last_name) as title"), "a.name as objectTitle", "ar.details", "ar.booking_time_from as start", "ar.booking_time_to as end", "ar.amenity_id as objectId", "ar.comment", "ar.progress as status", DB::raw("1 as action"), DB::raw("2 as type_switch"))
    //                 ->join('tenant_detail as td',function($join){
    //                     $join->on('td.user_id','=','ar.tenant_id');
    //                 }, null,null,'left')
    //                 ->join('amenity as a',function($join){
    //                     $join->on('a.id','=','ar.amenity_id');
    //                 }, null,null,'left')
    //                 ->join('unit as u',function($join){
    //                     $join->on('u.id','=','td.unit_id');
    //                 }, null,null,'left')
    //                 ->where('a.status', '=', 1)
    //                 ->where('ar.status', '=', 1)
    //                 ->where('a.building_id', '=', $building_id)
    //                 ->whereBetween('ar.booking_time_from', [$first_day, $last_day])
    //                 ->get()->toArray();
    // }

    // public function get_admintask_list($first_day, $last_day, $building_id) {
    //     $result = DB::table('admin_task as at')
    //                     ->select("at.*")
    //                     ->where('at.status', '=', 1)
    //                     ->where('at.building_id', '=', $building_id)
    //                     ->where(function($query)  use ($first_day, $last_day) {
    //                         $query->where('at.recurrence', '1')
    //                                 ->where('at.recurrence_till', '>=', $first_day)
    //                                 ->orWhere('at.recurrence_till', '=', '')
    //                                 ->orWhere('at.recurrence_till', '=', null);
    //                     })
    //                     ->orWhere(function($query)  use ($first_day, $last_day) {
    //                         $query->where('at.recurrence', '0')
    //                                 ->whereBetween('at.time_from', [$first_day, $last_day]);
    //                     })
    //                     ->get()->toArray();
    //                     // ->toSql();
        
    //     $list = [];
    //     foreach($result as $key => $val) {
    //         if ($val->recurrence == 1) {
    //             $start_time = date('H:i:s', strtotime($val->time_from));
    //             $end_time = date('H:i:s', strtotime($val->time_to));

    //             $recurrence = [];
    //             $rules = explode(";", $val->recurrence_rule);
    //             foreach($rules as $rule_key) {
    //                 $rule_key = explode("=",$rule_key);
    //                 $recurrence[trim($rule_key[0])] = trim($rule_key[1]);
    //             }

    //             $recurrence['task'] = new \stdClass();
    //             $recurrence['task']->id = $val->id;
    //             $recurrence['task']->type_switch = 1;                                       // 1 - reservation 2 - admin task
    //             $recurrence['task']->objectId = 0;
    //             $recurrence['task']->objectTitle = '';
    //             $recurrence['task']->start = $val->time_from;
    //             $recurrence['task']->end = $val->time_to;
    //             $recurrence['task']->recurrenceRule = $val->recurrence_rule;
    //             $recurrence['task']->title = $val->title." - ".$recurrence['FREQ'];
    //             $recurrence['task']->description = $val->description;
    //             $recurrence['task']->action = 1;
    //             $list[] = $recurrence['task'];

    //         }
    //     }
    //     return $list;           
    // }


    
    // public function update_scheduler(Request $request) {
    //     $user = Auth::user();
    //     if (Auth::check()) {
    //         $data = request('data');
    //         $starttime = date('Y-m-d H:i:s', strtotime($data['start']));
    //         $endtime = date('Y-m-d H:i:s', strtotime($data['end']));

    //         if ($data['type_switch'] == 2) {
    //             if ($data['id'] > 0) {
    //                 $ar = Amenity_Reservation::where('id', '=', $data['id'])->update(array('blg_admin_id' => $user['id'], 'progress' => $data['progress'], 'comment' => $data['comment'], 'booking_time_from' => $starttime, 'booking_time_to' => $endtime));

    //                 $record['success'] = true;
    //                 $record['id'] = $data['id'];
    //                 $record['action'] = 'update_scheduler';
    //             } else {
    //                 $ar = new Amenity_Reservation;
    //                 $ar->amenity_id =  $data['newAmenity'];
    //                 $ar->tenant_id =  $data['newTenant'];
    //                 $ar->blg_admin_id =  $user['id'];
    //                 $ar->booking_time_from = $starttime;
    //                 $ar->booking_time_to = $endtime;
    //                 $ar->booking_recurring = 0;
    //                 $ar->details = $data['details'];
    //                 $ar->progress = $data['progress'];
    //                 $ar->comment = $data['comment'];
    //                 $ar->status = 1;

    //                 $ar->save();
    //                 $record['id'] = $ar->id;
    //                 $record['action'] = 'create_scheduler';
    //                 $record['success'] = true;
    //             }
    //         } else {
    //             if ($data['id'] > 0) {
    //                 $at = Admin_Task::where('id', '=', $data['id'])->update(array('blg_admin_id' => $user['id'], 'recurrence' => count($data['recurrenceRule']) > 0 ? 1 : 0, 'recurrence_rule' => $data['recurrenceRule'], 'time_from' => $starttime, 'time_to' => $endtime, 'title' => $data['title'], 'description' => $data['description']));

    //                 $record['success'] = true;
    //                 $record['id'] = $data['id'];
    //                 $record['action'] = 'update_scheduler';
    //             } else {
    //                 $at = new Admin_Task;
    //                 $at->building_id =  $user['building_id'];
    //                 $at->blg_admin_id =  $user['id'];
    //                 $at->time_from = $starttime;
    //                 $at->time_to = $endtime;
    //                 $at->recurrence = count($data['recurrenceRule']) > 0 ? 1 : 0;
    //                 $at->recurrence_rule = $data['recurrenceRule'];
    //                 $at->title = $data['title'];
    //                 $at->description = $data['description'];
    //                 $at->status = 1;

    //                 $at->save();
    //                 $record['id'] = $at->id;
    //                 $record['action'] = 'create_scheduler';
    //                 $record['success'] = true;
    //             }

    //         }

    //     } else {

    //     }  
    //     // return response()->json($record);
    // }
}
