<?php

namespace App\Http\Controllers\Mgr;

use Auth;
use App\Contractor;
use App\Contractor_Attachment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Building_Category;
use App\Http\Controllers\Mgr\MgrSettingController;
use Illuminate\Support\Facades\DB;

class MgrContractorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:building_admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];

            $search = new \stdClass;
            if (isset($_REQUEST['input_search']) && $_REQUEST['input_search'] != '' && $_REQUEST['input_search'] != null) {
                $search->input_search = $_REQUEST['input_search'];
            }

            if (isset($_REQUEST['input_cate']) && $_REQUEST['input_cate'] != '' && $_REQUEST['input_cate'] != null) {
                $search->input_cate = $_REQUEST['input_cate'];
            }

            $c_cate = $this->get_contractor_catelist();

            $contractors = Contractor::select('contractor.id','contractor.cont_cate','contractor.comp_name','contractor.comp_addr','contractor.comp_postcode','building_category_contractor.name as cate_name', 'contractor.comp_suburb','contractor.comp_state','contractor.contact_person','contractor.contact_no','contractor.contact_email')
                            ->leftJoin('building_category_contractor','building_category_contractor.id','=','contractor.cont_cate')
                            ->where('contractor.status', '=', 1)
                            ->where('contractor.building_id', '=', $building_id)
                            ->when(isset($_REQUEST['input_search']) && $_REQUEST['input_search'] != '' && $_REQUEST['input_search'] != null, function ($query)  {
                                $query->where('contractor.comp_name', 'like', '%' . $_REQUEST['input_search'] . '%');
                            })
                            ->when(isset($_REQUEST['input_cate']) && $_REQUEST['input_cate'] != '' && $_REQUEST['input_cate'] != null, function ($query)  {
                                $query->where('contractor.cont_cate', '=', $_REQUEST['input_cate']);
                            })
                            ->paginate(12);
        } else {

        }
        //
        return view('mgr.contractor_list', compact('contractors','c_cate','search'));
    }

    public function get_catelist_by_mainID($id) {
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];
        
            return Building_Category::select('id','parent_id','cate_id','name','description')
                                    ->where('status', '=', 1)
                                    ->where(function($query) use ($id) {
                                        $query->where('parent_id', $id)
                                        ->orWhere('cate_id', $id);
                                    })
                                    ->where('building_id', '=', $building_id)
                                    ->orderBy('cate_id', 'ASC')
                                    ->get()->toArray();
        } else {

        }
    }

    public function get_contractor_catelist(){
        $user = Auth::user();
        if(Auth::check()){
            $building_id = $user['building_id'];

            return DB::table('building_category_contractor')
                        ->select('id','name')
                        ->get()->toArray();
        }
    }


    public function get_contractor_by_building() {
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];
            return Contractor::select('contractor.id','contractor.cont_cate','contractor.comp_name','contractor.comp_addr','contractor.comp_postcode','building_category.name as cate_name', 'contractor.comp_suburb','contractor.comp_state','contractor.contact_person','contractor.contact_no','contractor.contact_email')
                            ->leftJoin('building_category','building_category.id','=','contractor.cont_cate')
                            ->where('contractor.status', '=', 1)
                            ->where('contractor.building_id', '=', $building_id)
                            ->get()->ToArray();
        } else {

        }
    }
    public function get_contractor_by_building_dashboard() {
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];
            $contractor = Contractor::select('contractor.id','contractor.cont_cate','contractor.comp_name','contractor.comp_addr','contractor.comp_postcode','building_category.name as cate_name',
             'contractor.comp_suburb','contractor.comp_state','contractor.contact_person','contractor.contact_no','contractor.contact_email')
                        ->leftJoin('building_category','building_category.id','=','contractor.cont_cate')
                        ->where('contractor.status', '=', 1)
                        ->where('contractor.building_id', '=', $building_id)
                        ->get()->ToArray();

        } else {

        }
        return response()->json($contractor);
    }

    public function recommend_contractor(){
        //
        $contractor = new \stdClass;
        $mgrsetting = new MgrSettingController;
        // $cont_cate = $mgrsetting->get_catelist_by_mainID(5);
        $cont_cate = $this->get_contractor_catelist();

        return view('mgr.recommend_contractor', compact('contractor','cont_cate'));
    }

    public function contractor_detail($id){
        //
        // $contractor = new \stdClass;
        $mgrsetting = new MgrSettingController;
        
        $cont_cate = $this->get_contractor_catelist();
        // $cont_cate = $mgrsetting->get_catelist_by_mainID(5);
        $contractor = Contractor::select('contractor.id','contractor.cont_cate','contractor.comp_name','contractor.comp_addr','contractor.comp_postcode','building_category.name as cate_name', 'contractor.comp_suburb','contractor.comp_state','contractor.contact_person','contractor.contact_no','contractor.contact_email')

                            ->leftJoin('building_category','building_category.id','=','contractor.cont_cate')
                            ->where('contractor.id', '=', $id)
                            ->where('contractor.status', '=', 1)
                            ->get()->first();
        

        return view('mgr.recommend_contractor', compact('contractor','cont_cate'));
    }

    public function save_contractor(Request $request) {
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];

            if (request('contractor_id') > 0) {
                $contractor = Contractor::find(request('contractor_id'));
            } else {
                $contractor = new Contractor;
                $contractor->status = 1;
            }

            $contractor->building_id = $building_id;
            $contractor->comp_name = request('txt_compname');
            $contractor->cont_cate = request('txt_compcate');
            $contractor->comp_addr = request('txt_compaddr');
            $contractor->comp_postcode = request('txt_comppostcode');
            $contractor->comp_suburb = request('txt_compsuburb');
            $contractor->comp_state = request('txt_compstate');
            $contractor->contact_person = request('txt_contactperson');
            $contractor->contact_no = request('txt_contactno');
            $contractor->contact_email = request('txt_contactemail');
            $contractor->note = request('txt_note');

            $contractor->save();

            if (request('contractor_id') > 0) {
                $contractor_id = request('contractor_id');
            } else {
                $contractor_id = $contractor->id;
            }


        } else {

        }
        return redirect('mgr/contractor_list');
    }

    
}
