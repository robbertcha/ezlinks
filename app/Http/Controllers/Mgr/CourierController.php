<?php

namespace App\Http\Controllers\Mgr;

use Auth;
use App\Courier;
use Illuminate\Http\Request;
use App\Tenant_Detail;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Mgr\MgrNotificationController;

class CourierController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:building_admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($page = 1, $torder = 'd', $torderby = 'id', $search_term = null) {
        //
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];
            $tenant = new Tenant_Detail();
            $tlist = $tenant->getTenantListByBuildingID($building_id);

            return view('mgr.courier_list', compact('page','torder','torderby','search_term','tlist'));
        }
    }


     // ajax return
     public function get_courier_list($page, $ttype, $torder, $orderby, $search_term) {
        $user = Auth::user();
        $record = array();
        if (Auth::check()) {
            $ttype_result = 3;
            $building_id = $user['building_id'];

            if ($torder == 'a') {
                $torder = 'asc';
            } else {
                $torder = 'desc';
            }

            if ($orderby == 'courier_comp') {
                $orderby = 'building_couriers.courier_comp';
            } else {
                $orderby = 'building_couriers.id';
            }

            if ($ttype == 'p') {
                $ttype_action = '!=';
            } else {
                $ttype_action = '=';
            }

            $courier = DB::table('building_couriers')
                        ->select('building_couriers.id', 'building_couriers.tenant_id', 'building_couriers.admin_id', 'building_couriers.courier_comp', 'building_couriers.picker_name', 'building_couriers.picker_cert_type', 'building_couriers.picker_cert_number','building_couriers.unique_code','building_couriers.barcode', 'building_couriers.pickup_at', 'building_couriers.status', 'tenant_detail.first_name', 'tenant_detail.last_name' , 'building_admin.name as admin_name','building_couriers.created_at',
                        DB::raw("
                            (CASE 
                                WHEN building_couriers.pickup_at = NULL && building_couriers.picker_name = NULL THEN 'not_pick'
                                WHEN building_couriers.pickup_at != NULL && building_couriers.picker_name != NULL  THEN 'pick'
                            END) AS courier_status
                        "))
                        ->leftJoin('tenant_detail','building_couriers.tenant_id','=','tenant_detail.user_id')
                        ->leftJoin('building_admin','building_couriers.admin_id','=','building_admin.id')
                        ->where('building_couriers.building_id', '=', $building_id)
                        ->where('building_couriers.parcel_status', $ttype_action, $ttype_result)
                        // ->where('building_couriers.pickup_at', $ttype_action, $ttype_result)
                        // ->where('building_couriers.picker_name', $ttype_action, $ttype_result)
                        ->where('building_couriers.status', '=', 1)
                        ->when($search_term != null && $search_term != '' && $search_term != 'null', function ($query) use ($search_term) {
                            $query->where(function ($q) use ($search_term) {
                                $q->where('building_couriers.courier_comp', 'like', '%' . $search_term . '%');
                                $q->orWhere('building_couriers.barcode', 'like', '%' . $search_term . '%');
                            });
                        })
                        ->orderBy($orderby, $torder)
                        ->paginate(12, ['*'], 'page', $page);

            return response()->json($courier);
        } else {

        }
    }

    public function receive_new_parcel(Request $request) {
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];

            $c = new Courier;
            $c->tenant_id = request('tenant_id');
            $c->admin_id  = $user['id'];
            $c->building_id  = $building_id;
            $c->courier_comp  = request('courier_comp');
            $c->barcode  = request('barcode_result');
            $c->comment = request('txt_comment');
            $c->parcel_status = 1;
            $c->status = 1;
            $c->save();

            $result = [];
            $result['success'] = true;
            $record['success'] = true;
            $record['id'] = $c->id;

            $mgrnotifications = new MgrNotificationController;
            $recipients = [$request->tenant_id];
            $action = 'new_parcel';
            $mgrnotifications->adminSendToTenant($recipients, $action);
            
            return response()->json($result);
        }
    }

    public function insert_parcel_picker(Request $request) {
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];

            $c = Courier::find(request('parcel_id'));
            $c->picker_name = request('txt_pickername');
            $c->picker_cert_type = request('txt_pickercerttype');
            $c->picker_cert_number = request('txt_pickercertno');
            $c->pickup_at = new \DateTime();
            $c->parcel_status = 3;
            $c->save();

            $record['success'] = true;
            $record['id'] = request('parcel_id');
            return response()->json($record);
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Complaint  $complaint
     * @return \Illuminate\Http\Response
     */
    public function show(Complaint $complaint)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Complaint  $complaint
     * @return \Illuminate\Http\Response
     */
    public function edit(Complaint $complaint)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Complaint  $complaint
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Complaint $complaint)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Complaint  $complaint
     * @return \Illuminate\Http\Response
     */
    public function destroy(Complaint $complaint)
    {
        //
    }
}
