<?php


namespace App\Http\Controllers\Mgr;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class ContactListController extends Controller{

    public function __construct()
    {
        $this->middleware('auth:building_admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        //
        $user = Auth::user();
        if(Auth::check()){
            if(!Schema::hasTable('contact_list')){
                Schema::create('contact_list', function($table)
                    {
                        $table->increments('id');
                        $table->integer('building_id')->default(1);
                        $table->string('country')->nullable();
                        $table->string('location')->nullable();
                        $table->string('name');
                        $table->string('number')->nullable();
                        $table->string('email')->nullable();
                        $table->integer('status')->default(1);
                    });
                    DB::table('contact_list')->insert([
                        ['name' => 'Police/Fire/Ambulance','number' => '000','status'=>3],
                        ['name' => 'ES assistance in floods and storms','number' => '132500','status'=>3],
                        ['name' => 'Police attendance except Victoria','number' => '131444','status'=>3]
                    ]);
                    $building_id=$user['building_id'];
                    $contactList = DB::table('contact_list')
                    ->select('id','name','number','country','location','email','status')
                                                        ->whereIn('status',[1,3])
                                                        ->where('building_id', $building_id)
                                                        ->get()->toArray();
    
                    return response()->json($contactList);

            }else{
                $building_id=$user['building_id'];
                $contactList = DB::table('contact_list')
                ->select('id','name','number','country','location','email','status')
                                                    ->whereIn('status',[1,2,3])
                                                    ->where('building_id', $building_id)
                                                    ->get()->toArray();

                return response()->json($contactList);
            }

        }
    }


    public function store_contact(Request $request){

            $user = Auth::user();
            $building_id=$user['building_id'];
            
            if(Auth::check()){
                if(empty(request('id'))){
                    DB::table('contact_list')->insert([
                        ['name' => request('name'),'number' => strval(request('number')), 'email' => request('email')]
                    ]);
                }
                else{
                    DB::table('contact_list')
                        ->where('id',request('id'))
                        ->update(['name'=> request('name'),'number' =>strval(request('number')), 'email' => request('email')]);
                }
            };
    }

    public function edit_contact(Request $request,$id){

        $user = Auth::user();
        if(Auth::check()){
            $building_id=$user['building_id'];
            $contactList = DB::table('contact_list')
            ->select('id','name','number','country','location','email','status')
                                                ->where('status',1)
                                                ->where('building_id', $building_id)
                                                ->where('id',$id)
                                                ->get()->toArray();
            }
            return response()->json($contactList);
        }

    public function delete_contact(Request $request,$id)
    {
        $user = Auth::user();
        if(Auth::check()){
            $building_id=$user['building_id'];
            DB::table('contact_list')
                            ->where('id',$id)
                            ->update(['status'=>0]);
            }
            return;

    }
}