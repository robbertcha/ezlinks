<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\Tenant_Detail;

use Validator;
use Config;
use Route;
use Auth;
use App\User; 
use Hash;

class BlgAdminLoginController extends Controller
{
    use AuthenticatesUsers;
    protected $redirectTo = '/home';

    public function __construct()
    {
      $this->middleware('guest:building_admin', ['except' => ['logout_blgAdmin']]);
    }
    //
    public function showBlgAdminLogin()
    {
        return view('auth.blgadmin-login');
    }

    public function validate_blgAdmin(Request $request)
    {
        // validate the info, create rules for the inputs
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        if (Auth::guard('building_admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
            $url = Config::get('app.url');
            return Redirect::to($url.'/mgr/dashboard');
        } 
    

        if (Auth::guard('users')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
            $url = Config::get('app.url');
            return Redirect::to($url.'/tenants/dashboard');
        } 
    }


    public function validate_user(Request $request)
    {
        // validate the info, create rules for the inputs
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        // print_r($request);

        if (Auth::guard('building_admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
            $record['success'] = true;
            $record['user_type'] = 'Building Manager';
        } elseif (Auth::guard('users')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
            $record['success'] = true;
            $record['user_type'] = 'Tenant';
        } else {
            $record['success'] = false;
            $record['msg'] = 'Email or Password is not correct, please try again.';
        }
        return response()->json($record);
    }

    public function register_user(Request $request)
    {
        $url = Config::get('app.url');
        $validation = $this->validate(request(), [
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string', 'min:6'],
        ]);

        $isExists = User::where('email',$request['email'])->first();
        if($isExists){
            $record['success'] = false;
            $record['msg'] = "The email is already exising, please try again.";
            return response()->json($record);
        }

        $user =  User::create([
            'name' => $request['first_name']." ".$request['last_name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'type' => $request['user_type']
        ]);

        if (Auth::guard('users')->attempt(['email' => $request['email'], 'password' => $request['password']], true)) {
            $record['success'] = true;
            $record['user_id'] = $user->id;

            $tenant = new Tenant_Detail();
            $tenant->user_id = $user->id;
            $tenant->tenant_code = rand(1111111111, 9999999999);
            $tenant->type = $request['user_type'];
            $tenant->first_name = $request['first_name'];
            $tenant->last_name = $request['last_name'];
            $tenant->mobile = $request['mobile'];
            $tenant->save();
        } 

        return response()->json($record);
    }

    protected function guard(){
        return Auth::guard('building_admin');
    }


    public function logout_blgAdmin(){
        Auth::guard('building_admin')->logout();
        $url = Config::get('app.url');
        return Redirect::to($url.'/blgadmin-login');
    }
}
