<?php

namespace App\Http\Controllers\Tenant_end;

use Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Tenant_end\TenantController;
use stdClass;

class FrontPageController extends Controller
{
      public function homepage() {
            $user_detail = new stdClass;

            return view('welcome', compact('user_detail'));
      }

      public function feature() {
            $user_detail = new stdClass;

            return view('feature', compact('user_detail'));
      }

      public function building_manager() {
            $user_detail = new stdClass;

            return view('building_manager', compact('user_detail'));
      }

      public function residents() {
            $user_detail = new stdClass;

            return view('residents', compact('user_detail'));
      }
}

?>