<?php

namespace App\Http\Controllers\Tenant_end;

use App\TenantVerification;
use App\Tenant_Detail;
use App\Building_Stats;

class TenantVerificationController extends Controller
{
      public function __construct(){

      }

      public function tenant_verification($code) {
            $verification = TenantVerification::where('token', '=', $code)->where('token_verified', '=', 0)->first();
    
            if ($verification) {
                $vid = $verification->id;
                $tenant_id = $verification->tenant_id;
                $unit_id = $verification->unit_id;
                $building_id = $verification->building_id;
    
                if ($verification != null) {
                    $v = TenantVerification::where('token', '=', $code)->update(array('token_verified' => 1));
                    $t = Tenant_Detail::where('user_id', '=', $tenant_id)->update(array('building_id' => $building_id, 'unit_id' => $unit_id, 'link_status' => 1));
    
                    // update stats
                    $building_stats = new Building_Stats();
                    $building_stats->increase('total_tenant', 1, $building_id);
    
                    if ($v == 1 && $t == 1) {
                        echo "Congratulation, your account is linked.";
                    } else {
                        echo "Sorry, we cannot connect to your account, please try again later.";
                    }
                }
            } else {
                echo "Sorry, this tenant is not exists or it has been verified.";
            }
        }

}