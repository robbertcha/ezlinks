<?php

namespace App\Http\Controllers\Tenant_end;

use Auth;
use File;
use DateTime;
use App\User;


use App\Building_Case;
use App\Model\Common\Building_Amenity;
use App\Amenity_Reservation;
use App\Building_Category;
use App\Building_Case_Attachment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\TenantVerification;

use App\Building_Stats;
use App\Push_Subscriptions;
use App\Http\Controllers\Controller;
//use App\Http\Controllers\Mgr\MgrSettingController;
use App\Http\Controllers\Mgr\ComplaintController;
use App\Http\Controllers\Mgr\MgrNotificationController;
use App\Building_Announcement_Participant;
use Notification;
use App\Notifications\PushDemo;
// tenant model;
use App\Model\Tenant\Tenant_Detail;
use App\Model\Tenant\Unit_Document;
use App\Model\Tenant\Tenant_Case;
use App\Model\Tenant\Tenant_Advertisement;
use App\Model\Tenant\Tenant_Reservation;
use App\Model\Tenant\Tenant_pet;
use App\Model\Tenant\Tenant_Vehicle;
// common model;
use App\Model\Common\Building_Announcement;
use App\Model\Common\Case_Progress;
use App\Model\Common\Case_Attachment;
use App\Model\Common\Building_Notification;
//
use App\Building_couriers;
use App\Building_Admin;
use App\Tenant_address_book;
use Carbon\Carbon;

use Illuminate\Support\Facades\Input;


class TenantController extends Controller
{
    var $user_name = null;

    public function __construct()
    {
        $this->middleware('auth:users');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $user = Auth::user();
        if (Auth::check()) {
            $user_detail = $this->get_header_details($user->id);
            $adv_list = new Tenant_Advertisement();
            $adv_list = $adv_list::get_advertisement($user_detail);
    
            $reservation_list =  $this->get_reservation_list(1, 3, $user_detail->building_id, $user->id);
            $announcement_list = $this->get_announcement_list(1, 1, $user_detail->building_id);

            return view('tenant/home', compact('user_detail','adv_list','reservation_list','announcement_list'));
        } else {

        }
    }

    public function get_announcement_detail($aid) {
        $user = Auth::user();

        if (Auth::check()) {
            $content = new Building_Announcement();
            $content = $content::get_announcement_detail($aid);
            
            $record['content'] = $content;
            
            $record['content']->content = strip_tags(preg_replace('/(<[^>]+) style=".*?"/i', '$1',html_entity_decode($record['content']->content)));

            $record['success'] = true;
            $record['action'] = 'get_announcement_detail';
        } else {
            return null;
        }
        return response()->json($record);
    }

    public function update_thumb(Request $request) {
        $record = array();
        $user = Auth::user();
        if (Auth::check()) {
            $fileName = time().'_'.request('thumb_url')->getClientOriginalName();
            $path = public_path().'/uploads/tenant/'.$user['id'].'/';
            File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
            request('thumb_url')->move($path.'/', $fileName);
            
            $tenant = Tenant_Detail::update_thumb($user->id, $fileName);

            $record['success'] = true;
            $record['name'] = $fileName;
            $record['type'] = request('thumb_url')->getClientOriginalExtension();
            $record['action'] = 'update_thumb';
        } else {

        }
        return response()->json($record);
    }

    public function update_tenant_details(Request $request) {
        $user = Auth::user();

        if (Auth::check()) {
            date_default_timezone_set('Australia/Brisbane');
            $record['action'] = request('action');
            
            $tenant = new Tenant_Detail();
            $tenant = $tenant::update_tenant_details($user->id, $request);
            $tenant = Tenant_Detail::where('user_id',$user->id)->first();
          
            $record['action'] = request('action');
            $record['success'] = true;
        } else {
            $record['action'] = 'update_tenant_details';
            $record['success'] = false;
        }
        return response()->json($record);
    }

    public function get_list_by_buildingID($p, $building_id) {
        $user = Auth::user();
        if (Auth::check()) {
            $announcements = $this->get_announcement_list($p, 10, $building_id);
                                
            return response()->json($announcements);
        } else {
                    
        }
    }

    public function get_announcement_list($p, $amount = 10, $building_id) {
        $user = Auth::user();

        if (Auth::check()) {
            $announcements = new Building_Announcement();
            $announcements = $announcements::tenant_announcement($p, $amount, $building_id, $user);
            return $announcements;
        } else {
            return null;
        }
    }

    public function get_reservation_list_by_tenantID($p) {
        $user = Auth::user();
        if (Auth::check()) {
            $user_detail = $this->get_header_details($user->id);
            $reservation_list =  $this->get_reservation_list($user_detail->building_id, $user->id);
            
            return response()->json($reservation_list);
        } else {
                                            
        }
    }

    public function get_reservation_list($building_id, $user_id) {
        $reservation_list = new Tenant_Reservation();
        $reservation_list = $reservation_list::tenant_reservation_list(1, 10, $building_id, $user_id);
        return $reservation_list;

    }

    public function get_header_details($user_id) {
        $header_detail = new Tenant_Detail();
        $header_detail = $header_detail::get_header_details($user_id);
        return $header_detail;
        
    }

    public function tannouncement_list() {
        $user = Auth::user();
        if (Auth::check()) {
            $user_detail = $this->get_header_details($user->id);

            return view('tenant/announcement_list', compact('user_detail'));
        } else {

        }
    }

    public function tservice_list() {
        $user = Auth::user();
        if (Auth::check()) {
            $user_detail = $this->get_header_details($user->id);

            return view('tenant/service_list', compact('user_detail'));
        } else {

        }
    }

    public function treservation_list() {
        $user = Auth::user();
        if (Auth::check()) {
            $user_detail = $this->get_header_details($user->id);

            return view('tenant/reservation_list', compact('user_detail'));
        } else {

        }
    }

    public function create_booking() {
        $user = Auth::user();
        if (Auth::check()) {
            $user_detail = $this->get_header_details($user->id);

            $amenity = new Building_Amenity();
            $amenity = $amenity::create_booking_page($user_detail->building_id);

            return view('tenant/create_booking', compact('user_detail','amenity'));
        } else {

        } 
    }

    public function create_join_event(Request $request) {
        $user = Auth::user();
        $record = array();
        if (Auth::check()) {
            $bap = new Building_Announcement_Participant;

            $bap->ba_id =  request('aid');
            $bap->user_id = $user->id;
            $bap->message = '';
            $bap->status = 1;
            $bap->save();

            if ($bap->id > 0) {
                $record['success'] = true;
                $record['id'] = $bap->id;
            } else {
                $record['success'] = false;
                $record['msg'] = "We cannot process your request due to some technical problems, please try again later or contact your building manager, thanks.";
            }
            return response()->json($record);
        } else {

        }
        
    }

    public function rm_veh(Request $request) {
        $user = Auth::user();
        if (Auth::check()) {
            $ar = Tenant_Vehicle::where('id', '=', request('id'))->update(array('status' => 0));

            $record['success'] = true;
            $record['id'] = request('id');
            $record['action'] = 'rm_vehicle';
        } else {

        }
        return response()->json($record);
    }


    public function create_vehicle(Request $request) {
        $user = Auth::user();
        $record = array();
        if (Auth::check()) {
            $user_detail = $this->get_header_details($user->id);
            if (request('action') == 'add_veh') {
                $vehicle = new Tenant_Vehicle();
            } else {
                $vehicle = Tenant_Vehicle::find(request('id'));
            }
            $vehicle_id = $vehicle::create_vehicle($user->id, $request);

            
            $record['id'] = request('action') == 'add_veh' ? $vehicle_id : request('id');
            $record['action'] = request('action') == 'add_veh' ? 'create_vehicle' : 'update_vehicle';
            $record['success'] = $record['id'] > 0 ? true : false;

            return response()->json($record);
        } else {

        }
        
    }

    public function rm_pet(Request $request) {
        $user = Auth::user();
        if (Auth::check()) {
            $ar = Tenant_Pet::where('id', '=', request('id'))->update(array('status' => 0));

            $record['success'] = true;
            $record['id'] = request('id');
            $record['action'] = 'rm_pet';
        } else {

        }
        return response()->json($record);
    }


    public function create_pet(Request $request) {
        $user = Auth::user();
        $record = array();
        if (Auth::check()) {
            $user_detail = $this->get_header_details($user->id);
                if (request('action') == 'add_pet') {
                    $pet = new Tenant_Pet();
                } else {
                    $pet = Tenant_Pet::find(request('id'));
                }
                $pet_id = $pet::create_pet($user->id, $request);
                $record['id'] = request('action') == 'add_pet' ? $pet_id : request('id');
                $record['action'] = request('action') == 'add_veh' ? 'create_pet' : 'update_pet';
                $record['success'] = $record['id'] > 0 ? true : false;

            return response()->json($record);
        } else {

        }
        
    }


    public function create_report(Request $request) {
        $user = Auth::user();
        $record = array();
        if (Auth::check()) {
            $user_detail = $this->get_header_details($user->id);

            $title = request('title');
            $selected_case_type = request('selected_case_type');
            $selected_case_area = request('selected_case_area');
            $selected_case_cate = request('selected_case_cate');
            $selected_case_desc = request('selected_case_desc');
            $selected_case_img = request('selected_case_img');

            $bc = new Building_Case;
            $bc->tenant_id = $user->id;
            $bc->building_id = $user_detail->building_id;
            $bc->category_id = $selected_case_cate; 
            $bc->case_type = $selected_case_type;
            $bc->job_area = $selected_case_area;
            $bc->title = $title;
            $bc->content = $selected_case_desc;
            $bc->progress = 1;
            $bc->priority = 1;
            $bc->status = 1;
            $bc->save();

            if ($bc->id > 0) {
                $record['success'] = true;
                $record['id'] = $bc->id;
            } else {
                $record['success'] = false;
                $record['msg'] = "We cannot process your request due to some technical problems, please try again later or contact your building manager, thanks.";
                return response()->json($record);
            }


            if ($selected_case_img != null) {
                $fileName = time().'_'.$user_detail->building_id.'_'.$selected_case_img->getClientOriginalName();
                $path = public_path().'/uploads/building_cases/'.date('Y_m_d');
                File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
                $selected_case_img->move(public_path().'/uploads/building_cases/'.date('Y_m_d').'/', $fileName);
                
                $case_images = new Building_Case_Attachment;
                $case_images->case_id = $bc->id;
                $case_images->uploader_id = $user->id;
                $case_images->type = 1;
                $case_images->url = '/uploads/building_cases/'.date('Y_m_d').'/'.$fileName;
                $case_images->name = $fileName;
                $case_images->size = $selected_case_img->getClientSize();
                $case_images->ext = $selected_case_img->getClientOriginalExtension();
                $case_images->status = 1;
                $case_images->save();

                if ($case_images->id > 0) {
                    $record['img_id'] = $case_images->id;
                } else {
                    $record['success'] = false;
                    $record['msg'] = "We cannot process your request due to some technical problems, please try again later or contact your building manager, thanks.";
                    return response()->json($record);
                }
            }
            return response()->json($record);

        } else {

        }
        
    }


    public function create_booking_request(Request $request) {
        $user = Auth::user();
        $record = array();
        if (Auth::check()) {
            $user_detail = $this->get_header_details($user->id);
            date_default_timezone_set('Australia/Brisbane');
            
            $building_id = $user_detail->building_id;
            $amenity = request('selected_amenity');
            $date = request('selected_date');
            $time = request('selected_time');
            $booking_type = request('booking_type');
            $number_plate = request('number_plate');
            $car_detail = request('car_detail');

            $time = explode(" - ",$time);
            $time[0] = str_replace("AM","", $time[0]);
            $time[0] = str_replace("PM","", $time[0]);
            $time[1] = str_replace("AM","", $time[1]);
            $time[1] = str_replace("PM","", $time[1]);

            $start_time = $date ." ".$time[0];
            $end_time = $date ." ".$time[1];

            $start_time = date('Y-m-d H:i:s', strtotime($start_time));
            $end_time = date('Y-m-d H:i:s', strtotime($end_time));
            
        
            if ($booking_type == 3) {
                
                $ar = Amenity_Reservation::where('building_id', '=',  $user_detail->building_id)
                                        ->where('status', '=', 1)
                                        ->where('type', '=', 3)
                                        ->where('amenity_id', '=', $amenity)
                                        ->where('progress', '=', 1)
                                        ->where(function($query) use ($start_time, $end_time) {
                                            $query->whereBetween('booking_time_from', [$start_time, $end_time])
                                                    ->orWhere(function ($query) use ($start_time, $end_time) {
                                                        $query->whereBetween('booking_time_to', [$start_time, $end_time]);
                                                    });
                                            })      
                                        ->get()->toArray();
                                        
            } else {
                $ar = null;
            }
            if (count($ar) == 0 ) {
                $booking = new Amenity_Reservation;
                $booking->building_id = $building_id;
                if ($booking_type == 3) {
                    $booking->amenity_id = $amenity;
                }
                $booking->tenant_id = $user->id;
                $booking->type = $booking_type;
                $booking->booking_time_from = $start_time;
                $booking->booking_time_to = $end_time;
                $booking->booking_recurring = 0;
                $booking->progress = 0;
                if ($booking_type == 5) {
                    $booking->details = $number_plate . '|' . $car_detail;
                } 
                $booking->status = 1;
                $booking->save();

                if ($booking->id > 0) {
                    $record['success'] = true;
                    $record['id'] = $booking->id;

                    $building_admin = new Building_Admin();
                    $pusher = $building_admin::getBuildingAdminByBID($user_detail->building_id);
                    
                    // $notification = new MgrNotificationController;
                    
                    // $notification->push_msg_mail('create_booking', $pusher[0]['id'], $booking, true);
                    
                } else {
                    $record['success'] = false;
                    $record['msg'] = "We cannot process your request due to some technical problems, please try again later or contact your building manager, thanks.";
                }

            } else {
                $record['success'] = false;
                
                $record['msg'] = "The amenity has already been booked, please choose another time, thanks.";
            }
            
        } else {

        }
        return response()->json($record);
    }

    public function make_a_report() {
        $user = Auth::user();
        if (Auth::check()) {
            //$mgrsetting = new MgrSettingController;
            $mgrcomplaint = new ComplaintController;
            $user_detail = $this->get_header_details($user->id);

            $cate_id = 3;
            $case_type = $mgrcomplaint->get_case_type(); 
            $case_area = $mgrcomplaint->get_case_area();
            $case_cate = Building_Category::select('id','parent_id','cate_id','name','description')
                                                ->where('status', '=', 1)
                                                ->where(function($query) use ($cate_id) {
                                                    $query->where('parent_id', $cate_id)
                                                    ->orWhere('cate_id', $cate_id);
                                                })
                                                ->where('building_id', '=', $user_detail->building_id)
                                                ->orderBy('cate_id', 'ASC')
                                                ->get()->toArray();

            return view('tenant/make_a_report', compact('user_detail', 'case_type', 'case_area','case_cate'));
        } else {

        }
    }

    public function tuser_acc() {
        $user = Auth::user();
        if (Auth::check()) {
            $user_detail = $this->get_header_details($user->id);

            $pets = Tenant_Pet::where('status', 1)->where('tenant_id', $user->id)->get()->toArray();
            $vehicles = Tenant_Vehicle::where('status', 1)->where('tenant_id', $user->id)->get()->toArray();

            return view('tenant/user_acc', compact('user_detail','vehicles','pets'));
        } else {

        }
    }

    
    public function insert_webpush_info(Request $request) {
        $user = Auth::user();
        $record = array();
        if (Auth::check()) {
            $user_detail = $this->get_header_details($user->id);

            $record['p256dh'] = $request->subscription['keys']['p256dh'];
            $record['auth'] = $request->subscription['keys']['auth'];
            $record['uniqueid'] = $request->uniqueid;
            $record['endpoint'] = $request->subscription['endpoint'];
            $record['expirationtime'] = $request->subscription['expirationTime'];

            $user = Auth::user();
            $user->updatePushSubscription( $record['endpoint'], $record['p256dh'], $record['auth']);

            // if ($request->action == 'new') {
            //     $push = new push_subscriptions;
            // } else {
            //     $push = push_subscriptions::where('user_id' , '=', $user->id)->first();
            // }

            // $push->user_id = $user->id;
            // $push->uniqueid = $request->uniqueid;
            // $push->endpoint = $request->subscription['endpoint'];
            // $push->auth = $request->subscription['keys']['auth'];
            // $push->p256dh = $request->subscription['keys']['p256dh'];
            // $push->expiredTime = $request->subscription['expirationTime'];
            // $push->save();

            // if ($push->id > 0) {
            //      $record['success'] = true;
            //     $record['id'] = $push->id;
            // } else {
            //     $record['success'] = false;
            //     $record['msg'] = "We cannot process your request due to some technical problems, please try again later or contact your building manager, thanks.";
            // }
        }

    }

    // tenant-documents parts

    /**
     * Document main page
     * @Return view -> tenants.documents
     */
    public function documents_main_page() {
        
        $user = Auth::user();
        $user_detail = $this->get_header_details($user->id);

        if (Auth::check()) {
            return view('tenant.documents', compact('user_detail'));
        } else {
            return;
        }
    }

    public function upload_documents(Request $request) {
        $record = [];
        $user = Auth::user();
        $user_detail = $this->get_header_details($user->id);
    
        if (Auth::check()) {
            if(Input::method() == 'POST') {
                // echo $request ->hasFile('upload_file');
                
            
                if ($request -> hasFile('upload_file') && $request -> file('upload_file') -> isValid()) {
                    
                    $model = new Unit_Document();
                    $result = $model::upload_file($user_detail, $request);
                    
                    if ($result) {
                        $record['success'] = true;
                        $record['id'] = $result->id;
                    } else {
                        $record['success'] = false;
                        $record['msg'] = 'The file is not uploaded correctly, please try again or contact your building manager.';
                    };
                    
                    
                } else {
                    $record['success'] = false;
                    $record['msg'] = "not allow";
                }
                return response()->json($record);
            }
        }
        
        
    }

    
    /**
     * Get the list of files which were sent by admin.
     */
    public function get_sent_docs_list() {

        $user = Auth::user();
        $user_detail = $this->get_header_details($user->id);

        if (Auth::check()) {
            $model = new Unit_Document();
            $model = $model::get_sent_file_list($user->id);
            return $model;
        } else {

            return null;
        }

    }


    /**
     * Get the list of files which uploaded by tenants.
     */
    public function get_upload_docs_list($p, $amount = 50) {

        $user = Auth::user();
        $user_detail = $this->get_header_details($user->id);

        if (Auth::check()) {

            $model = new Unit_Document();
            $model = $model::get_uploaded_file_list($user->id);
            return $model;
        } else {

            return null;
        }
    }

    /**
     * Make the file invisble for user.
     */
    public function delete_docs(Request $request) {
        $user = Auth::user();
        $user_detail = $this->get_header_details($user->id);
        
        if (Auth::check()) {
            $model = new Unit_Document();
            $file_id =  $request->file_id;
            
            if ($model::where('id', '=', $file_id) ->exists()) {
                $model = $model::shield_file_by_id($user->id, $file_id);
                
                $record['success'] = true;
                $record['id'] = $request->file_id;
                return response() -> json($record);
            } else {
                $record['success'] = false;
                $record['id'] = $request->file_id;
                return response() -> json($record);
            }
        } else {
            return;
        }
    }

    


    public function push(){
        $user = Auth::user();
        $record = array();

        if (Auth::check()) {
            Notification::send(User::find($user->id),new PushDemo);
        }
    }


    public function business_directory() {
        $user = Auth::user();
        $record = array();
        if (Auth::check()) {
            $user_detail = $this->get_header_details($user->id);

            
        }
    }


    // tenant-parcel

    /**
     * Getting the parcel main page.
     */
    public function parcel_main_page() {
        $user = Auth::user();
        $user_detail = $this->get_header_details($user->id);

        if (Auth::check()) {
            return view('tenant.parcel', compact('user_detail'));
        }
    }

    /**
     * Getting new parcel list by user id.
     */
    public function get_new_parcel() {
        
        $user = Auth::user();
        $user_detail = $this->get_header_details($user->id);
        
        if (Auth::check()) {
            $model = new Building_couriers();
            $list = $model::get_new_parcel_list($user->id);
            return $list;
        }
    }

    /**
     * Getting parcel history list by user id.
     */
    public function get_parcel_history() {
        $user = Auth::user();
        $user_detail = $this->get_header_details($user->id);

        if (Auth::check()) {
            $model = new Building_couriers();
            $model = $model::get_parcel_history_list($user->id);
            return $model;
        }
        
    }

    /**
     * Creating a unique code for each parcel if its code is not existed yet and return it;
     * Or returning directly if it is already existed.
     */
    public function create_unique_code(Request $request) {
        
        $record = [];
        $user = Auth::user();
        $user_detail = $this->get_header_details($user->id);

        if (Auth::check()) {
            if ($request->generate_code == 'y') {
                $parcel_id = $request->parcel_id;
            
                $generate_code = new Building_couriers();
                $result = $generate_code::create_unique_code($user->id, $parcel_id);

                if ($result) {
                    $model = new Building_couriers();
                    $record['data'] = $model::get_code($user->id, $parcel_id);
                    $record['success'] = true;
                } else {
                    $record['success'] = false;
                    $record['msg'] = 'The file is not uploaded correctly, please try again or contact your building manager.';
                }
                

            } else {
                $model = new Building_couriers();
                $parcel_id = $request->parcel_id;
                $record['data'] = $model::get_code($user->id, $parcel_id);
                $record['success'] = true;
            }
            
            return response()->json($record);
        }
    }

    
    // Maintenance

    public function maintenance_list() {
        $user = Auth::user();
        if (Auth::check()) {

            $user_detail = $this->get_header_details($user->id);
            // $maintenance_list = DB::table('case')
            //                 ->select('id','title','created_at','content','progress','status',
            //                 DB::raw("
            //                 (CASE
            //                     WHEN cases.progress = '1' THEN 'New'
            //                     WHEN cases.progress = '2' THEN 'In Progress'
            //                     WHEN cases.progress = '3' THEN 'Completed'
            //                     WHEN cases.progress = '4' THEN 'Closed'
            //                     WHEN cases.progress = '5' THEN 'Awaiting Contractor'
            //                     WHEN cases.progress = '6' THEN 'Awaiting Approval'
            //                     WHEN cases.progress = '7' THEN 'Commitee Approval'
            //                     WHEN cases.progress = '8' THEN 'Admin Approval'
            //                     WHEN cases.progress = '9' THEN 'Building Manager Discretion'
            //                 END) AS progress_txt
            //                 "))
            //                 ->where('cases.status','=',1)
            //                 ->where('cases.building_id', '=', $user_detail['building_id'])
            //                 ->orderBy('cases.created_at','desc')
            //                 ->paginate(1);


            return view('tenant/maintenance_list',  compact('user_detail'));
        }
         else {

        }
    }

    public function maintenance_list_detail($id){
        $user = Auth::user();
        $user_detail = $this->get_header_details($user->id);
        //print_r($id);
        $detail = new Tenant_Case();
        $detail = $detail::maintenance_list_detail($id);
   
        $progress_detail_log = new Case_Progress();
        $progress_detail_log = $progress_detail_log::case_progress($id);
       
        $attachment = new Case_Attachment();
        $attachment = $attachment::case_attachment($id);
        

        return view('tenant/maintenance_list_detail',compact('user_detail','detail','progress_detail_log','attachment'));
    }


    public function maintenance_list_content($page = 1, $type = 'a',$id){
        $user = Auth::user();
        if (Auth::check()) {
            $user_detail = $this->get_header_details($user->id);

            $maintenance_list = new Tenant_Case();
            $maintenance_list = $maintenance_list::maintenance_list_content(1, 'a', $id, $user_detail);

            return response()->json($maintenance_list);
        }
         else {

        }
    }



//  notification

    public function get_notification($month){
        $user = Auth::user();
        if (Auth::check()){
            $user_detail = $this->get_header_details($user->id);
            $notes = new Building_Notification();
            $notes = $notes::get_building_notification($user_detail['building_id']);

            return response()->json($notes);
        
         }
    }

    public function address_book_page() {
        $user = Auth::user();
        if (Auth::check()){
            $user_detail = $this->get_header_details($user->id);
            return view('tenant/address_book', compact('user_detail'));
        }
    }

    public function get_address_book() {
        $user = Auth::user();
        if (Auth::check()){
            $user_detail = $this->get_header_details($user->id);
            $building_id = $user_detail['building_id'];
            $model = new Tenant_address_book();
            $model = $model::get_agency_phone_number($building_id);
            return $model;
        } 
    }

}