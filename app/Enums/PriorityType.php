<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static Low
 * @method static static Medium
 * @method static static High
 * @method static static Urgent
 */
final class PriorityType extends Enum
{   
    // Starting from 1 is because the previous setting
    const Low = 1;
    const Medium = 2;
    const High = 3;
    const Urgent = 4;
}
