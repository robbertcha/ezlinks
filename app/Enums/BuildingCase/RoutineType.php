<?php

namespace App\Enums\BuildingCase;

use BenSampo\Enum\Enum;

/**
 * @method static static None
 * @method static static Daily
 * @method static static Weekly
 * @method static static Fortnightly
 * @method static static Monthly
 * @method static static HalfYearly
 * @method static static Yearly
 */
final class RoutineType extends Enum{
    const NONE = 0;
    const DAILY = 1;
    const WEEKLY = 2;
    //const Fortnightly = 3;
    const MONTHLY = 4;
    //const HalfYearly = 5;
    const YEARLY = 6;
}
