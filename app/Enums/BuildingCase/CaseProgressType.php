<?php

namespace App\Enums\BuildingCase;

use BenSampo\Enum\Enum;

/**
 * @method static static NewCase
 * @method static static InProgress
 * @method static static Completed
 * @method static static Closed
 * @method static static AwaitingContractor
 * @method static static AwaitingApproval
 * @method static static CommiteeApproval 
 * @method static static AdminApproval 
 * @method static static BuildingManagerDiscretion
 */
final class CaseProgressType extends Enum
{   
    // Start from 1 is because of the previous setting, otherwise start from 0.
    const NewCase = 1;
    const InProgress = 2;
    const Completed = 3;
    const Closed = 4;
    const AwaitingContractor = 5;
    const AwaitingApproval = 6;
    const CommiteeApproval = 7;
    const AdminApproval = 8;
    const ManagerDiscretion = 9;

}
