<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static Job
 * @method static static Reservation
 * @method static static CarParking
 */
final class SchedulerType extends Enum{
    const Job = 1;
    const Reservation = 3;
    const Parking = 4;
}
