<?php
 namespace App\Listeners;

use App\Log_Notifications;
use Illuminate\Mail\Events\MessageSent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Support\Facades\Log;

class LogSentMessage
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(MessageSent $event)
    {
        // The Swift_Message has a __toString method so you should be able to log it ;)
        // print_r($event->message->getHeaders()->get('From')->getFieldBody());
        $ln = new Log_Notifications();
        $ln->type = 'mail';
        $ln->recipient = $event->message->getHeaders()->get('To')->getFieldBody();
        $ln->action = 'Announcement Email';
        $ln->title = $event->message->getHeaders()->get('Subject')->getFieldBody();
        $ln->content = $event->message->getBody();
        $ln->save();

      
    }
}
