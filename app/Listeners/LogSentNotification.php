<?php
 namespace App\Listeners;

use App\Log_Notifications;
use Illuminate\Notifications\Events\NotificationSent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Support\Facades\Log;

class LogSentNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(NotificationSent $event)
    {
          //print_r( $event);
            // The Swift_Message has a __toString method so you should be able to log it ;)
            // print_r($event->message->getHeaders()->get('From')->getFieldBody());
            $ln = new Log_Notifications();
            $ln->type = 'notification';
            $ln->building_id = $event->notification->object_arr->building_id;
            $ln->action = $event->notification->action;
            $ln->recipient = $event->notifiable->id;
            $ln->webpush_id = $event->notification->id;
            $ln->title = $event->notification->object_arr->title;
            $ln->content = $event->notification->object_arr->content;
            $ln->save();

      
    }
}
