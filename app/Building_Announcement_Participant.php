<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Building_Announcement_Participant extends Model
{
    //
    protected $table = 'building_announcement_participant';
    protected $dates = ['created_at'];


}
