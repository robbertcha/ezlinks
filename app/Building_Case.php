<?php

namespace App;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use App\Contractor;
use App\Building_Log;

// Service
use App\Services\Mgr\Cases\CaseService;

class Building_Case extends Model
{
    //
    protected $table = 'cases';

    public function get_case_detail_by_tid ($tid) {
        $user = Auth::user();
        if (Auth::check()) {
            $building_id = $user['building_id'];

            $case = $this->select("cases.tenant_id","cases.building_id","cases.case_type","cases.content", DB::raw("
                        (CASE 
                            WHEN cases.job_area = '1' THEN 'Common area - Asset'
                            WHEN cases.job_area = '2' THEN 'Common area - Non-Asset'
                            WHEN cases.job_area = '3' THEN 'Private Lot'
                        END) AS job_area
                        "), DB::raw("
                        (CASE 
                            WHEN cases.progress = '1' THEN 'New'
                            WHEN cases.progress = '2' THEN 'In Progress'
                            WHEN cases.progress = '3' THEN 'Completed'
                            WHEN cases.progress = '4' THEN 'Closed'
                            WHEN cases.progress = '5' THEN 'Awaiting Contractor'
                            WHEN cases.progress = '6' THEN 'Awaiting Approval'
                            WHEN cases.progress = '7' THEN 'Commitee Approval'
                            WHEN cases.progress = '8' THEN 'Admin Approval'
                            WHEN cases.progress = '9' THEN 'Building Manager Discretion'
                        END) AS progress
                        "))
                        ->where('cases.building_id', '=', $building_id)
                        ->where('cases.tenant_id', '=', $tid)
                        ->where('cases.status', '=', 1)
                        // ->toSql();
                        ->get()->toArray();


             foreach ($case as $key => $val) {
                $case[$key]['content'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1',html_entity_decode($case[$key]['content']));
                $case[$key]['content'] = substr(strip_tags($case[$key]['content']), 0, 80);
             }

             return $case;
        } else {

        }
    }

    public static function get_case_by_date($building_id, $date_from, $date_to) {
        return self::select('category_id', 'case_type', 'job_area', 'title', 'content', 'progress', 'priority', 'created_at', 'updated_at')
                    ->where('building_id', '=', $building_id)
                    ->where(function($query) use ($date_from, $date_to) {
                        $query->whereBetween('created_at',[$date_from, $date_to]);
                    })
                    ->where('status', '=', 1)
                    ->orderBy('created_at', 'asc')
                    ->get()->toArray();
    }

    public function contractors() {
        return $this->hasMany(contractor::class);
    }


    public static function get_case_type() {
        $case = (new Building_Case)->getTable();
        $case_types = DB::select(DB::raw('SHOW COLUMNS FROM `'.$case.'` WHERE Field = "case_type"'))[0]->Type;;
        preg_match('/^enum\((.*)\)$/', $case_types, $matches);
        $enum = array();
        foreach(explode(',', $matches[1]) as $value){
            $v = trim( $value, "'" );
            $q['value'] = $v;
            $q['title'] = ucfirst(str_replace('_', ' ', $v));
            $enum[] = $q;
        }

        return $enum;
    }

    public function get_case_progress() {
        return Array(
                Array ('title' => 'New','value' => '1'),
                Array ('title' => 'In Progress','value' => '2'),
                Array ('title' => 'Completed','value' => '3'),
                Array ('title' => 'Closed','value' => '4'),
                Array ('title' => 'Awaiting Contractor','value' => '5'),
                Array ('title' => 'Awaiting Approval','value' => '6'),
                Array ('title' => 'Commitee Approval','value' => '7'),
                Array ('title' => 'Admin Approval','value' => '8'),
                Array ('title' => 'Building Manager Discretion','value' => '9')
        );
    }
    
    public function get_case_area() {
        return Array(
                Array ('title' => 'Common Area - Asset','value' => '1'),
                Array ('title' => 'Common Area - Non Asset','value' => '2'),
                Array ('title' => 'Private Lot','value' => '3')
        );
    }

}
