<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Building_Case_Invoice extends Model
{
    //
    protected $table = 'case_invoice';

    public function get_case_invoices($cid) {
        return self::select("case_invoice.id", "c.id as cid", "c.comp_name", "case_invoice.invoice_no", 'case_invoice.invoice_value','case_invoice.invoice_fee','case_invoice.total_value','case_invoice.invoice_doc', DB::raw("DATE_FORMAT(case_invoice.created_at, '%d %M') as created_at"))
                                    ->join('contractor as c',function($join){
                                        $join->on('c.id','=','case_invoice.contractor_id');
                                    }, null,null,'left')
                                    ->where('case_invoice.case_id', '=', $cid)
                                    ->where('case_invoice.status', '=', 1)
                                    ->get()->toArray();
    }
}
