<?php

namespace App;

// use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class MgrAdmin1 extends Authenticatable
{
    use Notifiable;
    // The authentication guard for admin
    protected $guard = 'building_admin';
    public $table = "building_admin";
    protected $fillable = [
        'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function service() {
        return $this->hasMany('App\Service');
    }

    public function getClientIdAttribute(){

        return $client_id = 123456;
        
    }
}
