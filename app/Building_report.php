<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Dompdf\Dompdf;
use PDF;

class Building_report extends Model
{
    //
    protected $table = 'building_report';

    protected $fillable = ['id', 'file_path', 'building_id', 'status'];

    public static function save_contents($data) {

        $building_id = $data['building_id'];
        // $pdf_name = 'building' . $building_id . "_" . date("Ymdhis") . "_report". ".pdf";
        // $building_report_dir_path = './uploads/building_lib/' . $building_id . '/report';

        $file_path = $data['file_path'];

        // $report_file = $data['report_file'];
        // $file_name = $data['file_name'];
        // $building_report_dir_path = $building_report_dir_path  . '/' .$file_name;
        if (file_exists($file_path)) {

            $store = new Building_report;
            $store['building_id'] = $building_id;
            $store['status'] = 1;
            $store['file_path'] = $data['file_path'];
            $store->save();
            return $store;
        }  
    }

    public static function get_file_path($id) {
        return self::select('file_path')
                    ->where('id', '=', $id)
                    ->where('status', '=', 1)
                    ->get()->pluck('file_path');
    }

    public static function delete_file($building_id, $file_name) {
        $building_report_dir_path = 'uploads/building_lib/' . $building_id . '/report';
        $file_path = $building_report_dir_path . '/' . $file_name;
       
        unlink($file_path);
        return true;
    }

}