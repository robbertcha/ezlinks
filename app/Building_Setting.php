<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Building_Setting extends Model {
    public $table = "building_setting";
    protected $fillable = ['slug', 'description', 'url', 'title', 'setting_table', 'setting_field', 'setting_method'];

    public static function getSettingContent() {
        return self::select("id",'slug', 'description', 'url', 'title')
                    ->where('status', '=', 1)
                    ->get()->ToArray();
    }

    public static function getSettingMethod($slug) {
        return self::select("setting_method", 'setting_table', 'setting_field')
                     ->where('slug', '=', $slug)
                     ->where('status', '=', 1)
                     ->get()->ToArray(); 
    }


}