<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Building_Inspection_History extends Model {
    //
    protected $table = 'building_inspection_history';

     /**
     * The "booting" method of the model.
     * Only search the result when its status is 1.
     * @return void
     */
    protected static function boot() {
        parent::boot();
        static::addGlobalScope('status', function(Builder $builder) {
            $builder->where('status', '=', 1);
        });
    }

    // Database: 1-many connection, 1 item only belongs to 1 area
    public function area() {
        return $this->belongsTo('App\Building_Inspection_Area','bi_id');
    }

}
