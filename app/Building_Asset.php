<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Building_Asset extends Model
{
    //
    protected $table = 'building_assets';

    /**
     * The "booting" method of the model.
     * Only search the result when its status is 1.
     * @return void
     */
    protected static function boot() {
        parent::boot();
        static::addGlobalScope('status', function(Builder $builder) {
            $builder->where('status', '=', 1);
        });
    }

    // Database: 1-many connection, 1 item only belongs to 1 area
    public function building() {
        return $this->belongsTo('App\Building','building_id');
    }


    public static function getListByBuildingID($building_id) {
        return self::select('id','name', 'location', 'make', 'model', 'note', 'photo', 'created_at')
                    ->where('building_id', '=', $building_id)
                    ->where('status', '=', 1)
                    ->orderBy('created_at', 'asc')
                    ->get()->toArray();
    }

    public static function getDetailByID($id) {
        return self::select('id','name', 'location', 'make', 'model', 'note', 'photo', 'created_at')
                    ->where('id', '=', $id)
                    ->where('status', '=', 1)
                    ->orderBy('created_at', 'asc')
                    ->get()->first();
    }
}
