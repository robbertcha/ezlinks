<?php

namespace App;
use DateTime;
use DateInterval;
use DatePeriod;
use \Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Building_Inspection_Area extends Model {
    //
    protected $table = 'building_inspection_area';
    protected $fillable = ['id', 'building_id', 'area_name', 'frequency', 'status'];

    /**
     * The "booting" method of the model.
     * Only search the result when its status is 1.
     * @return void
     */
    protected static function boot() {
        parent::boot();

        static::addGlobalScope('status', function(Builder $builder) {
            $builder->where('status', '=', 1);
        });
    }

    // Database: 1-many connection, 1 area can has many inspection items
    public function items() {
        return $this->hasMany('App\Building_Inspection_Item','bi_area_id', 'id');
    }


    // Database: 1-many connection, 1 area can has many inspection items
    public function history() {
        return $this->hasMany('App\Building_Inspection_History','bi_id', 'id');
    }

    /**
     * Get single area by id
     * @param int $id area id
     * @return object area detail
     */
    public function getArea($id) {
        return self::where('id', $id)->first();
    }

    /**
     * Get inspection items by id
     * @param int $id building inspection area id
     * @return array inspection items
     */
    public static function getItemsByID($id) {
        return self::find($id)->items->toArray();
    }

    /**
     * Get inspection history by id
     * @param int $id building inspection area id
     * @return array history
     */
    public static function getHistoryByID($id) {
        return self::find($id)->history->toArray();
    }

    /**
     * Create or update inspection area
     * @param request inspection 
     * @return id
     */
    public function saveInspectionArea($request) {
        return self::updateOrCreate(['id'=>isset($request['id']) ? $request['id'] : null], $request);
    }

    /**
     * Save bulks of items
     * @param int $bi_id inspection area id
     * @param array $item inspection items
     * @return boolean
     */
    public function saveNewItemsByAreaID($bi_id, $items) {
        return $this::find($bi_id)->items()->createMany($items);
    }

    /**
     * Get inspections by building id
     * @param int $building_id 
     * @return array
     */
    public static function get_building_inspections_by_building($building_id, $get_all = false) {
        $q = [];
        //if ($get_all == false) {
            $q = DB::table('amenity_reservation as ar')
                    ->where('ar.status', '=', 1)
                    ->where('ar.type', '=', 1)
                    ->where('ar.booking_end', '!=', null)
                    ->where('ar.building_id', '=', $building_id)
                    ->where('bi_id','>', 0)
                    ->distinct()
                    ->pluck('bi_id')->toArray();
        //}

        $bi = self::select('id','area_name as title', 'frequency','created_at', 
                        DB::raw("(
                            SELECT ifnull(
                                GROUP_CONCAT( 
                                    CONCAT(bii.id,'-',bii.title)
                                SEPARATOR '|'),
                            '') as items
                            from building_inspection_item as bii
                            where bii.bi_area_id = building_inspection_area.id 
                            and building_inspection_area.status = 1
                            and bii.status = 1
                        ) as items") 
                    )
                    ->where('status', '=', 1)
                    ->where('building_id', '=', $building_id)
                    // ->when($get_all == false, function ($query) use ($i)  {
                    //     $query->whereNotIn('id', $i);
                    // })
                    // ->when(isset($_REQUEST['input_search']), function ($query) {
                    //     $query->where('title', 'like', '%' . $_REQUEST['input_search'] . '%');
                    // })
                    ->get()->ToArray();

        for ($i = 0; $i < count($bi);$i++) {
            if (in_array($bi[$i]['id'], $q) ) {
                $bi[$i]['set'] = true;
            } else {
                $bi[$i]['set'] = false;
            }
        }
        return $bi;
    }




    // remove
    public static function update_inpection_area($inspections, $building_id) {
        date_default_timezone_set('Australia/Brisbane');
        if ($inspections['id'] > 0) {
            $bia = Building_Inspection_Area::find($inspections['id']);
        } else {
            $bia = new Building_Inspection_Area;
            $bia->status = 1;
        }

        if (strtotime($inspections['starttime']) > 0) {
            date('H:i A', strtotime($inspections['starttime']));
        } else {
            $inspections['starttime'];
        }


        if (strtotime($inspections['endtime']) > 0) {
            date('H:i A', strtotime($inspections['endtime']));
        } else {
            $inspections['endtime'];
        }

        
        $bia->start_date = $start_date = $date_reservation = date('Y-m-d', strtotime($inspections['startdate']));
        $start_time = date('H:i:s', strtotime($start_date." ".$inspections['starttime']));
        $end_time = date('H:i:s', strtotime($start_date." ".$inspections['endtime']));
        $bia->time_from = $date_reservation.' '.$start_time;
        $bia->time_to = $date_reservation.' '.$end_time;
        switch ($inspections['frequency']) {
            case "none":
                $bia->frequency = 0;
                break;
            case "DAILY":
                $bia->frequency = 1;
                break;
            case "WEEKLY":
                $bia->frequency = 2;
                break;
            case "MONTHLY":
                $bia->frequency = 3;
                break;
            case "YEARLY":
                $bia->frequency = 4;
                break;
            default:
            $bia->frequency = 0;
        }

        $bia->building_id = $building_id;
        $bia->area_name = $inspections['area'];
        $bia->save();

        return $bia->id;
    }

    public static function checkIsAValidDate($myDate){
        return (bool)strtotime($myDate);
    }

    public static function get_inspection_area($bi_area_id) {
        return self::select('id', 'area_name', 'frequency')
                    -> where('id', '=', $bi_area_id)
                    -> where('status', '=', 1)
                    -> orderBy('created_at', 'desc')
                    ->get()->first();
    }






    public static function get_building_inspections_by_date($building_id, $date_from, $date_to) {
        return self::select('id','area_name as title','created_at', 
                        DB::raw("(
                            SELECT ifnull(
                                GROUP_CONCAT( 
                                    CONCAT(bii.id,'-',bii.title)
                                SEPARATOR '|'),
                            '') as items
                            from building_inspection_item as bii
                            where bii.bi_area_id = building_inspection_area.id 
                            and building_inspection_area.status = 1
                            and bii.status = 1
                        ) as items") 
                    )
                    ->where('status', '=', 1)
                    ->where('building_id', '=', $building_id)
                    ->when(isset($_REQUEST['input_search']), function ($query) {
                        $query->where('title', 'like', '%' . $_REQUEST['input_search'] . '%');
                    })
                    ->get()->ToArray();
    }



    public function update_next_date($building_id) {
        date_default_timezone_set('Australia/Brisbane');
        // $update_count = self::where('status', '=', 1)
        //                     ->where('building_id', '=', $building_id)
        //                     ->where(function($query) {
        //                         $query->where('next_date',  null)
        //                         ->orWhere('next_date', '')
        //                         ->orWhereDate('next_date', '<', Carbon::now());
        //                     })->count();
                            
        //if ($update_count > 0) {
            $bi = self::select('id','area_name as title',DB::raw("(CASE 
                                            WHEN  frequency = '' THEN 'NONE'
                                            WHEN  frequency = '0' THEN 'NONE'
                                            WHEN  frequency = '1' THEN 'DAILY'
                                            WHEN  frequency = '2' THEN 'WEEKLY'
                                            WHEN  frequency = '3' THEN 'MONTHLY'
                                            WHEN  frequency = '4' THEN 'YEARLY'
                                         END) AS frequency"), 
                                         DB::raw("DATE_FORMAT(building_inspection_area.start_date,'%Y-%m-%d') as start_date"), 
                                         DB::raw("DATE_FORMAT(building_inspection_area.time_from,'%h:%i:%s') as time_from"), 
                                         'next_date')
                        ->where('status', '=', 1)
                        ->where('building_id', '=', $building_id)
                        ->where('frequency', '>', 0)
                        // ->where(function($query) {
                        //     $query->where('next_date',  null)
                        //             ->orWhere('next_date', '')
                        //             ->orWhereDate('next_date', '<', Carbon::now());
                        // })
                        ->get()->ToArray();
                        
            for ($i = 0; $i < count($bi); $i++) {
                $bi[$i]['next_date'] = $this->calculate_next_date($bi[$i]);
                
                // update
                self::where('id', $bi[$i]['id'])->update(['next_date'=> $bi[$i]['next_date']]);
            }

            return $bi;
        //}
    }

    
    // public function calculate_next_date($bi) {
    //     if ($bi['frequency'] == 'WEEKLY') {
    //         $total_distance = floor((time() - strtotime($bi['start_date']." ".$bi['time_from']))/(86400*7));
                
    //         $last_date =  strtotime("+".$total_distance." weeks", strtotime($bi['start_date']." ".$bi['time_from']));
    //         return date("Y-m-d H:i:s", strtotime("+1 week", $last_date));
    //     } else if ($bi['frequency'] == 'DAILY') {
    //         $total_distance = floor((time() - strtotime($bi['start_date']." ".$bi['time_from']))/(86400));
                
    //         $last_date =  strtotime("+".$total_distance." days", strtotime($bi['start_date']." ".$bi['time_from']));
    //         return date("Y-m-d H:i:s", strtotime("+1 day", $last_date));
    //     } else if ($bi['frequency'] == 'YEARLY') {
    //         $total_distance = floor((time() - strtotime($bi['start_date']." ".$bi['time_from']))/(86400*365));
                
    //         $last_date =  strtotime("+".$total_distance." years", strtotime($bi['start_date']." ".$bi['time_from']));
    //         return date("Y-m-d H:i:s", strtotime("+1 year", $last_date));
    //     } else if ($bi['frequency'] == 'MONTHLY') {
    //         $start    = (new DateTime($bi['start_date']))->modify('first day of this month');
    //         $end      = (new DateTime(date('Y-m-d')))->modify('first day of next month');
    //         $interval = DateInterval::createFromDateString('1 month');
    //         $mon_period = new DatePeriod($start, $interval, $end);

    //         $count = 0;
    //         foreach ($mon_period as $dt) {
    //             $count++;
    //         }

    //         $current_mon = date("Y-m-d H:i:s", strtotime("+".($count-1)." month", strtotime($bi['start_date']." ".$bi['time_from'])));
    //         $today_time = strtotime(date("Y-m-d H:i:s"));
    //         $expire_time = strtotime($current_mon);
    //         if ($expire_time < $today_time) { 
    //             $current_mon = date("Y-m-d H:i:s", strtotime("+".$count." month", strtotime($bi['start_date']." ".$bi['time_from'])));
    //          }

    //          return  $current_mon;
    //     } else {
    //         return  null;
    //     }
    // }

    public function update_next_date_by_id($bia_arr) {
        return self::where('id', $bia_arr['id'])->update(['next_date'=> $bia_arr['next_date']]);
    }


    public function update_next_date_by_date($id, $start_time, $end_time) {
        $bi = self::select('id','area_name as title',DB::raw("(CASE 
                        WHEN  frequency = '' THEN 'NONE'
                        WHEN  frequency = '0' THEN 'NONE'
                        WHEN  frequency = '1' THEN 'DAILY'
                        WHEN  frequency = '2' THEN 'WEEKLY'
                        WHEN  frequency = '3' THEN 'MONTHLY'
                        WHEN  frequency = '4' THEN 'YEARLY'
                    END) AS frequency"), 
                    DB::raw("DATE_FORMAT(building_inspection_area.start_date,'%Y-%m-%d') as start_date"), 
                    DB::raw("DATE_FORMAT(building_inspection_area.time_from,'%h:%i:%s') as time_from"), 
                    'next_date')
                ->where('status', '=', 1)
                ->where('id', '=', $id)
                ->get()->first();

        // $bi['next_date'] = $this->calculate_next_date($bi);
        if ($bi['frequency'] == 'WEEKLY') { 
            $bi['next_date'] = date("Y-m-d H:i:s", strtotime("+1 week", strtotime($start_time)));
            $bi['next_date_str'] = date("jS F, H:i A", strtotime("+1 week", strtotime($start_time)));
        } else if ($bi['frequency'] == 'DAILY') {
            $bi['next_date'] = date("Y-m-d H:i:s", strtotime("+1 day", strtotime($start_time)));
            $bi['next_date_str'] = date("jS F, H:i A", strtotime("+1 day", strtotime($start_time)));
        } else if ($bi['frequency'] == 'YEARLY') {
            $bi['next_date'] = date("Y-m-d H:i:s", strtotime("+1 year", strtotime($start_time)));
            $bi['next_date_str'] = date("jS F, H:i A", strtotime("+1 year", strtotime($start_time)));
        } else if ($bi['frequency'] == 'MONTHLY') {
            $bi['next_date'] = date("Y-m-d H:i:s", strtotime("+1 month", strtotime($start_time)));
            $bi['next_date_str'] = date("jS F, H:i A", strtotime("+1 month", strtotime($start_time)));
        } else {
            $bi['next_date'] = null;
            $bi['next_date_str'] = '-';
        }

        // update
        self::where('id', $id)->update(['next_date'=> $bi['next_date']]);
        return $bi;
    }

    
}
