<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Model\Mgr\Cases\BuildingCase;
class Contractor extends Model
{
    //
    protected $table = 'contractor';

    public function get_contractor_by_building($building_id) {
        return $this->select('contractor.id','contractor.cont_cate','contractor.comp_name','contractor.comp_addr','contractor.comp_postcode','building_category.name as cate_name', 'contractor.comp_suburb','contractor.comp_state','contractor.contact_person','contractor.contact_no','contractor.contact_email')
                            ->leftJoin('building_category','building_category.id','=','contractor.cont_cate')
                            ->where('contractor.status', '=', 1)
                            ->where('contractor.building_id', '=', $building_id)
                            ->get()->ToArray();
    }


    
    public function get_contractor_by_id($id) {
        return Contractor::select('contractor.id','contractor.cont_cate','contractor.comp_name','contractor.comp_addr','contractor.comp_postcode','building_category.name as cate_name', 'contractor.comp_suburb','contractor.comp_state','contractor.contact_person','contractor.contact_no','contractor.contact_email')
                            ->leftJoin('building_category','building_category.id','=','contractor.cont_cate')
                            ->where('contractor.status', '=', 1)
                            ->where('contractor.id', '=', $id)
                            ->get()->first();

    }

    public function case() {
        return $this->hasOne('App\Model\Mgr\Cases\BuildingCase', 'assignee_id', 'id');
    }
}
