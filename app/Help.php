<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Help_details;

class Help extends Model
{
    //
    protected $table = "help";

    public static function getDetails(){
        return Help_details::all();
    }

}
