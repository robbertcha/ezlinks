<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use NotificationChannels\WebPush\HasPushSubscriptions; //import the trait
use Auth;

class User extends Authenticatable
{
    use Notifiable;
    use HasPushSubscriptions; // add the trait to your class
    protected $guard = 'users';
    public $table = "users";
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function getTenantsByBuildingID($building_id) {
        return $this::select('users.id as value', 'unit.id as unit_id', DB::raw("CONCAT(unit.unit_title, ' - ',tenant_detail.first_name ,' ', tenant_detail.last_name)  as text"), DB::raw("'' as color"))
                    ->leftJoin('tenant_detail','tenant_detail.user_id','=','users.id')
                    ->leftJoin('unit','tenant_detail.unit_id','=','unit.id')
                    ->where(function($query) {
                        $query->where('users.type', '4')
                        ->orWhere('users.type', '3')
                        ->orWhere('users.type', '5');
                    })
                    ->where('users.status', '=', 1)
                    ->where('tenant_detail.building_id', '=', $building_id)
                    ->get()->toArray();
    }

}
