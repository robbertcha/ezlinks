<?php
namespace App\Services\Mgr\Cases;

use File;
use Illuminate\Http\Request;

// model
use App\Case_Progress;
use App\Building_Case_Quote;
use App\Building_Case_Invoice;
use App\Model\Mgr\Cases\BuildingCase;
use App\Model\Mgr\Cases\CaseComment;
use App\Model\Mgr\Cases\CaseAttachment;
use App\Contractor;

// enum
use App\Enums\BuildingCase\RoutineType;
use App\Enums\BuildingCase\CaseProgressType;

class CaseService {
    public function __construct() {
        date_default_timezone_set('Australia/Brisbane');
    }

    public function getCase($buidingId) {
        $case = (new BuildingCase())->getCaseListWithContractor($buidingId);
        return $case;
    }

    /**
     * Get case details by case ID
     * Note: Reservation event created on case -> parent_id = null && case_id > 0 && time_id = null
     * @param int $case_id
     * @return object
     */
    public function getCaseDetailsByBuildingID($case_id, $building_id) {
        return (new BuildingCase())->getCaseDetailsByBuildingID($case_id, $building_id);
    }
    
    /**
     * Save case detail
     * @param caseDetail
     * @return savedCase
     */
    public function saveCase($request) {
        $type = gettype($request);
        // dd($request);
        // posted request, an object instead of an array.
        if (!is_array($request)) {
            $request = $this->formatDateAndTime($request);
            $request = $request->all();
        }
        
        // In Case create from histroy.
        // $request['routine'] = gettype($request['routine']) === "integer" ? $request['routine']:RoutineType::getValue($request['routine']);
        // $request['progress'] = gettype($request['progress']) === "integer" ? $request['progress']:CaseProgressType::getValue($request['progress']);
        if (!empty($request['id'])) {
            $case = (new BuildingCase())->saveCase(collect($request)->except('tenant_id',
                                                            'case_type','job_area', 
                                                            // 'reporter', 'reporter_phone',
                                                            'unit_id', 'comment',
                                                            'asset_id', 'area_detail',
                                                            'title', 'status','attachments')->toArray());
        } else {
            $request['id'] = NULL;
            $case = (new BuildingCase())->saveCase($request);
        }
        return $case;
    }

    /**
     * Save multiple cases
     * @param array cases
     * @return boolean
     */
    public function saveCases($cases) {
        return (new BuildingCase())->saveManyCases($cases);
    }
    
    /**
     * Format date and time 
     * @return request
     */
    public function formatDateAndTime($request) {
        $request['due_date'] = date('Y-m-d', strtotime(request('due_date')));
        $request['due_time'] = date('H:i:s', strtotime(request('due_time')));
        return $request;
    }

    /**
     * Get details by case id
     * @param caseId
     * @return caseDetails
     */
    public function getCaseById($id) {
        $caseDetail = (new BuildingCase())->getCaseById($id);
        return $caseDetail;
    }

    /**
     * Get attachments by case id
     * @param caseId
     * @return attachments 
     */
    public function getAttachments($caseId) {
        $caseAttachment = (new BuildingCase())->getAttachementsByCaseId($caseId);
        // $caseAttachment = $this::separateImgDoc($caseAttachment);
        return $caseAttachment;
    }

    /**
     * Seprate photos and docs
     * @param attachments
     * @return array
     */
    public function separateImgDoc($caseAttachment) {
        $photos = [];
        $docs = [];
        foreach($caseAttachment as $key => $val) {
            if($val['type'] == 1) {
                $photos[] = $val;
            } else {
                $docs[] = $val;
            }
        }
        $attachments = [];
        $attachments['photos'] = $photos;
        $attachments['docs'] = $docs;
        
        return $attachments;
    }

    /**
     * Save attachments
     * @param caseId&attachments&userInformation
     * @return boolean 
     */
    public function saveAttachments($caseId, $attachments, $userInf) {
        $empty = TRUE;
        $savePhotos = $saveDocs = $saveAttachment = null;
        if (!empty($attachments['photos'])) {
            
            $empty = FALSE;
            $photos = $this->attachmentsArray($attachments->photos, $userInf, 1);
            
            $savePhotos = (new BuildingCase())->saveAttachmentsByCaseId($caseId, $photos);
        } 

        if (!empty($attachments['docs'])) {
            $empty = FALSE;
            $docs = $this->attachmentsArray($attachments->docs, $userInf, 2);
            $saveDocs = (new BuildingCase())->saveAttachmentsByCaseId($caseId, $docs);
        }

        if (!empty($attachments['existed'])) {
            
            $empty = FALSE;
            $saveAttachment = $this->saveExistedAttachments($caseId, $attachments['existed'] );
        }
        
        return $empty? $empty:($savePhotos || $saveAttachment || $saveDocs ) ? TRUE:FALSE;
    }

    /**
     * Put details of attachments into an new array and move to directory.
     * @param attachments&userInf
     * @return newAttachmentsArray
     */
    public function attachmentsArray($attachments, $userInf, $type) {
        // need to be add building id under building cases.
        $path = public_path().'/uploads/building_cases/'.date('Y_m_d');
        File::isDirectory($path) or File::makeDirectory($path, 0777, true, true);
        $buildingId = $userInf['buildingId'];
        $userId = $userInf['userId'];
        $attachmentsArray = [];
        
        foreach($attachments as $key => $val) { 
            $fileName = time().'_'.$buildingId.'_'.$val->getClientOriginalName();
            
            $attachment['uploader_id'] = $userId;
            $attachment['name'] = $fileName;
           
            $attachment['size'] = $val->getClientSize();
            
            $attachment['ext'] = $val->getClientOriginalExtension();
            $attachment['type'] = $type;
            $attachment['status'] = 1;
            $attachment['url'] = '/uploads/building_cases/'.date('Y_m_d').'/'.$fileName;
            $val->move($path.'/', $fileName);
            $attachmentsArray[] = $attachment;
        }
        return $attachmentsArray;
    }

    /**
     * Save comment
     * @param comment&userInf
     * @return boolean
     */
    public function saveComments($caseId, $comment,$userInf){

        if (!$comment) return TRUE;
        $caseComment = [];
        $caseComment['author_id'] = $userInf['userId'];
        $caseComment['author'] = $userInf['name'];
        $caseComment['comment'] = $comment;
        $caseComment['status'] = 1;
        $saveComment = (new BuildingCase())->saveCommentsByCaseId($caseId, $caseComment);
   
        return $saveComment ? TRUE:FALSE;
    }

    /**
     * Save existed attachements for cases.
     * @param attachment
     * @return boolean
     */
    public function saveExistedAttachments($caseId, $attachment) {
        $attachmentArray = [];
        foreach($attachment as $key => $val) {
            if ($val['id'] && $val['case_id']) {
                $val['id'] = '';
                $val['updated_at'] = '';
                $val['created_at'] = '';
                $val['case_id'] = $caseId;
                $attachmentArray[] = $val;
            } else {
                continue;
            }
        }
        $saveAttachment = (new BuildingCase())->saveAttachmentsByCaseId($caseId, $attachmentArray);
        return $saveAttachment;
    }
    
    /**
     * @return array routine type
     */
    public function getRoutineType() {
        return RoutineType::getKeys();
    }

    /**
     * @return array progress type 
     */
    public function getProgressType() {
        return CaseProgressType::getKeys();
    }

    /**
     * @return array routine case id
     */
    public function getRoutineCaseId() {
        return (new BuildingCase())->getRoutineCaseId();
    }

    /**
     * Create new routine case according to routine inspection by contractors.
     * Templates are invisible to users
     * 
     */
    public function createCaseByInspection() {

    }

    /**
     * Save multiple cases
     * @param array case id
     * @return boolean 
     */
    public function saveRoutineCases($caseIds) {
        for ($i = 0; $i < count($caseIds); $i++) {
            $case = $this::saveRoutineCase($caseIds[$i]['id']);
            if (!$case) return FALSE;
        }
        return TRUE;
    }

    /**
     * Save single case
     * @param string case id
     * @return boolean 
     */
    public function saveRoutineCase($caseId) {
        
        if ($caseId) {
            $case = $this::createRoutineCase($caseId);
            $newCase = $case['caseDetail'];
            $caseAttachment = $case['caseAttachment'];
            $newCase = $this::saveCase($newCase);
           
            // Attachments
            if (sizeof($caseAttachment) > 0) {
                $newAttachments = [];
                foreach($caseAttachment as $key => $attachment) {
                    
                    $newAttachment = $attachment;
                    $newAttachment['id'] = '';
                    $newAttachment['case_id'] = $newCase['id'];
                    $newAttachments[] = $newAttachment;
                }
                $attachments = (new CaseAttachment())->saveManyAttachments($newAttachments);
            } else {
                $attachments = TRUE;
            }

            $oldCaseUpdate = $this::setCaseProgress($caseId, CaseProgressType::getValue('Closed'));
            return ($newCase && $oldCaseUpdate && $attachments);
        }
        return FALSE;
    }

    public function setCaseStatus($caseId, $status) {
        return (new BuildingCase())->setCaseStaus($caseId, $status);
    }

    public function setCaseProgress($caseId, $progress) {
        return (new BuildingCase())->setCaseProgress($caseId, $progress);
    }

    /**
     * Create single case by previous case id.
     * @param caseId
     * @return array with case data
     */
    public function createRoutineCase($caseId) {
        if ($caseId) {
            $caseDetail = $this::getCaseById($caseId);
            $caseAttachment = $this::getAttachments($caseId);
            
            $routine = $caseDetail['routine'];
            $duration = $this::getRoutineDuration($routine);
            $newCase = $caseDetail;
            $newCase['id'] = "";
            $newCase['progress'] = '1';
            $newDate = date("Y-m-d", strtotime($newCase['due_date'].$duration));
            $newCase['due_date'] = date("Y-m-d", strtotime($newCase['due_date'].$duration));
            $case = [];
            $case['caseDetail'] = $newCase;
            $case['caseAttachment'] = $caseAttachment;
            return $case;
        }
    }

    /**
     * Get duration string
     * @param enum routine type
     * @return string duration
     */
    public function getRoutineDuration($routine) {
        switch ($routine) {
            case 1:
                return " +1 Day";
            case 2: 
                return " +7 Days"; 
            case 3: 
                return " +14 Days";
            case 4: 
                return " +1 Month";
            case 5: 
                return " +6 Months";
            case 6: 
                return " +1 Year";
            default:
                return "";
        }
    }

    public function dailyCheckRoutineCase() {
        $caseId = $this::getRoutineCaseId();
        
        if (empty($caseId)) return 'empty';
        if (count($caseId) === 1) {
            $case = $this::saveRoutineCase($caseId[0]['id']);
            
            return 'one case ';
        } else {
            $cases = $this::saveRoutineCases($caseId);
            
            return 'cases' ;
        }
    }

    /**
     * Get case type
     */
    public function getCaseType() {
        return (new BuildingCase())->get_case_type(); 
    }

    /**
     * Get case area
     */
    public function getCaseArea() {
        return (new BuildingCase())->get_case_area(); 
    }

    public function getCaseProgress() {
        return (new BuildingCase())->get_case_progress(); 
    }

    public function getCaseCommentsByCaseID($cid) {
        return (new BuildingCase())->getCaseCommentsByCaseID($cid);
    }

    /**
     * Get case attachments by case id
     * @param int $cid
     * @param int $type - 1, 2
     */
    public function getCaseAttachmentsByCaseID($cid, $type) {
        return (new CaseAttachment())->get_case_attachments($cid, $type);
    }

    /**
     * Get case quotes
     * @param int $cid
     * @return array
     */
    public function getCaseQuotesByCaseID($cid) {
        return (new Building_Case_Quote())->get_case_quotes($cid);
    }

    /**
     * Get case invoices
     * @param int $cid
     * @return array
     */
    public function getCaseInvoices($cid) {
        return (new Building_Case_Invoice())->get_case_invoices($cid);
    }

    /**
     * Get logs by case id
     * @param int $case_id
     * @return array
     */
    public function getCaseLogs($cid) {
        return (new Case_Progress())->get_log($cid);
    }

    /**
     * Update inspection status in case record
     * @param int $case_id
     * @return boolean
     */
    public function updateInspectionsStatus($cid) {
        return (new BuildingCase())->updateInspectionsStatus($cid);
    }
}