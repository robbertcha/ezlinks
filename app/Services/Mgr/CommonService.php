<?php

namespace App\Services\Mgr;

use App\Service;
use Illuminate\Http\Request;

use App\Tag;

class CommonService extends Service {
    private $tag;

    public function __construct() {
        date_default_timezone_set('Australia/Brisbane');
        $this->tag = new Tag();
    }

    /**
     * Get tag by type
     * @param var $type
     * @param int $building_id
     * 
     */
    public function getTagByType($type, $building_id) {
        return $this->tag->getTagByType($type, $building_id);
    }

    /**
     * Get unit tags by building id
     * @param int $building_id
     */
    public function getUnitTags($building_id) {
        return $this->tag->getUnitTags($building_id);
    }

}