<?php
namespace App\Services\Mgr\Building;

use Illuminate\Http\Request;
use App\Building_Admin;
use App\Building_Admin_Profile;

class BuildingAdminService {
      private $BuildingAdmin;
      private $BuildingAdminProfile;

      public function __construct() {
          date_default_timezone_set('Australia/Brisbane');
          $this->BuildingAdmin = new Building_Admin();
          $this->BuildingAdminProfile = new Building_Admin_Profile();
      }

      /**
       * Get admin infor & format admin object
       * @param int $admin_id
       * @param string $admin_email
       * @return object
       */
      public function getInfoByBuildingAdminID($admin_id, $admin_email) {
            $admin = new \stdClass;
            $admin_profile = $this->BuildingAdminProfile->getAdminInfoByBAID($admin_id);
            $admin->id = $admin_id;
            $admin->name = $admin_profile->first_name." ".$admin_profile->last_name;
            $admin->email_signature = $admin_profile->email_signature;
            $admin->phone = $admin_profile->cperson_phone;
            $admin->email = $admin_email;
            return $admin;
      }
}