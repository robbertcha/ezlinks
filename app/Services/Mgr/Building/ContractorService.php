<?php
namespace App\Services\Mgr\Building;

use Illuminate\Http\Request;
use App\Contractor;

class ContractorService {
      private $contractor;

      public function __construct() {
          date_default_timezone_set('Australia/Brisbane');
          $this->contractor = new Contractor();
      }

      public function getContractorsByBuildingID($building_id) {
            return $this->contractor->get_contractor_by_building($building_id);
      }

}