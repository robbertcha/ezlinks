<?php
namespace App\Services\Mgr\Building;

use App\Building_Amenity;
use Illuminate\Http\Request;

use App\Unit;
use App\Building;
use App\Building_Asset;
use App\Building_Stats;
use App\Building_Template;

class BuildingService {
      public function __construct() {
          date_default_timezone_set('Australia/Brisbane');
      }

      public function decreaseStats($object, $amount = 1, $building_id) {
            return (new Building_Stats())->decrease($object, $amount, $building_id);
      }

      /**
       * Get building infor by building id
       * @param int $bid
       * @return object
       */
      public function getBuildingInfoById($building_id) {
            return (new Building())->getBuildingInfoById($building_id);
      }

      // ------------------------------------------------------ Unit
      /**
       * Get Unit Detail By ID
       * @param int $case_id
       * @return array
       */
      public function getUnitDetailsByID($case_id) {
            return (new Unit())->getDetailByID($case_id);
      }

      /**
       * Get All Units By Building ID
       * @param int $building_id
       * @return array
       */
      public function getUnitsByBuildingID($building_id) {
            return (new Unit())->get_all_units($building_id);
      }
      // ------------------------------------------------------ Unit




      // ------------------------------------------------------ Asset
      /**
       * Get Asset Detail By ID
       * @param int $asset_id
       * @return array
       */
      public function getAssetDetailsByID($asset_id) {
            return (new Building_Asset())->getDetailByID($asset_id);
      }

      /**
       * Get assets by building id
       * @param int $id building id
       * @return array assets 
       */
      public function getAssetsByBuildingID($id) {
            return (new Building())->getAssetsByBuildingID($id);
      }





      // ------------------------------------------------------ Template
      /**
       * Get email template list by building id
       * @param int $building id
       * @return array
       */
      public function getEmailTemplateList($building_id) {
            return (new Building_Template())->get_template_list($building_id);
      }





      // ------------------------------------------------------ Amenity
      /** 
       * Get amenities by building id 
       * @return ajax
       */
      public function getAmenitiesByBuildingID($buildingID) {
            return (new Building_Amenity())->getAllAmenitiesByBID($buildingID);
      }

}