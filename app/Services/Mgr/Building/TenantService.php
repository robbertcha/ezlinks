<?php
namespace App\Services\Mgr\Building;

use Illuminate\Http\Request;
use App\Tenant_Detail;

class TenantService {
      private $TenantDetail;

      public function __construct() {
          date_default_timezone_set('Australia/Brisbane');
          $this->TenantDetail = new Tenant_Detail();
      }

      /**
       * Get Tenant lists by building id
       * @param int $building_id
       * @return array
       */
      public function getTenantsByBuildingID($building_id) {
            return $this->TenantDetail->getTenantsByBuildingID($building_id);
      }

}