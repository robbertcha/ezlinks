<?php

namespace App\Services\Mgr\Reservations;

use Auth;
use File;
use Illuminate\Http\Request;
use App\Amenity_Reservation;
use App\Building_Stats;
use App\Model\Mgr\Cases\BuildingCase; 

class ReservationsService {
      private $ar;

      public function __construct() {
            date_default_timezone_set('Australia/Brisbane');
            $this->ar = new Amenity_Reservation();
      }

      /**
      * Get reservation array where next date is before yesterday.
      * @return array reservation 
      */
      public function getRoutines() {
            return $this->ar->getRoutines();
      }

      /**
       * CRON - Update reservation routine
       * Run this once everyday to auto generate recurring sub-events and cases
       * @return boolean
       */
      public function dailyCheckRoutine() {
            $user = Auth::user();
            $routines = $this::getRoutines();
         
            if (empty($routines)) return 'empty';

            for ($i = 0; $i < count($routines); $i++) {
                  $case = (new BuildingCase())->getLatestCaseByEventID($routines[$i]['id'], ['id','category_id','bii_id','location','title','due_date','due_time']);

                  if (count($case) > 0) {
                        $next_date = $this->ar->calculate_next_date($routines[$i]);
                        $routines[$i]['next_date'] = $next_date['next_date'];
                        $new_routine = array_merge($routines[$i], $case[0]);
                        $new_routine['id'] = null;
                        $sub = $this->ar->createSubEvent($new_routine, $user, $next_date['next_booking_time_from'], $next_date['next_booking_time_to'],  $routines[$i]['id'] );

                        // Update next_date into parent reservation record
                        $this->ar->whereId($routines[$i]['id'])->update(['next_date' => $routines[$i]['next_date']]);
                  }
            }    
      }

      /**
       * Create sub event and case
       * @param array $new_record array of new amenity reservation 
       * @param array $user user array
       */
      public function createSubEventAndCase($new_record, $user) {
            $case = (new BuildingCase())->getLatestCaseByEventID($new_record['id'], ['id','category_id','bii_id','location','title','due_date','due_time']);

            if (count($case) > 0) {
                  $next_date = $this->calculate_next_date($new_record);
                  $new_record['next_date'] = $next_date['next_date'];
                  $new_routine = array_merge($new_record, $case[0]);
                  $sub = $this->ar->createSubEvent($new_routine, $user, $next_date['next_booking_time_from'], $next_date['next_booking_time_to'],  $new_record['id'] );
                  
                  // Update next_date into parent reservation record
                  $this->ar->whereId($new_record['id'])->update(['next_date' => $new_record['next_date']]);
            }
      }

       /**
       * Create or update a amenity reservation record
       * @param request array of amenity reservation
       * @return not known
       */
      public function saveReservation($request) {
            return $this->ar->saveReservation($request);
      }

      /**
       * Count next date by recurring
       * @param array $bi amenity reservation record
       * @return array
       */
      public function calculate_next_date($routine) {
            return $this->ar->calculate_next_date($routine);
      }

      /**
       * Get single scheduler record by inspection area id
       * @param int $bia_id
       * @param int $building_id
       * @return object amenity_reservation record
       */
      public function getSchedulerByInspectionID($bia_id, $building_id) {
            return $this->ar->getSchedulerByInspectionID($bia_id, $building_id);
      }

      /**
       * Convert number to recurring string, check kendo scheduler
       * @param int $frequency 
       * @return string
       */
      public function convertRecurringByNumber($frequency) {
            if ($frequency == 1) {
                  return 'FREQ=DAILY';
            } elseif ($frequency == 2) {
                  return 'FREQ=WEEKLY';
            } elseif ($frequency == 3) {
                  return 'FREQ=MONTHLY';
            } elseif ($frequency == 4) {
                  return 'FREQ=YEARLY';
            } else {
                  return null; 
            }
      }

      /**
       * Set booking end by id
       * @param int $id amenity_reservation id
       * @param datetime $time current time
       * @return boolean
       */
      public function setBookingEnd($id, $time) {
            return $this->ar->setBookingEnd($id, $time);
      }

      /**
       * Get list of reservation & sub-events
       * @param date $first_day       First day of the filter
       * @param date $last_day        Last day of the filter
       * @param string $type          Job Type
       * @param int $building_id      building id
       * @param string $search_txt    search keyword
       * @param int $page             page number
       * @param string $order         asc or desc
       * @param int $page_qty         page number
       * @param boolean $hideClosedBookings
       * @return array
       */
      public function get_reservation_list($first_day, $last_day, $type, $building_id, $search_txt = '', $page = 1, $order = 'asc', $page_qty = 200, $event_id = null, $hideClosedBookings = false) {
            $result = $this->ar->get_reservation_list($first_day, $last_day, $type, $building_id, $search_txt, $page, $order, $page_qty, $event_id, $hideClosedBookings);
            // Get sub-events
            foreach ($result as $key => $val) {
                  if ($val->typeId == 1) {
                        $result[$key]->subEvents = $this->ar->getSubEventsByParentID($result[$key]->id, null, null);
                  }
            }
            return $result;
      }

      /**
       * Get job type array 
       * @return job type array
       */
      public function getSchedulerType() {
            return $this->ar->getSchedulerType();
      }

      /**
      * Save event
      * @param array $request Event posted by building manager
      * @param object $user Current user
      * @param DateTime $starttime Event start time format: Y-m-d H:i:s
      * @param DateTime $endtime Event end time format: Y-m-d H:i:s
      * @return event ID
     */
      public function saveEvent($request, $user, $starttime, $endtime) {
            return $this->ar->saveEvent($request, $user, $starttime, $endtime);
      }


      /**
       * Remove event from calendar
       * @param array $request - id: event id or parent event id
       * @param array $request - type: same as typeID, 1 - Job, 3 - Reservation, 4 - Parking
       * @param array $request - action: 1 - not job type, 2 - job type, delete occurence, 3 - job type, delete series
      */
      public function rmEvent($request) {
            // Get start time details from id
            $recurring_arr = $this->ar->getStartTimeByEventID($request['id']);
            // Remove event by id, but not truely delete, if it is job, then add today as booking_end, if not, set status = 0
            // Remove event in the case when parent_id == null & type is job
            $v['result'] = $this->ar->rmEventByID($request['id'], $request['type'], $recurring_arr['booking_time_from'], $recurring_arr['parent_id']);

            if ($v['result'] == 1 && $recurring_arr['parent_id'] > 0) {
                  $count = 1;
                  $subEventIDs = [];
                  $subEventCaseIDs = [];   
                  if ($request['type'] == 1 && $request['action'] == 1) {
                        // if it is job && if it is delete 1 event from recurring
                        // Add booking exception on its main id
                        // Get start time & parent id by event id
                        $result = $this->ar->insertBookingExceptionTime($recurring_arr['parent_id'], date("Y-m-d h:i:s", time() - 1));
                        if ($result) {
                              $subEventIDs[0] = $recurring_arr['id'];
                              $subEventCaseIDs[0] = $recurring_arr['case_id'];

                              // Decrease stats 
                              $count = 1;
                        }
                  } else if ($request['type'] == 1 && $request['action'] == 2) {
                        // if it is job && if it is delete all series
                        // Get all sub-events && cases after today by parent id, so we get count of total of this array
                        $subEvents = $this->ar->getSubEventsByParentID($request['id'], date("Y-m-d H:i:s"));
                                       
                        foreach($subEvents as $key =>$val) {
                              $subEventIDs[] = $val['id'];
                              $subEventCaseIDs[] = $val['case_id'];
                        }

                        // Decrease stats by count of total of this array, count($subEvents)
                        $count = count($subEvents);
                  }

                  // Remove sub-events by sub-event ids array
                  $resultSubEvents = $this->ar->removeSubEventsByIDs($subEventIDs);
                  // Remove cases by sub-event case-ids array
                  $resultCases = (new BuildingCase())->removeCasesByIDs($subEventCaseIDs);
                  // Update stats
                  $stats = (new Building_Stats())->decrease('total_case', $count, $request['building_id']);

                  // get the edit event details
                  if(isset($recurring_arr['parent_id'])) {
                        $v['parent_id'] = $recurring_arr['parent_id'];
                        $v['data'] = $this->get_reservation_list(null, null, '', $request['building_id'], '', 1, 'asc', 1, $recurring_arr['parent_id']);
                  } else {
                        $v['data'] = $this->get_reservation_list(null, null, '', $request['building_id'], '', 1, 'asc', 1, $request['id']);
                  }
            } else {
                  // Get the edit event details when it is 
                  $v['data'] = $this->get_reservation_list(null, null, '', $request['building_id'], '', 1, 'asc', 1, $request['id']);
            }
            return $v;
      }

      /**
       * Get all reservation events by case id
       * @param int $case_id
       * @return array
       */
      public function getEventsByCaseID($case_id) {
            return $this->ar->getEventsByCaseID($case_id);
      }

}

?>