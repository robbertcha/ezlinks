<?php

namespace App\Services\Mgr\Inspections;

use Illuminate\Http\Request;
use App\Building_Inspection_Area;
use App\Building_Inspection_Item;
use App\Building_Inspection_Record;

class InspectionService {

      public function __construct() {
          date_default_timezone_set('Australia/Brisbane');
      }

      /**
       * Get inspection items by area id
       * @param int $id inspection area id
       * @return array items 
       */
      public function getArea($id) {
            return (new Building_Inspection_Area())->getArea($id);
      }

      /**
       * Get inspection items by area id
       * @param int $id inspection area id
       * @return array items 
       */
      public function getItems($id) {
            return (new Building_Inspection_Area())->getItemsByID($id);
      }

      /**
       * Get inspection history by area id
       * @param int $id inspection area id
       * @return array history 
       */
      public function getHistory($id) {
            return (new Building_Inspection_Area())->getHistoryByID($id);
      }

      /**
       * Create or update inspection area
       * @param $inspections array from AJAX
       * @return int inspection id
       */
      public function saveInspectionArea($inspections) {
            $except = ['actions','ar_id','created_at','history','next_date','start_date','time_from','time_to','updated_at','status', 'isChangeFrequency'];

            $bia = new Building_Inspection_Area();
            if (isset($inspections['id'])) {
                  $bia = Building_Inspection_Area::find($inspections['id']);
            }
            return $bia->saveInspectionArea(collect($inspections)->except($except)->toArray());
      }

      /**
       * Remove items by inspection area id
       * @param int $bi_id inspection area id
       */
      public function removeItemsByAreaID($bi_id) {
            return (new Building_Inspection_Item())->removeItemsByAreaID($bi_id);
      }

      /**
       * Save items with inspection area id
       * @param array new items
       * @return 
       */
      public function saveNewItemsByAreaID($items, $bi_id) {
            return (new Building_Inspection_Area())->saveNewItemsByAreaID($bi_id, collect($items)->except('created_at','updated_at', 'id')->toArray());
      }

      /**
       * Save items 
       * @param array new items
       * @return 
       */
      public function saveItems($items, $bi_id) {
            $bii = new Building_Inspection_Item();
            for ($i = 0; $i < count($items); $i++) {
                  $items[$i]['bi_area_id'] = $bi_id;
                  $bii->updateOrCreate(['id'=> $items[$i]['id']], $items[$i]);
            }
      }

      public function getInspectionsByBuildingID($building_id){
            return (new Building_Inspection_Area())->get_building_inspections_by_building($building_id);
      }

      /**
       * Get Inspection items by area ID
       * @param int $area_id
       * @return array
       */
      public function getInspectionItemsByAreaID($area_id, $case_id){
            return (new Building_Inspection_Item())->getDetailsByAreaID($area_id, $case_id);
      }

      /**
       * Save inspection records by case
       * @param array $inspectionResult
       * @return boolean
       */
      public function updateInspectionsByCase($inspectionResult) {
            return (new Building_Inspection_Record())->UpdateInspectionsByCase($inspectionResult);
      }

}