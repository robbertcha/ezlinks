<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Building_Admin_Profile extends Model {
    //
    protected $table = 'building_admin_profile';

    public static function getAdminInfoByBAID($ba_id){
        return self::select("id","first_name", "last_name", "cperson_phone", "cperson_mobile")
                    ->where('ba_id', '=', $ba_id)
                    ->get()->first();
    }
}
