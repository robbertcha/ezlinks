<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Tenant_address_book extends Model { 
    protected $table = "tenant_address_book";
    protected $fillable = ['agency', 'phone_number', 'status'];

    public static function get_agency_phone_number($building_id) {
        return self::select('id', 'agency', 'phone_number')
                    ->where('building_id', '=', $building_id)
                    ->where('status', '=', 1)
                    ->orderBy('created_at', 'asc')
                    ->get()->toArray();
    }
}