<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Send_To_Contractor extends Model {

    protected $table = "contractor";
    // protected $fillable = ['id', 'contact_email'];

    public function get_contractor_email($contractor_id) {
        return self::select('comp_name', 'contact_person','contact_email')
                -> where('status', '=', 1)
                -> whereIn('id', $contractor_id)
                -> get()-> toArray();
    }
}