<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Contractor;
use App\User;
use App\Building_Admin;
use Auth;

class Mail_Modify extends Model
{   
    public function init($content,$id_key){
        if (strlen($content) > 0) {
            preg_match_all('~\{{1}\%{1}(.*?)\|{1}(.*?)\%{1}\}{1}~', $content, $match);
        }

        // 判断是否有替换字段
        if(empty($match[0])){
            return $content;
        }else{
            // 执行文本字段替换
           return $this->string_replace($match,$content,$id_key);
        }
    }

    public function string_replace($match,$content,$id_key){
        $user = Auth::user();
        $id_key['company_id'] =$user['building_id'];
        // 执行匹配查找数据
        $replace_string = self::find_info($match,$id_key);

        $content = str_replace($match[0],$replace_string,$content);

        return $content;
    }

    public function find_info($match,$id_key){
        for ($i=0; $i<count($match[0]); $i++) { 
            $var=explode("|",substr($match[0][$i],2,-2));
            $dbname = $var[0]; //数据库名字
            $attr = $var[1]; //查到的值
            $k = $var[2]; //匹配id_key的固定字符
            $replace_string[$i]=self::find_db($dbname,$attr,$k,$id_key);
           
        }
        return $replace_string;
    }

    public function find_db($dbname,$attr,$k,$id_key){
        foreach ($id_key as $key => $value) {
            if($k == $key){
            $info = DB::table($dbname)
                    ->where($k,$value)
                    ->value($attr);
            break;
            }
            else{
                $info = "___";
            }
        }
      return $info;
       
    }

}
