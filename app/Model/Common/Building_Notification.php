<?php

namespace App\Model\Common;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Building_Notification extends Model {
    protected $table = 'log_notifications';

    public static function get_building_notification($building_id) {
        return self::select('title','created_at','status','content')
            ->where('log_notifications.building_id','=',$building_id)
            ->where('log_notifications.status','=',1)
            ->where( 'created_at', '>', Carbon::now()->subDays(60))
            ->orderBy('log_notifications.created_at','desc')
            ->get();
    }
}