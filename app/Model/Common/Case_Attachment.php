<?php

namespace App\Model\Common;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Case_Attachment extends Model {
    protected $table = 'case_attachment';

    public static function case_attachment($id) {
        return self::select('type','url')
            ->where('case_attachment.id','=',$id)
            ->where('case_attachment.status','=',1)
            ->where('case_attachment.type','=',1)
            ->get()->toArray();
    }
}