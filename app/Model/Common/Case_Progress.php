<?php

namespace App\Model\Common;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Case_Progress extends Model {
    protected $table = 'case_progress';

    public static function case_progress($id) {
        return self::select('created_at','message')
            ->where('case_progress.case_id','=',$id)
            ->get()->toArray();
    }
}