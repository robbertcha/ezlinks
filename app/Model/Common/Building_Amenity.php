<?php

namespace App\Model\Common;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Building_Amenity extends Model
{
    //
    public $table = "building_amenity";
    public function getAmenityDetailsByAID($aid) {
        return $this->select("name","booking_before","allocation_limit","hours_limit","rules")
                    ->where('id', '=', $aid)
                    ->where('status', '=', 1)
                    ->get()->first();
    }

    public static function create_booking_page($building_id) {
        return self::select('name', 'id','booking_before','allocation_limit','hours_limit','rules','hours_limit')
            ->where('status', '=', 1)
            ->where('building_id', '=', $building_id)
            ->get()->toArray();
    }
}
