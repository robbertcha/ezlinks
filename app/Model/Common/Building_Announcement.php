<?php

namespace App\Model\Common;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Building_Announcement extends Model {
    protected $table = 'building_announcement';
    protected $dates = ['created_at'];

    public static function tenant_announcement($p, $amount = 10, $building_id, $user) {
        $announcements = self::select("building_announcement.*", "bap.user_ids", DB::raw("
                                    (CASE 
                                        WHEN building_announcement.type = '1' THEN 'Announcement'
                                        WHEN building_announcement.type = '2' THEN 'Event'
                                        WHEN building_announcement.type = '3' THEN 'Maintenance/Repair'
                                        WHEN building_announcement.type = '4' THEN 'Great Offer'
                                        WHEN building_announcement.type = '5' THEN 'Other'
                                    END) AS type_title
                                    "))
                                    ->leftJoin(DB::raw("(select ba_id, GROUP_CONCAT( CONCAT(user_id) SEPARATOR '|') as user_ids from `building_announcement_participant` where status = 1 group by ba_id) as bap"),function($join){
                                        $join->on("bap.ba_id","=","building_announcement.id");
                                    })
                                    ->where('building_announcement.status', '=', 1)
                                    ->where('building_announcement.building_id', '=', $building_id)
                                    ->orderBy('building_announcement.created_at', 'desc')
                                    ->paginate($amount, ['*'], 'page', $p);

            foreach ($announcements as $key => $val) {
                $announcements[$key]->joint = false;
                $user_ids = explode("|",$announcements[$key]->user_ids);
                if (in_array($user->id, $user_ids)) {
                    $announcements[$key]->joint = true;
                }

                $announcements[$key]->content = strip_tags(preg_replace('/(<[^>]+) style=".*?"/i', '$1',html_entity_decode($val->content)));
                $announcements[$key]->content = substr(strip_tags($announcements[$key]->content), 0, 140). ' ... ';
            }

            return $announcements;
    }

    public static function get_announcement_detail($aid) {
        return self::where('status', '=', 1)
            ->where('id', '=', $aid)
            ->get()->first();
    }
}