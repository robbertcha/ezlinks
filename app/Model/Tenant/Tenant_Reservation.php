<?php

namespace App\Model\Tenant;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Tenant_Reservation extends Model {

    protected $table = "amenity_reservation";

    public static function tenant_reservation_list($p, $amount, $building_id, $user_id) {
        return self::select("amenity_reservation.id", "amenity_reservation.tenant_id", "amenity_reservation.bi_id as bi_id", "amenity_reservation.contractor_id as cid", "a.name as amenity_title", "bi.title as bi_title", "c.comp_name", "amenity_reservation.type as typeId", "amenity_reservation.details", "amenity_reservation.booking_time_from as start", "amenity_reservation.booking_time_to as end", "amenity_reservation.amenity_id as amenity_id", "amenity_reservation.comment", "amenity_reservation.type as action", DB::raw('DATE_FORMAT(amenity_reservation.booking_time_from, "%d-%b-%Y") as start_date'), DB::raw('DATE_FORMAT(amenity_reservation.booking_time_from, "%H:%i") as start_time'), DB::raw('DATE_FORMAT(amenity_reservation.booking_time_to, "%H:%i") as end_time'), DB::raw("(CASE WHEN amenity_reservation.progress = '0' THEN 'Pending' WHEN amenity_reservation.progress = '1' THEN 'Approved' WHEN amenity_reservation.progress = '2' THEN 'Rejected' ELSE 'Pending' END) AS status"), DB::raw('DATE_FORMAT(amenity_reservation.created_at, "%d-%b-%Y") as created_date'))
            ->join('tenant_detail as td',function($join){
                $join->on('td.user_id','=','amenity_reservation.tenant_id');
            }, null,null,'left')
            ->join('building_amenity as a',function($join){
                $join->on('a.id','=','amenity_reservation.amenity_id');
            }, null,null,'left')
            ->join('building_inspection as bi',function($join){
                $join->on('bi.id','=','amenity_reservation.bi_id');
            }, null,null,'left')
            ->join('contractor as c',function($join){
                $join->on('c.id','=','amenity_reservation.contractor_id');
            }, null,null,'left')
            ->where('amenity_reservation.status', '=', 1)
            ->where('amenity_reservation.type', '=', 3)
            ->orWhere('amenity_reservation.type', '=', 5 )
            ->where('amenity_reservation.building_id', '=', $building_id)
            ->where('amenity_reservation.tenant_id', '=',  $user_id)
            ->orderBy('amenity_reservation.id', 'desc')
            ->paginate($amount, ['*'], 'page', $p);
       
    }
}