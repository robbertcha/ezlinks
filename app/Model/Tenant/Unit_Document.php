<?php

namespace App\Model\Tenant;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Unit_Document extends Model {

    protected $table = "unit_document";
    protected $fillable = ['unit_id', 'tenant_id', 'uploader_id', 'uploader_type', 'title', 'path', 'type', 'size', 'status'];

    /**
     * The file list that tenant uploaded to the system.
     * @Return file list in array.
     */
    public static function get_uploaded_file_list($user_id) {

        return self::select('id', 'tenant_id', 'title', 'type', 'created_at', 'path')
                            -> where('status', '=',1)
                            -> where('uploader_id', '=', $user_id)
                            -> where('tenant_id', '=', $user_id)
                            -> where('uploader_type', '=',2)
                            -> orderBy('created_at', 'desc')
                            -> get()-> ToArray();
    }


    /**
     * The file list that admins sent to tenant.
     * @Return file list in array.
     */
    public static function get_sent_file_list($user_id) {

        return self::select('unit_document.id', 'uploader_id', 'title', 'type', 'unit_document.created_at', 'path', 'building_admin.name')
                            -> leftJoin('building_admin', 'building_admin.id', '=', 'uploader_id')
                            -> where('unit_document.status', '=',1)
                            -> where('unit_document.uploader_type', '=', 1)
                            -> where('unit_document.tenant_id', '=', $user_id)
                            -> orderBy('unit_document.created_at', 'desc')
                            -> get()-> ToArray();
    }


    /**
     * Tenant upload file and store file into the database.
     * User & Unit directories will be created if they are not existed. 
     * @Require file is exist and valid.
     * @Return 
     */
    public static function upload_file($user_detail, $request) {
        
        $unit_id = $user_detail->unit_id;
        
        $user_id = $user_detail->user_id;
        
        $file_name = $request -> file('upload_file') -> getClientOriginalName();
        
        $unit_dir_path = './uploads/unit_documents/' . "unit_" . $unit_id;
        
        $tenant_dir_path = $unit_dir_path . '/' . "user_" . $user_id ;
        
        // To create a directory for user & his/her unit 
        // if the directory is not exist.
        if (!is_dir($unit_dir_path)) {
            Storage::makeDirectory($unit_dir_path);
            Storage::makeDirectory($tenant_dir_path);
        }

        if (!is_dir($tenant_dir_path)) {
            Storage::makeDirectory($tenant_dir_path);
        }

        // To sheild the last same name file in database.
        if (is_file($tenant_dir_path . '/' . $file_name)) {
            self::shield_file_by_name($user_id, $file_name);
        }

        //$data = $request -> all();
        if ($request -> file('upload_file') -> move($tenant_dir_path, $file_name)) {
            $data = new Unit_Document;
            $data['unit_id'] = $unit_id;
            $data['tenant_id'] = $user_id;
            $data['uploader_id'] = $user_id;
            $data['uploader_type'] = 2;
            $data['title'] = $file_name;
            $data['path'] = $tenant_dir_path;
            $data['type'] = $request -> file('upload_file') -> getClientOriginalExtension();
            $data['size'] = $request -> file('upload_file')-> getClientSize();
            $data['status'] = 1;
            $data->save();
            return $data;
        } else {

        } 
    }
    
    /**
     * To set the file status from 1 to 0 by file id.
     */
    public static function shield_file_by_id($user_id, $file_id) {

        $file = self::where('status', '=', 1)
                        -> where('tenant_id', '=', $user_id)
                        -> where('id', '=', $file_id)
                        -> where('uploader_id', '=', $user_id)
                        -> orderBy('created_at', 'desc');

        if ($file -> exists()) {
            $file = $file -> first() -> update(['status' => '0']);
        } else {
            return;
        }
                       
    }

    /**
     * To set the file status from 1 to 0 by file id.
     */
    public static function shield_file_by_name($user_id, $file_name) {
        
        $file = self::where('status', '=', 1)
                        -> where('tenant_id', '=', $user_id)
                        -> where('title', '=', $file_name)
                        -> where('uploader_id', '=', $user_id)
                        -> orderBy('created_at', 'desc');
        if ($file -> exists()) {
            $file = $file -> first() -> update(['status' => '0']);
        } else {
            return;
        }
                        
    }
}

