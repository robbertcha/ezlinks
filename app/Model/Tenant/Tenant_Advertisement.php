<?php

namespace App\Model\Tenant;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Tenant_Advertisement extends Model {
    protected $table = "advertisement";

    public static function get_advertisement($user_detail) {
        return self::select("advertisement.*","ba.*")
        ->join('building_advertisement as ba',function($join){
            $join->on('ba.advertisement_id','=','advertisement.id');
        }, null,null,'left')
        ->where('advertisement.status', '=', 1)
        ->where('ba.status', '=', 1)
        ->where('ba.building_id', '=', $user_detail->building_id)
        ->where('advertisement.valid_from', '<', \DB::raw('NOW()'))
        ->where('advertisement.valid_to', '>=', \DB::raw('NOW()'))
        ->get()->toArray();
    }
}