<?php

namespace App\Model\Tenant;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Tenant_Detail extends Model
{
    //
    public $table = "tenant_detail";
    public function unit() {
        return $this->belongsTo('App\Unit');
    }

    public function tenantVerification() {
        return $this->hasOne('App\TenantVerification');
    }

    public function getTenantsByBuildingID($building_id) {
        return $this->select("tenant_detail.id", DB::raw("CONCAT(unit.unit_title,' - ',tenant_detail.first_name, ' ', tenant_detail.last_name) as tenant_text"))
                    ->leftJoin('users','users.id','=','user_id')
                    ->leftJoin('unit','tenant_detail.unit_id','=','unit.id')
                    ->where('tenant_detail.building_id', '=', $building_id)
                    ->where('users.status', '=', 1)
                    ->where('tenant_detail.link_status', '=', 1)
                    ->where(function ($query) {
                        $query->where('users.type', '=', 3)
                              ->orWhere('users.type', '=', 4)
                              ->orWhere('users.type', '=', 5);
                    })
                    ->get()->toArray();
    }

    public function getTenantListByBuildingID($building_id) {
        return $this->select("tenant_detail.id","tenant_detail.user_id","tenant_detail.first_name","tenant_detail.last_name", "u.email","tenant_detail.type as type_id", "tv.vehicles", "unit.unit_title", DB::raw("CONCAT(unit.unit_title,' - ',tenant_detail.first_name, ' ', tenant_detail.last_name) as tenant_text"),
                                    DB::raw("
                                        (CASE 
                                            WHEN tenant_detail.type = '3' THEN 'Owner'
                                            WHEN tenant_detail.type = '4' THEN 'Tenant'
                                            WHEN tenant_detail.type = '5' THEN 'Invester'
                                        END) AS type
                                    "),
                                    DB::raw("
                                        (CASE 
                                            WHEN tenant_detail.link_status = '0' THEN ''
                                            WHEN tenant_detail.link_status = '1' THEN 'Verified'
                                            WHEN tenant_detail.link_status = '2' THEN 'Unverified'
                                        END) AS link_status
                                    "),
                                    DB::raw("
                                        (CASE 
                                            WHEN tenant_detail.link_status = '0' THEN ''
                                            WHEN tenant_detail.link_status = '1' THEN tenant_detail.mobile
                                            WHEN tenant_detail.link_status = '2' THEN ''
                                        END) AS mobile
                                    "),
                                    DB::raw("
                                        (CASE 
                                            WHEN tenant_detail.link_status = '0' THEN ''
                                            WHEN tenant_detail.link_status = '1' THEN tenant_detail.contact
                                            WHEN tenant_detail.link_status = '2' THEN ''
                                        END) AS contact
                                    "),
                                    DB::raw("
                                        (CASE 
                                            WHEN tenant_detail.link_status = '0' THEN ''
                                            WHEN tenant_detail.link_status = '1' THEN tenant_detail.emergency_person
                                            WHEN tenant_detail.link_status = '2' THEN ''
                                        END) AS emergency_person
                                    "),
                                    DB::raw("
                                        (CASE 
                                            WHEN tenant_detail.link_status = '0' THEN ''
                                            WHEN tenant_detail.link_status = '1' THEN tenant_detail.emergency_no
                                            WHEN tenant_detail.link_status = '2' THEN ''
                                        END) AS emergency_no
                                    "))
                                    ->leftJoin('users as u','tenant_detail.user_id','=','u.id')
                                    ->leftJoin('unit','tenant_detail.unit_id','=','unit.id')
                                    ->leftJoin(DB::raw("(select tenant_id, GROUP_CONCAT( CONCAT(id,'-',make,'-',model,'-',plate_no) SEPARATOR '|') as vehicles from `tenant_vehicle` where status = 1 group by tenant_id) as tv"),function($join){
                                        $join->on("tv.tenant_id","=","tenant_detail.user_id");
                                    })
                                    ->where('tenant_detail.building_id', '=', $building_id)
                                    ->where('u.status', '=', 1)
                                    ->get()->toArray();

    }

    public static function get_header_details($user_id) {
        return self::select('tenant_detail.*','unit.unit_title','unit.building_id','building.name as building_name', 'push_subscriptions.endpoint','push_subscriptions.auth_token')
            ->join('unit',function($join){
                $join->on('tenant_detail.unit_id','=','unit.id');
            }, null,null,'left')
            ->join('building',function($join){
                $join->on('unit.building_id','=','building.id');
            }, null,null,'left')
            ->join('push_subscriptions',function($join){
                $join->on('tenant_detail.user_id','=','push_subscriptions.user_id');
            }, null,null,'left')
            ->where('tenant_detail.user_id', '=', $user_id)
            ->first();
    }

    public function getTenantDetailsByTID($tenant_id) {
        return $this->select("tenant_detail.id","tenant_detail.user_id","tenant_detail.first_name","tenant_detail.last_name", "u.email","tenant_detail.type as type_id", "tv.vehicles", "unit.unit_title", DB::raw("CONCAT(unit.unit_title,' - ',tenant_detail.first_name, ' ', tenant_detail.last_name) as tenant_text"),
                                    DB::raw("
                                        (CASE 
                                            WHEN tenant_detail.type = '3' THEN 'Owner'
                                            WHEN tenant_detail.type = '4' THEN 'Tenant'
                                            WHEN tenant_detail.type = '5' THEN 'Invester'
                                        END) AS type
                                    "),
                                    DB::raw("
                                        (CASE 
                                            WHEN tenant_detail.link_status = '0' THEN ''
                                            WHEN tenant_detail.link_status = '1' THEN 'Verified'
                                            WHEN tenant_detail.link_status = '2' THEN 'Unverified'
                                        END) AS link_status
                                    "),
                                    DB::raw("
                                        (CASE 
                                            WHEN tenant_detail.link_status = '0' THEN ''
                                            WHEN tenant_detail.link_status = '1' THEN tenant_detail.mobile
                                            WHEN tenant_detail.link_status = '2' THEN ''
                                        END) AS mobile
                                    "),
                                    DB::raw("
                                        (CASE 
                                            WHEN tenant_detail.link_status = '0' THEN ''
                                            WHEN tenant_detail.link_status = '1' THEN tenant_detail.contact
                                            WHEN tenant_detail.link_status = '2' THEN ''
                                        END) AS contact
                                    "),
                                    DB::raw("
                                        (CASE 
                                            WHEN tenant_detail.link_status = '0' THEN ''
                                            WHEN tenant_detail.link_status = '1' THEN tenant_detail.emergency_person
                                            WHEN tenant_detail.link_status = '2' THEN ''
                                        END) AS emergency_person
                                    "),
                                    DB::raw("
                                        (CASE 
                                            WHEN tenant_detail.link_status = '0' THEN ''
                                            WHEN tenant_detail.link_status = '1' THEN tenant_detail.emergency_no
                                            WHEN tenant_detail.link_status = '2' THEN ''
                                        END) AS emergency_no
                                    "))
                                    ->join('users as u',function($join){
                                        $join->on('tenant_detail.user_id','=','u.id');
                                    }, null,null,'left')
                                    ->leftJoin('unit','tenant_detail.unit_id','=','unit.id')
                                    ->leftJoin(DB::raw("(select tenant_id, GROUP_CONCAT( CONCAT(id,'-',make,'-',model,'-',plate_no) SEPARATOR '|') as vehicles from `tenant_vehicle` where status = 1 group by id) as tv"),function($join){
                                        $join->on("tv.tenant_id","=","tenant_detail.user_id");
                                    })
                                    ->where('tenant_detail.user_id', '=', $tenant_id)
                                    ->where('u.status', '=', 1)
                                    ->get()->first();

    }

    public static function update_thumb($user_id, $file_name) {
        $tenant = self::where('user_id',$user->id)->first();
        $tenant->thumb = '/uploads/tenant/'.$user['id'].'/'.$fileName;
        $tenant->save();
    }

    public static function update_tenant_details($user_id, $request) {
        date_default_timezone_set('Australia/Brisbane');

        $tenant = self::where('user_id',$user_id)->first();
        $tenant->first_name = request('fname');
        $tenant->last_name = request('lname');
        $tenant->gender = request('gender');
        $tenant->dob = request('dob');
        $tenant->mobile = request('mobile');

        $tenant->emergency_no = request('emergency_no');
        $tenant->emergency_person = request('emergency_person');
        $tenant->emergency_title = request('emergency_title');

        $tenant->preferred_lang = request('preferred_lang');

        
        $tenant->save();
    }
}
