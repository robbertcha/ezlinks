<?php

namespace App\Model\Tenant;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Tenant_Case extends Model {
    protected $table = "case";

    public static function maintenance_list_detail($id) {
        return self::select('id','title','progress','created_at','case_type','category_id','job_area','priority','content')
        ->where('cases.id','=',$id)
        ->where('cases.status','=',1)
        ->get()->first();
    }

    public static function maintenance_list_content($page = 1, $type = 'a',$id, $user_detail) {
        return self::select('id','title','created_at','content','progress','status',
        DB::raw("
        (CASE
            WHEN cases.progress = '1' THEN 'New'
            WHEN cases.progress = '2' THEN 'In Progress'
            WHEN cases.progress = '3' THEN 'Completed'
            WHEN cases.progress = '4' THEN 'Closed'
            WHEN cases.progress = '5' THEN 'Awaiting Contractor'
            WHEN cases.progress = '6' THEN 'Awaiting Approval'
            WHEN cases.progress = '7' THEN 'Commitee Approval'
            WHEN cases.progress = '8' THEN 'Admin Approval'
            WHEN cases.progress = '9' THEN 'Building Manager Discretion'
        END) AS progress_txt
        "))
        ->where('cases.status','=',1)
        ->where('cases.building_id', '=', $user_detail['building_id'])
        ->when($type != 'a', function ($query) use ($type) {
            $query->where(function ($q) use ($type) {
                $q->where('cases.progress', '!=', '3');
                $q->Where('cases.progress', '!=', '4');
            });
        })
        ->when($id != 0, function($q)use($id){
            $q->where('cases.progress','=', $id);
        })
        ->orderBy('cases.created_at','desc')
        ->paginate(10, ['*'], 'page', $page);
    }
}