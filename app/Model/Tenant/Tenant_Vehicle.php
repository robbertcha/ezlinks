<?php

namespace App\Model\Tenant;

use Illuminate\Database\Eloquent\Model;

class Tenant_Vehicle extends Model
{
    //
    public $table = "tenant_vehicle";
    public function tenant() {
        return $this->belongsTo('App\Tenant');
    }

    public static function create_vehicle($user_id, $request) {
        $vehicle = new tenant_vehicle();
        $vehicle->tenant_id = $user_id;
        $vehicle->year = request('year');
        $vehicle->make = request('make');
        $vehicle->model = request('model');
        $vehicle->color = request('color');
        $vehicle->plate_no = request('plate_number');
        $vehicle->state = request('state');
        $vehicle->status = 1;

        $vehicle->save();
        return $vehicle->id;
    }
}
