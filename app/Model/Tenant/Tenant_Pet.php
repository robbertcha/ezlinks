<?php

namespace App\Model\Tenant;

use Illuminate\Database\Eloquent\Model;

class Tenant_Pet extends Model
{
    public $table = "tenant_pet";
    public static $pet_types = array(
                            0 => array(
                                'id' => 1,
                                'title' => 'Dog'
                            ),
                            1 => array(
                                'id' => 2,
                                'title' => 'Cat'
                            ),
                            2 => array(
                                'id' => 3,
                                'title' => 'Birds'
                            ),
                            3 => array(
                                'id' => 4,
                                'title' => 'Rabbits'
                            ),
                            4 => array(
                                'id' => 5,
                                'title' => 'Reptile'
                            ),
                            5 => array(
                                'id' => 6,
                                'title' => 'Small Mammals'
                            ),
                            6 => array(
                                'id' => 7,
                                'title' => 'Others'
                            )
                        );
    public static function getAllPetTypes(){
        return self::$pet_types;
    }

    public static function create_pet($user_id, $request) {
        $pet = new tenant_pet();
        $pet->tenant_id =  $user_id;
        $pet->type = $request['type'];
        $pet->variety = $request['variety'];
        $pet->color = $request['color'];
        $pet->borned_in = $request['year'];
        $pet->status = 1;

        $pet->save();
        return $pet->id;
    }

}
