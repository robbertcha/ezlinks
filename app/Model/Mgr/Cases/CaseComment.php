<?php

namespace App\Model\Mgr\Cases;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Model\Mgr\Cases\BuildingCase;

class CaseComment extends Model {
    protected $table = "case_comment";
    protected $guarded = [];

    /**
     * The "booting" method of the model.
     * Only search the result when its status is 1.
     * @return void
     */
    protected static function boot() {
        parent::boot();

        static::addGlobalScope('status', function(Builder $builder) {
            $builder->where('status', '=', 1);
        });
    }

    /**
     * Bind one to one relationship with comment.
     */
    public function case() {
        return $this->belongsTo('BuildingCase');
    }
}