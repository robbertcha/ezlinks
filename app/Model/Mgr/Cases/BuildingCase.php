<?php

namespace App\Model\Mgr\Cases;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Contractor;
use App\Model\Mgr\Cases\CaseAttachment;
use App\Model\Mgr\Cases\CaseComment;
// enum type
use App\Enums\BuildingCase\RoutineType;

use App\Building_Stats;
use App\Building_Case_Attachment;

// Service
use App\Services\Mgr\Cases\CaseService;

// Building Case
class BuildingCase extends Model {
    protected $table = 'cases';
    // all fields can be filled in 
    protected $guarded = [];
    
    public $timestamps = true;

    /**
     * The "booting" method of the model.
     * Only search the result when its status is 1.
     * @return void
     */
    protected static function boot() {
        parent::boot();

        static::addGlobalScope('cases.status', function(Builder $builder) {
            $builder->where('cases.status', '=', 1);
        });
    }

    /**
     * Find by building Id
     */
    public function scopeOfBuildingId($query, $buildingId){
        return $query->where('building_id', $buildingId);
    }

    /**
     * Bind one to one relationship with contractor.
     */
    public function contractor() {
        return $this->belongsTo('App\Contractor', 'assignee_id');
    }

    /**
     * Bind one to many relationship with attachments.
     */
    public function caseAttachments() {
        return $this->hasMany('App\Model\Mgr\Cases\CaseAttachment', 'case_id','id');
    }

    /**
     * Bind one to one relationship with comment.
     */
    public function caseComments() {
        return $this->hasMany('CaseComment', 'case_id');
    }
    
    /**
     * Get Case By id
     * @param caseId
     * @return caseDetails
     */
    public function getCaseById($id) {
        return $this::find($id)->toArray();
    }
    /**
     * Get required fields of case list including contractors' comp name.
     * 
     * @param  buildingId see the OfBuildingId method
     * @return array with case & contractor field
     */
    public function getCaseListWithContractor($buidingId) {
        return $this::OfBuildingId($buidingId)->select('id', 'content', 'case_type', 'reporter', 'assignee_id',
                    'due_date', 'progress')->with(['contractor' => function ($q) {
                        $q->select('id', 'comp_name')->orderBy('created_at', 'desc');
                    }])->paginate(12);
    }

    /**
     * Update a case if it is exist, otherwise create a new one
     * @param request array of details of case
     * @return not known
     */
    public function saveCase($request) {
        
        return $this::updateOrCreate(['id'=>$request['id']], $request);
    }

    /**
     * Get the cases' history for new case
     * @param  buildingId   
     * @return array with case & contractor field
     */ 
    public function getCaseHistory($buildingId) {
        return $this::ofBuiildingId($buildingId)->select('id', 'assignee_id', 'content', 'case_type'
        ,'job_area', 'area_detail', 'asset_id', 'unit_id');
    }

    /**
     * Get attachments by case id
     * @param int
     * @return attachments 
     */
    public function getAttachementsByCaseId($caseId) {
        return $this::find($caseId)->caseAttachments->toArray();
    }

    /**
     * Save attachments
     * @param caseId&attachments
     * @return boolean
     */
    public function saveAttachmentsByCaseId($caseId, $attachments) {
        return $this::find($caseId)->caseAttachments()->createMany($attachments);
    }

    /**
     * Get comment by case id
     * @param caseId
     * @return comment
     */
    public function getCommentsByCaseId($caseId) {
        return $this::find($caseId)->caseComments->get();
    }

    /**
     * Save comment by case id
     * @param caseId&request
     */
    public function saveCommentsByCaseId($caseId, $request) {
        return $this::find($caseId)->caseComments()->create($request);
    }

    /**
     * Get case id where routine is not none with status is 1 and due_date is yesterday.
     * @return array case id
     */
    public function getRoutineCaseId() {
        $yesterdayDate = date("Y-m-d", strtotime("-1 day"));
        
        return $this::select('id')->from(DB::raw('`cases` FORCE INDEX (`routine`)'))
                        ->where('routine', '>', 0)
                        ->where('due_date', $yesterdayDate)->get()->ToArray();
    }

    /**
     * Insert many new cases
     * @param array of cases
     */
    public function saveManyCases($cases) {
        return $this::insert($cases);
    }

    /**
     * @param int,int
     * @return boolean
     */
    public function setCaseProgress($caseId, $progress) {
        if (!$caseId) return FALSE;
        return $this::where('id', $caseId)->update(['progress' => $progress, 'routine' => 0]);
        
    }

    /**
     * @param int,int
     * @return boolean
     */
    public function setCaseStaus($caseId, $status) {
        if (!$caseId) return FALSE;
        return $this::where('id', $caseId)->update(['status' => $status]);  
    }

    /**
     * @param int $caseID
     * @return boolean
     */
    public function updateInspectionsStatus($caseID) {
        if (!$caseID) return FALSE;
        return $this::where('id', $caseID)->update(['inspection_done' => true]);  
    }

    /**
     *  Create case when new event is created and type is "job"
     *  @param $user user data
     *  @param $request
     */
    public function createCaseFromSchedule($request, $user) {
        if ((isset($request['typeId']) && $request['typeId'] == 1) || (isset($request['type']) && $request['type'] == 1)) {
            $case['tenant_id'] = isset($request['newTenant']) ? $request['newTenant'] : $request['tenant_id'];
            $case['building_id'] = $user['building_id'];
            $case['assignee_id'] = isset($request['contractor_list']) ? $request['contractor_list'] : (isset($request['contractor_id']) ? $request['contractor_id'] : $request['assignee_id']);
            $case['asset_id'] = $request['asset_id'];
            $case['unit_id'] = $request['unit_id'];
            $case['bi_id'] = isset($request['bi_list']) ? $request['bi_list'] : $request['bi_id'];
            // $case['location'] = $request['job_area'];
            $case['case_type'] = isset($request['case_type']) ? $request['case_type'] : null;
            $case['job_area'] = $request['job_area'];
            $case['area_detail'] = $request['area_detail'];
            $case['content'] = isset($request['details']) ? $request['details'] : null;
            $case['progress'] = isset($request['case_progress']) ? $request['case_progress'] : $request['progress'];
            $case['priority'] = $request['priority'];
            $case['routine'] = isset($request['routine']) ? $request['routine'] : 0;
            $case['status'] = 1;

            $caseService = new CaseService();
            $saveCase = $caseService->saveCase($case);

            if ($saveCase['progress'] != 3 && $saveCase['progress'] != 4) {
                // update stats
                $building_stats = new Building_Stats();
                $building_stats->increase('total_case', 1, $user['building_id']);
            }

            // deprecated
            // $log = new Building_Log;
            // $log_bi_data = $log->generate_case_log($saveCase['id'], $user['name'], $user['building_id'], 'create', 'case', null, $request['case_progress']);

            return $saveCase;
        }
    }

    /**
     * Get latest record of case based on amenity reservation
     * @param $event_id amenity_reservation id
     * @return case
    */
    public function getLatestCaseByEventID($event_id, $except = null) {
        return collect(self::select("cases.tenant_id","cases.building_id","cases.assignee_id", "cases.progress as case_progress","cases.category_id","cases.asset_id","cases.unit_id","cases.bi_id","cases.location","cases.reporter","cases.reporter_phone","cases.case_type","cases.job_area","cases.area_detail","cases.title","cases.content","cases.priority","cases.due_date","cases.due_time")
                    ->leftJoin('amenity_reservation as ar','ar.case_id','=','cases.id')
                    ->where('ar.parent_id', '=', $event_id)
                    ->where('ar.status', '=', 1)
                    // ->where('cases.status', '=', 1)
                    ->orderBy('cases.id', 'desc')
                    ->limit(1)->get()->toArray())->except($except);
    }

    /**
     * Remove cases by sub-event case-ids array
     *  @param array $caseIDs case ids
     */
    public function removeCasesByIDs($caseIDs) {
        return self::whereIn('id', $caseIDs)->update(array('status' => 0));
    }

    public static function get_case_type() {
        $case = (new BuildingCase)->getTable();
        $case_types = DB::select(DB::raw('SHOW COLUMNS FROM `'.$case.'` WHERE Field = "case_type"'))[0]->Type;;
        preg_match('/^enum\((.*)\)$/', $case_types, $matches);
        $enum = array();
        foreach(explode(',', $matches[1]) as $value){
            $v = trim( $value, "'" );
            $q['value'] = $v;
            $q['title'] = ucfirst(str_replace('_', ' ', $v));
            $enum[] = $q;
        }

        return $enum;
    }

    public function get_case_progress() {
        return Array(
                Array ('title' => 'New','value' => '1'),
                Array ('title' => 'In Progress','value' => '2'),
                Array ('title' => 'Completed','value' => '3'),
                Array ('title' => 'Closed','value' => '4'),
                Array ('title' => 'Awaiting Contractor','value' => '5'),
                Array ('title' => 'Awaiting Approval','value' => '6'),
                Array ('title' => 'Commitee Approval','value' => '7'),
                Array ('title' => 'Admin Approval','value' => '8'),
                Array ('title' => 'Building Manager Discretion','value' => '9')
        );
    }
    
    public function get_case_area() {
        return Array(
                Array ('title' => 'Common Area - Asset','value' => '1'),
                Array ('title' => 'Common Area - Non Asset','value' => '2'),
                Array ('title' => 'Private Lot','value' => '3')
        );
    }

    public function getCaseCommentsByCaseID($cid) {
        return DB::table('case_comment')->select("id", "case_id", "author_id", "author", "comment", "created_at")
                        ->where('case_id', '=', $cid)
                        ->where('status', '=', 1)
                        ->get()->toArray();
    }

    /**
     * Get case details by case ID
     * Note: Reservation event created on case -> parent_id = null && case_id > 0 && time_id = null
     * @param int $case_id
     * @return object
     */
    public function getCaseDetailsByBuildingID($case_id, $building_id) {
        return  self::select("cases.id", "cases.routine", "cases.tenant_id", "cases.title", "cases.assignee_id", "cases.location", "cases.asset_id", "cases.unit_id", "cases.bi_id", "bia.area_name as bi_txt", "cases.reporter", "cases.reporter_phone", "cases.due_date", "cases.due_time", "cases.case_type", "cases.job_area", "cases.title", "cases.content", "cases.progress", "cases.created_at", "cases.priority", "cases.area_detail", "cases.inspection_done","td.first_name","td.last_name", "unit.unit_title", DB::raw("(SELECT GROUP_CONCAT( CONCAT(cp.id, '&&', ba.id,'&&',ba.name,'&&',cp.message,'&&',cp.progress,'&&', cp.created_at) SEPARATOR '|') as progress FROM case_progress as cp left join building_admin as ba on ba.id = cp.admin_id where cases.id = cp.case_id order by cp.created_at desc) as case_progress "), DB::raw("(CASE 
                        WHEN  cases.job_area = '1' THEN 'Common Area - Asset'
                        WHEN  cases.job_area = '2' THEN 'Common Area - Non Asset'
                        WHEN  cases.job_area = '3' THEN 'Private Lot'
                    END) AS job_area_name"), 'ar.parent_id'
                    )
                    ->join('amenity_reservation as ar',function($join){
                        $join->on('ar.case_id','=','cases.id');
                    }, null,null,'left')
                    ->join('tenant_detail as td',function($join){
                        $join->on('td.user_id','=','cases.tenant_id');
                    }, null,null,'left')
                    ->join('unit',function($join){
                        $join->on('unit.id','=','td.unit_id');
                    }, null,null,'left')
                    ->join('building_inspection_area as bia',function($join){
                        $join->on('bia.id','=','cases.bi_id');
                    }, null,null,'left')
                    ->where('ar.parent_id', '>', 0)
                    ->where('cases.id', '=', $case_id)
                    ->where('cases.building_id', '=', $building_id)
                    ->where('cases.status', '=', 1)
                    ->where('ar.status', '=', 1)
                    ->orderBy('cases.created_at', 'desc')
                    ->get()->first();
    }


    
}

