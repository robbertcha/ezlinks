<?php

namespace App\Model\Mgr\Cases;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Model\Mgr\Cases\BuildingCase;

class CaseAttachment extends Model {
    protected $table = 'case_attachment';
    protected $guarded = [];

    public $timestamps = true;
    /**
     * The "booting" method of the model.
     * Only search the result when its status is 1.
     * @return void
     */
    protected static function boot() {
        parent::boot();

        static::addGlobalScope('status', function(Builder $builder) {
            $builder->where('status', '=', 1);
        });
    }

    /**
     * Bind one to many relationship with attachments.
     */
    public function case() {
        return $this->belongsTo('App\Model\Mgr\Cases\BuildingCase', 'case_id');
    }

    /**
     * Save existed attachment for other case;
     * @param array
     * @return boolean;
     */
    public function saveDupAtForCase($attachment) {
        return $attachment->save();
    }

    /**
     * Save existed attachment for other case;
     * @param array
     * @return boolean;
     */
    public function saveManyAttachments($attachments) {
        
        return $this::insert($attachments);
    }


    public function get_case_attachments($cid, $type) {
        return self::select("id","type", "url" , "name", 'size',"ext as extension", "status")
                            ->where('case_id', '=', $cid)
                            ->where('type', '=', $type)
                            ->where('status', '=', 1)
                            ->get()->toArray();
    }
    
}