<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use NotificationChannels\WebPush\WebPushMessage;
use NotificationChannels\WebPush\WebPushChannel;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PushDemo extends Notification
{
    use Queueable;
    var $object_id;
    var $action;
    var $object_arr;
    
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($action, $object_arr, $object_id)
    {
        //
        $this->object_arr = $object_arr;
        $this->action = $action;
        $this->object_id = $object_id;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        // return ['mail'];
        return [WebPushChannel::class];
    }

    public function toWebPush($notifiable, $notification)
    {
        //if ($this->action == 'announcement') {
            return (new WebPushMessage)
                    ->title($this->object_arr->title)
                    ->icon('https://smallimg.pngkey.com/png/small/10-104244_location-png-icon-transparent-stock-location-icon-red.png')
                    ->body($this->object_arr->content);
                    // ->action('View App', 'open_url');
                    // ->data(['id' => 12]);
        //}
        
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
