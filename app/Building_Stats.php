<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Building_Stats extends Model
{
      protected $table = 'building_stats';

      public function increase($object, $amount = 1, $building_id) {
            return $this->where('building_id', $building_id)
                        ->update([
                              $object=> DB::raw($object.'+'.$amount)
                        ]);
      }

      public function decrease($object, $amount = 1, $building_id) {
            return $this->where('building_id', $building_id)
                        ->update([
                              $object=> DB::raw($object.'-'.$amount)
                        ]);
      }
}
