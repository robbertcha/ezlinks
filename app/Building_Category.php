<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Building_Category extends Model
{
    //
    protected $table = 'building_category';

    protected $fillable = ['name', 'building_id', 'cate_id', 'parent_id', 'description', 'status'];
    
    public static function remove_category($id) {
        $cate_status = self::where('id', '=', $id)
                        -> update(['status' => '0']);
        
        return $cate_status;
    }

    public static function update_category($id, $name, $description) {
        $cate = self::where('id', '=', $id)
                    -> update(['name' => $name, 'description' => $description]);
        return $cate;
    }
    // cate id is not equal to id, used to sepreate asset and inventory.
    public static function add_category($request, $building_id, $cate_id) {
        $store = new Building_Category;
        $store->name = request('name');
        $store->building_id = $building_id;
        $store->cate_id = $cate_id;
        $store->parent_id = 0;
        $store->description = request('description');
        $store->status = 1;
        $store->save();
        return $store->id;
    }

    public static function get_asset_category($building_id) {
        $cate = self::select('id', 'name', 'description')
                    ->where('building_id', '=', $building_id)
                    ->where('cate_id', '=', 1)
                    ->where('status', '=', 1)
                    ->orderBy('cate_id', 'ASC')
                    ->get()->toArray();
        return $cate;
    }

    public static function get_inventory_category($building_id) {
        $cate = self::select('id', 'name', 'description')
                    ->where('building_id', '=', $building_id)
                    ->where('cate_id', '=', 6)
                    ->where('status', '=', 1)
                    ->orderBy('cate_id', 'ASC')
                    ->get()->toArray();
        return $cate;
    }
}
