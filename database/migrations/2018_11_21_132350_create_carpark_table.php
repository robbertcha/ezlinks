<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarparkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('carpark', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('building_id');
            $table->integer('company_id');
            $table->string('carpark_no');
            $table->string('type');
            $table->timestamps('updated_by');
            $table->boolean('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('carpark');
    }
}
