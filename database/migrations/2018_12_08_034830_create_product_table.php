<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('product', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('merchant_id');
            $table->integer('product_cate_id');
            $table->string('selling_type');     //  直营， 第三方
            $table->string('selling_model');    //  normal / groupon
            $table->string('able_delivery');
            $table->string('title');
            $table->string('content');
            $table->integer('width');
            $table->integer('height');
            $table->timestamp('start_time');
            $table->timestamp('end_time');
            $table->timestamp('deal_valid');
            $table->string('inclusions');
            $table->string('availability');
            $table->string('condition_deal');
            $table->string('redeem');
            $table->string('cancellation');
            $table->string('rate');
            $table->integer('total_sales');
            $table->integer('total_feedback');
            $table->integer('total_like');
            $table->timestamps('updated_by');
            $table->boolean('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('product');
    }
}
