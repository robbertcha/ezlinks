<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmenityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('mgr_amenity', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('admin_id');
            $table->integer('building_id');
            $table->integer('company_id');
            $table->string('amenity');
            $table->longText('gallery');
            $table->integer('allocation_limit');
            $table->integer('hours_limit');
            $table->longText('instruction');
            $table->longText('policy');
            $table->longText('openning_hour');
            $table->longText('disable_date');
            $table->timestamps('updated_by');
            $table->boolean('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('mgr_amenity');
    }
}
