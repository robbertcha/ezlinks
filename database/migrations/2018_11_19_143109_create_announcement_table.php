<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnouncementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('mgr_announcement', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('admin_id');
            $table->integer('type');
            $table->string('tag');
            $table->string('title');
            $table->longText('content');
            $table->timestamp('start_time');
            $table->timestamp('end_time');
            $table->timestamp('publish_time');
            $table->longText('recipients');
            $table->boolean('show_event_btn');
            $table->string('btn_words');
            $table->integer('total_participants');
            $table->timestamps('updated_by');
            $table->boolean('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('mgr_announcement');
    }
}
