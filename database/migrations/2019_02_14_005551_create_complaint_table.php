<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComplaintTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('complaint', function (Blueprint $table) {
            $table->integer('id');
            $table->integer('tenant_id');
            $table->integer('assignee_id');
            $table->integer('category_id');
            $table->string('title');
            $table->longText('content');
            $table->string('progress');
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('complaint');
    }
}
