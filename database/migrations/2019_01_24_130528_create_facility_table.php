<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacilityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('amenity', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('building_id');
            $table->string('name');
            $table->string('gallery');
            $table->string('allocation_limit');
            $table->string('hours_limit');
            $table->string('disable_date');
            $table->string('able_recurring');
            $table->string('rules');
            $table->timestamps();
            $table->boolean('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('amenity');
    }
}
