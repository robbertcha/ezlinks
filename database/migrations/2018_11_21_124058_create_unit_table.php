<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('unit', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('admin_id');
            $table->integer('building_id');
            $table->integer('company_id');
            $table->integer('owner_id');
            $table->integer('carpark_id');
            $table->string('unit_title');
            $table->timestamps('updated_by');
            $table->boolean('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('unit');
    }
}
