<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMerchantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('merchant', function (Blueprint $table) {
            $table->increments('id');
            $table->string('selling_area');
            $table->string('name');
            $table->string('logo');
            $table->string('gov_lic');
            $table->string('address');
            $table->string('city');
            $table->string('postcode');
            $table->string('state');
            $table->string('contact_person');
            $table->string('contact_no');
            $table->string('fax');
            $table->string('email');
            $table->string('website');
            $table->boolean('open_public_holiday');
            $table->string('shipping_policy');
            $table->timestamps();
            $table->boolean('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('merchant');
    }
}
