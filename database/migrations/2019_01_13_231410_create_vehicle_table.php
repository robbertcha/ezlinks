<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('vehicle', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tenant_id');
            $table->integer('year');
            $table->integer('parking_id');
            $table->string('make');
            $table->string('model');
            $table->string('color');
            $table->string('state');
            $table->string('tag');
            $table->timestamps();
            $table->boolean('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('vehicle');
    }
}
