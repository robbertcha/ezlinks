<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('event_participants', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('announcement_id');
            $table->integer('user_id');
            $table->timestamp('created_at')->after('user_id');
            $table->boolean('status');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('event_participants');
    }
}
