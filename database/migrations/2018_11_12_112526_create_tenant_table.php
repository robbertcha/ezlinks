<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTenantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->index()->unsigned();
            $table->integer('building_id');
            $table->integer('unit_id');
            $table->integer('emer_contact_id');
            $table->string('type');
            $table->string('thumb');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('gender');
            $table->string('contact');
            $table->string('preferred_lang');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenant_detail');
    }
}
