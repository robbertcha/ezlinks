<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;

class UsersTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->delete();
        User::create([
            'name' => 'robbertcha',
            'email'    => 'robbertcha@live.com',
            'password' => Hash::make('820907'),
            'remember_token' =>  str_random(10),
            'type' => 'tenant',
        ]);
    }
}
