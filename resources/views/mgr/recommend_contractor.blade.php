@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div id="main_content">
                <div id="content_header">
                    <i id="bk_to_previous_pg" class="fas fa-angle-left" onclick="window.history.go(-1);"></i>
                    @if(\Request::route()->getName() == 'mgr_recommend_contractor') 
                        <h2 id="content_title">Recommend New Contractor</h2>
                    @else
                        <h2 id="content_title">{{$contractor['comp_name']}}</h2>
                    @endif
                </div>
                
                <div class="content_body">
                    <recommend-contractor :contractor="{{  json_encode($contractor) }}" :cont_cate="{{  json_encode($cont_cate) }}"></recommend-contractor>
                </div>
            </div>
        </div>
    </div>

@endsection