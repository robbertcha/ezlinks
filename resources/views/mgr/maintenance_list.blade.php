@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div id="main_content">
                <div id="content_header">
                    <h2 id="content_title">Maintanence List</h2>
                    <!-- <a href="{{ url('/mgr/create_maintenance') }}" class="btn btn_new" >New +</a> -->
                </div>
                
                <div class="content_body">
                    <maintanence-list  :reservation="{{  json_encode($reservation) }}" :search="{{  json_encode($search) }}"></maintanence-list>
                </div>
            </div>
        </div>
    </div>

@endsection