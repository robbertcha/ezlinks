@extends('layouts.app')

@section('content')


    <div class="row">
        <div class="col-md-12">
            <div id="main_content">
                <div id="content_header">
                    <h2 id="content_title">Settings</h2>
                </div>

                <div class="content_body">

                    <ul class="setting_menu">

                        <li>
                            <div class="setting_option">
                                <a href="">
                                    <i class="far fa-building"></i>
                                    <span>Setup</span>
                                </a>
                            </div>
                        </li>

                        <li>
                            <div class="setting_option">
                                <a href="">
                                    <i class="fas fa-couch"></i>
                                    <span>Assets</span>
                                </a>
                            </div>
                        </li>

                        <li>
                            <div class="setting_option">
                                <a href="">
                                    <i class="fas fa-boxes"></i>
                                    <span>Inventory</span>
                                </a>
                            </div>
                        </li>

                        <li>
                            <div class="setting_option">
                                <a href="">
                                    <i class="far fa-folder-open"></i>
                                    <span>Categories</span>
                                </a>
                            </div>
                        </li>

                        <li>
                            <div class="setting_option">
                                <a href="">
                                    <i class="fas fa-book-reader"></i>
                                    <span>Library</span>
                                </a>
                            </div>
                        </li>

                        <li>
                            <div class="setting_option">
                                <a href="">
                                    <i class="far fa-file-alt"></i>
                                    <span>Reports</span>
                                </a>
                            </div>
                        </li>

                        <li>
                            <div class="setting_option">
                                <a href="">
                                    <i class="far fa-file-word"></i>
                                    <span>Templates</span>
                                </a>
                            </div>
                        </li>

                        <li>
                            <div class="setting_option">
                                <a href="">
                                    <i class="fas fa-info"></i>
                                    <span>Info</span>
                                </a>
                            </div>
                        </li>
                        <li>
                            <div class="setting_option">
                                <a href="">
                                    <i class="fa fa-question"></i>
                                    <span>Help</span>
                                </a>
                            </div>
                        </li>


                    </ul>
                </div>
            </div>
        </div>
    </div>

@endsection