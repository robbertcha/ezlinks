@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div id="main_content">
                <div id="content_header">
                    <h2 id="content_title">Complaints</h2>
                </div>
                
                <div class="content_body">
                    <complaint-list :complaints="{{  json_encode($complaints) }}"></complaint-list>
                    {{ $complaints->render() }}
                </div>
            </div>
        </div>
    </div>

@endsection