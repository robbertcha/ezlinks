@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div  id="main_content">
                <div id="content_header">
                    <h2 id="content_title">Reservations</h2>
                    <button id="show_modal" class="btn btn_new" onclick="window.location.href='{{ url('/mgr/facilities') }}'">Facilities</button>
                </div>

                <div class="content_body">
                    <full-calendar :events="fcEvents" locale="en"></full-calendar>
                </div>
            </div>
        </div>
    </div>

@endsection