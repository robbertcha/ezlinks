@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div  id="main_content">
                <div id="content_header">
                    <h2 id="content_title">Product List</h2>
                    <button class="btn btn_new" onclick="window.location.href='{{ url('/mgr/create_service') }}'">New +</button>
                </div>

                <div class="content_body">
                    <div class="list_cont">
                        <div class="list_header">
                            <div class="list_fields w_10">#</div>
                            <div class="list_fields w_30">TITLE</div>
                            <div class="list_fields w_15 c_align">MERCHANTS</div>
                            <div class="list_fields w_15 c_align">RATE</div>
                            <div class="list_fields w_15 c_align">ISSUED DATE</div>
                            <div class="list_fields w_15 c_align">PUBLISHER</div>
                        </div>
                        @foreach($all_products as $data)
                            <div class="list_row">
                                <div class="list_fields w_10">{{ $data->id }}</div>
                                <div class="list_fields w_10">{{ $data->title }}</div>
                                <div class="list_fields w_15 c_align">{{ $data->merchant_name }}</div>
                                <div class="list_fields w_15 c_align">$ {{ $data->rate }}</div>
                                <div class="list_fields w_15 c_align">{{ $data->created_at }}</div>
                                <div class="list_fields w_15 c_align">{{ $data->name }}</div>
                            </div>
                        @endforeach
                    </div>
                    {{ $all_products->render() }}
                </div>
            </div>
        </div>
    </div>


@endsection