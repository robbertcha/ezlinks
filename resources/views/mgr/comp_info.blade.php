@extends('layouts.app')

@section('content')


    <div class="row">
        <div class="col-md-12">
            <div id="main_content">
                <div id="content_header">
                    <h2 id="content_title">Building Profile</h2>
                </div>

                <div class="content_body">
                    <building-info :building="{{  json_encode($building) }}"></building-info>
                </div>
            </div>
        </div>
    </div>

@endsection