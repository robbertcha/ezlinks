@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div id="main_content">
                <div id="content_header">
                    <h2 id="content_title">Parking Breaches</h2>
                    <a href="{{ url('/mgr/create_parking_breaches') }}" class="btn btn_new" >New +</a>
                </div>
                
                <div class="content_body">
                    <parking-breaches-list :parking_breaches="{{  json_encode($parking_breaches) }}"></parking-breaches-list>

                    {{ $parking_breaches->render() }}
                </div>
            </div>
        </div>
    </div>

@endsection