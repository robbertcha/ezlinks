@extends('layouts.app')

@section('content')
    <!-- <question-mark slug="manage-unit-detail"></question-mark> -->
    <div class="row">
        <div class="col-md-12" id="unit_form">
            <div id="main_content">
                <div id="content_header"> 
                    <h2 id="content_title">Unit Details</h2>
                </div>

                <div class="content_body">
                    <unit-form :current_uid="{{ $uid }}" :unit_details="{{  json_encode($unit) }}" :tags="{{  json_encode($unit['tags']) }}" :units="{{  json_encode($units) }}"></unit-form>
                </div>
            </div>
        </div>
    </div>
    <popup-cpdetails></popup-cpdetails>
@endsection