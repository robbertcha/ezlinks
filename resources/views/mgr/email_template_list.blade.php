@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div id="main_content">
                <div id="content_header">
                    <h2 id="content_title">Email Template</h2>
                    <button id="show_modal" class="btn btn_new" @click="showModal = true">New +</button>
                </div>

<div class="content_body">
                <div class="template_content">
                    <h4>Welcome Template</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <textarea class="email_template" id="welcome_content">{{$content}}</textarea>
                    <div class="field_cont btn_cont">
                        <button class="btn">Save</button>
                    </div>
                </div>
                
                


                <div class="template_content">
                    <h4>Reservation Template</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <textarea class="email_template" id="reservation_content">{{$content}}</textarea>
                    <div class="field_cont btn_cont">
                        <button class="btn">Save</button>
                    </div>
                </div>

</div>
            </div>
        </div>
    </div>
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
    <script>tinymce.init({ selector:'textarea.email_template' });</script>
@endsection