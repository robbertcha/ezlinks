@extends('layouts.app')

@section('content')


    <div class="row">
        <div class="col-md-12">
            <div id="main_content">
                  <div id="content_header">
                        @if($pb_id > 0 )  
                            <h2 id="content_title">Parking Breaches Details</h2>
                        @else
                            <i id="bk_to_previous_pg" class="fas fa-angle-left" onclick="window.history.go(-1);"></i>
                            <h2 id="content_title">New Parking Breaches</h2>
                        @endif
                  </div>

                  <div class="content_body">
                        <parking-breaches-detail :pb_id="{{  json_encode($pb_id) }}" :pb="{{  json_encode($pb) }}"></parking-breaches-detail>
                  </div>
            </div>
        </div>
    </div>

@endsection