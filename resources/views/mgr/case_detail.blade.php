@extends('layouts.app')

@section('content')
<!-- <question-mark slug="case-detail"></question-mark> -->
<div class="row">
        <div class="col-md-12">
            <div id="main_content">
                <div id="content_header">
                    <i id="bk_to_previous_pg" class="fas fa-angle-left" onclick="window.history.go(-1);"></i>
                    @if(!$case['id'])
                        <h2 id="content_title">New Job</h2>
                    @else
                        <h2 id="content_title">Edit Job</h2>
                    @endif
                </div>
                
                <div class="content_body">
                    <case :assets="{{  json_encode($assets) }}" :units="{{  json_encode($units) }}" :tenant_list="{{  json_encode($tenant_list) }}" :case_detail="{{  json_encode($case) }}" :type="{{  json_encode($case_type) }}" :case_area="{{  json_encode($case_area) }}"  :case_progress="{{  json_encode($case_progress) }}" :case_scheduler="{{ json_encode($case_scheduler) }}" :case_docs="{{ json_encode($case_docs) }}" :case_images="{{ json_encode($case_images) }}" :case_quotes="{{ json_encode($case_quotes) }}" :case_invoices="{{ json_encode($case_invoices) }}" :case_log="{{ json_encode($case_log) }}" :contractor_list="{{ json_encode($contractor_list) }}" :template_list="{{ json_encode($template_list) }}" :admin="{{ json_encode($admin) }}" :bi_list="{{ json_encode($bi_list) }}"   :case_comments="{{ json_encode($case_comments) }}"></case>
                </div>
            </div>
        </div>
</div>

<popup-case-reservation :case_detail="{{  json_encode($case) }}"></popup-case-reservation>
<!-- <popup-case-email-contractor></popup-case-email-contractor> -->
<popup-case-invoice></popup-case-invoice>
<popup-case-quote></popup-case-quote>
@endsection