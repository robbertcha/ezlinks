@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div id="main_content">
                <div id="content_header">
                    <h2 id="content_title">Jobs</h2>
                    <help-all detail_id='4' direction='l'></help-all>
                    <btn-reservation-popup :text="'New +'" :type="1"></btn-reservation-popup>
                    <!-- <a href="{{ url('/mgr/create_inspection') }}" class="btn btn_new" >New +</a> -->
                </div>
                
                <div class="content_body">
                    <case-list :page="{{  json_encode($page) }}"  :search_term="{{  json_encode($search_term) }}"></case-list>
                </div>
            </div>
        </div>
    </div>

    <popup-booking-cont :stype="{{  json_encode($stype) }}" :contractor_list="{{ json_encode($contractor_list) }}" :bi_list="{{ json_encode($bi_list) }}" :tenants="{{ json_encode($tenants) }}" :amenity="{{ json_encode($amenity) }}" :units="{{ json_encode($units) }}" :selected_action="''"></popup-booking-cont>
    <popup-inspection-detail></popup-inspection-detail>
    <popup-calendar-event></popup-calendar-event>
@endsection