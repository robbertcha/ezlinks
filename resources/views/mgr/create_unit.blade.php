@extends('layouts.app')

@section('content')
    <!-- <question-mark slug="manage-unit-detail"></question-mark> -->
    <div class="row col">
        <div class="col-md-12" id="unit_form">
            <!-- <unit-form :current_uid="{{  $uid }}" :unit_details="{{  json_encode($unit) }}" :tags="{{  json_encode($unit['tags']) }}" :units="{{  json_encode($units) }}"></unit-form> -->
            <create-unit :current_uid="{{ $uid }}" :tags="{{  json_encode($unit['tags']) }}"></create-unit>
        </div>
    </div>

@endsection