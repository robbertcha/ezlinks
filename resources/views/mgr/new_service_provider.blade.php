@extends('layouts.app')

@section('content')


    <div class="row">
        <div class="col-md-12">
            <div id="main_content">
                <div id="content_header">
                    <h2 id="content_title">New Service Provider</h2>
                </div>

                <div class="content_body">
                    <div class="col form_cont">
                        <div class="col-md-6">
                            <div class="field_cont">
                                <input type="text" name="txt_buildingname" class="col-md-12" placeholder="Company Name">
                            </div>

                            <br>

                            <div class="field_cont">
                                <input type="text" name="txt_buildingaddress" class="col-md-12" placeholder="Address">
                            </div>

                            <br>

                            <div class="field_cont">
                                <div class="col-md-6">
                                    <input type="text" name="txt_buildingpostcode" class="col-md-12" placeholder="Postcode">
                                </div>

                                <div class="col-md-6">
                                    <input type="text" name="txt_buildingsurburb" class="col-md-12" placeholder="Surburb">
                                </div>
                            </div>

                            <br>

                            <div class="field_cont">
                                <div class="col-md-6">
                                    <input type="text" name="txt_buildingstate" class="col-md-12" placeholder="State">
                                </div>

                                <div class="col-md-6">
                                    <input type="text" name="txt_buildingcountry" class="col-md-12" placeholder="Country">
                                </div>    
                            </div>

                            <br>

                            <div class="field_cont">
                                <div class="col-md-6">
                                    <input type="text" name="txt_buildingworkphone" class="col-md-12" placeholder="Work">
                                </div>    
                            
                                <div class="col-md-6">
                                    <input type="text" name="txt_buildingfaxnumber" class="col-md-12" placeholder="Fax Number">
                                </div>
                            </div>

                            <br>

                            <div class="field_cont">
                                <div class="col-md-6">
                                    <input type="text" name="txt_buildingemail" class="col-md-12" placeholder="Email Address">
                                </div>
                                                            
                                <div class="col-md-6">
                                    <input type="text" name="txt_buildingwebsite" class="col-md-12" placeholder="Website">
                                </div>
                            </div>
                            
                            <br>

                            <div class="field_cont">
                                <div class="col-md-6">
                                    <input type="text" name="txt_contactperson" class="col-md-12" placeholder="Contact Person">
                                </div>
                                                            
                                <div class="col-md-6">
                                    <input type="text" name="txt_contactemail" class="col-md-12" placeholder="Contact Person Email">
                                </div>
                            </div>

                            <div class="field_cont">
                                <div class="col-md-6">
                                    <input type="file" name="txt_govlic" class="col-md-12" placeholder="Government License">
                                </div>
                                                            
                                <div class="col-md-6">
                                    <label class="lbl_radio col-md-6">
                                          <input type="radio" name="holiday_off" value="y"/>
                                          <div>Holiday Off</div>
                                    </label><label class="lbl_radio col-md-6">
                                          <input type="radio" name="holiday_off" value="n" checked="checked"/>
                                          <div>Holiday On</div>
                                    </label>
                                </div>
                            </div>
                            <div class="field_cont">
                                <textarea name="txt_buildingdesc" class="txt_aboutcomp col-md-12" row="20" placeholder="Company Descriptions"></textarea>
                            </div>
                        </div>
                        
                        <div class="col-md-6">
                            <div class="field_cont">
                                <img src="{{ asset('images/building_profile.png') }}" class="col-md-12" id="comp_profile">
                            </div>
                            
                        </div>

                        </div>
                        <div class="field_cont btn_cont">
                              <button class="btn">Close</button>
                              <button class="btn">Save</button>
                        </div>
                </div>
            </div>
        </div>
    </div>

@endsection