@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div  id="main_content">
                <div id="content_header">
                    <h2 id="content_title">Service Provider List</h2>
                    <button class="btn btn_new" onclick="window.location.href='{{ url('/mgr/new_service_provider') }}'">New +</button>
                </div>

                <div class="content_body">
                    <div class="list_cont">
                        <div class="list_header">
                            <div class="list_fields w_10">#</div>
                            <div class="list_fields w_20">COMPANY NAME</div>
                            <div class="list_fields w_15 c_align">CITY</div>
                            <div class="list_fields w_25 c_align">SELLING AREA</div>
                            <div class="list_fields w_15 c_align">CONTACT PERSON</div>
                            <div class="list_fields w_15 c_align">ACTIONS</div>
                        </div>
                        @foreach($sproviders as $data)
                            <div class="list_row">
                                <div class="list_fields w_10">{{ $data->id }}</div>
                                <div class="list_fields w_20">{{ $data->name }}</div>
                                <div class="list_fields w_15 c_align">{{ $data->city }}</div>
                                <div class="list_fields w_25 c_align">{{ $data->selling_area }}</div>
                                <div class="list_fields w_15 c_align">{{ $data->contact_person }}</div>
                                <div class="list_fields w_15 c_align">

                                </div>
                            </div>
                        @endforeach
                    </div>
                    {{ $sproviders->render() }}
                </div>
            </div>
        </div>
    </div>


@endsection