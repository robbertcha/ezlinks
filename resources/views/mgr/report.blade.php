@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col-md-12">
        <div id="main_content">
            <div id="content_header">
                <h2 id="content_title">Report </h2>
                <help-all detail_id='17' direction='l'></help-all>
            </div>
            
            <div class="content_body">
                <report></report>
            </div>
        </div>
    </div>
</div>

@endsection