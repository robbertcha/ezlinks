@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div id="main_content">
                <div id="content_header">
                    <h2 id="content_title">Assets</h2>
                    <a href="{{ url('/mgr/create_asset') }}" class="btn btn_new" >New +</a>
                </div>
                
                <div class="content_body">
                    <asset-list :assets="{{  json_encode($assets) }}" :asset_cate="{{  json_encode($asset_cate) }}" :search="{{  json_encode($search) }}"></asset-list>
                </div>
            </div>
        </div>
    </div>

    <popup-asset-history></popup-asset-history>
@endsection