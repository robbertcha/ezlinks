@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div id="main_content">
                <div id="content_header">
                    <h2 id="content_title">Template</h2>
                </div>
                
                <div class="content_body">
                    <template-list :list="{{  json_encode($list) }}"></template-list>
                </div>
            </div>
        </div>
    </div>

@endsection