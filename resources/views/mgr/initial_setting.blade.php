@extends('layouts.app')

@section('content')

     <div class="row">
        <div class="col-md-12">
            <div  id="main_content">
                <!-- <div id="content_header">
                    <h2 id="content_title">Dashboard</h2>
                </div> -->
                <div class="content_body">
                    <initial-setting :settings="{{ json_encode($setting_check)}}" :setting_count="{{ json_encode($setting_count)}}" :tags="{{ json_encode($tags)}}" :building_info="{{ json_encode($building_info)}}"></initial-setting>
                </div>
            </div>
        </div>
    </div>

@endsection
