@extends('layouts.app')

@section('content')
    <!-- <question-mark slug="contractors"></question-mark> -->
    <div class="row">
        <div class="col-md-12">
            <div id="main_content">
                <div id="content_header">
                    <h2 id="content_title">Contractors</h2>
                    <a href="{{ url('/mgr/recommend_contractor') }}" class="btn btn_new">
                        <span>New + </span>
                    </a>
                </div>
                
                <div class="content_body">
                        <contractor-list :contractors="{{  json_encode($contractors) }}" :c_cate="{{  json_encode($c_cate) }}" :search="{{  json_encode($search) }}"></contractor-list>
                        {{ $contractors->render() }}
                </div>
            </div>
        </div>
    </div>

@endsection