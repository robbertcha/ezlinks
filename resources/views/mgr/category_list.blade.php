@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div id="main_content">
                <div id="content_header">
                    <h2 id="content_title">Category</h2>
                </div>
                
                <div class="content_body">
                    <category-list :cate="{{  json_encode($cate) }}"></category-list>
                </div>
            </div>
        </div>
    </div>

@endsection