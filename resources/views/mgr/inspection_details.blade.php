@extends('layouts.app')

@section('content')


    <div class="row">
        <div class="col-md-12">
            <div id="main_content">
                  <div id="content_header">
                        @if($bia)  
                            <h2 id="content_title">Inspection Details</h2>
                        @else
                            <i id="bk_to_previous_pg" class="fas fa-angle-left" onclick="window.history.go(-1);"></i>
                            <h2 id="content_title">New Inspection</h2>     
                        @endif
                        <help-all detail_id='5' direction='l'></help-all>
                  </div>

                  <div class="content_body">
                        <inspection-details :bia="{{  json_encode($bia) }}" :assets="{{  json_encode($assets) }}"></inspection-details>
                  </div>
            </div>
        </div>
    </div>

@endsection