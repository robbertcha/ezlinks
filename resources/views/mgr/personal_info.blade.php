@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div id="main_content">
                <div id="content_header">
                    <h2 id="content_title">User Information</h2>
                </div>

                <div class="content_body">

                    <personal-info :user="{{  json_encode($user) }}"></personal-info>

                </div>
            </div>
        </div>
    </div>

@endsection