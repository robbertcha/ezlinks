@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div id="main_content">
                <div id="content_header">
                    <h2 id="content_title">Reservations</h2>
                    <help-all detail_id='3' direction='l'></help-all>
                    <btn-reservation-popup :text="'New +'" :type="3"></btn-reservation-popup>
                </div>
                
                <div class="content_body">
                        <scheduler-list :page="{{  json_encode($page) }}" :page_qty="{{  json_encode($page_qty) }}" :type="{{  json_encode($type) }}"  :order="{{  json_encode($order) }}"  :search_term="{{  json_encode($search_term) }}" :search_date="{{  json_encode($search_date) }}" :amenities="{{  json_encode($amenities) }}" :stype="{{  json_encode($stype) }}"></scheduler-list>
                </div>
            </div>
        </div>
    </div>

    <popup-booking-cont :stype="{{  json_encode($stype) }}" :contractor_list="{{ json_encode($contractor_list) }}" :bi_list="{{ json_encode($bi_list) }}" :tenants="{{ json_encode($tenants) }}" :amenity="{{ json_encode($amenity) }}" :units="{{ json_encode($units) }}" :selected_action="''"></popup-booking-cont>
@endsection