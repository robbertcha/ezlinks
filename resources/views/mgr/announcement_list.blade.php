@extends('layouts.app')

@section('content')
    <!-- <question-mark slug="announcement"></question-mark> -->
    <div class="row">
        <div class="col-md-12">
            <div id="main_content">
                <div id="content_header">
                    <h2 id="content_title">Announcement</h2>
                    <help-all detail_id='1' direction='l'></help-all>
                    <a href="{{ url('/mgr/create_announcement') }}" class="btn btn_new" >New +</a>
                </div>
                
                <div class="content_body">
                    <announcement-list :search="{{  json_encode($search) }}" :announcement="{{  json_encode($announcement) }}" ></announcement-list>

                    {{-- {{ $announcement->render() }} --}}
                </div>
            </div>
        </div>
    </div>

@endsection