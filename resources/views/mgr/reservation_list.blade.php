@extends('layouts.app')

@section('content')
 <!-- <question-mark slug="scheduler"></question-mark> -->
    <div class="row">
        <div class="col-md-12">
            <div  id="main_content">
                <div id="content_header">
                    <h2 id="content_title">Calendar</h2>
                    <help-all detail_id='17' direction='l'></help-all>
                    <!-- <button id="show_modal" class="btn btn_new" onclick="window.location.href='{{ url('/mgr/facilities') }}'">New Amenity</button> -->
                </div>

                <div class="content_body">
                    <reservation :contractor_list="{{  json_encode($contractor_list) }}" :bi_list="{{  json_encode($bi_list) }}" :reservation="{{  json_encode($reservation) }}" :amenity="{{  json_encode($amenity) }}" :tenants="{{  json_encode($tenants) }}" :stype="{{  json_encode($type) }}" :units="{{  json_encode($units) }}"></reservation>
                </div>
            </div>
        </div>
    </div>

    <!-- <popup-booking :stype="{{  json_encode($type) }}" :contractor_list="{{ json_encode($contractor_list) }}" :bi_list="{{ json_encode($bi_list) }}" :tenants="{{ json_encode($tenants) }}" :amenity="{{ json_encode($amenity) }}" :units="{{ json_encode($units) }}" :selected_action="1"></popup-booking> -->
@endsection