@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div id="main_content">
                <div id="content_header">
                    <h2 id="content_title">Amenity List</h2>
                    <button id="show_modal" class="btn btn_new" onclick="window.location.href='{{ url('/mgr/new_amenity') }}'">New +</button>
                </div>
                
                <div class="content_body">
                    <amenity-list :amenity="{{  json_encode($amenity) }}"></amenity-list>
                </div>
            </div>
        </div>
    </div>

@endsection