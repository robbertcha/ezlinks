@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div id="main_content">
                <div id="content_header">
                    <h2 id="content_title">Reminders</h2>
                    <a href="{{ url('/mgr/create_reminder') }}" class="btn btn_new" >New +</a>
                </div>
                
                <div class="content_body">
                        <reminder-list></reminder-list>
                </div>
            </div>
        </div>
    </div>

@endsection