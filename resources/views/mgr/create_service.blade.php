@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div id="main_content">
                <div id="content_header">
                    <h2 id="content_title">New Service/Product</h2>
                  
                </div>

                <div class="content_body">
                  <div class="col form_cont">
                        <div class="col-md-6">
                              <div class="field_cont">
                                    <input type="text" name="txt_servicetitle" class=" fct_input col-md-12" placeholder="Service/Product Title">
                              </div>
                              <div class="field_cont col">
                                    <div class="col-md-6">
                                          <select class="col-md-12" name="select_servicetype">
                                                <option value="svc" >Service</option>
                                                <option value="prd" >Product</option>
                                          </select>
                                    </div>

                                    <div class="col-md-6 scate_cont">
                                          <select class="col-md-10" name="select_servicecate">
                                                <option value="clg" >Cleaning</option>
                                                <option value="lry" >Laundry</option>
                                          </select>
                                          <div class="col-md-2 new_servicecate">
                                                {!! fa('plus')->lg() !!}
                                          </div>
                                    </div>
                              </div>

                              <div class="field_cont col">
                                    <div class="col-md-6">
                                          <input type="text" name="txt_servicewidth" class=" fct_input col-md-12" placeholder="Product Width (cm)">
                                    </div>
                                    <div class="col-md-6 scate_cont">
                                          <input type="text" name="txt_serviceheight" class=" fct_input col-md-12" placeholder="Product Height (cm)">
                                    </div>
                              </div>

                              <div class="field_cont col">
                                    <div class="col-md-6">
                                          <input type="text" name="txt_servicerate" class=" fct_input col-md-12" placeholder="Service/Product Price">
                                    </div>

                                    <div class="col-md-6">
                                          <label class="lbl_radio col-md-6">
                                                <input type="radio" name="allow_groupbuy" value="y"/>
                                                <div>Groupbuy</div>
                                          </label><label class="lbl_radio col-md-6">
                                                <input type="radio" name="allow_groupbuy" value="n" checked="checked"/>
                                                <div>Normal</div>
                                          </label>
                                    </div>
                              </div>

                              <div class="field_cont col">
                                    <div class="col-md-6">
                                          <input type="text" name="txt_servicegrate" class=" fct_input col-md-12" placeholder="Groupbuy Price">
                                    </div>

                                    <div class="col-md-6">
                                          <vue-date-range-picker> </vue-date-range-picker>
                                    </div>
                              </div>

                              

                              <div class="field_cont col">
                                    <div class="col-md-6">
                                          <select class="col-md-12" name="select_servicemerchant" >
                                                <option value="1">merchant 1</option>
                                                <option value="1">merchant 2</option>
                                                <option value="1">merchant 3</option>
                                                <option value="1">merchant 4</option>
                                                <option value="1">merchant 5</option>
                                          </select>
                                    </div>
                                    <div class="col-md-6">
                                          <label class="lbl_radio col-md-6">
                                                <input type="radio" name="allow_recurring" value="y"/>
                                                <div>Recurring</div>
                                          </label><label class="lbl_radio col-md-6">
                                                <input type="radio" name="allow_recurring" value="n" checked="checked"/>
                                                <div>No</div>
                                          </label>
                                    </div>
                              </div>

                              <div class="field_cont col">
                                    <div class="col-md-6">
                                          <select class="col-md-12" name="select_recurringperiod" >
                                                <option value="1">Once Every Week</option>
                                                <option value="1">Once Every 2 Weeks</option>
                                                <option value="1">Once Every Month</option>
                                                <option value="1">Once Every 2 Months</option>
                                                <option value="1">Once Every 3 Months</option>
                                                <option value="1">Once Every 4 Months</option>
                                                <option value="1">Once Every 6 Months</option>
                                                <option value="1">Once Every year</option>
                                          </select>
                                    </div>
                                    <div class="col-md-6"></div>
                              </div>

                               <div class="field_cont col">
                                    <textarea class="email_template" id="txt_inclusion" name="txt_inclusion"></textarea>
                               </div>
                         </div>

                        <div class="col-md-6">
                              <div class=" service_imgs_cont">
                                    <div class="col-md-12 service_img">
                                          <img src="{{ asset('images/product_no_img.png') }}" width="100%">
                                    </div>
                                    <div class="col-md-3 service_img_add">
                                          {!! fa('plus')->lg() !!}
                                    </div>
                              </div>
                              <div class="field_cont col">
                                    <textarea class="email_template" id="txt_dsc" name="txt_desc"></textarea>
                              </div>
                        </div>

                  
                        <div class="field_cont btn_cont">
                              <button class="btn">Close</button>
                              <button class="btn">Save</button>
                        </div>
                </div>
            </div>
        </div>
    </div>

<script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
    <script>tinymce.init({ selector:'textarea.email_template' });</script>
@endsection