@extends('layouts.app')

@section('content')


    <div class="row">
        <div class="col-md-12">
            <div id="main_content">
                  <div id="content_header">
                        @if(isset($asset->id) > 0 )  
                            <h2 id="content_title">Asset Details</h2>
                        @else
                            <!-- <i id="bk_to_previous_pg" class="fas fa-angle-left" onclick="window.history.go(-1);"></i> -->
                            <h2 id="content_title">New Asset</h2>
                        @endif
                  </div>

                  <div class="content_body">
                        <asset-details  :asset="{{  json_encode($asset) }}" :asset_doc="{{  json_encode($asset_doc) }}"  :asset_cate="{{  json_encode($asset_cate) }}"></asset-details>
                  </div>
            </div>
        </div>
    </div>

@endsection
