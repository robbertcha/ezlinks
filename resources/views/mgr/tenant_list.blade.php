@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div id="main_content">
                <div id="content_header">
                    <h2 id="content_title">Tenants</h2>
                </div>
                
                <div class="content_body">
                    <tenant-list :tenant="{{  json_encode($tenant) }}" :search="{{  json_encode($search) }}"></tenant-list>

                    {{ $tenant->render() }}
                </div>
            </div>
        </div>
    </div>

    <popup-tenantdetails></popup-tenantdetails>
@endsection