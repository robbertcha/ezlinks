@extends('layouts.app')

@section('content')


    <div class="row">
        <div class="col-md-12">
            <div id="main_content">
                <div id="content_header">
                    
                    @if(\Request::route()->getName() == 'mgr_amenity') 
                        <i id="bk_to_previous_pg" class="fas fa-angle-left" onclick="window.history.go(-1);"></i>
                        <h2 id="content_title">New Amenity</h2>
                    @else
                        <h2 id="content_title">{{ $amenity->name }}</h2>
                    @endif
                </div>

                <div class="content_body">
                    <facility :amenity="{{  json_encode($amenity) }}"></facility>
                </div>
            </div>
        </div>
    </div>

@endsection