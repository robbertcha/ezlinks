@extends('layouts.app')

@section('content')


    <div class="row">
        <div class="col-md-12">
            <div id="main_content">
                  <div id="content_header">
                        @if(isset($inv->id) > 0 )  
                            <h2 id="content_title">Inventory Details</h2>
                        @else
                            <i id="bk_to_previous_pg" class="fas fa-angle-left" onclick="window.history.go(-1);"></i>
                            <h2 id="content_title">New Inventory</h2>
                        @endif
                  </div>

                  <div class="content_body">
                        <inventory-details :inv="{{  json_encode($inv) }}" :inv_doc="{{  json_encode($inv_doc) }}" :inv_cate="{{  json_encode($inv_cate) }}"></inventory-details>
                  </div>
            </div>
        </div>
    </div>
@endsection