@extends('layouts.app')

@section('content')
<!-- <question-mark slug="courier"></question-mark> -->
    <div class="row">
        <div class="col-md-12">
            <div  id="main_content">
                <div id="content_header">
                    <h2 id="content_title">Courier</h2>
                </div>

                <div class="content_body">

                    <div class="col-md-8">
                        <courier-list :page="{{  json_encode($page) }}" :torder="{{  json_encode($torder) }}" :torderby="{{  json_encode($torderby) }}" :search_term="{{  json_encode($search_term) }}" :tlist="{{ json_encode($tlist) }}"></courier-list>
                    </div>

                    <div class="col-md-4">
                        <div class="container_block">
                            <div class="container_subtitle"><i class="fa fa-cog"></i> <h5>NEW PARCEL</h5></div>
                            <div class="field_block" id="new_courier_cont">
                                <!-- barcode scanner -->
                                <div id="barcode_scanner">
                                    <a class="button" id="startButton">Start</a>
                                    <a class="button" id="resetButton">Reset</a>
        
                                    <video id="video" width="100%" height="300" style="border: 1px solid gray"></video>
                                
                                    <div id="sourceSelectPanel" style="display:none">
                                        <label for="sourceSelect">Change video source:</label>
                                        <select id="sourceSelect" style="max-width:400px"></select>
                                    </div>
                                </div>
                                <!-- barcode scanner -->

                                <div id="barcode_result">
                                    <input type="text" id="barcode_result_txt" placeholder="Barcode Number" class="col-md-10 input_filter">
                                    <button id="btn_on_zxing" class="col-md-2"><i class="fas fa-camera"></i></button>
                                    <button id="btn_on_manual" class="col-md-2"><i class="fas fa-pencil-alt"></i></button>
                                </div>

                                <!-- <select id="tenant_list"> -->
                                    <?php 
                                        // foreach($tlist as $tkey => $tval) {
                                        //     echo "<option value='".$tval['user_id']."'>".$tval['tenant_text']."</option>";
                                        // }
                                    ?> 
                                <!-- </select> -->

                                <tenant-dropdown :tlist="{{  json_encode($tlist) }}"></tenant-dropdown>

                                <select id="courier_list">
                                    <option value="dhl">DHL</option>
                                    <option value="fastway">Fastway</option>
                                    <option value="startrack">Startrack</option>
                                    <option value="aus_post">Australia Post</option>
                                    <option value="others">Others</option>
                                </select>

                                <textarea id="txt_comment" placeholder="Add new comment ... ..."></textarea>

                                <button id="btn_new_parcel" class="btn_filter">Add Now</button>

                                
                            </div>
                        </div>

                </div>
            </div>
        </div>
    </div>

    <popup-parcel-details></popup-parcel-details>
   
    <script type="application/javascript" src="https://unpkg.com/@zxing/library@latest"></script>
    <script type="application/javascript">
        // receive a new courier
        jQuery(document).on("click","#btn_new_parcel",function() {
            var _this = jQuery(this);
            var barcode_result = jQuery("#barcode_result_txt").val();
            var courier_comp = jQuery("#courier_list > option:selected").text();
            var tenant_id = jQuery("#tenant_list").val();
            var txt_comment = jQuery("#txt_comment").val();

            if (barcode_result.length == 0) {
                alert("Please enter the barcode number");
                return false;
            }

            if (courier_comp.length == 0) {
                alert("Please select a courier company");
                return false;
            }

            if (tenant_id.length == 0) {
                alert("Please select a tenant");
                return false;
            }


            $.ajax({
                type:'POST',
                url:'/mgr/receive_new_parcel',
                data:{ 'barcode_result': barcode_result, 'courier_comp': courier_comp, 'tenant_id': tenant_id, 'txt_comment': txt_comment},
                beforeSend: function() {
                    _this.text("Processing ...").attr("disabled", true).addClass("is_processing");
                },
                success:function(data){
                    _this.text("Add Now").attr("disabled", false).removeClass("is_processing");
                    if (data.success == true) {
                        jQuery("#new_courier_cont").append('<span id="response_msg" class="good_response">Congratulation, your data is saved, and the page is refreshed automatically now ...</now>');
                        setTimeout(function(){
                            location.reload();
                        }, 4000);
                    } else {
                        jQuery("#new_courier_cont").append('<span id="response_msg" class="err_response">Sorry, we cannot process your requests dut to technical problems, please try again, thank you</now>');
                        setTimeout(function(){
                            jQuery(".err_response").remove();
                        }, 4000);
                    }
                },
                complete: function() {
                    _this.text("Add Now").attr("disabled", false).removeClass("is_processing");
                },
                error: function(xhr) { 
                    _this.text("Add Now").attr("disabled", false).removeClass("is_processing");
                }
            });
        });

    (function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

 


        jQuery(document).on('click','#btn_on_manual',function(){
            // button switch
            jQuery("#btn_on_zxing").show();
            jQuery("#btn_on_manual").hide();
            jQuery("#barcode_scanner").hide();
            jQuery("#barcode_result").find("#barcode_result_txt").removeAttr("readonly").val("");
        });

        jQuery(document).on('click','#btn_on_zxing',function(){
            // button switch
            jQuery("#btn_on_zxing").hide();
            jQuery("#btn_on_manual").show();
            jQuery("#barcode_scanner").show();
            jQuery("#barcode_result").find("#barcode_result_txt").attr("readonly","readonly").val("");

            // ZXing code reader initialized
            let selectedDeviceId;
            const codeReader = new ZXing.BrowserMultiFormatReader();
            console.log('ZXing code reader initialized');

            codeReader.getVideoInputDevices().then((videoInputDevices) => {
                const sourceSelect = document.getElementById('sourceSelect')
                selectedDeviceId = videoInputDevices[0].deviceId;

                if (videoInputDevices.length >= 1) {
                    videoInputDevices.forEach((element) => {
                        const sourceOption = document.createElement('option');
                        sourceOption.text = element.label;
                        sourceOption.value = element.deviceId;
                        sourceSelect.appendChild(sourceOption);
                    });

                    sourceSelect.onchange = () => {
                        selectedDeviceId = sourceSelect.value;
                    };
                    const sourceSelectPanel = document.getElementById('sourceSelectPanel');
                    sourceSelectPanel.style.display = 'block';
                }

                // start camera event
                document.getElementById('startButton').addEventListener('click', () => {
                    codeReader.decodeFromVideoDevice(selectedDeviceId, 'video', (result, err) => {
                        if (result) {
                            // console.log(result)
                            document.getElementById('barcode_result_txt').val( result.text );
                        }
                        if (err && !(err instanceof ZXing.NotFoundException)) {
                            // console.error(err)
                            document.getElementById('barcode_result_txt').val( err );
                        }
                    })
                    console.log(`Started continous decode from camera with id ${selectedDeviceId}`)
                });

                // reset camera event
                document.getElementById('resetButton').addEventListener('click', () => {
                    codeReader.reset()
                    document.getElementById('barcode_result_txt').val();
                })
            })
            .catch((err) => {
                console.error(err);
            });
            // ZXing code reader initialized

        });
    })();
    </script>
    
@endsection

    
<style>
    #response_msg {
        font-weight: 900;
        font-size: .75em;
        margin-top: 10px;
        display: block !important;
    }
    .good_response {
        color: #55ce63;
    }
    .err_response {
        color: #770000;
    }
    .is_processing {
        background-color: #ccc !important;
    }
    #btn_new_parcel {
        height:40px;background-color: #55ce63;margin-top:15px;color: #fff;border:0px;border-radius:4px;width: 100%;
    }
    #new_courier_cont select {
        width: 100%;height:40px;margin-bottom: 15px;margin-top: 15px;
    }

    #btn_on_manual {
        display: none;
    }

    #btn_on_zxing, #btn_on_manual {
        padding-left: 5px;padding-right: 5px;background: transparent;border: 0;height: 40px;
    }
    .container_block .container_subtitle {
        margin-top:0px !important;
    }

    #barcode_result {
        height: 40px; margin: 0px 0px 15px;border: 1px solid #dedede;border-radius:4px;
    }

    #barcode_result #barcode_result_txt {
        border:0px;
    }
    #barcode_scanner {
        display: none;
    }
</style>