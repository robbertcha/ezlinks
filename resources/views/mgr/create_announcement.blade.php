@extends('layouts.app')

@section('content')
{{-- <close-window></close-window> --}}
    <!-- <question-mark slug="announcement-detail"></question-mark> -->
    <div class="row">
        <div class="col-md-12">
            <div  id="main_content">
                <div id="content_header">
                    @if(\Request::route()->getName() == 'mgr_create_announcement') 
                        <i id="bk_to_previous_pg" class="fas fa-angle-left" onclick="window.history.go(-1);"></i>
                        <h2 id="content_title">New Announcement</h2>
                    @else
                        <h2 id="content_title">Edit Announcement</h2>
                    @endif
                    <help-all detail_id='2' direction='l'></help-all>
                </div>

                <div class="content_body">
                    <announcement-form :recipients="{{  json_encode($recipients) }}" :recipient-tags="{{  json_encode($recipientTags) }}" :announcement_tag="{{ json_encode($announcement_tag) }}" :template_list="{{ json_encode($template_list) }}" ></announcement-form>
                </div>
            </div>
        </div>
    </div>
@endsection
