@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div id="main_content">
                <div id="content_header">
                    <h2 id="content_title">Inspections</h2>
                    <a href="{{ url('/mgr/create_inspection') }}" class="btn btn_new" >New +</a>
                </div>
                
                <div class="content_body">
                    <inspection-list :page="{{  json_encode($page) }}"  :search_term="{{  json_encode($search_term) }}"></inspection-list>
                </div>
            </div>
        </div>
    </div>

@endsection