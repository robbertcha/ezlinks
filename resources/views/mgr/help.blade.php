@extends('layouts.app')

@section('content')

    {{-- <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default help">
                <div id="content_header" class="help-nav">
                    <h2 id="content_title">Help</h2>
                    <div class="h-manual">
                        <i class="fa fa-life-ring fa-lg"></i>
                        <a href="">User Manualv1</a>
                    </div>
                </div>
                <div class="panel-body help-panel">
                <help :info_detail="{{ json_encode($info_detail)}}" :info_top="{{json_encode($info_top)}}"></help>
                </div>
            </div>
        </div>
    </div> --}}
    {{-- <help :info_detail="{{ json_encode($info_detail)}}" :info_top="{{json_encode($info_top)}}"></help> --}}
    @if(isset($t)&&isset($d))
        <help :title_id="{{$t}}" :detail_id="{{$d}}"></help>
    @else
        <help></help>
    @endif

@endsection