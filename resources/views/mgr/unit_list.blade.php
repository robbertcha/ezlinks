@extends('layouts.app')

@section('content')
    <!-- <question-mark slug="manage"></question-mark> -->
    <div class="row">
        <div class="col-md-12">
            <div id="main_content">
                <div id="content_header">
                    <h2 id="content_title">Manage</h2>
                    <div class="manage_new_cont" >
                        <button class="btn btn_new">
                            New &nbsp;&nbsp;&nbsp;&nbsp;
                            <i class="fas fa-angle-double-down"></i>
                        </button>

                        <ul id="manage_new_list">
                            <li onclick="window.location.href='{{ url('/mgr/create_unit') }}'">New Units</li>
                            {{-- <li>New Carparks</li> --}}
                            <add-carpark></add-carpark>
                        </ul>
                    </div>
                </div>

                <div class="content_body">
                    <unit-list :page="{{  json_encode($page) }}" :ttype="{{  json_encode($ttype) }}" :torder="{{  json_encode($torder) }}" :torderby="{{  json_encode($torderby) }}" :search_term="{{  json_encode($search_term) }}" :search_tag="{{ json_encode($search_tag)}}" :tag="{{ json_encode($tag)}}"></unit-list>
                </div>
            </div>
        </div>
    </div>

    <!-- <popup-key></popup-key>
    <popup-createkey></popup-createkey>
    <popup-keyout></popup-keyout> -->
    <popup-carpark></popup-carpark>
    <popup-tenantdetails></popup-tenantdetails>
    <popup-cpdetails></popup-cpdetails>
    <popup-add-tenant></popup-add-tenant>



    <style>



    </style>
@endsection