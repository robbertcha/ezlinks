@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div id="main_content">
                <div id="content_header">
                    <h2 id="content_title">Inventory</h2>
                    <a href="/mgr/create_inventory" class="btn btn_new">
                            <span>New + </span>
                    </a>
                </div>
                
                <div class="content_body">
                    <inventory-list :inv="{{  json_encode($inv) }}" :inv_cate="{{  json_encode($inv_cate) }}"  :search="{{  json_encode($search) }}"></inventory-list>

                </div>
            </div>
        </div>
    </div>

@endsection