@extends('layouts.app_tenant')

@section('content_tenant')


        <div class="col-md-12">
            <div  id="main_content">
                <div id="content_header"></div>

                <div class="content_body">
                    <tenant-announcement-detail :user_detail="{{  json_encode($user_detail) }}" :announcement="{{  json_encode($announcement) }}"></tenant-announcement-detail>
                </div>
            </div>
        </div>


@endsection
