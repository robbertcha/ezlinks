@extends('layouts.app_tenant')

@section('content_tenant')


        <div id="main" class="col-md-12">
            <div  id="main_content">
                <div id="content_header"></div>

                <div class="content_body">
                    <tenant-announcements :user_detail="{{  json_encode($user_detail) }}"></tenant-announcements>
                </div>
            </div>
        </div>


@endsection
