@extends('layouts.app_tenant')

@section('content_tenant')


        <div id="main" class="col-md-12">
            <div  id="main_content">
                <div id="content_header"></div>

                <div class="content_body">
                    <tenant-makeareport :user_detail="{{  json_encode($user_detail) }}" :case_type="{{  json_encode($case_type) }}" :case_area="{{  json_encode($case_area) }}" :case_cate="{{  json_encode($case_cate) }}"></tenant-makeareport>
                </div>
            </div>
        </div>


@endsection
