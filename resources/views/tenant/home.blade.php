@extends('layouts.app_tenant')

@section('content_tenant')
        <div id="main" class="col-md-12">
            <div  id="main_content">
                <div id="content_header"></div>

                <div class="content_body">
                    <tenant-home :user_detail="{{  json_encode($user_detail) }}" :adv_list="{{  json_encode($adv_list) }}" :reservation_list="{{  json_encode($reservation_list) }}" :announcement_list="{{  json_encode($announcement_list) }}"></tenant-home>
                </div>
            </div>
        </div>


@endsection
