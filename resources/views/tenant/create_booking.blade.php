@extends('layouts.app_tenant')

@section('content_tenant')


        <div id="main" class="col-md-12">
            <div  id="main_content">
                <div id="content_header"></div>

                <div class="content_body">
                    <tenant-createbooking :user_detail="{{  json_encode($user_detail) }}"  :amenity="{{  json_encode($amenity) }}"></tenant-createbooking>
                </div>
            </div>
        </div>


@endsection
