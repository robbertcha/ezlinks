@extends('layouts.app_tenant')

@section('content_tenant')


        <div id="main" class="col-md-12">
            <div  id="main_content">
                <div id="content_header"></div>

                <div class="content_body">
                    <tenant-useracc :user_detail="{{  json_encode($user_detail) }}"  :user_vehicles="{{  json_encode($vehicles) }}"  :user_pets="{{  json_encode($pets) }}" :log_out="{{ json_encode(route('logout')) }}"></tenant-useracc>
                </div>
            </div>
        </div>

<popup-tenant-user></popup-tenant-user>
@endsection
