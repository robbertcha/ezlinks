@extends('layouts.app_tenant')

@section('content_tenant')

<div id="main" class="col-md-12">
            <div  id="main_content">
                <div id="content_header"></div>

                <div class="content_body">
                    <maintenance-list-detail :attachment="{{json_encode($attachment)}}" :detail="{{json_encode($detail)}}" :progress_detail_log="{{json_encode($progress_detail_log)}}"></maintenance-list-detail>
                </div>
            </div>
        </div>



@endsection