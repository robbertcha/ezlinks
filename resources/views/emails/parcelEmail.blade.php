<!DOCTYPE html>
<html>
<head>
    <title>{{$email_title}}</title>
</head>

<body>
    A new parcel has arrived, please generate your unique code in {{$domain}} to take it at the reception.
</body>

</html>