<!DOCTYPE html>
<html>
<head>
    <title>Welcome Email</title>
</head>

<body>
<h2>Welcome to your new home, {{$tenant->first_name}} {{$tenant->last_name}}</h2>
<br/>
Please click on the below link to activate your status in unit {{$unit_title}}
<br/>
<a href="{{url('tenants/tenant_verification', $verification_token)}}">Activate Link</a>
</body>

</html>