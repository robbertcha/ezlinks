<!DOCTYPE html>
<html>
<head>
    <title>{{$email_title}}</title>
</head>

<body>
    {!! $content !!}
</body>

</html>