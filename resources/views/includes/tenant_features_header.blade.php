<div class="btn_profile" id="back_button">
    <button type="button" id="back_last_page" class="back_last_page" >
        <i class="fas fa-chevron-left"  ></i>
    </button>	
    
</div>
@if ($uri_tail == "Reservation" || $uri_tail == "Maintenance List")
    <div class="building_title_25">
        {{ $uri_tail }}
    </div>
    @if ($uri_tail == "Reservation")
        <div class="reservation_25">
            <a href="/tenants/create_booking">
                <i class="far fa-calendar-plus"></i>
            </a>
        </div>
    @else
        <div class="maintenance_25">
            <a href="/tenants/make_a_report" >
                <i class="far fa-edit"></i>
            </a>
        </div>  
    @endif  
@else
    <div class="building_title">
        @if ($uri_tail == "User")
            About Me
        @else
            {{ $uri_tail }}
        @endif
    </div>
@endif
<div class="btn_profile" id="notification_button">
    <button type="button" class="btn_case" id="btn_case">
        <i class="far fa-bell"></i>
    </button>
</div>
