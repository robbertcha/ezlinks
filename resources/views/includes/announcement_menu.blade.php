<div class="submenu_cont" v-show="showAnnouncementSlider">    
      <h2>Announcement</h2>              
      <ul class="submenu_list">
                        <li>
                            <div class="submenu_cont">
                                <a href="{{ url('/mgr/announcement') }}">
                                    <i class="far fa-file-alt"></i>
                                    <span>Announcement List</span>
                                </a>
                            </div>
                        </li>

                        <li>
                            <div class="submenu_cont">
                                <a href="{{ url('/mgr/create_announcement') }}">
                                    <i class="fas fa-bullhorn"></i>
                                    <span>New Announcement</span>
                                </a>
                            </div>
                        </li>

                       
      </ul>
</div>