<div class="submenu" v-show="showSettingSlider">    
        <h2>Building Data Management</h2>              
        <ul class="submenu_list">
                        <li>
                            <div class="submenu_cont">
                                <a href="{{ url('/mgr/setting') }}">
                                    <i class="far fa-building"></i>
                                    <span>Building Info</span>
                                </a>
                            </div>
                        </li>

                        <li>
                            <div class="submenu_cont">
                                <a href="{{ url('/mgr/personal_info') }}">
                                    <i class="fas fa-info"></i>
                                    <span>Admin Info</span>
                                </a>
                            </div>
                        </li>

                        <li>
                            <div class="submenu_cont">
                                <a href="{{ url('/mgr/unit') }}">
                                    <i class="fas fa-home"></i>
                                    <span>Units<label>
                                </a>
                            </div>
                        </li> 


                        <li>
                            <div class="submenu_cont">
                                <a href="{{ url('/mgr/unit') }}">
                                    <i class="fa fa-users fa-lg"></i>
                                    <span>Tenants / Owners / Investors<label>
                                </a>
                            </div>
                        </li> 


                        <li>
                            <div class="submenu_cont">
                                <a href="{{ url('/mgr/unit') }}">
                                <i class="fas fa-car"></i>
                                    <span>Carparks<label>
                                </a>
                            </div>
                        </li>

                        <li>
                            <div class="submenu_cont">
                                <a href="{{ url('/mgr/assets') }}">
                                    <i class="fas fa-couch"></i>
                                    <span>Assets</span>
                                </a>
                            </div>
                        </li>

                        <li>
                            <div class="submenu_cont">
                                <a href="{{ url('/mgr/inspection') }}">
                                    <i class="fa fa-check-square fa-lg"></i>
                                    <span>Inspections</span>
                                </a>
                            </div>
                        </li>

                        <li>
                            <div class="submenu_cont">
                                <a href="{{ url('/mgr/inventory') }}">
                                    <i class="fas fa-warehouse"></i>
                                    <span>Inventory</span>
                                </a>
                            </div>
                        </li>

                        <li>
                            <div class="submenu_cont">
                                <a href="{{ url('/mgr/amenity_list') }}">
                                    <i class="fas fa-swimming-pool"></i>
                                    <span>Amenities</span>
                                </a>
                            </div>
                        </li>

                        <li>
                            <div class="submenu_cont">
                                <a href="{{ url('/mgr/contractor_list') }}">
                                    <i class="fa fa-users-cog fa-lg"></i>
                                    <span>Contractors<span>
                                </a>
                            </div>
                        </li>

        </ul>

        <h2>Additional Features</h2> 
        <ul class="submenu_list">
            <li>
                            <div class="submenu_cont">
                                <a href="{{ url('/mgr/template') }}">
                                    <i class="far fa-file-word"></i>
                                    <span>Templates</span>
                                </a>
                            </div>
            </li>

            <!-- <li>
                            <div class="submenu_cont">
                                <a href="{{ url('/mgr/category') }}">
                                    <i class="far fa-folder-open"></i>
                                    <span>Categories</span>
                                </a>
                            </div>
            </li> -->

                        <!-- <li>
                            <div class="submenu_cont">
                                <a href="{{ url('/mgr/parking_breaches') }}">
                                    <i class="fas fa-parking"></i>
                                    <span>Parking Breaches</span>
                                </a>
                            </div>
                        </li> -->

            <li>
                            <div class="submenu_cont">
                                <a href="{{ url('/mgr/library') }}">
                                    <i class="fas fa-book-reader"></i>
                                    <span>Library</span>
                                </a>
                            </div>
            </li>

            
        </ul>
</div>