<!-- Left Side Of Navbar -->
<ul class="panel mgr_menu">

      <li class="{{ (\Request::route()->getName() == 'mgr_dashboard') ? 'active' : '' }}">
            <a href="{{ url('/mgr/dashboard') }}">
                  <i class="fa fa-chart-line fa-lg"></i>
                  <label>Dashboard<label>
            </a>
      </li>
      <li class="{{ (\Request::route()->getName() == 'mgr_announcement' || \Request::route()->getName() == 'mgr_create_announcement' || \Request::route()->getName() == 'mgr_edit_announcement')  ? 'active' : '' }}" >
            <a href="{{ url('/mgr/announcement') }}">
                  <i class="fa fa-bullhorn fa-lg"></i>
                  <label>Announcements<label>
            </a>
      </li>

      <li class="{{ (\Request::route()->getName() == 'mgr_scheduler_list' || \Request::route()->getName() == 'mgr_reservation') ? 'active' : '' }}">
            <a href="{{ url('/mgr/scheduler_list') }}">
                  <i class="fa fa-calendar fa-lg"></i>
                  <label>Reservations<label>
            </a>
      </li>

      <!-- <li class="{{ (\Request::route()->getName() == 'mgr_inspection_list' || \Request::route()->getName() == 'mgr_create_inspection'  || \Request::route()->getName() == 'mgr_edit_inspection') ? 'active' : '' }}">
            <a href="{{ url('/mgr/inspection') }}">
                  <i class="fa fa-check-square fa-lg"></i>
                  <label>Jobs<label>
            </a>
      </li> -->
      
      <li class="{{ (\Request::route()->getName() == 'mgr_case' || \Request::route()->getName() == 'mgr_complaint_detail'  || \Request::route()->getName() == 'mgr_create_case') ? 'active' : '' }}">
            <a href="{{ url('/mgr/case') }}">
                  <i class="fa fa-wrench fa-lg"></i>
                  <label>Jobs<label>
            </a>
      </li>


      <li class="{{ (\Request::route()->getName() == 'mgr_report' ) ? 'active' : '' }}">
            <a href="{{ url('/mgr/report') }}">
                  <i class="fa fa-book-open"></i>
                  <label>Reports<label>
            </a>
      </li>


      <li class="{{ (\Request::route()->getName() == 'mgr_setting')  || (\Request::route()->getName() == 'mgr_personal_info') ||  (\Request::route()->getName() == 'mgr_email_template') ? 'active' : '' }}"  v-on:click="toggleSlideMenu('setting')">
            <i class="fa fas fa-building"></i>
            <label>Building<label>
      </li>

      

      <!-- <li class="{{ (\Request::route()->getName() == 'mgr_unit') ? 'active' : '' }}">
            <a href="{{ url('/mgr/unit') }}">
                  <i class="fa fa-users fa-lg"></i>
                  <label>Manage<label>
            </a>
      </li> 
     
      <li class="{{ (\Request::route()->getName() == 'mgr_contractor_list' || \Request::route()->getName() == 'mgr_recommend_contractor' || \Request::route()->getName() == 'mgr_contractor_detail') ? 'active' : '' }}">
            <a href="{{ url('/mgr/contractor_list') }}">
                  <i class="fa fa-users-cog fa-lg"></i>
                  <label>Contractors<label>
            </a>
      </li>-->


      <!-- 
      <li class="{{ (\Request::route()->getName() == 'mgr_courier' ) ? 'active' : '' }}">
            <a href="{{url('/mgr/courier') }}">
                  <i class="fa fa-boxes fa-lg"></i>
                  <label>Parcels<label>
            </a>
      </li>

      
      <li class="btn_extend">
            <button class="btn_more">
                  <span>More</span>
                  &nbsp;
                  <i class="fas fa-angle-double-down"></i>
            </button>
      </li> -->
     
      
</ul>


