<!-- Left Side Of Navbar -->
<ul id="menu_list">
      <!-- <li id="menu_bar"></li> -->
      <li class="{{ (\Request::route()->getName() == 'tenant_dashboard') ? 'active' : '' }}">
            <a href="{{ url('/tenants/dashboard') }}">
                  <i class="far fa-building fa-lg"></i>
                  <label>Building</label>
            </a>
      </li>
      <li class="{{ (\Request::route()->getName() == 'tenant_announcement') ? 'active' : '' }}">
            <a href="{{ url('/tenants/announcement') }}">
                  <i class="fas fa-broom fa-lg"></i>
                  <label>Business</label>
            </a> 
      </li>
<!-- 
      <li class="{{ (\Request::route()->getName() == 'tenant_services') ? 'active' : '' }}">
            <a href="{{ url('/service') }}">
                  <i class="fas fa-tools fa-lg"></i>
                  <label>Services</label>
            </a>
      </li> -->

     
      <li class="{{ (\Request::route()->getName() == 'tenant_reservation') ? 'active' : '' }}">
            <a href="{{ url('/tenants/reservation') }}">
                  <i class="fas fa-tags fa-lg"></i>
                  <label>Offers</label>
            </a>
      </li>


     
      <li class="{{ (\Request::route()->getName() == 'tenant_user') ? 'active' : '' }}">
            <a href="{{ url('/tenants/user') }}">
                  <i class="fas fa-user fa-lg"></i>
                  <label>Me</label>
            </a>
      </li>
</ul>


