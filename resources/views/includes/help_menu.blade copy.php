<div class="submenu_cont help" v-show="showHelpSlider">    
      <h2>Help</h2>              
            <ul class="submenu_list help">
                        <li v-bind:class="{ is_active: helpIndex == '4' }">
                              <label  class="" v-on:click="toggleHelpContent(4)" style="background-color: #5faee3;">
                                    I am a new user, how to do the initial setting&nbsp;&nbsp;&nbsp;
                                    <i class="fas fa-angle-double-down"></i> 
                              </label>
      
                              <p v-show="helpIndex == '4'">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                              </p>

                              <div class="help_links" v-show="helpIndex == '4'">
                                    <a href=""><i class="fas fa-plus-square"></i>&nbsp;&nbsp;Add</a>
                                    <a href=""><i class="fa fa-edit"></i>&nbsp;&nbsp;Edit</a>
                                    <a href=""><i class="fa fa-times"></i>&nbsp;&nbsp;Delete</a>
                                    <a href=""><i class="fa fa-question"></i>&nbsp;&nbsp;Link 4</a>
                              </div>
                        </li>

                        <li v-bind:class="{ is_active: helpIndex == '1' }">
                              <label v-on:click="toggleHelpContent(1)" style="background-color: rgb(67, 219, 189);">
                              I want to insert units, carparks or tenants in bulks &nbsp;&nbsp;&nbsp;
                                    <i class="fas fa-angle-double-down"></i> 
                              </label>
      
                              <p v-show="helpIndex == '1'">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                              </p>

                              <div class="help_links" v-show="helpIndex == '1'">
                                    <a href=""><i class="fa fa-question"></i>&nbsp;&nbsp;Link 1</a>
                                    <a href=""><i class="fa fa-question"></i>&nbsp;&nbsp;Link 2</a>
                                    <a href=""><i class="fa fa-question"></i>&nbsp;&nbsp;Link 3</a>
                                    <a href=""><i class="fa fa-question"></i>&nbsp;&nbsp;Link 4</a>
                              </div>
                        </li>

                        <li v-bind:class="{ is_active: helpIndex == '2' }">
                              <label v-on:click="toggleHelpContent(2)" style="background-color: rgb(255, 194, 107);">
                                    I want to make a broadcast to tenants &nbsp;&nbsp;&nbsp;
                                    <i class="fas fa-angle-double-down"></i> 
                              </label>
      
                              <p v-show="helpIndex == '2'">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                              </p>

                              <div class="help_links" v-show="helpIndex == '2'">
                                    <a href=""><i class="fa fa-question"></i>&nbsp;&nbsp;Link 1</a>
                                    <a href=""><i class="fa fa-question"></i>&nbsp;&nbsp;Link 2</a>
                                    <a href=""><i class="fa fa-question"></i>&nbsp;&nbsp;Link 3</a>
                                    <a href=""><i class="fa fa-question"></i>&nbsp;&nbsp;Link 4</a>
                              </div>
                        </li>


                        <li v-bind:class="{ is_active: helpIndex == '3' }">
                              <label v-on:click="toggleHelpContent(3)" style="background-color: rgb(255, 115, 138);">
                                    I want to send emails to tenants or contractors in person &nbsp;&nbsp;&nbsp;
                                    <i class="fas fa-angle-double-down"></i> 
                              </label>
      
                              <p v-show="helpIndex == '3'">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                              </p>

                              <div class="help_links" v-show="helpIndex == '3'">
                                    <a href=""><i class="fa fa-question"></i>&nbsp;&nbsp;Link 1</a>
                                    <a href=""><i class="fa fa-question"></i>&nbsp;&nbsp;Link 2</a>
                                    <a href=""><i class="fa fa-question"></i>&nbsp;&nbsp;Link 3</a>
                                    <a href=""><i class="fa fa-question"></i>&nbsp;&nbsp;Link 4</a>
                              </div>
                        </li>


                        <li v-bind:class="{ is_active: helpIndex == '4' }">
                              <label v-on:click="toggleHelpContent(4)" style="background-color: rgb(67, 219, 189);">
                                    How to get the amenity booking from tenants? &nbsp;&nbsp;&nbsp;
                                    <i class="fas fa-angle-double-down"></i> 
                              </label>
      
                              <p v-show="helpIndex == '4'">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                              </p>

                              <div class="help_links" v-show="helpIndex == '4'">
                                    <a href=""><i class="fa fa-question"></i>&nbsp;&nbsp;Link 1</a>
                                    <a href=""><i class="fa fa-question"></i>&nbsp;&nbsp;Link 2</a>
                                    <a href=""><i class="fa fa-question"></i>&nbsp;&nbsp;Link 3</a>
                                    <a href=""><i class="fa fa-question"></i>&nbsp;&nbsp;Link 4</a>
                              </div>
                        </li>

                        

                        <li v-bind:class="{ is_active: helpIndex == '5' }">
                              <label v-on:click="toggleHelpContent(5)" style="background-color: rgb(67, 219, 189);">
                                    I want to create a task into my scheduler &nbsp;&nbsp;&nbsp;
                                    <i class="fas fa-angle-double-down"></i> 
                              </label>
      
                              <p v-show="helpIndex == '5'">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                              </p>

                              <div class="help_links" v-show="helpIndex == '5'">
                                    <a href=""><i class="fa fa-question"></i>&nbsp;&nbsp;Link 1</a>
                                    <a href=""><i class="fa fa-question"></i>&nbsp;&nbsp;Link 2</a>
                                    <a href=""><i class="fa fa-question"></i>&nbsp;&nbsp;Link 3</a>
                                    <a href=""><i class="fa fa-question"></i>&nbsp;&nbsp;Link 4</a>
                              </div>
                        </li>

                        <li v-bind:class="{ is_active: helpIndex == '6' }">
                              <label v-on:click="toggleHelpContent(6)" style="background-color: rgb(255, 194, 107);">
                                    I want to generate a body corporate report &nbsp;&nbsp;&nbsp;
                                    <i class="fas fa-angle-double-down"></i> 
                              </label>
      
                              <p v-show="helpIndex == '6'">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                              </p>

                              <div class="help_links" v-show="helpIndex == '6'">
                                    <a href=""><i class="fa fa-question"></i>&nbsp;&nbsp;Link 1</a>
                                    <a href=""><i class="fa fa-question"></i>&nbsp;&nbsp;Link 2</a>
                                    <a href=""><i class="fa fa-question"></i>&nbsp;&nbsp;Link 3</a>
                                    <a href=""><i class="fa fa-question"></i>&nbsp;&nbsp;Link 4</a>
                              </div>
                        </li>


                        <li v-bind:class="{ is_active: helpIndex == '7' }">
                              <label v-on:click="toggleHelpContent(7)" style="background-color: rgb(255, 115, 138);">
                                    I want to sent a parcel notification to tenants  &nbsp;&nbsp;&nbsp;
                                    <i class="fas fa-angle-double-down"></i> 
                              </label>
      
                              <p v-show="helpIndex == '7'">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                              </p>

                              <div class="help_links" v-show="helpIndex == '7'">
                                    <a href=""><i class="fa fa-question"></i>&nbsp;&nbsp;Link 1</a>
                                    <a href=""><i class="fa fa-question"></i>&nbsp;&nbsp;Link 2</a>
                                    <a href=""><i class="fa fa-question"></i>&nbsp;&nbsp;Link 3</a>
                                    <a href=""><i class="fa fa-question"></i>&nbsp;&nbsp;Link 4</a>
                              </div>
                        </li>
                        

                        <li v-bind:class="{ is_active: helpIndex == '8' }">
                              <label v-on:click="toggleHelpContent(8)" style="background-color: rgb(67, 219, 189);">
                                    How to create a case or requests? or track it  &nbsp;&nbsp;&nbsp;
                                    <i class="fas fa-angle-double-down"></i> 
                              </label>
      
                              <p v-show="helpIndex == '8'">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                              </p>

                              <div class="help_links" v-show="helpIndex == '8'">
                                    <a href=""><i class="fa fa-question"></i>&nbsp;&nbsp;Link 1</a>
                                    <a href=""><i class="fa fa-question"></i>&nbsp;&nbsp;Link 2</a>
                                    <a href=""><i class="fa fa-question"></i>&nbsp;&nbsp;Link 3</a>
                                    <a href=""><i class="fa fa-question"></i>&nbsp;&nbsp;Link 4</a>
                              </div>
                        </li>
             </ul>
</div>