<!-- Left Side Of Navbar -->
<ul class="panel mgr_menu">
      <li class="{{ (\Request::route()->getName() == 'tenant_dashboard') ? 'active' : '' }}">
            <a href="{{ url('/tenants/dashboard') }}">
                  <i class="fas fa-home fa-lg"></i>
                  <label>Home<label>
            </a>
      </li>
      <li class="{{ (\Request::route()->getName() == 'tenant_announcement') ? 'active' : '' }}">
            <a href="{{ url('/tenants/announcement') }}">
                  <i class="fa fa-bullhorn fa-lg"></i>
                  <label>Announcements<label>
            </a> 
      </li>
<!-- 
      <li class="{{ (\Request::route()->getName() == 'tenant_services') ? 'active' : '' }}">
            <a href="{{ url('/service') }}">
                  <i class="fas fa-tools fa-lg"></i>
                  <label>Services<label>
            </a>
      </li> -->

     
      <li class="{{ (\Request::route()->getName() == 'tenant_reservation') ? 'active' : '' }}">
            <a href="{{ url('/tenants/reservation') }}">
                  <i class="fa fa-calendar fa-lg"></i>
                  <label>Reservations<label>
            </a>
      </li>
     
      <li class="{{ (\Request::route()->getName() == 'tenant_user') ? 'active' : '' }}">
            <a href="{{ url('/tenants/user') }}">
                  <i class="fas fa-user fa-lg"></i>
                  <label>Me<label>
            </a>
      </li>

      
      
</ul>

