

<div class="btn_profile cos">
    @if($user_detail->thumb =='' || $user_detail->thumb == null)  
        <div class="navbar-toggle collapsed"  style="background:url('https://profiles.utdallas.edu/img/default.png') center center no-repeat;"></div>
    @else
        <div class="navbar-toggle collapsed"  style="background:url('<?php echo $user_detail->thumb; ?>') center center no-repeat;"></div>
    @endif
        <span>{{ $user_detail->unit_title }}</span>
</div>

<div class="building_title">
    {{ $user_detail->building_name }}
</div>
<div class="btn_profile" id="home_page_notify">
    <button type="button" class="btn_case" id="btn_case">
        <i class="far fa-bell"></i>
    </button>
</div>