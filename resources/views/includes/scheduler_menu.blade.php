<div class="submenu_cont" v-show="showSchedulerSlider">    
      <h2>Scheduler</h2>              
      <ul class="submenu_list">

                        <li>
                            <div class="submenu_cont">
                                <a href="{{ url('/mgr/reservation') }}">
                                    <i class="far fa-calendar-alt"></i>
                                    <span>Calendar</span>
                                </a>
                            </div>
                        </li>

                        <li>
                            <div class="submenu_cont">
                                <a href="{{ url('/mgr/task_list') }}">
                                    <i class="fas fa-tasks"></i>
                                    <span>Tasks</span>
                                </a>
                            </div>
                        </li>

                        <li>
                            <div class="submenu_cont">
                                <a href="{{ url('/mgr/maintenance_list') }}">
                                    <i class="fas fa-tools"></i>
                                    <span>Maintenance</span>
                                </a>
                            </div>
                        </li>

                        <li>
                            <div class="submenu_cont">
                                <a href="{{ url('/mgr/booking_list') }}">
                                    <i class="far fa-calendar-check"></i>
                                    <span>Reservations</span>
                                </a>
                            </div>
                        </li>

                        <li>
                            <div class="submenu_cont">
                                <a href="{{ url('/mgr/inspection_list') }}">
                                    <i class="far fa-check-square"></i>
                                    <span>Inspections</span>
                                </a>
                            </div>
                        </li>

                        <!-- <li>
                            <div class="submenu_cont">
                                <a href="{{ url('/mgr/reminder_list') }}">
                                    <i class="far fa-clock"></i>
                                    <span>Reminders</span>
                                </a>
                            </div>
                        </li> -->

                       
      </ul>
</div>