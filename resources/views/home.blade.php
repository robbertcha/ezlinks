@extends('layouts.app')

@section('content')

     <div class="row">
        <div class="col-md-12">
            <div  id="main_content">
                <div id="content_header">
                    <h2 id="content_title">Dashboard</h2>
                    <help-all detail_id='6' direction='l'></help-all>
                </div>
                <div class="content_body">
                    <dashboard :building_id ="{{  json_encode($building_id) }}" :stype="{{  json_encode($type) }}" :contractor_list="{{  json_encode($contractor_list) }}" :bi_list="{{  json_encode($bi_list) }}" :stats="{{  json_encode($stats) }}"  :reservation="{{  json_encode($reservation) }}" :amenity="{{  json_encode($amenity) }}" :tenants="{{  json_encode($tenants) }}" :term="{{ json_encode($term)  }}" :setting_check="{{ json_encode($setting_check)}}" :setting_count="{{ json_encode($setting_count)}}" :units="{{ json_encode($units)}}"></dashboard>
                </div>
            </div>
        </div>
    </div>

    <popup-add-tenant></popup-add-tenant>
    <popup-cpdetails></popup-cpdetails>
@endsection
