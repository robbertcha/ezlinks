<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Aurora Living') }}</title>

    <!-- 引入css -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/tenant/al_app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/@progress/kendo-theme-default@latest/dist/all.css">
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"> -->

    <link rel="manifest" href="/manifest.json"/>
    
    <!-- ios -->
    <meta name="apple-mobile-web-app-capable" content="yes" />

    <!-- ios splash -->
    <link href="images/ios_splash_2208.png" sizes="2048x2732" rel="apple-touch-startup-image" />

    <!-- ios icon -->
    <link rel="apple-touch-icon" href="images/logo_192.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images/logo_152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="images/logo_180.png">
    <link rel="apple-touch-icon" sizes="167x167" href="images/logo_167.png">


    <script src="//cdn.bootcss.com/vue/2.5.13/vue.min.js"></script>
    <script src="//cdn.bootcss.com/axios/0.17.1/axios.min.js"></script>
    <script src="//cdn.bootcss.com/lodash.js/4.17.4/lodash.min.js"></script>
    <script src="//code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://kendo.cdn.telerik.com/2019.3.1023/js/kendo.all.min.js"></script>
    
    <script>
        var user_detail = null
		
        <?php if  ($user_detail != null && $user_detail != '') { ?>
            user_detail = <?php echo json_encode($user_detail); ?>
			<?php 
				$uri_path = url()->full();
				$uri_parts = explode('/', $uri_path);
				$uri_tail = end($uri_parts);
                $uri_tail = ucwords(str_replace("_"," ",$uri_tail));
			?>;
        <?php }  ?>
		
        window.myToken =  <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    <script src="{{ asset('js/pwa.js') }}"></script>
</head>
<body>
    <div id="app">
        

        <section class=" col pg_content">
            @auth
                <div id="sidebar">
                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                       
                    </a>
                    @include('includes.sidebar_tenant')
                </div>
                
                <div id="content">
                    <nav class="navbar navbar-default navbar-static-top">
                        <div style="width: 100%;padding:0px 0%;">
                            <div class="navbar-header">
                                <!-- Collapsed Hamburger -->
                                @if ($uri_tail == "Dashboard")
                                    @include('includes.tenant_homepage_header')
                                    <div class="tenant_edit_menu container" id="tenant_edit_menu">
                                        <notification></notification>
                                    </div>
                                @else
                                    @include('includes.tenant_features_header')
                                    <div class="tenant_edit_menu container" id="tenant_edit_menu">
                                        <notification></notification>
                                    </div>
                                @endif
                            </div>

                            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                                <!-- Left Side Of Navbar -->
                                <ul class="nav navbar-nav">
                                    &nbsp
                                </ul>

                                <!-- Right Side Of Navbar -->
                                <ul id="tenant_nav" class="nav navbar-nav navbar-right">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                            {{ Auth::user()->name }} 
                                            <span class="caret"></span>
                                        </a>

                                        <ul class="dropdown-menu" role="menu">
                                            <li>
                                                <a href="{{ route('blgadmin.logout') }}" >
                                                    Logout
                                                </a>

                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    {{ csrf_field() }}
                                                </form>
                                            </li>
                                        </ul>
                                    </li>
                                  
                                </ul>
                            </div>
                        </div>
                    </nav>
                    @yield('content_tenant')
                </div>

                <div id="main_menu">
                        @include('includes.menu_tenant')
                </div>
            @endauth

            <div id="loading_cont"><div id="loading-bg"></div><img id="loading-image"  src="{{ asset('images/icons/icn_loading.gif') }}" width="100"> </div>
        </section>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
        jQuery(window).on('beforeunload', function() {
            jQuery('#loading_cont').show();
        });
        window.onload = function(){
            
            if(document.getElementById("btn_case")){

                document.getElementById("btn_case").onclick = function(){
                    var clickDiv = document.getElementById("btn_case").parentNode;
                    if (document.getElementById("tenant_edit_menu").style.display == "none" ) {
                        clickDiv.className += " active";
                        document.getElementById("tenant_edit_menu").style.display = "block";
                    } else {
                        clickDiv.className = "btn_profile";
                        document.getElementById("tenant_edit_menu").style.display = "none";
                    }
                }
            }else{
                
            }

            if(document.getElementById("back_last_page")){
                document.getElementById("back_last_page").onclick = function() {
                    var clickDiv = document.getElementById("back_last_page").parentNode;
                    if (clickDiv.className == "btn_profile active") {
                        clickDiv.className = "btn_profile";
                    } else {
                        clickDiv.className += " active";
                    }
                    
                    if (document.referrer.split('.')[1] != "ezlinks" || document.referrer.length == 0) {
                       window.location.href = "dashboard";
                    } else {
                        let tail = window.location.href.split('/')[4];
                       
                        if (tail == 'reservation' || tail == 'announcement' || tail == 'parcel' || tail == 'maintenance_list' || tail == 'documents' || tail == 'address_book' || tail == 'user') {
                            if (window.location.href.split('/')[5] == null) {
                                window.location.href = "dashboard";
                            } else {
                                window.history.back(-1);
                            }
                            // window.location.href = "dashboard";
                            
                        } else {
                            
                            window.history.back(-1);
                            
                        }
                        
                        
                    }
  
                }
            }
        }

        // axios call notification data

        // function get_notification(){
        //     axiso.get()
        // }

        // document.getElementById("menu_bar").onclick = function(){
        //     var elem = jQuery('#main_menu');   
        //     var pos = jQuery('#main_menu').height() ;

        //     if (jQuery('#main_menu').height() < 50) {
        //             setInterval(function() {
        //                 if (pos <= 50) {
        //                     jQuery('#main_menu').height(pos);
        //                     pos++; 
        //                 }
        //             }, 1);
        //     } else {
        //         setInterval(function() {
        //                 if (pos >= 10) {
        //                     jQuery('#main_menu').height(pos);
        //                     pos--; 
        //                 }
        //             }, 1);
        //     }
        // }

        jQuery( document ).ready(function() {

            // var previousPosition = window.pageYOffset || document.documentElement.scrollTop;
            // jQuery(window).scroll(function() {          
            //     var currentPosition = window.pageYOffset || document.documentElement.scrollTop;
            //     if (previousPosition > currentPosition) {
            //         if (jQuery('#main_menu').height() > 10) {
            //             var elem = jQuery('#main_menu');   
            //             var pos =jQuery('#main_menu').height()  ;
            //             setInterval(function() {
            //                 if (pos >= 10) {
            //                     jQuery('#main_menu').height(pos);
            //                     pos--; 
            //                 }
            //             }, 1);
            //         }
            //     } else {
            //         if (jQuery('#main_menu').height() < 50) {
            //             var elem = jQuery('#main_menu');   
            //             var pos = jQuery('#main_menu').height() ;
            //             setInterval(function() {
            //                 if (pos <= 50) {
            //                     jQuery('#main_menu').height(pos);
            //                     pos++; 
            //                 }
            //             }, 1);
            //         }
            //     }
            //     previousPosition = currentPosition;
            // });
        });
    </script>
</body>
</html>
