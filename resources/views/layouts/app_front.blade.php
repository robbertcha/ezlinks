<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Aurora Living') }}</title>

    <!-- 引入css -->
    
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/al_app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/al_chloe.css') }}" rel="stylesheet">
    <link href="{{ asset('css/al_will.css') }}" rel="stylesheet">
    <link href="{{ asset('css/al_mobile.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/@progress/kendo-theme-default@latest/dist/all.css">

    
    <link rel="manifest" href="/manifest.json"/>
   
    
    <!-- ios -->
    <meta name="apple-mobile-web-app-capable" content="yes" />

    <!-- ios splash -->
    <link href="images/ios_splash_2208.png" sizes="2048x2732" rel="apple-touch-startup-image" />

    <!-- ios icon -->
    <link rel="apple-touch-icon" href="images/logo_192.png">
    <link rel="apple-touch-icon" sizes="152x152" href="images/logo_152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="images/logo_180.png">
    <link rel="apple-touch-icon" sizes="167x167" href="images/logo_167.png">

    <script src="//cdn.bootcss.com/vue/2.5.13/vue.min.js"></script>
    <script src="//cdn.bootcss.com/axios/0.17.1/axios.min.js"></script>
    <script src="//cdn.bootcss.com/lodash.js/4.17.4/lodash.min.js"></script>
    <script src="//code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://kendo.cdn.telerik.com/2019.3.1023/js/kendo.all.min.js"></script>

    <script>
        var user_detail = null
        <?php if  ($user_detail != null && $user_detail != '') { ?>
            user_detail = <?php echo json_encode($user_detail); ?>
        <?php }  ?>

        window.myToken =  <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    <script src="{{ asset('js/pwa.js') }}"></script>
</head>
<body>
    <div id="app">
        

        <section class=" col pg_content">
            @guest
                <nav id="front_header">
                        <div class="nav_content container">
                              <a href="">
                                    <img src="">
                              </a>

                            <button id="btn_mobile_menu">
                                <i class="fas fa-bars"></i>
                            </button>

                              <ul id="main_menu" class="menu">
                                    <li><a href="{{ url('/feature') }}">Features</a></li>
                                    <li><a href="{{ url('/building_manager') }}">Building Managers</a></li>
                                    <li><a href="{{ url('/residents') }}">Residents</a></li>
                                    <li><a href="">Support</a></li>
                                    <li class="user">
                                          <i class="fas fa-user"></i>
                                          <a href="{{ url('/blgadmin-login') }}">Login</a>
                                    </li>
                              </ul>
                        </div>
                </nav>
                
                <div id="front_content">
                        @yield('content_front')
                </div>

                <footer id="front_footer">
                    <ul id="footer_menu" class="container">
                        <li class="col-md-4">
                            <h5>Aurora Living</h5>
                            <a class="">Features </a>
                            <a class="">Owners & Tenants </a>
                            <a class="">Customer Feedbacks </a>
                            <a class="">Developers </a>
                            <a class="">Newsletter </a>
                            <a class="">Services </a>

                        </li>

                        <li class="col-md-4">
                            <h5>The Experience</h5>
                            <a class="">For Building Managers </a>
                            <a class="">For Residents </a>
                            <a class="">Strata Managers </a>
                            <a class="">Investers / Partners </a>
                            <a class="">Facilities Managers </a>
                        </li>

                        <li class="col-md-4">
                            <h5>Contact Us</h5>
                            <a class="">Brisbane </a>
                            <a class="">Gold Coast </a>
                            <a class="">Sydney </a>
                        </li>
                    </ul>

                    <ul id="social_media" class="container">
                        <li>
                            <a href=""><i class="fab fa-facebook-f"></i></a>
                        </li>

                        <li>
                            <a href=""><i class="fab fa-instagram"></i></a>
                        </li>

                        <li>
                            <a href=""><i class="fab fa-twitter"></i></a>
                        </li>
                    </ul>

                    <span class="copyright container">
                        © Copyright 2019. All Rights Reserved. 88 Doggett Street, Newstead QLD 4006. Proudly Made In Australia. 
                    </span>
                    <span class="tnc container">
                        <a href="">Privacy policy</a> | <a href="">Terms And Conditions</a>
                    </span>
                </footer>
            @endguest
        </section>
    </div>


    <div id="ios_prompt">
        <span style="color: rgb(187, 187, 187); float: right; margin-top: -14px; margin-right: -11px;">&times;</span>
        <i class="fas fa-plus-square" style="font-size: 2.4em;float: left; height: auto; width: auto; margin-right: .25em;"></i>
        <p style="margin-top: -3px; line-height: 1.3rem;">To install this Web App in your iPhone/iPad press <img src="images/ios_popup.png" style="display: inline-block; margin-top: 4px; margin-bottom: -4px; height: 20px; width: auto;"> and then Add to Home Screen.</p>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <script>
        document.getElementById("btn_mobile_menu").onclick = function(){
            if (jQuery("ul#main_menu").is(":visible")) {
                jQuery("ul#main_menu").slideUp();
            } else {
                jQuery("ul#main_menu").slideDown();
            }
        };
    </script>
</body>
</html>
