<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="../images/logo.png" type=" image/png" >
    <title>{{ config('app.name', 'Aurora Living') }}</title>

    <!-- 引入css -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/al_app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/al_chloe.css') }}" rel="stylesheet">
    <link href="{{ asset('css/al_will.css') }}" rel="stylesheet">
    <link href="{{ asset('css/al_mobile.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/@progress/kendo-theme-default@latest/dist/all.css">
    
    <link rel="manifest" href="/manifest.json"/>

    <script src="//cdn.bootcss.com/vue/2.6.11/vue.min.js"></script>
    <script src="//cdn.bootcss.com/axios/0.17.1/axios.min.js"></script>
    <script src="//cdn.bootcss.com/lodash.js/4.17.15/lodash.min.js"></script>
    <script src="//code.jquery.com/jquery-3.5.0.min.js"></script>
    <script src="https://kendo.cdn.telerik.com/2019.3.1023/js/kendo.all.min.js"></script>

<!--
    @if(Request::is('mgr/dashboard'))
        <script src="https://unpkg.com/@progress/kendo-scheduler-vue-wrapper/dist/cdn/kendo-scheduler-vue-wrapper.js"></script>
    @endif
-->
    <script>
         var user_detail = null
        <?php 
            if (isset($user)) {
                if  ($user != null && $user != '') { 
        ?>
                    user_detail = <?php echo json_encode($user); ?>
        <?php   
                }
            }  
        ?>

        // rename myToken as you like
        window.myToken =  <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    <script src="{{ asset('js/pwa.js') }}"></script>
</head>
<body>
    <div id="app">
        <loader :is-loading = isLoading></loader>
        <section class=" col pg_content">
            @auth
                <header>
                    <!-- Branding Image -->
                    <a class="navbar-brand" href="#">
                        <div class="navbar-header">
                            <!-- Collapsed Hamburger -->
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" onclick="toggleMainMenu()">
                                <i class="fas fa-bars"></i>
                            </button>
                        </div>

                        <span class="txt_logo">
                            {{ Auth::user()->building_info->building->name }}
                        </span>
                    </a>

                    <nav class="navbar navbar-default navbar-static-top">
                        <div style="width: 92%;margin: 0px 6% 0px 2%;">
                        
                            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                                <!-- Left Side Of Navbar -->
                                <ul class="nav navbar-nav">
                                    &nbsp;
                                </ul>

                                <!-- Right Side Of Navbar -->
                                <ul class="nav navbar-nav navbar-right">
                                    <!-- Authentication Links -->
                                    @if (Auth::guest())
                                        <li><a href="{{ route('login') }}">Login</a></li>
                                        <li><a href="{{ route('register') }}">Register</a></li>
                                    @else
                                        <li class="">
                                            <a href="{{url('/mgr/reservation')}}">
                                                <i class="far fa-calendar-alt"></i>
                                            </a>
                                        </li>
                                        <!--  v-on:click="toggleSlideMenu('help')"-->
                                        <li class="">
                                            <a href="{{url('/mgr/help')}}"><i class="fa fa-question fa-lg"></i></a>
                                            <!-- <label>Help<label> -->
                                        </li>
                                        <li>
                                            <a href="#" id="admin_notification">
                                                <i class="fa fa-bell fa-lg"></i>
                                                <div id="notification_point"></div>
                                            </a>

                                            <ul id="admin_notification_cont" class="dropdown-menu">
                                                <!-- <a href="{{ url('/mgr/admin_push') }}">Push</a> -->
                                            </ul>
                                        </li>

                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                                {{ Auth::user()->name }} <span class="caret"></span>
                                            </a>

                                            <ul class="dropdown-menu" role="menu">
                                                <li>
                                                    <a href="{{ route('blgadmin.logout') }}" >
                                                        Logout
                                                    </a>

                                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                        {{ csrf_field() }}
                                                    </form>
                                                </li>
                                            </ul>
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </nav>
                </header>

                <main>
                    <div id="sidebar">
                        @include('includes.sidebar')
                    </div>
                    
                    <div id="content">
                        @yield('content')
                    </div>
                </main>
            @endauth

            @guest
                @yield('guest-content')
            @endguest

            <transition name="slide">
                <nav id="mySidenav" class="sidenav col-md-10" v-show="showSlideMenu" style="display: none;"  v-on:click.self="toggleSlideMenu('')">
                    @include('includes.setting_menu')
                   <!-- 
                    @include('includes.help_menu')
                    -->
                </nav>
            </transition>
        </section>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <script>

        // menu toggle
        // var menu_active = 4;
        // menuToggle(menu_active);
        // jQuery("ul.mgr_menu > li.btn_extend > button").removeClass('btn_less').addClass('btn_more');

        // jQuery(document).on('click','ul.mgr_menu > li.btn_extend > button',function(){
        //     if (jQuery(this).hasClass('btn_less')) {
        //         jQuery(this).removeClass('btn_less').addClass('btn_more');
        //         jQuery(this).find('span').html("More");
        //         menuToggle(menu_active);
        //     } else {
        //         jQuery(this).removeClass('btn_more').addClass('btn_less');
        //         jQuery(this).find('span').html("Narrow");
        //         menuToggle(30);
        //     }
        // });

        // function menuToggle(ma) {
        //     jQuery("ul.mgr_menu > li").each(function(index){
        //         if (index >= ma) {
        //             if (!jQuery(this).hasClass('btn_extend')) {
        //                 jQuery(this).addClass('menu_hide');
        //             } 
        //         } else {
        //             if (!jQuery(this).hasClass('btn_extend')) {
        //                 jQuery(this).removeClass('menu_hide');
        //             } 
        //         }
        //     });
        // }



        function toggleMainMenu() {
            if (jQuery("main #sidebar").is(":visible")) {
                jQuery("main #sidebar").hide();
                jQuery("main").removeClass("menu-on");
            } else {
                jQuery("main #sidebar").show();
                jQuery("main").addClass("menu-on");
            }
        }


        jQuery(document).on('click','.manage_new_cont button.btn_new',function(){
            if (jQuery("ul#manage_new_list").is(":visible")) {
                jQuery(this).removeClass("is_active");
                jQuery("ul#manage_new_list").hide();
            } else {
                jQuery(this).addClass("is_active");
                jQuery("ul#manage_new_list").show();
            }
        });

        jQuery(document).on('click','#admin_notification',function(e){
            e.preventDefault();
            if (jQuery("#admin_notification_cont").is(":visible")) {
                jQuery("#admin_notification_cont").hide();
            } else {
                jQuery("#admin_notification_cont").show();
            }
            jQuery("#admin_notification .notification_point").removeClass('new');
        });

        jQuery(document).ready(function(){
            var latest_nid = 0;
            if (jQuery("ul#admin_notification_cont > li:first").length > 0) {
                latest_nid = jQuery("ul#admin_notification_cont > li:first").attr("data-id");
            }

            jQuery.ajax({
                    type:'GET',
                    url:'/mgr/get_notification_list',
                    data:{ 'latest_nid': latest_nid},
                    success:function(data){
                        var content = '';
                        if (data.length > 0) {
                            for (var i = 0 ; i < data.length; i++) {
                                content += "<li data-id='"+data[i].id+"'>";
                                    content += "<label>"+data[i].title+"</label>";
                                    content += "<span>"+data[i].created_at+"</span>";
                                    content += "<div>"+data[i].content+"</div>";
                                content += "</li>";
                            }

                            jQuery("ul#admin_notification_cont").prepend(content);
                        }
                    }
            });
        });

        // get message from service worker
        const swListener = new BroadcastChannel('swListener');
        swListener.onmessage = function(e) {
            console.log('swListener Received', e.data);
            if(e.data == 'new_notification') {
                var notification_point = window.document.getElementById("notification_point");
                notification_point.classList.add("new");
            }
        };
    </script>
</body>
</html>
