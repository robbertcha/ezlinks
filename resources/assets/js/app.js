
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

// import Vuetify from 'vuetify'
// import DaySpanVuetify from 'dayspan-vuetify'

Vue.config.productionTip = false
// Vue.use(Vuetify);
// Vue.use(DaySpanVuetify, {
//   methods: {
//     getDefaultEventColor: () => '#1976d2'
//   }
// });

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */



// frontend
Vue.component('homepage', function (resolve) {
  require(['./components/homepage/homepage.vue'], resolve)
});

Vue.component('feature', function (resolve) {
  require(['./components/feature.vue'], resolve)
});

Vue.component('building-mng', function (resolve) {
  require(['./components/setting/building_mng.vue'], resolve)
});

Vue.component('residents', function (resolve) {
  require(['./components/residents.vue'], resolve)
});



// tenant panel
Vue.component('tenant-home', function (resolve) {
  require(['./components/tenants/tenant_home.vue'], resolve)
});

Vue.component('tenant-announcement-detail', function (resolve) {
  require(['./components/tenants/tenant_announcement_detail.vue'], resolve)
});

Vue.component('tenant-announcements', function (resolve) {
  require(['./components/tenants/tenant_announcements.vue'], resolve)
});

Vue.component('tenant-reservations', function (resolve) {
  require(['./components/tenants/tenant_reservations.vue'], resolve)
});

Vue.component('tenant-useracc', function (resolve) {
  require(['./components/tenants/tenant_useracc.vue'], resolve)
});

Vue.component('tenant-createbooking', function (resolve) {
  require(['./components/tenants/tenant_createbooking.vue'], resolve)
});

Vue.component('tenant-makeareport', function (resolve) {
  require(['./components/tenants/tenant_makeareport.vue'], resolve)
});

Vue.component('popup-tenant-user', function (resolve) {
  require(['./components/tenants/popup_tenant-user.vue'], resolve)
});

Vue.component('maintenance-list', function (resolve) {
  require(['./components/tenants/maintenance_list.vue'], resolve)
});

Vue.component('maintenance-list-detail', function (resolve) {
  require(['./components/tenants/maintenance_list_detail.vue'], resolve)
});

Vue.component('tenant-documents', function (resolve) {
  require(['./components/tenants/tenant_documents.vue'], resolve)
});

Vue.component('tenant-parcel', function (resolve) {
  require(['./components/tenants/tenant_parcel.vue'], resolve)
});

Vue.component('notification', function (resolve) {
  require(['./components/tenants/notification.vue'], resolve)
});

Vue.component('tenant-address-book', function (resolve) {
  require(['./components/tenants/tenant_address_book.vue'], resolve)
});


Vue.component('tenant-dropdown', function (resolve) {
  require(['./components/manage/tenant_dropdown.vue'], resolve)
});

Vue.component('unit-dropdown', function (resolve) {
  require(['./components/unit/unit_dropdown.vue'], resolve)
});



//wiki
// Vue.component('question-mark', function (resolve) {
//   require(['./components/question_mark.vue'], resolve)
// });

// Vue.component('panel', function (resolve) {
//   require(['./components/panel.vue'], resolve)
// });


Vue.component('check-box', function (resolve) {
  require(['./components/check_box.vue'], resolve)
});

// list page checkbox bulk action
// Vue.component('calendarEvent', function (resolve) {
//   require(['./components/calendar_event.vue'], resolve)
// });

Vue.component('check', function (resolve) {
  require(['./components/check.vue'], resolve)
});

//page loading
Vue.component('loader', function (resolve) {
  require(['./components/loader.vue'], resolve)
});


//contact list
Vue.component('contact-list', function (resolve) {
  require(['./components/contractor/contact_list.vue'], resolve)
});

// dashboard
Vue.component('initial-setting', function (resolve) {
  require(['./components/setting/initial_setting.vue'], resolve)
});

Vue.component('dashboard', function (resolve) {
  require(['./components/dashboard/dashboard.vue'], resolve)
});


// mail template - dynamic keyword
Vue.component('template-content', function (resolve) {
  require(['./components/template_content.vue'], resolve)
});

Vue.component('add-mail-select', function (resolve) {
  require(['./components/add_mail_select.vue'], resolve)
});

Vue.component('add', function (resolve) {
  require(['./components/add.vue'], resolve)
});


// announcement list & announcement
Vue.component('announcement-list', function (resolve) {
  require(['./components/announcement/announcement_list.vue'], resolve)
});

Vue.component('announcement-form', function (resolve) {
  require(['./components/announcement/announcement.vue'], resolve)
});


// unit
Vue.component('create-unit', function (resolve) {
  require(['./components/unit/create_unit.vue'], resolve)
});

Vue.component('unit-list', function (resolve) {
  require(['./components/unit/unit_list.vue'], resolve)
});

// unit header
Vue.component('unit-head',function(resolve){
  require(['./components/unit/unit_header.vue'],resolve)
});


Vue.component('unit-form', function (resolve) {
  require(['./components/unit/unit.vue'], resolve)
});

Vue.component('popup-carpark', function (resolve) {
  require(['./components/popup/popup_carpark.vue'], resolve)
});

Vue.component('popup-key', function (resolve) {
  require(['./components/popup/popup_key.vue'], resolve)
});


// tenant
Vue.component('tenant-list', function (resolve) {
  require(['./components/manage/tenant_list.vue'], resolve)
});

Vue.component('parking-breaches-list', function (resolve) {
  require(['./components/manage/parking_breaches_list.vue'], resolve)
});

Vue.component('parking-breaches-detail', function (resolve) {
  require(['./components/manage/parking_breaches_detail.vue'], resolve)
});


// scheduler
Vue.component('task-list', function (resolve) {
  require(['./components/calendar/task_list.vue'], resolve)
});

Vue.component('task-details', function (resolve) {
  require(['./components/calendar/task_details.vue'], resolve)
});

Vue.component('maintanence-list', function (resolve) {
  require(['./components/case/maintanence_list.vue'], resolve)
});

Vue.component('maintanence-details', function (resolve) {
  require(['./components/case/maintanence_details.vue'], resolve)
});

Vue.component('inspection-list', function (resolve) {
  require(['./components/inspection/inspection_list.vue'], resolve)
});

Vue.component('popup-inspection-detail', function (resolve) {
  require(['./components/popup_inspection_detail.vue'], resolve)
});


Vue.component('inspection-details', function (resolve) {
  require(['./components/inspection/inspection_details.vue'], resolve)
});

Vue.component('reminder-list', function (resolve) {
  require(['./components/reminder_list.vue'], resolve)
});

Vue.component('booking-list', function (resolve) {
  require(['./components/calendar/booking_list.vue'], resolve)
});

Vue.component('inspection-booking-list', function (resolve) {
  require(['./components/inspection/inspection_booking_list.vue'], resolve)
});



// reservation
Vue.component('reservation', function (resolve) {
  require(['./components/calendar/reservation.vue'], resolve)
});

Vue.component('calendar', function (resolve) {
  require(['./components/calendar/calendar.vue'], resolve)
});

Vue.component('scheduler-list', function (resolve) {
  require(['./components/calendar/scheduler_list.vue'], resolve)
});


// facility
Vue.component('facility', function (resolve) {
  require(['./components/building_stuffs/facility.vue'], resolve)
});


// complaint
Vue.component('case-list', function (resolve) {
  require(['./components/case/case_list.vue'], resolve)
});

Vue.component('case', function (resolve) {
  require(['./components/case/case.vue'], resolve)
});

Vue.component('case-history', function (resolve) {
  require(['./components/case/case_history.vue'], resolve)
});


// courier
Vue.component('courier-list', function (resolve) {
  require(['./components/courier/courier_list.vue'], resolve)
});


// report
Vue.component('report', function (resolve) {
  require(['./components/report/new_report.vue'], resolve)
});


// contractor
Vue.component('contractor-list', function (resolve) {
  require(['./components/contractor/contractor_list.vue'], resolve)
});

Vue.component('recommend-contractor', function (resolve) {
  require(['./components/contractor/contractor_recommend.vue'], resolve)
});


// setting
Vue.component('building-info', function (resolve) {
  require(['./components/setting/building_info.vue'], resolve)
});

Vue.component('asset-list', function (resolve) {
  require(['./components/building_stuffs/asset_list.vue'], resolve)
});

Vue.component('btn-reservation-popup', function (resolve) {
  require(['./components/btn_reservation_popup.vue'], resolve)
});

Vue.component('popup-booking', function (resolve) {
  require(['./components/popup/popup_booking.vue'], resolve)
});

Vue.component('popup-booking-cont', function (resolve) {
  require(['./components/popup/popup_booking_cont.vue'], resolve)
});

Vue.component('popup-asset-history', function (resolve) {
  require(['./components/popup_asset_history.vue'], resolve)
});

Vue.component('asset-details', function (resolve) {
  require(['./components/building_stuffs/asset_details.vue'], resolve)
});

Vue.component('inventory-list', function (resolve) {
  require(['./components/building_stuffs/inventory_list.vue'], resolve)
});

Vue.component('inventory-details', function (resolve) {
  require(['./components/building_stuffs/inventory_details.vue'], resolve)
});

Vue.component('category-list', function (resolve) {
  require(['./components/building_stuffs/category_list.vue'], resolve)
});

Vue.component('library-list', function (resolve) {
  require(['./components/building_stuffs/library_list.vue'], resolve)
});

Vue.component('report-list', function (resolve) {
  require(['./components/report/report_list.vue'], resolve)
});

Vue.component('template-list', function (resolve) {
  require(['./components/building_stuffs/template_list.vue'], resolve)
});

Vue.component('personal-info', function (resolve) {
  require(['./components/setting/personal_info.vue'], resolve)
});

Vue.component('amenity-list', function (resolve) {
  require(['./components/building_stuffs/amenity_list.vue'], resolve)
});


// popup modal
Vue.component('popup-category', function (resolve) {
  require(['./components/popup/popup_category.vue'], resolve)
});

Vue.component('popup-case-history', function (resolve) {
  require(['./components/popup/case_history_popup.vue'], resolve)
});

Vue.component('category', function (resolve) {
  require(['./components/category.vue'], resolve)
});

Vue.component('popup-parcel-details', function (resolve) {
  require(['./components/popup/popup_parcel_details.vue'], resolve)
});

Vue.component('popup-cpdetails', function (resolve) {
  require(['./components/popup/popup_cpdetails.vue'], resolve)
});

Vue.component('popup-tenantdetails', function (resolve) {
  require(['./components/popup/popup_tenantdetails.vue'], resolve)
});

Vue.component('popup-createkey', function (resolve) {
  require(['./components/popup/popup_createkey.vue'], resolve)
});

Vue.component('popup-keyout', function (resolve) {
  require(['./components/popup/popup_keyout.vue'], resolve)
});

Vue.component('popup-case-reservation', function (resolve) {
  require(['./components/popup/popup_case_reservation.vue'], resolve)
});

Vue.component('popup-add-tenant', function (resolve) {
  require(['./components/popup/popup_add_tenant.vue'], resolve)
});

Vue.component('popup-add-carpark', function (resolve) {
  require(['./components/popup/popup_add_carpark.vue'], resolve)
});

Vue.component('popup-case-email-contractor', function (resolve) {
  require(['./components/popup/popup_case_email_contractor.vue'], resolve)
});

Vue.component('popup-email-recipents', function (resolve) {
  require(['./components/popup/popup_common_email.vue'], resolve)
});

Vue.component('popup-case-quote', function (resolve) {
  require(['./components/popup/popup_case_quote.vue'], resolve)
});

Vue.component('popup-calendar-event', function (resolve) {
  require(['./components/popup/popup_calendar_event.vue'], resolve)
});

Vue.component('popup-case-invoice', function (resolve) {
  require(['./components/popup/popup_case_invoice.vue'], resolve)
});


Vue.component('add-carpark', function (resolve) {
  require(['./components/manage/add_carpark.vue'], resolve)
});

Vue.component('close-window', function (resolve) {
  require(['./components/close_window.vue'], resolve)
});


// login page
Vue.component('login', function (resolve) {
  require(['./components/homepage/login.vue'], resolve)
});

// Help page
Vue.component('help', function(resolve) {
  require(['./components/help/help.vue'], resolve)
});

Vue.component('help-all', function(resolve){
  require(['./components/help/help_all.vue'], resolve)
});



// Vue.component('test', function(resolve){
//   require(['./components/test.vue'], resolve)
// });

// Vue.component('modal', {
//     // template: '#modal-template',
//     // props: ['show'],
//     methods: {
//         close: function () {
//             this.$emit('close');
//         },
//     }
// });


Vue.directive('click-outside', {
  bind: function (el, binding, vnode) {
    el.clickOutsideEvent = function (event) {
      // here I check that click was outside the el and his children
      if (!(el == event.target || el.contains(event.target))) {
        // and if it did, call method provided in attribute value
        vnode.context[binding.expression](event);
      }
    };
    document.body.addEventListener('click', el.clickOutsideEvent)
  },
  unbind: function (el) {
    document.body.removeEventListener('click', el.clickOutsideEvent)
  },
});


var vm = new Vue({
    el: '#app',
    data: {
      rlist: [],
      modal : false,
      showSlideMenu: false, //false
      showSettingSlider: false,
      showHelpSlider: false, //false
      isLoading:false,
      helpContentVisible: false,
      helpIndex: '0',
      closeWindow:false,
      org:true, //true
      col_md_10:"col-md-10",
      sidenav:"sidenav",
      sidenav_help:"sidenav help_side"
    },
    methods: {
      goToDetailPage:function(link) {
        let a= document.createElement('a');
        a.target= '_blank';
        a.href= link;
        a.click();
      },
      getCaseProgressTxt:function() {
        let p = [];
          p[1] = 'New';
          p[2] = 'In Progress';
          p[3] = 'Completed';
          p[4] = 'Closed';
          p[5] = 'Awaiting Contractor';
          p[6] = 'Awaiting Approval';
          p[7] = 'Commitee Approval';
          p[8] = 'Admin Approval';
          p[9] = 'Building Manager Discretion';
        return p;
      },
      isLoader:function(){
        this.isLoading = true;
        setTimeout(()=>this.isLoading = false, 1000)
      },

      sendInfo:function(type, recipients, recipient_type, action){
        axios.post('/mgr/send_email', {'type': type, 'recipients': recipients, 'recipient_type': recipient_type, 'action': action}).then(function(response){
                console.log(response.data)
              });;
      },
      sendInfoByAddress:function(email_address, action){
        axios.post('/mgr/send_email_by_address', {'email_address': email_address, 'action':action}).then(function(response){
          console.log(response)
        });
      },
      isMobile: function() {
        var check = false;
        (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
        return check;
      },
      toggleSlideMenu(option){
        if (this.showSlideMenu == false) {
          if (option == 'setting') {
            this.showSettingSlider = true;
            this.org = true;
          } else if (option == 'help') {
            this.showHelpSlider = true;
            this.org = false;
          } 
        } else {
          this.showSettingSlider = false;
          this.showHelpSlider = false;
        }
        this.showSlideMenu = !this.showSlideMenu;
      },
      toggleHelpContent(index) {
        console.log(this.helpIndex);
        if (this.helpIndex == index) {
          this.helpIndex = '0';
        } else {
          this.helpIndex = index;
        }
      }
    },
    created() {
      
    }
});


