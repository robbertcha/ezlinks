export default {
      data: function () {
            return {
                  event_start_time: '6:00:00',
                  event_end_time: '21:00:00',
                  event_format: {
                        amenity_id: null,
                        amenity_title: "",
                        area_name: null,
                        bi_id: null,
                        bi_items: null,
                        booking_end: null,
                        booking_recurring: null,
                        car_plate: null,
                        comment: "",
                        details: "",
                        end: null,
                        event_details: "",
                        event_object: "",
                        id: null,
                        //inspection_history: null,
                        parent_id: null,
                        progress: 1,
                        progress_title: "Pending",
                        recurrenceException: null,
                        recurrenceRule: null,
                        start_date: null,
                        start: null,
                        tenant_id: null,
                        typeId: "3",
                        assignee_id: null
                  },
                  event_case_detail: {
                        area_detail: "",
                        asset_id: null,
                        bi_id: 1,
                        case_type: "inspection",
                        content: null,
                        job_area: 2,
                        priority: 1,
                        progress: 1,
                        routine: "YEARLY",
                        unit_id: null
                  },
            }
      },
      methods: {
            goToDetailPage:function(link) {
                  let a= document.createElement('a');
                  a.target= '_blank';
                  a.href= link;
                  a.click();
            },
            getFormatDateBydate:function(e, seperator='-', showTime=false) {
                  if (e.hasOwnProperty('start')) {
                        var y = new Date(e.start).getFullYear();
                        var m = new Date(e.start).getMonth()+1;
                        var d = new Date(e.start).getDate();

                        m = ("0" + m).slice(-2);
                        d = ("0" + d).slice(-2);

                        if (showTime == false) {
                              return  y+seperator+m+seperator+d;
                        } else {
                              var h = new Date(e.start).getHours();
                              var mm = new Date(e.start).getMinutes();
                              var s = '00';

                              h = ("0" + h).slice(-2);
                              mm = ("0" + mm).slice(-2);
                              return  y+seperator+m+seperator+d+seperator+h+seperator+mm+seperator+s;
                        }
                  }
            },
            isExpired:function(date) {
                  return new Date() < new Date(date);
            },
            isDate:function(date) {
                  return (new Date(date) !== "Invalid Date") && !isNaN(new Date(date));
            },
            getDateTime:function(val, type, options={}) {
                  if (type == 'date') {
                        return new Date(val).toLocaleDateString(undefined, options);
                  } else if (type == 'time') {
                        return new Date(val).toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
                  } 
            },
            isNumeric:function(n) {
                  return !isNaN(parseFloat(n)) && isFinite(n);
            },
            setEventBeforeSave:function(event_data, case_detail) {
                  var start_date = new Date(event_data.start_date);
                  start_date = new Date(start_date.getTime() - (start_date.getTimezoneOffset() * 60000)).toISOString().split('T')[0];
                  var start = this.isDate(event_data.start) == true ? new Date(event_data.start).toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true }) : event_data.start;
                  var end =  this.isDate(event_data.end) == true ? new Date(event_data.end).toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true }) : event_data.end;

                  return {
                        // global
                        id: event_data.hasOwnProperty("id") ? event_data.id : null,
                        typeId: event_data.typeId,
                        progress: event_data.typeId == 1 ? 1 : event_data.progress,
                        details: event_data.details,
                        start_date: start_date,
                        start: start_date+" "+start, 
                        end: start_date+" "+end,     
                        
                        // car parking or reservation
                        newTenant: event_data.typeId == 3 || event_data.typeId == 4 ? event_data.tenant_id : null,    
                        amenityId: event_data.typeId == 3 ? event_data.amenity_id : null,                                    
                        car_plate: event_data.typeId == 4 ? event_data.car_plate : null, 
                        comment: event_data.comment,

                        // job
                        case_id: case_detail.id > 0 ? case_detail.id : null,
                        case_type: event_data.typeId == 1 ? case_detail.case_type : null,    
                        parent_id: event_data.typeId == 1 ? (case_detail.parent_id > 0 ? case_detail.parent_id : null) : null,                              
                        bi_list: event_data.typeId == 1 && case_detail.case_type == 'inspection' ? case_detail.bi_id : null,           
                        job_area: event_data.typeId == 1 && case_detail.case_type != 'inspection' ? case_detail.job_area : null,       
                        area_detail: event_data.typeId == 1 && case_detail.job_area == 2 ? case_detail.area_detail : null,             
                        asset_id: event_data.typeId == 1 && case_detail.job_area == 1 ? case_detail.asset_id : null,
                        unit_id: event_data.typeId == 1 && case_detail.job_area == 3 ? case_detail.unit_id : null,
                        case_progress: event_data.typeId == 1 ? case_detail.progress : null,
                        priority: event_data.typeId == 1 ? case_detail.priority : null,
                        contractor_list: event_data.typeId == 1 ? event_data.hasOwnProperty("assignee_id") ? event_data.assignee_id : case_detail.assignee_id : null,
                        routine: event_data.typeId == 1 && case_detail.routine.toString().toLowerCase() == 'none' ? 0 : (event_data.typeId != 1 ? 0 : case_detail.routine),
                        booking_recurring: event_data.typeId == 1 && case_detail.routine.toString().toLowerCase() == 'none' ? null : (event_data.typeId != 1 || this.isNumeric(case_detail.routine)  ? null : 'FREQ='+case_detail.routine)
                  }
            }
      },
      created() {
            let tomorrow = new Date().setDate(new Date().getDate() + 1);
            tomorrow = new Date(tomorrow).toISOString().slice(0, 10);
            this.event_format.start_date = tomorrow;
            this.event_format.start = tomorrow+" "+this.event_start_time;
            this.event_format.end = tomorrow+" "+this.event_end_time;
            this.event_format.start = new Date(this.event_format.start).toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
            this.event_format.end = new Date(this.event_format.end).toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
      }
};