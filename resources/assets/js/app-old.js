
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

// import Vuetify from 'vuetify'
// import DaySpanVuetify from 'dayspan-vuetify'

Vue.config.productionTip = false
// Vue.use(Vuetify);
// Vue.use(DaySpanVuetify, {
//   methods: {
//     getDefaultEventColor: () => '#1976d2'
//   }
// });

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */



// frontend
Vue.component('homepage', require('./components/homepage.vue'));
Vue.component('feature', require('./components/feature.vue'));
Vue.component('building-mng', require('./components/building_mng.vue'));
Vue.component('residents', require('./components/residents.vue'));

// tenant panel
Vue.component('tenant-home', require('./components/tenants/tenant_home.vue'));
Vue.component('tenant-announcement-detail', require('./components/tenants/tenant_announcement_detail.vue'));
Vue.component('tenant-announcements', require('./components/tenants/tenant_announcements.vue'));
Vue.component('tenant-reservations', require('./components/tenants/tenant_reservations.vue'));
Vue.component('tenant-useracc', require('./components/tenants/tenant_useracc.vue'));
Vue.component('tenant-createbooking', require('./components/tenants/tenant_createbooking.vue'));
Vue.component('tenant-makeareport', require('./components/tenants/tenant_makeareport.vue'));
Vue.component('popup-tenant-user', require('./components/tenants/popup_tenant-user.vue'));
Vue.component('maintenance-list', require('./components/tenants/maintenance_list.vue'));
Vue.component('maintenance-list-detail', require('./components/tenants/maintenance_list_detail.vue'));
Vue.component('tenant-documents', require('./components/tenants/tenant_documents.vue'));
Vue.component('tenant-parcel', require('./components/tenants/tenant_parcel.vue'));
Vue.component('notification', require('./components/tenants/notification.vue'));
Vue.component('tenant-address-book', require('./components/tenants/tenant_address_book.vue'));

//question mark
Vue.component('question-mark', require('./components/question_mark'))
Vue.component('panel', require('./components/panel'))

// checkbox
Vue.component('check-box', require('./components/check_box'))

//check
Vue.component('check', require('./components/check'))

//loading
Vue.component('loader', require('./components/loader'))

//contact list
Vue.component('contact-list', require('./components/contact_list'))

// dashboard
Vue.component('dashboard', require('./components/dashboard.vue'));

// add
Vue.component('add-mail-select', require('./components/add_mail_select.vue'));
Vue.component('add', require('./components/add.vue'));
Vue.component('add-carpark', require('./components/add_carpark.vue'));

// unit head
Vue.component('unit-head',require('./components/unit_header.vue'));

// announcement list
Vue.component('announcement-list', function (resolve) {
  require(['./components/announcement_list.vue'], resolve)
});
// Vue.component('announcement-list', require('./components/announcement_list.vue'));

// announcement
Vue.component('announcement-form', require('./components/announcement.vue'));

// unit
Vue.component('unit-list', require('./components/unit_list.vue'));
Vue.component('unit-form', require('./components/unit.vue'));
Vue.component('popup-carpark', require('./components/popup_carpark.vue'));
Vue.component('popup-key', require('./components/popup_key.vue'));

// tenant
Vue.component('tenant-list', require('./components/tenant_list.vue'));
Vue.component('parking-breaches-list', require('./components/parking_breaches_list.vue'));
Vue.component('parking-breaches-detail', require('./components/parking_breaches_detail.vue'));


// scheduler
Vue.component('task-list', require('./components/task_list.vue'));
Vue.component('task-details', require('./components/task_details.vue'));
Vue.component('maintanence-list', require('./components/maintanence_list.vue'));
Vue.component('maintanence-details', require('./components/maintanence_details.vue'));
Vue.component('inspection-list', require('./components/inspection_list.vue'));
Vue.component('inspection-details', require('./components/inspection_details.vue'));
Vue.component('reminder-list', require('./components/reminder_list.vue'));
Vue.component('booking-list', require('./components/booking_list.vue'));
Vue.component('inspection-booking-list', require('./components/inspection_booking_list.vue'));

// reservation
Vue.component('reservation', function (resolve) {
  require(['./components/reservation.vue'], resolve)
});

// Vue.component('reservation', require('./components/reservation.vue'));
Vue.component('scheduler-list', require('./components/scheduler_list.vue'));

// facility
Vue.component('facility', require('./components/facility.vue'));

// complaint
Vue.component('case-list', require('./components/case_list.vue'));
Vue.component('case', require('./components/cases.vue'));

// courier
Vue.component('courier-list', require('./components/courier_list.vue'));

// report
Vue.component('report', require('./components/report.vue'));

// contractor
Vue.component('contractor-list', require('./components/contractor_list.vue'));
Vue.component('recommend-contractor', require('./components/contractor_recommend.vue'));

// setting
Vue.component('building-info', require('./components/building_info.vue'));
Vue.component('asset-list', require('./components/asset_list.vue'));
Vue.component('asset-details', require('./components/asset_details.vue'));
Vue.component('inventory-list', require('./components/inventory_list.vue'));
Vue.component('inventory-details', require('./components/inventory_details.vue'));
Vue.component('category-list', require('./components/category_list.vue'));
Vue.component('library-list', require('./components/library_list.vue'));
Vue.component('report-list', require('./components/report_list.vue'));
Vue.component('template-list', require('./components/template_list.vue'));
Vue.component('personal-info', require('./components/personal_info.vue'));
Vue.component('amenity-list', require('./components/amenity_list.vue'));

// popup modal
Vue.component('popup-parcel-details', require('./components/popup_parcel_details.vue'));
Vue.component('popup-cpdetails', require('./components/popup_cpdetails.vue'));
Vue.component('popup-tenantdetails', require('./components/popup_tenantdetails.vue'));
Vue.component('popup-createkey', require('./components/popup_createkey.vue'));
Vue.component('popup-keyout', require('./components/popup_keyout.vue'));
Vue.component('popup-case-reservation', require('./components/popup_case_reservation.vue'));
Vue.component('popup-case-email-contractor', require('./components/popup_case_email_contractor.vue'));

Vue.component('popup-email-recipents', require('./components/popup_common_email.vue'));
Vue.component('popup-case-quote', require('./components/popup_case_quote.vue'));
Vue.component('popup-case-invoice', require('./components/popup_case_invoice.vue'));

// login page
Vue.component('login', require('./components/login.vue'));

Vue.component('modal', {
    // template: '#modal-template',
    // props: ['show'],
    methods: {
        close: function () {
            this.$emit('close');
        },
    }
});


new Vue({
    el: '#app',
    data: {
      rlist: [],
      modal : false,
      showSlideMenu: false,
      showSettingSlider: false,
      showAnnouncementSlider: false,
      showResidentsSlider: false,
      showSchedulerSlider: false,
      isLoading:false
    },
    methods: {
      isLoader:function(){
        this.isLoading = true;
        setTimeout(()=>this.isLoading = false, 1000)
      },

      sendInfo:function(type, recipients, recipient_type, action){
        axios.post('/mgr/send_email', {'type': type, 'recipients': recipients, 'recipient_type': recipient_type, 'action': action}).then(function(response){
                console.log(response.data)
              });;
      },
      sendInfoByAddress:function(email_address, action){
        axios.post('/mgr/send_email_by_address', {'email_address': email_address, 'action':action}).then(function(response){
          console.log(response)
        });
      },
      isMobile: function() {
        var check = false;
        (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
        return check;
      },
      toggleSlideMenu(option){
        
        if (this.showSlideMenu == false) {
          console.log(option);
          if (option == 'setting') {
            this.showSettingSlider = true;
          } else if (option == 'announcement') {
            this.showAnnouncementSlider = true;
          } else if (option == 'residents') {
            this.showResidentsSlider = true;
          } else if (option == 'scheduler') {
            this.showSchedulerSlider = true;
          }
        } else {
          this.showSettingSlider = false;
          this.showAnnouncementSlider = false;
          this.showResidentsSlider = false;
          this.showSchedulerSlider = false;
        }
        this.showSlideMenu = !this.showSlideMenu;
      }
      


    }
});




