const cacheName  = 'el-dev-0-02';
var cacheFiles = [
    '/',
];

// 监听install事件，安装完成后，进行文件缓存
self.addEventListener('install', function (e) {
    console.log('Service Worker 状态： install');
    e.waitUntil(self.skipWaiting()); // Activate worker immediately
    var cacheOpenPromise = caches.open(cacheName).then(function (cache) {
        return cache.addAll(cacheFiles);
    }).catch(function (err) {
          console.log(err);
    });;
    e.waitUntil(cacheOpenPromise);
});


// 监听activate事件，激活后通过cache的key来判断是否更新cache中的静态资源
self.addEventListener('activate', function (e) {
  console.log('Service Worker 状态： activate');
  e.waitUntil(self.clients.claim()); // Become available to all pages
  var cachePromise = caches.keys().then(function (keys) {
      return Promise.all(keys.map(function (key) {
          if (key !== cacheName) {
              return caches.delete(key);
          }
      }));
  })
  e.waitUntil(cachePromise);
  return self.clients.claim();
});


// 使用缓存的静态资源
self.addEventListener('fetch', function (e) {
  // 如果有cache则直接返回，否则通过fetch请求
  e.respondWith(
      caches.match(e.request).then(function (cache) {
          return cache || fetch(e.request);
      }).catch(function (err) {
          console.log(err);
          return fetch(e.request);
      })
  );
});



self.addEventListener('push', function (e) {
    if (!(self.Notification && self.Notification.permission === 'granted')) {
        //notifications aren't supported or permission not granted!
        return;
    }
    console.log(e.data.json());

    if (e.data) {
        
        const swListener = new BroadcastChannel('swListener');
        swListener.postMessage('new_notification');

        var msg = e.data.json();
        self.registration.showNotification(msg.title, {
            body: msg.body,
            icon: msg.icon,
            actions:  msg.actions
        });
    }
});

self.addEventListener('notificationclick', function(event) {
    switch(event.action){
        case 'open_url':
        clients.openWindow(event.notification.data.url); //which we got from above
        break;
        case 'any_other_action':
        clients.openWindow("https://www.example.com");
        break;
        }
    }
, false);