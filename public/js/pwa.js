// VAPID_PUBLIC_KEY=BPMJKg7SftyqEJKUPgi8zljNoUxl0mz3sHOltpr1OVUiLCWgf/h2hrblEFqdWD+2WEKRCvYM6O3Ld1pj624Wd+o=
// VAPID_PRIVATE_KEY=uzpIqSnpSNxOUOlxH5cuQ/3k+5T7+fKGo1sol3C4yXQ=

jQuery(document).ready(function(){
    // Detects if device is on iOS 
    const isIos = () => {
        const userAgent = window.navigator.userAgent.toLowerCase();
        return /iphone|ipad|ipod/.test( userAgent );
    }
    // Detects if device is in standalone mode
    const isInStandaloneMode = () => ('standalone' in window.navigator) && (window.navigator.standalone);

    // Checks if should display install popup notification:
    if (isIos() && !isInStandaloneMode()) {
        jQuery("#ios_prompt").show();
    } else {
        jQuery('#ios_prompt').hide();
    }


    jQuery(document).on('click', '#ios_prompt', function(e){
        jQuery("#ios_prompt").css("display","none");
    });
});


var publicKey = 'BPMJKg7SftyqEJKUPgi8zljNoUxl0mz3sHOltpr1OVUiLCWgf/h2hrblEFqdWD+2WEKRCvYM6O3Ld1pj624Wd+o=';
if ('serviceWorker' in navigator ) {
      navigator.serviceWorker.register('/sw.js').then(function () {
          console.log('Service Worker 注册成功');
      });
}


// push feature
//Check `push notification` is supported or not
if (!('PushManager' in window)) {
    console.log('Sorry, Push notification isn\'t supported in your browser.');
} 

if (user_detail.hasOwnProperty('id')) {
    if ('serviceWorker' in navigator && 'PushManager' in window) {
        console.log('Push notification is supported.');
        
        navigator.serviceWorker.addEventListener("controllerchange", (evt) => {
            console.log("controller changed");
            this.controller = navigator.serviceWorker.controller;
        });
        
        if (navigator.serviceWorker.controller) {
            console.log("[PWA Builder] active service worker found, no need to register");

            navigator.serviceWorker.ready.then(function(registration) {
                if (Notification.permission == 'default'){
                    console.log('step 1 - check notification permission - default');
                    return subscribeUserToPush(registration, publicKey);
                } else if (Notification.permission == 'granted') {
                    console.log('step 1 - check notification permission - granted');
                    return getSubscriptionDetails(registration);
                } else if (Notification.permission === 'denied') {
                    console.log('step 1 - check notification permission - denied');
                }
            }).then(function (subscription) {
                    console.log("step 3 - ready to send the subscription info to server");

                    if (typeof subscription != "undefined" && subscription != null && subscription != '') {
                        var endpoint = user_detail.hasOwnProperty('endpoint') ? user_detail.endpoint : null;

                        if (endpoint == null || endpoint != subscription.endpoint) {
                            var body = {subscription: subscription};
                            body.uniqueid = new Date().getTime();
                            body.action = endpoint == null ? 'new' : 'update';
                            return sendSubscriptionToServer(JSON.stringify(body));
                        }
                    } 
                
            }).catch(function (e) {
                    console.log(e);
                    if (Notification.permission === 'denied') {
                        console.warn('Permission for Notifications was denied');
                    } else {
                        console.error('Unable to subscribe to push.', e);
                    }
            });
        }
    }
}


/**
   return subscription info when user press allow button, which means they subscribe push notification from our site
*/
function subscribeUserToPush(registration, publicKey) {
    var subscribeOptions = {
        userVisibleOnly: true,
        applicationServerKey: urlBase64ToUint8Array(publicKey)
    }; 

    console.log('Step 2 - awaiting user to subscribe the notification');
    return registration.pushManager.subscribe(subscribeOptions).then(function (pushSubscription) {
        console.log('Step 2.1 - subscribed has been done, and return subscription info ');
        console.log(pushSubscription);
        return pushSubscription;
    }).catch(function (err) {
          console.log('Step 2.2 - user might reject the subscription and cause it is blocked');
          for (var key in err) {
                console.log(key + ': ' + err[key]);
          }
    });
}


function unsubscribePush() {
    navigator.serviceWorker.ready.then(function(registration) {
        //Get `push subscription`
        registration.pushManager.getSubscription().then(function (subscription) {
            //If no `push subscription`, then return
            if(!subscription) {
                alert('Unable to unregister push notification.');
                return;
            }

            //Unsubscribe `push notification`
            subscription.unsubscribe()
                        .then(function () {
                            console.log('Unsubscribed successfully.');
                            console.log(subscription);
                        })
                        .catch(function (error) {
                            console.error(error);
                        });
        })
        .catch(function (error) {
            console.error('Failed to unsubscribe push notification.');
        });
    })
}



function getSubscriptionDetails(registration) {
    return registration.pushManager.getSubscription()
                            .then(function(subscription) {
                                  console.log('Step 2 - get the current subscription and return - ');
                                  console.log(subscription);
                                  if (typeof subscription == "undefined" || subscription == null || subscription == '') {
                                        return subscribeUserToPush(registration, publicKey);
                                  } else {
                                        return subscription;
                                  }
                            });
}



/**
   * send the subscription info to server
*/
function sendSubscriptionToServer(body, url) {
    url = "/insert_webpush_info";

    return new Promise(function (resolve, reject) {
        var xhr = new XMLHttpRequest();
        xhr.timeout = 60000;
        xhr.onreadystatechange = function () {
            var response = {};console.log(response);
            if (xhr.readyState === 4 && xhr.status === 200) {
                try {
                    response = JSON.parse(xhr.responseText);
                    
                }
                catch (e) {
                    response = xhr.responseText;
                }
                resolve(response);
            }
            else if (xhr.readyState === 4) {
                resolve();
            }
        };
        xhr.onabort = reject;
        xhr.onerror = reject;
        xhr.ontimeout = reject;
        xhr.open('POST', url, true);
        xhr.setRequestHeader('Content-Type', 'application/json');
        // xhr.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content'));
        xhr.send(body);
        
    });
}




function urlBase64ToUint8Array (base64String) {
    const padding = '='.repeat((4 - base64String.length % 4) % 4);
    const base64 = (base64String + padding)
      .replace(/\-/g, '+')
      .replace(/_/g, '/');
  
    const rawData = window.atob(base64);
    const outputArray = new Uint8Array(rawData.length);
  
    for (let i = 0; i < rawData.length; ++i) {
      outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
}

