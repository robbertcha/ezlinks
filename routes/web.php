<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => 'web'], function () { 
    Route::get('/', 'Tenant_end\FrontPageController@homepage')->name('homepage');
    Route::get('/feature', 'Tenant_end\FrontPageController@feature')->name('feature');
    Route::get('/building_manager', 'Tenant_end\FrontPageController@building_manager')->name('building_manager');
    Route::get('/residents', 'Tenant_end\FrontPageController@residents')->name('residents');

});

Auth::routes();

Route::get('/home', 'Tenant_end\HomeController@index')->name('home');

Route::post('/login', array('uses' => 'Auth\LoginController@validate_user'));
Route::get('logout', array('uses' => 'Auth\LoginController@logout_user'));
Route::get('/blgadmin-login', 'Auth\BlgAdminLoginController@showBlgAdminLogin')->name('blgadmin.login');
Route::get('blgadmin-logout/', 'Auth\BlgAdminLoginController@logout_blgAdmin')->name('blgadmin.logout');
Route::post('/blgadmin-login', 'Auth\BlgAdminLoginController@validate_blgAdmin')->name('blgadmin.login.submit');
Route::post('/user-login', 'Auth\BlgAdminLoginController@validate_user')->name('user.login.ajaxsubmit');
Route::post('/user-registration', 'Auth\BlgAdminLoginController@register_user')->name('user.register.ajaxsubmit');
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');

// tenant
Route::get('/tenants/tenant_verification/{code}', 'TenantVerificationController@tenant_verification')->name('tenant_verification');
Route::get('/tenants/dashboard', 'TenantController@index')->name('tenant_dashboard');
Route::get('/tenants/announcement', 'TenantController@tannouncement_list')->name('tenant_announcement');
Route::get('/tenants/announcement_detail/{id}', 'TenantController@tannouncement_detail')->name('tenant_announcement_detail');
Route::get('/tenants/service', 'TenantController@tservice_list')->name('tenant_services');
Route::get('/tenants/reservation', 'TenantController@treservation_list')->name('tenant_reservation');
Route::get('/tenants/user', 'TenantController@tuser_acc')->name('tenant_user');
Route::get('/tenants/create_booking', 'TenantController@create_booking')->name('tenant_create_booking');
Route::get('/tenants/make_a_report', 'TenantController@make_a_report')->name('tenant_make_a_report');
Route::get('/tenants/business_directory','TenantController@business_directory')->name('tenant_business_directory');
Route::get('/tenants/maintenance_list','TenantController@maintenance_list')->name('tenant_maintenance_list');
Route::get('/tenants/maintenance_list/p={page}&type={type}&id={id}','TenantController@maintenance_list_content')->name('tenant_maintenance_list');
Route::get('/tenants/maintenance_list/{id}','TenantController@maintenance_list_detail')->name('tenant_maintenance_list_detail');
Route::get('/tenants/documents', 'TenantController@documents_main_page')->name('tenant_documents');
Route::get('/tenants/parcel', 'TenantController@parcel_main_page')->name('tenant_parcel');
Route::get('/tenants/get_new_parcel', 'TenantController@get_new_parcel')->name('tenant_new_parcel');
Route::get('/tenants/get_parcel_history', 'TenantController@get_parcel_history')->name('tenant_parcel_history');
Route::get('/tenants/get_notification/m={month}', 'TenantController@get_notification')->name('tenant_notification');

Route::get('/tenants/get_sent_docs','TenantController@get_sent_docs_list')->name('sent_docs_list');
Route::get('/tenants/get_upload_docs','TenantController@get_upload_docs_list')->name('upload_docs_list');
Route::get('/tenants/get_announcement_list/p={page}&building_id={building_id}', 'TenantController@get_list_by_buildingID')->name('tenant_get_list_by_buildingID');
Route::get('/tenants/get_reservation_list/p={page}', 'TenantController@get_reservation_list_by_tenantID')->name('tenant_get_reservation_list_by_tenantID');
Route::get('/tenants/get_announcement_detail/aid={aid}', 'TenantController@get_announcement_detail')->name('tenant_get_announcement_detail');
Route::post('/tenant/update_tenant_details/', 'TenantController@update_tenant_details')->name('tenant_update_tenant_details');
Route::post('/tenant/update_thumb', 'TenantController@update_thumb')->name('tenant_update_thumb');
Route::post('/tenant/create_booking_request/', 'TenantController@create_booking_request')->name('tenant_create_booking_request');
Route::post('/tenant/create_report/', 'TenantController@create_report')->name('tenant_create_report');
Route::post('/tenant/create_join_event/', 'TenantController@create_join_event')->name('tenant_create_join_event');
Route::post('/tenant/create_vehicle', 'TenantController@create_vehicle')->name('tenant_create_vehicle');
Route::post('/tenant/create_pet', 'TenantController@create_pet')->name('tenant_create_pet');
Route::post('/mgr/rm_pet', 'TenantController@rm_pet')->name('tenant_rm_pet');
Route::post('/mgr/rm_veh', 'TenantController@rm_veh')->name('tenant_rm_veh');
Route::post('/tenants/upload_docs', 'TenantController@upload_documents')->name('upload_documents');
Route::post('/tenants/delete_docs', 'TenantController@delete_docs')->name('delete_documents');
Route::any('/tenants/create_unique_code', 'TenantController@create_unique_code')->name('create_unique_code');
Route::get('/tenants/get_address_book', 'TenantController@get_address_book')->name('get_address_book');
Route::get('/tenants/address_book','TenantController@address_book_page')->name('address_book_page');


// Building manager
Route::group(['namespace' => 'Mgr'], function(){
    // 控制器在 "App\Http\Controllers\Mgr" 命名空间下
    // dashboard
    Route::get('/mgr/dashboard', 'MgrDashboardController@index')->name('mgr_dashboard');
    Route::get('/mgr/get_notification_list', 'MgrDashboardController@get_notification_list')->name('mgr_get_notification_list');
    Route::post('/mgr/get_reservations_tasks_by_month', 'MgrDashboardController@get_reservations_tasks_by_month')->name('mgr_get_reservations_tasks_by_month');
    Route::post('/mgr/update_scheduler_by_admin', 'MgrDashboardController@update_scheduler')->name('mgr_update_scheduler_by_admin');

    // instructions
    Route::get('/mgr/instruction/slug={slug}', 'InstructionController@index')->name('instruction');

    // contact list

    Route::get('/mgr/contact_list', 'ContactListController@index')->name('contact_list');
    Route::post('/mgr/contact_list_store','ContactListController@store_contact')->name('contact_list_store');
    Route::post('/mgr/contact_list_edit/id={id}','ContactListController@edit_contact')->name('contact_list_edit');
    Route::post('/mgr/contact_list_delete/id={id}','ContactListController@delete_contact')->name('contact_list_delete');

    // checkbox delete
    Route::post('/mgr/delete_setting_assets','MgrDeleteController@setting_assets')->name('setting_assets_delete');
    Route::post('/mgr/delete_setting_inventory','MgrDeleteController@setting_inventory')->name('setting_inventory_delete');
    Route::post('/mgr/delete_setting_amenity','MgrDeleteController@setting_amenity')->name('setting_amenity_delete');
    Route::post('/mgr/delete_setting_inspection','MgrDeleteController@setting_inspection')->name('setting_inspection_delete');
    Route::post('/mgr/delete_setting_parking','MgrDeleteController@setting_parking')->name('setting_parking_delete');
    Route::post('/mgr/del_units','MgrDeleteController@unit')->name('units_delete');
    Route::post('/mgr/delete_vehice','MgrDeleteController@vehice')->name('vehice_delete');
    Route::post('/mgr/delete_tenant','MgrDeleteController@tenant')->name('tenant_delete');
    Route::post('/mgr/delete_annoucement_list','MgrDeleteController@annoucement_list')->name('annoucement_list_delete');
    Route::post('/mgr/delete_scheduler_list','MgrDeleteController@scheduler_list')->name('scheduler_list_delete');




    // announcement
    Route::get('/mgr/announcement/{p?}', 'MgrAnnouncementController@index')->name('mgr_announcement');
    Route::post('/mgr/send_email', 'MgrNotificationController@send_middleware')->name('mgr_send_email');
    Route::post('/mgr/send_email_by_address', 'MgrNotificationController@sendEmailAddress')->name('mgr_send_email_address');
    Route::post('/mgr/announcement_store', 'MgrAnnouncementController@store')->name('mgr_announcement_store');
    Route::get('/mgr/del_announcement/{id}', 'MgrAnnouncementController@del_announcement')->name('mgr_del_announcement');
    Route::get('/mgr/create_announcement', 'MgrAnnouncementController@create_announcement')->name('mgr_create_announcement');
    Route::post('/mgr/create_announcement_type', 'MgrAnnouncementController@create_announcement_type')->name('mgr_create_announcement_type');
    Route::get('/mgr/edit_announcement/{id}', 'MgrAnnouncementController@edit_announcement')->name('mgr_edit_announcement');
    Route::get('/mgr/get_announcement/{id}', 'MgrAnnouncementController@get_announcement')->name('mgr_get_announcement');
    Route::get('/mgr/get_announcement_participants/{id}', 'MgrAnnouncementController@get_announcement_participants')->name('mgr_get_announcement_participants');

    // tag
    Route::post('/mgr/del_announcement_tag/{id}', 'MgrAnnouncementController@del_tag')->name('mgr_del_announcement_tag');
    Route::post('/mgr/del_unit_tag/{id}', 'MgrUnitController@del_tag')->name('mgr_del_unit_tag');
    Route::post('/mgr/create_new_tag', 'MgrAnnouncementController@create_tag')->name('mgr_create_tag');

    // unit
    Route::get('/mgr/unit', 'MgrUnitController@index')->name('mgr_unit');
    Route::get('/mgr/get_unit_list/p={page}&order={order}&orderby={orderby}&search_term={search_term}&search_tag={search_tag}&qty_per_page={qty_per_page}', 'MgrUnitController@get_unit_list')->name('mgr_get_unit_list');
    Route::get('/mgr/create_unit', 'MgrUnitController@create_unit')->name('mgr_create_unit');
    Route::get('/mgr/edit_unit/{id}', 'MgrUnitController@edit_unit')->name('mgr_edit_unit');
    Route::post('/mgr/del_unit', 'MgrUnitController@del_unit')->name('mgr_del_unit');
    Route::post('/mgr/save_unit', 'MgrUnitController@save_unit')->name('mgr_save_unit');
    Route::post('/mgr/save_carpark', 'MgrUnitController@save_carpark')->name('mgr_save_carpark');
    Route::post('/mgr/create_vehicle', 'MgrUnitController@create_vehicle')->name('mgr_create_vehicle');
    Route::post('/mgr/del_vehicle', 'MgrUnitController@del_vehicle')->name('mgr_del_vehicle');
    Route::post('/mgr/create_pet', 'MgrUnitController@create_pet')->name('mgr_create_pet');
    Route::post('/mgr/create_key', 'MgrUnitController@create_key')->name('mgr_create_key');
    Route::post('/mgr/create_key_action', 'MgrUnitController@create_key_action')->name('mgr_create_key_action');
    Route::post('/mgr/del_key', 'MgrUnitController@del_key')->name('mgr_del_key');
    Route::post('/mgr/del_pet', 'MgrUnitController@del_pet')->name('mgr_del_pet');
    Route::post('/mgr/create_custom_field', 'MgrUnitController@create_custom_field')->name('create_custom_field');
    Route::post('/mgr/del_cfield', 'MgrUnitController@del_cfield')->name('mgr_del_cfield');
    Route::post('/mgr/upload_doc', 'MgrUnitController@upload_doc')->name('upload_doc');
    Route::get('/mgr/download_doc/{id}', 'MgrUnitController@download_doc')->name('download_doc');
    Route::post('/mgr/del_doc', 'MgrUnitController@del_doc')->name('mgr_del_doc');
    Route::post('/mgr/update_tenant_type', 'MgrUnitController@update_tenant_type')->name('mgr_update_tenant_type');
    Route::post('/mgr/unlink_tenant', 'MgrUnitController@unlink_tenant')->name('mgr_unlink_tenant');
    Route::post('/mgr/link_tenant', 'MgrUnitController@link_tenant')->name('mgr_link_tenant');
    Route::get('/mgr/get_vehicle_by_tid/{id}', 'MgrUnitController@get_vehicle_by_tid')->name('mgr_get_vehicle_by_tid');
    Route::post('/mgr/create_carpark', 'MgrUnitController@create_carpark')->name('mgr_create_carpark');
    Route::post('/mgr/del_carpark', 'MgrUnitController@del_carpark')->name('mgr_del_carpark');
    Route::post('/mgr/del_carpark_uid', 'MgrUnitController@del_carpark_uid')->name('del_carpark_uid');
    Route::post('/mgr/unlink_vehicle', 'MgrUnitController@unlink_vehicle')->name('mgr_unlink_vehicle');
    Route::post('/mgr/update_carpark', 'MgrUnitController@update_carpark')->name('mgr_update_carpark');
    Route::get('/mgr/get_key_log/{id}', 'MgrUnitController@get_key_log')->name('mgr_get_key_log');
    Route::post('/mgr/create_key_log', 'MgrUnitController@create_key_log')->name('mgr_create_key_log');
    Route::post('/mgr/update_key_log', 'MgrUnitController@update_key_log')->name('mgr_update_key_log');
    Route::post('/mgr/del_all', 'MgrUnitController@del_all')->name('mgr_del_all');
    Route::post('/mgr/initial_create_units', 'MgrUnitController@initial_create_units')->name('mgr_initial_create_units');
    

    // complaint
    Route::get('/mgr/case', 'ComplaintController@index')->name('mgr_case');
    Route::get('/mgr/get_case_list/p={page}&ttype={ttype}&search_term={search_term}', 'ComplaintController@get_case_list')->name('mgr_get_case_list');
    Route::get('/mgr/case_service', 'ComplaintController@testService')->name('mgr_service');
    Route::get('/mgr/test_routine_case', 'ComplaintController@ ')->name('mgr_routine_case');
    // Route::get('/mgr/case/{p?}/{all?}', 'ComplaintController@index')->name('mgr_complaint');
    Route::get('/mgr/case_redirect', 'ComplaintController@redirectToCaseList')->name('mgr_redirect_case');
    Route::get('/mgr/create_case_by_id/id={id}', 'ComplaintController@createCaseById')->name('mgr_create_case_by_id');
    Route::get('/mgr/case_all/{c?}', 'ComplaintController@get_all_case')->name('get_all_case');
    Route::post('/mgr/email_contrator', 'ComplaintController@email_contrator')->name('mgr_email_contrator');
    Route::post('/mgr/save_attachment', 'ComplaintController@saveCaseAttachment')->name('mgr_save_attachment');
    Route::get('/mgr/create_case', 'ComplaintController@create_case')->name('mgr_create_case');
    Route::get('/mgr/case_detail/{id}', 'ComplaintController@case_detail')->name('mgr_complaint_detail');
    Route::post('/mgr/create_complaint_log', 'ComplaintController@create_complaint_log')->name('mgr_create_complaint_log');
    Route::post('/mgr/cancel_scheduler/', 'ComplaintController@cancel_scheduler')->name('mgr_cancel_scheduler');
    Route::post('/mgr/create_reservation_by_caseid/', 'MgrReservationController@create_reservation_by_caseid')->name('mgr_create_reservation_by_caseid');
    Route::post('/mgr/save_case', 'ComplaintController@saveCase')->name('mgr_save_case');
    Route::get('/mgr/getAttachments', 'ComplaintController@getAttachments')->name('mgr_getAttachments');
    Route::get('/mgr/create_case_docs', 'ComplaintController@create_case_docs')->name('mgr_create_case_docs');
    Route::post('/mgr/create_quote_by_caseid/', 'ComplaintController@create_quote_by_caseid')->name('mgr_create_quote_by_caseid');
    Route::post('/mgr/create_invoice_by_caseid/', 'ComplaintController@create_invoice_by_caseid')->name('mgr_create_invoice_by_caseid');
    Route::post('/mgr/remove_quote/', 'ComplaintController@remove_quote')->name('mgr_remove_quote');
    Route::post('/mgr/remove_invoice/', 'ComplaintController@remove_invoice')->name('mgr_remove_invoice');
    Route::post('/mgr/remove_case_docs/', 'ComplaintController@remove_case_docs')->name('mgr_remove_case_docs');
    Route::post('/mgr/remove_case_img/', 'ComplaintController@remove_case_img')->name('mgr_remove_case_img');
    Route::post('/mgr/update_case_inspections', 'ComplaintController@update_case_inspections')->name('mgr_update_case_inspections');

    // scheduler / reservation
    Route::get('/mgr/dailyCheckRoutine', 'MgrReservationController@dailyCheckRoutine')->name('mgr_dailyCheckRoutine');  //CRON
    Route::get('/mgr/get_all_list', 'MgrReservationController@get_all_list')->name('mgr_get_all_list');
    Route::get('/mgr/task_list', 'MgrReservationController@task_list')->name('mgr_task_list');
    Route::get('/mgr/create_task', 'MgrReservationController@create_task')->name('mgr_create_task');
    Route::get('/mgr/maintenance_list', 'MgrReservationController@maintenance_list')->name('mgr_maintenance_list');
    Route::get('/mgr/create_maintenance', 'MgrReservationController@create_maintenance')->name('mgr_create_maintenance');
    Route::get('/mgr/reminder_list', 'MgrReservationController@reminder_list')->name('mgr_reminder_list');
    Route::get('/mgr/reservation', 'MgrReservationController@index')->name('mgr_reservation');
    Route::get('/mgr/scheduler_list/{page?}/{page_qty?}/{type?}/{order?}/{search_date?}/{search_term?}', 'MgrReservationController@scheduler_list')->name('mgr_scheduler_list');
    Route::get('/mgr/get_scheduler_list/p={page}&page_qty={page_qty}&type={type}&order={order}&search_term={search_term}&search_date={search_date}&hideClosedBookings={hideClosedBookings}', 'MgrReservationController@get_scheduler_list')->name('mgr_get_scheduler_list');
    Route::get('/mgr/new_amenity', 'MgrReservationController@new_amenity')->name('mgr_amenity');
    Route::get('/mgr/amenity_list/{p?}', 'MgrReservationController@amenity_list')->name('mgr_amenity_list');
    Route::get('/mgr/amenity_detail/{id}', 'MgrReservationController@amenity_detail')->name('mgr_amenity_detail');
    Route::post('/mgr/create_amenity', 'MgrReservationController@create_amenity')->name('mgr_create_amenity');
    Route::post('/mgr/initial_create_amenities', 'MgrReservationController@initial_create_amenities')->name('mgr_initial_create_amenities');
    Route::post('/mgr/update_reservation_by_admin', 'MgrReservationController@update_reservation')->name('mgr_update_reservation');
    Route::post('/mgr/update_current_occurence', 'MgrReservationController@update_current_occurence')->name('mgr_update_current_occurence');
    Route::post('/mgr/get_reservations_by_month', 'MgrReservationController@get_reservations_by_month')->name('mgr_get_reservations_by_month');
    Route::get('/mgr/booking_list', 'MgrReservationController@booking_list')->name('mgr_booking_list');
    Route::get('/mgr/inspection_list', 'MgrReservationController@inspection_booking_list')->name('mgr_inspection_booking_list');
    Route::post('/mgr/remove_event', 'MgrReservationController@remove_event')->name('mgr_remove_event');
    Route::post('/mgr/remove_current_occurence', 'MgrReservationController@remove_current_occurence')->name('mgr_remove_current_occurence');
    Route::get('/mgr/remove_amenity/{id}', 'MgrReservationController@remove_amenity')->name('mgr_remove_amenity');
    Route::get('/mgr/get_job_initial_data', 'MgrReservationController@get_job_initial_data')->name('mgr_get_job_initial_data');
    Route::get('/mgr/getContractors', 'MgrReservationController@getContractorsByBuildingID')->name('mgr_get_contractor_list');
    Route::get('/mgr/getInspections', 'MgrReservationController@getInspectionsByBuildingID')->name('mgr_get_inspection_list');
    Route::get('/mgr/getAmenities', 'MgrReservationController@getAmenitiesByBuildingID')->name('mgr_get_amenity_list');

    // contractor
    Route::get('/mgr/get_contractor_dashboard', 'MgrContractorController@get_contractor_by_building_dashboard')->name('mgr_get_contractor');
    Route::get('/mgr/contractor_list', 'MgrContractorController@index')->name('mgr_contractor_list');
    Route::get('/mgr/recommend_contractor', 'MgrContractorController@recommend_contractor')->name('mgr_recommend_contractor');
    Route::post('/mgr/save_contractor', 'MgrContractorController@save_contractor')->name('mgr_save_contractor');
    Route::get('/mgr/contractor_detail/{id}', 'MgrContractorController@contractor_detail')->name('mgr_contractor_detail');

    // setting
    Route::get('/mgr/setting', 'MgrSettingController@comp_info')->name('mgr_setting');
    Route::post('/mgr/update_building', 'MgrSettingController@update_building')->name('mgr_update_building');
    Route::post('/insert_webpush_info', 'MgrSettingController@insert_webpush_info')->name('tenant_insert_webpush_info');
    Route::get('/mgr/admin_push','MgrSettingController@push')->name('admin_webpush');
    Route::post('/mgr/initial_update_building_logo', 'MgrSettingController@initial_update_building_logo')->name('mgr_initial_update_building_logo');
    Route::post('/mgr/initial_update_building', 'MgrSettingController@initial_update_building')->name('mgr_initial_update_building');
    Route::post('/mgr/skip_initial_setting', 'MgrSettingController@skip_initial_setting')->name('mgr_skip_initial_setting');

    // asset
    Route::get('/mgr/assets/{p?}', 'MgrSettingController@asset_list')->name('mgr_asset_list');
    Route::get('/mgr/create_asset', 'MgrSettingController@create_asset')->name('mgr_create_asset');
    Route::get('/mgr/edit_asset/{id}', 'MgrSettingController@edit_asset')->name('mgr_edit_asset');
    Route::post('/mgr/save_asset', 'MgrSettingController@save_asset')->name('mgr_save_asset');
    Route::get('/mgr/del_asset/{id}', 'MgrSettingController@del_asset')->name('mgr_del_asset');
    Route::post('/mgr/del_all_assets', 'MgrSettingController@del_all')->name('mgr_del_all');

    // inspection
    Route::get('/mgr/inspection', 'MgrSettingController@inspection_list')->name('mgr_inspection_list');
    Route::get('/mgr/get_inspection_list/p={page}&search_term={search_term}', 'MgrSettingController@get_inspection_list')->name('mgr_get_inspection_list');
    Route::get('/mgr/get_inpection_items/bi_id={bi_id}', 'MgrSettingController@get_inpection_items')->name('mgr_get_inpection_items');
    Route::get('/mgr/get_inpection_records/bih_id={bih_id}', 'MgrSettingController@get_inpection_records')->name('mgr_get_inpection_records');
    Route::get('/mgr/get_asset_inspection_history/asset_id={asset_id}', 'MgrSettingController@get_asset_inspection_history')->name('mgr_get_asset_inspection_history');
    Route::get('/mgr/create_inspection', 'MgrSettingController@create_inspection')->name('mgr_create_inspection');
    Route::post('/mgr/save_inspection', 'MgrSettingController@save_inspection')->name('mgr_save_inspection');
    Route::post('/mgr/create_inspection_items', 'MgrSettingController@create_inspection_items')->name('mgr_create_inspection_items');
    Route::get('/mgr/edit_inspection/{id}', 'MgrSettingController@edit_inspection')->name('mgr_edit_inspection');
    Route::get('/mgr/del_inspection/{id}', 'MgrSettingController@del_inspection')->name('mgr_del_inspection');
    Route::post('/mgr/remove_area/', 'MgrSettingController@remove_area')->name('mgr_remove_area');
    Route::post('/mgr/remove_area_item/', 'MgrSettingController@remove_area_item')->name('mgr_remove_area_item');
    Route::post('/mgr/initial_create_inspections', 'MgrSettingController@initial_create_inspections')->name('mgr_initial_create_inspections');

    // inventory
    Route::get('/mgr/inventory/{p?}', 'MgrSettingController@inventory_list')->name('mgr_inventory_list');
    Route::get('/mgr/create_inventory', 'MgrSettingController@create_inventory')->name('mgr_create_inventory');
    Route::post('/mgr/save_inv', 'MgrSettingController@save_inv')->name('mgr_save_inv');
    Route::get('/mgr/edit_inv/{id}', 'MgrSettingController@edit_inv')->name('mgr_edit_inv');
    Route::get('/mgr/del_inv/{id}', 'MgrSettingController@del_inv')->name('mgr_del_inv');

    // category
    Route::get('/mgr/get_category/type={type}', 'MgrSettingController@get_category')->name('mgr_get_category');
    Route::get('/mgr/category', 'MgrSettingController@category_list')->name('mgr_category_list');
    Route::post('/mgr/add_cate', 'MgrSettingController@add_cate')->name('mgr_save_cate');
    Route::post('/mgr/remove_category', 'MgrSettingController@remove_cate')->name('mgr_remove_category');
    Route::post('/mgr/update_category', 'MgrSettingController@update_cate')->name('mgr_update_category');
    Route::post('/mgr/save_cate', 'MgrSettingController@save_cate')->name('mgr_save_cate');

    // admin
    Route::get('/mgr/personal_info', 'MgrSettingController@personal_info')->name('mgr_personal_info');
    Route::post('/mgr/save_user', 'MgrSettingController@save_user')->name('mgr_save_user');

    // library
    Route::get('/mgr/library', 'MgrSettingController@library_list')->name('mgr_library_list');
    Route::post('/mgr/create_lib_folder', 'MgrSettingController@create_lib_folder')->name('mgr_create_lib_folder');
    Route::post('/mgr/choose_lib_folder', 'MgrSettingController@choose_lib_folder')->name('mgr_choose_lib_folder');
    Route::post('/mgr/upload_lib', 'MgrSettingController@upload_lib')->name('mgr_upload_lib');
    Route::post('/mgr/del_lib_folder', 'MgrSettingController@del_lib_folder')->name('mgr_del_lib');
    Route::post('/mgr/del_lib_file', 'MgrSettingController@del_lib_file')->name('mgr_del_lib_file');

    // template
    Route::get('/mgr/template', 'MgrSettingController@template_list')->name('mgr_email_template');
    Route::post('/mgr/save_template', 'MgrSettingController@save_template')->name('mgr_save_template');
    Route::get('/mgr/del_template/{id}', 'MgrSettingController@del_template')->name('mgr_del_template');

    
    
    Route::get('/mgr/get_template', 'MgrTemplateController@get_template')->name('mgr_get_template_tag');
     // report
    Route::get('/mgr/report', 'MgrReportController@index')->name('mgr_report');
    Route::get('/mgr/get_report_list', 'MgrReportController@get_report_list')->name('mgr_get_report_list');
    Route::get('/mgr/download_report/report_id={report_id}', 'MgrReportController@download_report')->name('mgr_download_report');
    Route::post('/mgr/generate_report', 'MgrReportController@generate_report_directly')->name('mgr_generate_report');
    Route::get('/mgr/report_content/date_from={date_from}&date_to={date_to}', 'MgrReportController@select_content')->name('mgr_report_select_content');
    Route::get('/mgr/report_list', 'MgrSettingController@report_list')->name('mgr_report_list');
    Route::get('/mgr/report_get_case_type', 'MgrReportController@get_case_type')->name('mgr_report_case_type');
    Route::post('/mgr/delete_report', 'MgrReportController@delete_report')->name('mgr_delete_report');

    Route::get('/mgr/service', 'MgrServiceController@index')->name('mgr_service');
    Route::get('/mgr/service_list', 'MgrServiceController@service_list')->name('mgr_service_list');
    Route::get('/mgr/create_service', 'MgrServiceController@create_service')->name('mgr_create_service');
    Route::get('/mgr/service_provider_list', 'MgrServiceController@service_provider_list')->name('mgr_service_provider_list');
    Route::get('/mgr/new_service_provider', 'MgrServiceController@create_service_provider')->name('mgr_create_service_provider');

    Route::get('/mgr/invoice', 'MgrInvoiceController@index')->name('mgr_invoice');
    // Route::get('/mgr/help', 'MgrHelpController@index')->name('mgr_help');

    // help
    Route::get('/mgr/help', 'HelpController@index')->name('mgr_help');
    Route::get('/mgr/help/{id?}', 'HelpController@get_info_top');
    Route::get('/mgr/help/script/{id}', 'HelpController@get_script');
    Route::get('/mgr/help/{t_id}/{d_id}', 'HelpController@get_info');
    
    // courier
    Route::get('/mgr/courier', 'CourierController@index')->name('mgr_courier');
    Route::get('/mgr/get_courier_list/p={page}&ttype={ttype}&torder={torder}&torderby={torderby}&search_term={search_term}', 'CourierController@get_courier_list')->name('mgr_get_courier_list');
    Route::post('/mgr/receive_new_parcel', 'CourierController@receive_new_parcel')->name('mgr_receive_new_parcel');
    Route::post('/mgr/insert_parcel_picker', 'CourierController@insert_parcel_picker')->name('mgr_insert_parcel_picker');

   

    Route::get('/mgr/emergenybell', 'EmergencyBellController@index')->name('mgr_emergency');
    

    // tenant
    // Route::get('/mgr/tenant', 'MgrTenantController@index')->name('mgr_tenant');
    Route::get('/tenants/vehicles/{unit_id}', 'MgrTenantController@get_vehicles_by_unitID')->name('mgr_get_vehicles_by_unitID');
    Route::get('/mgr/tenant/{page}/{ttype}/{order}/{orderby?}/{search_term?}', 'MgrTenantController@index')->name('mgr_tenant_by_type');
    Route::get('/mgr/get_tenant_list/p={page}&ttype={ttype}&torder={torder}&torderby={torderby}&search_term={search_term}', 'MgrTenantController@get_tenant_list')->name('mgr_get_tenant_list');
    Route::get('/mgr/get_carpark_list/p={page}', 'MgrTenantController@get_carpark_list')->name('mgr_get_carpark_list');
    Route::get('/mgr/getCPDetailsByID/{cp_id}', 'MgrTenantController@getCPDetailsByID')->name('mgr_carpark_detail');
    Route::get('/mgr/getTenantDetailsByID/{id}', 'MgrTenantController@getTenantDetailsByID')->name('mgr_tenant_detail');
    Route::get('/mgr/getTenantByUnitID/{id}', 'MgrTenantController@getTenantUnitID')->name('mgr_tenant_detail_by_unitId');
    Route::post('/mgr/create_tenant_note', 'MgrTenantController@createTenantNote')->name('mgr_create_tenant_note');
    Route::get('/mgr/parking_breaches', 'MgrTenantController@parking_breaches')->name('mgr_parking_breaches');
    Route::get('/mgr/create_parking_breaches', 'MgrTenantController@create_parking_breaches')->name('mgr_create_parking_breaches');
    Route::post('/mgr/save_parking_breaches', 'MgrTenantController@save_parking_breaches')->name('mgr_save_parking_breaches');
    Route::get('/mgr/edit_parking_breaches/{id}', 'MgrTenantController@edit_parking_breaches')->name('mgr_edit_parking_breaches');
    Route::get('/mgr/del_parking_breach/{id}', 'MgrTenantController@del_parking_breach')->name('mgr_parking_breach');
    Route::get('/mgr/owner/{id}', 'MgrTenantController@getOwner')->name('getOwner');
    Route::get('/mgr/tenant_log_name/{id}', 'MgrTenantController@getTenantsLogName')->name('getTenantsLogName');
    Route::get('/mgr/unit_carpark/{id}', 'MgrTenantController@getUnitCarParks')->name('getUnitCarParks');
    Route::get('/mgr/carpark/{id}', 'MgrTenantController@getCarparkByUnitId')->name('getCarparkByUnitId');


    // Route::get('/test', 'MgrServiceController@index')->name('mgr_test');
    // Route::post('/test_doc', 'MgrServiceController@test_doc')->name('mgr_test_doc');
});


