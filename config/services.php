<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    // 'mailgun' => [
    //     'domain' => env('https://api.mailgun.net/v3/ezlinks.com.au'),
    //     'secret' => env('key-3d3d165fa4b592041befbebf76aa0a6f'),
    // ],



    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_KEY_SECRET'),
        'region' => env('SES_REGION'),
    ],
    'mailgun' => array(
        'domain' => 'ezlinks.com.au',
        'secret' => 'key-3d3d165fa4b592041befbebf76aa0a6f',
    ),

];
